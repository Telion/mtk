<?php
class ParsingService {
	public $rusklimatDomen = 'https://www.rusklimat.ru';
	public $html_tmn;
	public $html_msc;
	public $rusklimatHowMuchPhoto;
	public $parsingStatus = 'enabled';

	function __construct() {
		addToLog('ParsingService: создал объект ParsingService');
	}

	function downloadPagesFromRusklimat($productUrl) {
		$this->downloadPageProductFromRusklimatTyumen($productUrl);
		$this->downloadPageProductFromRusklimatMoscow($productUrl);
		if (!isset($this->html_tmn->find('div.ctrl-restyled__price span')[0]->plaintext)){
		       	$this->parsingStatus = 'disabled';
		}
	}

	function downloadPageProductFromRusklimatTyumen($productUrl) {
		$url = $this->rusklimatDomen . $productUrl;
		addToLog('ParsingService->downloadPageProductFromRusklimatTyumen: получаю данные с сайта Русклимата, Тюмень: ' . $url);
		$this->html_tmn = file_get_html($url);
	}

	function downloadPageProductFromRusklimatMoscow($productUrl) {
		$url = $this->rusklimatDomen . str_replace("/tyumen", "", $productUrl);
		addToLog('ParsingService->downloadPageProductFromRusklimatMoscow: получаю данные с сайта Русклимата, Москва: ' . $url);
		$this->html_msc = file_get_html($url);
	}

	function parseProductImages() {
		addToLog('ParsingService->parseProductImages: получаю адреса картинок товара с сайта Русклимата');
		$this->rusklimatHowMuchPhoto = count($this->html_tmn->find('div.catalog-element__photo'));
		if ($this->rusklimatHowMuchPhoto == 0) {
			return 0;
		}
		$rusklimatImageUrlList = [];
		for ($i = 0; $i < $this->rusklimatHowMuchPhoto; $i++) {
			$rusklimatImageUrlList[] = $this->rusklimatDomen . $this->html_tmn->find('div.catalog-element__photo')[$i]->find('a')[0]->href;
		}
		return $rusklimatImageUrlList;
	}

	function getHowMuchPhoto() {
		return $this->rusklimatHowMuchPhoto;
	}

	function parseArticleFromRusklimat() {
		addToLog('ParsingService->parseArticleFromRusklimat: ищу артикул товара на сайте Русклимата');
		return $this->html_tmn->find('div.article span b')[0]->plaintext;
	}

	function parseTitleFromRusklimat() {
		addToLog('ParsingService->parseTitleFromRusklimat: ищу название товара на сайте Русклимата');
		return $this->html_tmn->find('h1.ttl')[0]->plaintext;
	}

	function parseDescriptionHeader() {
		addToLog('ParsingService->parseDescriptionHeader: ищу заголовок описания товара на сайте Русклимата');
		return $this->html_tmn->find('div.tab-hide article')[0]->outertext;
	}

	function parseDescriptionText() {
		addToLog('ParsingService->parseDescriptionText: ищу описание товара на сайте Русклимата');
		return $this->html_tmn->find('table.tbl')[0]->outertext;
	}

	function choosePriceRusklimatMscOrTmn() {
		addToLog('ParsingService->choosePriceRusklimatMscOrTmn: выбираю цену, где дешевле Москва vs Тюмень');
		$price_rusklimat_msk = $this->parsePriceRusklimatMoscow();
		$price_rusklimat_tmn = $this->parsePriceRusklimatTyumen();
		return ($price_rusklimat_msk > $price_rusklimat_tmn) ? $price_rusklimat_tmn : $price_rusklimat_msk;
	}	

	function parsePriceRusklimatTyumen() {
		addToLog('ParsingService->parsePriceRusklimatTyumen: ищу цену товара на сайте Русклимата Тюмень');
		$price_rusklimat_tmn = 999999999;
		if ($this->html_tmn) {
			$price_rusklimat_tmn = $this->html_tmn->find('div.ctrl-restyled__price span')[0]->plaintext;
			$price_rusklimat_tmn = (int) preg_replace("/[^,.0-9]/", '', $price_rusklimat_tmn);
		}
		return $price_rusklimat_tmn;
	}

	function parsePriceRusklimatMoscow() {
		addToLog('ParsingService->parsePriceRusklimatMoscow: ищу цену товара на сайте Русклимата Москва');
		$price_rusklimat_msk = 999999999;
		if (isset($this->html_msk)) {
			$price_rusklimat_msk = $this->html_msk->find('div.ctrl-restyled__price span')[0]->plaintext;
			$price_rusklimat_msk = (int) preg_replace("/[^,.0-9]/", '', $price_rusklimat_msk);
		}
		return $price_rusklimat_msk;
	}

	function parseDeliveryRusklimat() {
		addToLog('ParsingService->parseDeliveryRusklimat: ищу срок доставки товара на сайте Русклимата');
		return $this->html_tmn->find('.ctrl-restyled__detayls-list')[0]->plaintext; 
	}

	function parseDateDelivery($delivery) {
		addToLog('ParsingService->parseDateDelivery: ищу дату доставки товара на сайте Русклимата');
		if (strpos($delivery, 'под заказ')) {
			$delivery_date = 0;
		} elseif ((isset($this->html_tmn->find('div.ctrl-restyled__detayls-item')[2])) &&
				(isset($this->html_tmn->find('div.ctrl-restyled__detayls-item')[2]->find('.ctrl-restyled__detayls-desc')[0]))) {
			$delivery_date = $this->html_tmn->find('div.ctrl-restyled__detayls-item')[2]->find('.ctrl-restyled__detayls-desc')[0]->plaintext;
		} else {
			$delivery_date = 0;
		}
		return $delivery_date;
	}

}

?>