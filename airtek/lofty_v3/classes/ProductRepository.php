<?php

class ProductRepository extends Repository {
	public $productList;
	public $wxa43_virtuemart_products_nextid;
	public $wxa43_virtuemart_product_categories_nextid;
	public $wxa43_virtuemart_medias_nextid;
	public $wxa43_virtuemart_product_customfields_nextid;
	public $wxa43_virtuemart_product_manufacturers_nextid;
	public $wxa43_virtuemart_product_medias_nextid;
	public $wxa43_virtuemart_product_prices_nextid;

	function __construct() {
		addToLog('ProductRepository: создал объект ProductRepository');
		parent::__construct();
		$this->productList = $this->getProductList();
		$this->clearCountParams();
	}

	function getProductList() {
		$query = ' SELECT rusklimat_products.*, rusklimat_categories.type_filter FROM wxa43_virtuemart_products AS airtek_products
				JOIN tln_rusklimat_products_from_rusklimat AS rusklimat_products ON airtek_products.product_gtin = rusklimat_products.product_article_rusklimat
				JOIN (SELECT * FROM tln_rusklimat_category_list GROUP BY tln_rusklimat_category_list.id_airtek_category) AS rusklimat_categories ON rusklimat_products.airtek_category_id = rusklimat_categories.id_airtek_category 
				ORDER BY airtek_products.virtuemart_product_id 
		';
		addToLog('ProductRepository->getProductList: получаю список активных товаров Русклимата из базы, ' . $query);
		$this->db->setQuery($query);
		return $this->db->loadAssocList();
	}

	function querySelectProductListWithoutLabel() {
		$query = ' SELECT * FROM wxa43_virtuemart_products WHERE intnotes IS NULL ';
		addToLog('ProductRepository->querySelectProductListWithoutLabel: получаю список активных товаров Русклимата из базы, ' . $query);
		$this->db->setQuery($query);
		return $this->db->loadAssocList();
	}

	function queryUpdateProductCheckLabel($productId) {
		$query = ' UPDATE wxa43_virtuemart_products SET intnotes = 1 WHERE virtuemart_product_id = "' . $productId . '"; ';
		addToLog('ProductRepository->queryUpdateProductCheckLabel: очистил параметры count в таблице tln_rusklimat_config, ' . $query);		
		$this->db->setQuery($query);
		$this->db->query();
	}

	function clearCountParams() {
		$query = ' UPDATE tln_rusklimat_config SET param_value = 0 WHERE param_name = "count_no_change" OR param_name = "count_changed_price" OR param_name = "count_changed_delivery" OR param_name = "count_changed_all" OR param_name = "count_no_airtek"; ';
		addToLog('ProductRepository->clearCountParams: очистил параметры count в таблице tln_rusklimat_config, ' . $query);		
		$this->db->setQuery($query);
		$this->db->query();
	}

	function findProductByRusklimatArticle($product) {
		$query = ' SELECT published FROM wxa43_virtuemart_products WHERE product_gtin = "' . $product->rusklimatArticle . '" ';
		addToLog('ProductRepository->findProductByRusklimatArticle: ищу товар по артикулу в Базе, ' . $query);
		$this->db->setQuery($query);
		return $this->db->loadColumn()[0];
	}

	function updatePublishProduct($rusklimatArticle, $published) {
		$query = ' UPDATE wxa43_virtuemart_products SET published = "' . $published . '" WHERE product_gtin = "' . $rusklimatArticle . '" ';
		addToLog('ProductRepository->updatePublishProduct: опубликовал товар, ' . $query);		
		$this->db->setQuery($query);
		$this->db->query();
	}

	function getProductArticlesFromAirtek() {
		$query = '
		    SELECT 
			wxa43_virtuemart_products.virtuemart_product_id, 
			wxa43_virtuemart_products.published, 
			wxa43_virtuemart_products.product_sku, 
			wxa43_virtuemart_products.product_gtin, 
			wxa43_virtuemart_product_prices.product_price, 
			wxa43_virtuemart_product_customfields.customfield_value 
		    FROM 
			wxa43_virtuemart_products, 
			wxa43_virtuemart_product_prices, 
			wxa43_virtuemart_product_customfields 
		    WHERE 
			wxa43_virtuemart_product_prices.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id 
			AND wxa43_virtuemart_product_customfields.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id 
			AND wxa43_virtuemart_product_customfields.virtuemart_custom_id = 54 
		    ORDER BY wxa43_virtuemart_product_prices.virtuemart_product_id
		';
		addToLog('ProductRepository->getProductArticlesFromAirtek: получаю список артикулов из товаров Airtek, ' . $query);
		$this->db->setQuery($query);
		return $this->db->loadColumn(3);
	}

	function getProductsFromAirtek() {
		$query = '
		    SELECT 
			wxa43_virtuemart_products.virtuemart_product_id, 
			wxa43_virtuemart_products.published, 
			wxa43_virtuemart_products.product_sku, 
			wxa43_virtuemart_products.product_gtin, 
			wxa43_virtuemart_product_prices.product_price, 
			wxa43_virtuemart_product_customfields.customfield_value 
		    FROM 
			wxa43_virtuemart_products, 
			wxa43_virtuemart_product_prices, 
			wxa43_virtuemart_product_customfields 
		    WHERE 
			wxa43_virtuemart_product_prices.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id 
			AND wxa43_virtuemart_product_customfields.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id 
			AND wxa43_virtuemart_product_customfields.virtuemart_custom_id = 54 
		    ORDER BY wxa43_virtuemart_product_prices.virtuemart_product_id
		';
		addToLog('ProductRepository->getProductsFromAirtek: получаю список товаров из Airtek, ' . $query);
		$this->db->setQuery($query);
		return $this->db->loadObjectList('product_gtin');
	}

	function setNextIdForTables() {
		addToLog('ProductRepository->setNextIdForTables: вычислить id следующих записей в таблицах БД');
		$this->wxa43_virtuemart_products_nextid = $this->getNextId(' SELECT MAX(virtuemart_product_id) FROM wxa43_virtuemart_products ');
		$this->wxa43_virtuemart_product_categories_nextid = $this->getNextId(' SELECT MAX(id) FROM wxa43_virtuemart_product_categories ');
		$this->wxa43_virtuemart_medias_nextid = $this->getNextId(' SELECT MAX(virtuemart_media_id) FROM wxa43_virtuemart_medias ');
		$this->wxa43_virtuemart_product_customfields_nextid = $this->getNextId(' SELECT MAX(virtuemart_customfield_id) FROM wxa43_virtuemart_product_customfields ');
		$this->wxa43_virtuemart_product_manufacturers_nextid = $this->getNextId(' SELECT MAX(id) FROM wxa43_virtuemart_product_manufacturers ');
		$this->wxa43_virtuemart_product_medias_nextid = $this->getNextId(' SELECT MAX(id) FROM wxa43_virtuemart_product_medias ');
		$this->wxa43_virtuemart_product_prices_nextid = $this->getNextId(' SELECT MAX(virtuemart_product_price_id) FROM wxa43_virtuemart_product_prices ');
	}

	function updatePrice($id_airtek, $price_rusklimat_discount) {
		$query = 'UPDATE wxa43_virtuemart_product_prices SET product_price = "' . $price_rusklimat_discount . '" WHERE virtuemart_product_id = ' . (int) $id_airtek;
		$this->db->setQuery($query);
		addToLog('ProductRepository->updatePrice: обновил таблицу wxa43_virtuemart_product_prices, ' . $query);
		$this->db->query();
	}

	function increaseCounter($paramName) {
		$newValue = $this->getParamByParamName($paramName) + 1;
		addToLog('ProductRepository->increaseCounter: обновил таблицу tln_rusklimat_config, ' . $newValue);
		$this->updateCounter($paramName, $newValue);
	}

	function decreaseCounter($paramName) {
		$newValue = $this->getParamByParamName($paramName) - 1;
		addToLog('ProductRepository->decreaseCounter: обновил таблицу tln_rusklimat_config, ' . $newValue);
		$this->updateCounter($paramName, $newValue);
	}

	function getParamByParamName($paramName) {
		$query = ' SELECT param_value FROM tln_rusklimat_config WHERE param_name = "' . $paramName . '"; ';
		addToLog('ProductRepository->getParamByParamName: получаю значение параметра, ' . $query);
		$this->db->setQuery($query);
		return (isset($this->db->loadColumn()[0])) ? $this->db->loadColumn()[0] : 0;
	}

	function updateDelivery($id_airtek, $rusklimatSklad) {
		$query = 'UPDATE wxa43_virtuemart_product_customfields SET customfield_value = "' . $rusklimatSklad . '" WHERE virtuemart_custom_id = 54 and virtuemart_product_id = ' . (int) $id_airtek;
		$this->db->setQuery($query);
		addToLog('ProductRepository->updateDelivery: обновил таблицу wxa43_virtuemart_product_customfields, ' . $query);
		$this->db->query();
	}

	function getManufacturers() {
		$query = ' SELECT virtuemart_manufacturer_id, mf_name FROM wxa43_virtuemart_manufacturers_ru_ru ';
		addToLog('ProductRepository->getManufacturers: получаю список производителей из Airtek, ' . $query);
		$this->db->setQuery($query);
		return $this->db->loadColumn(1);
	}

	function getManufacturerIdByName($rusklimat_manufacturer) {
		$query = ' SELECT virtuemart_manufacturer_id, mf_name FROM wxa43_virtuemart_manufacturers_ru_ru WHERE mf_name LIKE "' . $rusklimat_manufacturer . '"';
		addToLog('ProductRepository->getManufacturerIdByName: получил производителя из базы Аиртек по производителю Русклимата, ' . $query);
		$this->db->setQuery($query);
		return $this->db->loadColumn(0)[0];
	}

	function updateCounter($paramName, $newValue) {
		$query = ' UPDATE tln_rusklimat_config SET param_value = ' . $newValue . ' WHERE param_name = "' . $paramName . '"; ';
		$this->db->setQuery($query);
		$this->db->query();
	}

	function insertProducts($article_airtek, $article_rusklimat_tmn) {
		$query = 'INSERT INTO wxa43_virtuemart_products (
				virtuemart_product_id, product_sku, product_gtin, product_mpn, 
				product_available_date, product_unit, product_params, published, created_on, created_by, modified_on, modified_by
			) VALUES (
				' . $this->wxa43_virtuemart_products_nextid . ', "' . $article_airtek . '", "' . $article_rusklimat_tmn . '", "made by bot (' . date("d-m-Y", time()) . ')", 
				' . $this->currentDate . ', "P", "min_order_level=0|max_order_level=0|", 1, ' . $this->currentDate . ', 597, ' . $this->currentDate . ', 597)';
		addToLog('ProductRepository->insertProducts: добавил запись в таблицу wxa43_virtuemart_products, ' . $query);
		$this->insertQuery($query);
	}

	function insertRuRu($descr_airtek, $airtek_title, $alias) {
		$descr_airtek = str_replace('"', '""', $descr_airtek);
		$query = 'INSERT INTO wxa43_virtuemart_products_ru_ru(virtuemart_product_id, product_desc, product_name, customtitle, slug
			) VALUES (' . $this->wxa43_virtuemart_products_nextid . ', "' . $descr_airtek . '", "' . $airtek_title . '", "' . $airtek_title . '", "' . $alias . '")';
		addToLog('ProductRepository->insertRuRu: добавил запись в таблицу wxa43_virtuemart_products_ru_ru, ' . $query);
		$this->insertQuery($query);
	}

	function insertCustomfieldsSklad($rusklimatSklad) {
		$query = 'INSERT INTO wxa43_virtuemart_product_customfields (
				virtuemart_product_id, virtuemart_custom_id, customfield_value, 
				published, created_on, created_by, modified_on, modified_by 
			) VALUES (
				' . $this->wxa43_virtuemart_products_nextid . ', 54, "' . $rusklimatSklad . '", 
				1, ' . $this->currentDate . ', 597, ' . $this->currentDate . ', 597)';
		addToLog('ProductRepository->insertCustomfields: добавил запись в таблицу wxa43_virtuemart_product_customfields, ' . $query);
		$this->insertQuery($query);
	}

	function insertProductCategories($cat_id) {
		$query = 'INSERT INTO wxa43_virtuemart_product_categories(id, virtuemart_product_id, virtuemart_category_id 
			) VALUES (' . $this->wxa43_virtuemart_product_categories_nextid . ', ' . $this->wxa43_virtuemart_products_nextid . ', ' . $cat_id . ')';
		addToLog('ProductRepository->insertProductCategories: добавил запись в таблицу wxa43_virtuemart_product_categories, ' . $query);
		$this->insertQuery($query);
	}

	function insertManufacturers($virtuemart_manufacturer_id) {
		$query = 'INSERT INTO wxa43_virtuemart_product_manufacturers( id, virtuemart_product_id, virtuemart_manufacturer_id 
				) VALUES (' . $this->wxa43_virtuemart_product_manufacturers_nextid . ', ' . $this->wxa43_virtuemart_products_nextid . ', ' . $virtuemart_manufacturer_id . ')';
		addToLog('ProductRepository->insertManufacturers: добавил запись в таблицу wxa43_virtuemart_product_manufacturers, ' . $query);
		$this->insertQuery($query);
	}

	function insertPrices($price_rusklimat_discount) {
		$query = 'INSERT INTO wxa43_virtuemart_product_prices (
				virtuemart_product_price_id, virtuemart_product_id, product_price, product_currency, 
				created_on, created_by, modified_on, modified_by
			) VALUES (
				' . $this->wxa43_virtuemart_product_prices_nextid . ', ' . $this->wxa43_virtuemart_products_nextid . ', ' . $price_rusklimat_discount . ', 131, 
				' . $this->currentDate . ', 597, ' . $this->currentDate . ', 597)';
		addToLog('ProductRepository->insertPrices: добавил запись в таблицу wxa43_virtuemart_product_prices, ' . $query);
		$this->insertQuery($query);
	}

	function insertImages($how_much_photo, $alias, $file_title) {
		$file_description = $file_title;
		$file_meta = $file_title.". Купить в Тюмени";
		for ($curr_photo = 0; $curr_photo < $how_much_photo; $curr_photo++){
			$alias_photo = $alias . '-' . $curr_photo;
			$path_img = '/images/stories/virtuemart/product/' . $alias_photo . '.jpg';
			$path_img_resize_for_db = 'images/stories/virtuemart/product/resized/' . $alias_photo . '_200x200.jpg';

			$query = 'INSERT INTO wxa43_virtuemart_medias (
					virtuemart_media_id, file_title, file_description, file_meta, file_mimetype, 
					file_type, file_url, file_url_thumb, file_is_product_image, published, 
					created_on, created_by, modified_on, modified_by
				) VALUES (
					' . $this->wxa43_virtuemart_medias_nextid . ', "' . $file_title . '", "' . $file_description . '", "' . $file_meta . '", "image/jpeg", 
					"product", "' . $path_img . '", "' . $path_img_resize_for_db . '", 1, 1, 
					' . $this->currentDate . ', 597, ' . $this->currentDate . ', 597)';
			addToLog('ProductRepository->insertImages: добавил запись в таблицу wxa43_virtuemart_medias, ' . $query);
			$this->insertQuery($query);

			$query = 'INSERT INTO wxa43_virtuemart_product_medias(id, virtuemart_product_id, virtuemart_media_id, ordering
				) VALUES (' . $this->wxa43_virtuemart_product_medias_nextid . ', ' . $this->wxa43_virtuemart_products_nextid . ', ' . $this->wxa43_virtuemart_medias_nextid . ', ' . $curr_photo . ')';
			addToLog('ProductRepository->insertImages: добавил запись в таблицу wxa43_virtuemart_product_medias, ' . $query);
			$this->insertQuery($query);

			$this->wxa43_virtuemart_medias_nextid++;
			$this->wxa43_virtuemart_product_medias_nextid++;
			isStopScript();
		}
	}

	function insertQuery($query) {
		$this->db->setQuery($query);
		$this->db->query();
	}

	function getNextId($query) {
		$this->db->setQuery($query);
		return $this->db->loadColumn()[0] + 1;
	}

	function querySelectVirtuemartProductsWithDescription($customId) {
		$query = ' SELECT * FROM wxa43_virtuemart_products_ru_ru WHERE virtuemart_product_id NOT IN (SELECT virtuemart_product_id FROM wxa43_virtuemart_product_customfields WHERE virtuemart_custom_id = ' . $customId . '); ';
		addToLog('ProductRepository->querySelectVirtuemartProductsWithDescription: получил товары из базы, с описанием, ' . $query);
		$this->db->setQuery($query);
		return $this->db->loadAssocList();
	}

	function querySelectCustomFields() {
		$query = ' SELECT virtuemart_custom_id, custom_title, custom_desc FROM wxa43_virtuemart_customs WHERE virtuemart_custom_id = 52 OR (virtuemart_custom_id >= 56 AND virtuemart_custom_id <= 70); ';
		addToLog('ProductRepository->querySelectCustomFields: получил список настраиваемых полей из базы, ' . $query);
		$this->db->setQuery($query);
		return $this->db->loadAssocList('custom_desc');
	}

	function updateProductName($id, $name) {
		$query = ' UPDATE wxa43_virtuemart_products_ru_ru SET product_name = "' . $name . '" WHERE virtuemart_product_id = ' . $id . '; ';
		addToLog('ProductRepository->updateProductName: обновляю название товара в базе, ' . $query);
		$this->db->setQuery($query);
		$this->db->query();
	}

	function updateProductDescription($id, $text) {
		$query = ' UPDATE wxa43_virtuemart_products_ru_ru SET product_desc = "' . $text . '" WHERE virtuemart_product_id = ' . $id . '; ';
		addToLog('ProductRepository->updateProductDescription: обновляю описание товара в базе, ' . $query);
		$this->db->setQuery($query);
		$this->db->query();
	}

	function queryInsertCustomField($productId, $customId, $value) {
		$query = 'INSERT INTO wxa43_virtuemart_product_customfields (virtuemart_product_id, virtuemart_custom_id, customfield_value, published) 
			  VALUES (' . $productId . ', ' . $customId . ', "' . $value . '", 1)';
		addToLog('ProductRepository->insertCustomfields: добавил запись в таблицу wxa43_virtuemart_product_customfields, ' . $query);
//		$this->insertQuery($query);
	} 

	function insertQueryForCustomfields($productId, $customId, $value) {
		$query = 'INSERT INTO wxa43_virtuemart_product_customfields (virtuemart_product_id, virtuemart_custom_id, customfield_value, published) 
			  VALUES (' . $productId . ', ' . $customId . ', "' . $value . '", 1)';
		addToLog('ProductRepository->generateInsertQueryForCustomfields: сгенерировал запрос на добавление настраиваемого поля в таблицу wxa43_virtuemart_product_customfields, ' . $query);
		$this->insertQuery($query);
	}

	function selectProductSeries($productId) {
		$query = ' SELECT customfield_value FROM wxa43_virtuemart_product_customfields WHERE virtuemart_custom_id = 58 AND virtuemart_product_id = ' . $productId . '; ';
		addToLog('ProductRepository->selectProductSeries: получил Серию товара, ' . $query);
		$this->db->setQuery($query);
		return $this->db->loadResult();
	}

}

?>