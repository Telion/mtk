<?php

class Sender {

	function sendScanCategoriesReportEmail($report, $countCategories) {
		addToLog('Sender->sendScanCategoriesReportEmail: отправил отчет по парсингу категорий');
		$subject = "Airtek vs Rusklimat. Сканирование категорий: " . $countCategories . " категорий"; 
		$this->sendEmail($report, $subject);
	}

	function sendEmail($report, $subject) {
		$fdreport = file_get_contents($report->file);
		$sender = "From: Mr. Airtek bot <postmaster@mirtelcom.nichost.ru>";
		$to  = "Telion <www.neon@mail.ru>"; 
		$message = $fdreport; 
		$headers  = "Content-type: text/html; charset=utf-8 \r\n";
		$headers .= $sender . "\r\n"; 
		mail($to, $subject, $message, $headers); 
	}

	function sendNewProductsToTelegram($repository) {
		addToLog('Sender->sendNewProductsToTelegram: отправил список новых товаров в телеграмм');
		$count_no_airtek = $repository->getParamByParamName('counter_no_airtek');
		if ($count_no_airtek > 0) {
			message_to_telegram('Airtek vs Rusklimat от ' . date("d.m.Y") . "\r\nДобавлены новые товары: " . $count_no_airtek . " шт." . "\r\n" . "Проверь их отображение на сайте");
		}
	}

	function sendScanProductsReportEmail($report, $countProductsToday) {
		addToLog('Sender->sendScanProductsReportEmail: отправил отчет по парсингу товаров');
		$subject = "Airtek vs Rusklimat. " . $countProductsToday . " позиций"; 
		$this->sendEmail($report, $subject);
	}

}

?>