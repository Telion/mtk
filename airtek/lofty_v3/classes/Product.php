<?php
class Product {
	public static $err_text = '';
	public $url;
	public $airtekCategoryId;

	public $rusklimatArticle;
	public $rusklimatTitle;
	public $rusklimatDescrHeader;
	public $rusklimatDescrText;
	public $rusklimatPrice;
	public $rusklimatAirtekNewPrice;
	public $rusklimatDelivery;
	public $rusklimatDeliveryDate;
	public $rusklimatSklad;
	public $rusklimatManufacturer;
	public $rusklimatProductImageUrlList;
	public $rusklimatHowMuchImages;

	public $airtekProductId;
	public $airtekArticle;
	public $airtekPrice;
	public $airtekSklad;
	public $airtekDelivery;
	public $status;
	public $productType;

	public $airtekTitle;
	public $rusklimat_manufacturer;
	public $descr_airtek_header;
	public $descr_airtek_text;


	function __construct() {
		addToLog('Product: создал объект Product');
	}

	function setParameters($product) {
		$this->url = $product['product_url'];
		$this->airtekCategoryId = $product['airtek_category_id'];
		$this->productType = $product['type_filter'];
		addToLog('Product->setParameters: записал URL и ID категории в Аиртек, ' . $this->url);
	}

	function parseProduct() {
		addToLog('Product->parseProduct: запускаю парсинг товара и записываю результаты парсинга');
		$parsingService = new ParsingService();
		$parsingService->downloadPagesFromRusklimat($this->url);
		if ($parsingService->parsingStatus == 'disabled') {
			return 0;
		}
		$this->rusklimatProductImageUrlList = $parsingService->parseProductImages();
		$this->rusklimatHowMuchImages = $parsingService->getHowMuchPhoto();
		if ($this->rusklimatHowMuchImages == 0) return 0;
		$this->rusklimatArticle = $parsingService->parseArticleFromRusklimat();
		$this->rusklimatTitle = $parsingService->parseTitleFromRusklimat();
		$this->rusklimatDescrHeader = $parsingService->parseDescriptionHeader();
		$this->rusklimatDescrText = $parsingService->parseDescriptionText();
		$this->rusklimatPrice = $parsingService->choosePriceRusklimatMscOrTmn();
		$this->rusklimatAirtekNewPrice = $this->calculateNewPriceAirtek();
		$this->rusklimatDelivery = $parsingService->parseDeliveryRusklimat();
		$this->rusklimatDeliveryDate = $parsingService->parseDateDelivery($this->rusklimatDelivery);
		$this->changeDeliveryDateToAirtek();
		$this->chooseWarehouseByDeliveryDate();

		return 1;
	}

	function calculateNewPriceAirtek() {
		addToLog('Product->calculateNewPriceAirtek: вычисляю цену Аиртека, на основе цены Русклимата');
		$price = $this->rusklimatPrice;
		$price_rusklimat_discount = (round($price / 100 - 0.5) * 100) - 10;
		$price_rusklimat_percent = ($price - $price_rusklimat_discount) / $price;
		if ($price < 300){
			$price_rusklimat_discount = $price;

		} elseif ($price < 5000) {
			if ($price_rusklimat_percent < 0.03) {
				$price_rusklimat_discount = $price_rusklimat_discount - 40;
			}
			if ($price_rusklimat_percent > 0.07) {
				$price_rusklimat_discount = $price_rusklimat_discount + 50;
			}
		} else {
			$price_rusklimat_discount = $price_rusklimat_discount - 100;
		}

		return $price_rusklimat_discount;
	}		

	function changeDeliveryDateToAirtek() {
		if (empty($this->rusklimatDelivery)){
			$this->rusklimatDelivery = 7;
		} elseif ($this->rusklimatDeliveryDate == 0) {
			$this->rusklimatDelivery = 0;
		} elseif ($this->rusklimatDeliveryDate != "") {
			$this->rusklimatDeliveryDate = preg_replace('/[^0-9]/', '', $this->rusklimatDeliveryDate);
			if ($this->rusklimatDeliveryDate > 9) {
				$this->rusklimatDelivery = 0;
			} elseif ($this->rusklimatDeliveryDate > 6){
				$this->rusklimatDelivery = 7;
			} elseif ($this->rusklimatDeliveryDate > 3){
				$this->rusklimatDelivery = 4;
			} else {
				$this->rusklimatDelivery = 1;
			}
		} else {
			$this->rusklimatDelivery = -1;
		}
		addToLog('Product->changeDeliveryDateToAirtek: вычисляю дату доставки, в зависимости от даты доставки в Русклимате. $rusklimatDelivery = ' . $this->rusklimatDelivery);
	}

	function chooseWarehouseByDeliveryDate() {
		if ($this->rusklimatDelivery == -2){ $this->rusklimatSklad = ''; }
		elseif ($this->rusklimatDelivery == 1) { $this->rusklimatSklad = 'Тюмень'; }
		elseif ($this->rusklimatDelivery == 4) { $this->rusklimatSklad = 'Екатеринбург'; }
		elseif ($this->rusklimatDelivery == 7) { $this->rusklimatSklad = 'Москва'; }
		elseif ($this->rusklimatDelivery == 0) { $this->rusklimatSklad = 'Под заказ'; }
		else {
			$this->rusklimatSklad = 'Error';
			if (self::$err_text == '') self::$err_text .= 'Ошибки при парсинге Аиртек:\r\n ' . $this->rusklimatArticle;
			else self::$err_text .= ', ' . $this->rusklimatArticle;
		}
		addToLog('Product->chooseWarehouseByDeliveryDate: устанавливаю склад доставки, в зависимости от даты доставки в Русклимате. $rusklimatSklad = ' . $this->rusklimatSklad);
	}

	function printProduct($index) {
		echo '<hr>
			Товар ' . $index . ':<br> 
			&nbsp;&nbsp;&nbsp;Русклимат: 
				Артикул: ' . $this->rusklimatArticle . ', 
				Цена: ' . $this->rusklimatPrice . ', 
				Доставка: ' . $this->rusklimatDeliveryDate . '<br> 
			&nbsp;&nbsp;&nbsp;Аиртек: 
				Цена: ' . $this->rusklimatAirtekNewPrice . ',
				Доставка: ' . $this->rusklimatSklad . '<br>';
	}

	function isPricesDiffer() {
		return $this->airtekPrice != $this->rusklimatAirtekNewPrice;
	}

	function changePrice($productRepository) {
		addToLog('Product->changePrice: обновляю цену товара');
		$this->setStatus($productRepository, "изменение цены");
		$productRepository->updatePrice($this->airtekProductId, $this->rusklimatAirtekNewPrice);
		$productRepository->increaseCounter('count_changed_price');
	}

	function changeDelivery($productRepository) {
		addToLog('Product->changeDelivery: обновляю дату доставки');
		if ($this->airtekSklad == 'Тюмень') {
			$this->airtekDelivery = 1;
		} elseif ($this->airtekSklad == 'Екатеринбург') {
			$this->airtekDelivery = 4;
		} elseif ($this->airtekSklad == 'Москва') {
			$this->airtekDelivery = 7;
		} elseif ($this->airtekSklad == 'Под заказ') {
			$this->airtekDelivery = 0;
		} else {
			$this->airtekDelivery = -1;
		}
		
		if ($this->isDeliveryDiffer()){
			if ($this->getStatus() == "без изменений"){
				$this->setStatus($productRepository, "изменение доставки");
				$productRepository->increaseCounter('count_changed_delivery');
			} else {
				$this->setStatus($productRepository, "изменение цены и доставки");
				$productRepository->increaseCounter('count_changed_all');
				$productRepository->decreaseCounter('count_changed_price');
			}
			$productRepository->updateDelivery($this->airtekProductId, $this->rusklimatSklad);
		}
	}

	function getStatus() {
		return $this->status;
	}

	function getManufacturerId($productRepository) {
		addToLog('Product->getManufacturerId: определяю id производителя в базе Аиртек по названию из Русклимата');
		$this->rusklimatManufacturer = -1;
		$manufacturers = $productRepository->getManufacturers();
		foreach ($manufacturers as $manufacturer){
			if(strpos(strtolower($this->rusklimatTitle), strtolower($manufacturer))){
				$this->rusklimatManufacturer = $manufacturer;
			}
		}
		if ($this->rusklimatManufacturer != -1) {
			return $productRepository->getManufacturerIdByName($this->rusklimatManufacturer);
		}
		return -1;
	}

	function downloadProductImages($alias) {
		addToLog('Product->downloadProductImages: скачиваю картинки с Русклимата: ' . $alias);
		for ($i = 0; $i < $this->rusklimatHowMuchImages; $i++){
			$image_url_rusklimat_tmn = $this->rusklimatProductImageUrlList[$i];
			$explodeImagePath = explode('.', $image_url_rusklimat_tmn);
			$fileExtensionPartNumber = count($explodeImagePath) - 1;
        	        $fileExtension = $explodeImagePath[$fileExtensionPartNumber];
			$imageSize = getimagesize($image_url_rusklimat_tmn);
			$required_memory = round($imageSize[0] * $imageSize[1] * $imageSize['bits'] * 1.4);
			if (($required_memory > 100000000) AND ($required_memory < 400000000)) {
				printr($required_memory);
				$new_limit = memory_get_usage() + $required_memory;
				ini_set("memory_limit", $new_limit);
			}
			if ($fileExtension == 'png') {
				$image = imagecreatefrompng($image_url_rusklimat_tmn);
				if (empty($image)) {
					$image = imagecreatefromjpeg($image_url_rusklimat_tmn);
				}
			} elseif ($fileExtension == 'jpg') {
				$image = imagecreatefromjpeg($image_url_rusklimat_tmn);
				if (empty($image)) {
					$image = imagecreatefrompng($image_url_rusklimat_tmn);
				}
			} else {
				message_to_telegram("Аиртек: Неизвестное расширение у картинки");
			}
			$image = imagescale($image, 1000); 
			ini_restore("memory_limit");

			$img_size_x = imagesx($image);
			$img_size_y = imagesy($image);
			$bg = imagecreatetruecolor($img_size_x, $img_size_y); // создание изображения с заданными размерами
			imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255)); // заливка изображения бг белым цветом
			imagealphablending($bg, TRUE); // сопряжение изображений
			imagecopy($bg, $image, 0, 0, 0, 0, $img_size_x, $img_size_y); // копирование исходного изображения (image) в созданную основу (bg)
			imagedestroy($image); // уничтожение исходного изображения
			$cropped_img = imagecropauto($bg , IMG_CROP_THRESHOLD, null, 16777215); // обрезание изображения
			imagedestroy($bg); // уничтожение исходного изображения
			$crp_w = ImageSX($cropped_img); // ширина обрезанного
			$crp_h = ImageSY($cropped_img); // высота обрезанного
			$crp_ratio = $crp_h / $crp_w;
			$ratio = 1.44;
			if ( $crp_ratio < $ratio ) {
				$new_h = $crp_w + $crp_w * 0.15;
				$new_w = $new_h;
			} else {
				$new_w = $crp_h + $crp_h * 0.15;
				$new_h = $new_w;
			}                   
			$dst_x = ($new_w - $crp_w) / 2;
			$dst_y = ($new_h - $crp_h) / 2;
			$canvas = imagecreatetruecolor($new_w, $new_h);
			$white = imagecolorallocate($canvas, 255, 255, 255);
			imagefilledrectangle($canvas, 0, 0, $new_w, $new_h, $white);
			imagecopyresampled($canvas, $cropped_img, $dst_x, $dst_y, 0, 0, $crp_w, $crp_h, $crp_w, $crp_h);
			if ($new_h < 550) {
				$canvas = imagescale($canvas, 500); 
			} else {
				$canvas = imagescale($canvas, 750); 
			}
			imagedestroy($cropped_img);
			$canvas_resize = imagescale($canvas, 200); 
			$alias_photo = $alias . '-' . $i;
			$path_img = '/images/stories/virtuemart/product/' . $alias_photo . '.jpg';
			$path_img_resize = '/images/stories/virtuemart/product/resized/' . $alias_photo . '_200x200.jpg';
			$path_img_resize_for_db = 'images/stories/virtuemart/product/resized/' . $alias_photo . '_200x200.jpg';
			$path_full_img = __DIR__ . "/../.." . $path_img;
			$path_full_img_resize = __DIR__ . "/../.." . $path_img_resize;
			echo '<span style="padding-left: 12px;"><b>Новый товар</b>: Путь к картинке: ' . $path_full_img . "</span><br>";
			try {
				imagejpeg($canvas, $path_full_img, 100);
				imagejpeg($canvas_resize, $path_full_img_resize, 100);
			} catch (\Exception $e) {
				file_put_contents("log.txt", $e->getMessage() . "\n");
			}
			imagedestroy($canvas);
			imagedestroy($canvas_resize);
		}
	}

	function isDeliveryDiffer() {
		return $this->rusklimatDelivery != $this->airtekDelivery;
	}

	function setStatus($productRepository, $message) {
		$this->status = $message;
		if ($message == "нет на Аиртек") {
			$this->airtekProductId = "";
			$this->airtekArticle = "";
			$this->airtekPrice = "";
			$productRepository->increaseCounter('count_no_airtek');
		}
	}

	function setDescription($descriptionHeader, $descriptionText) {
		$this->rusklimatDescrHeader = $descriptionHeader;
		$this->rusklimatDescrText = $descriptionText;
	}

}

?>