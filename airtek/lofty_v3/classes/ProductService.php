<?php 

class ProductService {
	public $productRepository;
	public $howManyProductsToParse;

	function __construct() {
		addToLog('ProductService: создал объект ProductService');
	}

	function setParameters($howManyProductsToParse) {
		$this->productRepository = new ProductRepository();
		$this->productRepository->getProductList();
		$this->productRepository->clearCountParams();
		$this->howManyProductsToParse = $howManyProductsToParse;
		addToLog('ProductService->setParameters: устанавливаю параметры: количество товаров, которые парсим и класс работы с БД');
	}

	function parseProductData() {
		addToLog('ProductService->parseProductData: запустить парсинг товаров (цена, количество, фото и др.)');
		$report = new Report();
		$totalCount = $this->productRepository->getCountProducts();
		$startProductNumber = $this->productRepository->getNextProductNumber();
		$finishProductNumber = $this->calculateFinishProductNumber($totalCount, $startProductNumber);
		$countProductsToday = $finishProductNumber - $startProductNumber;
		$productArticleListFromAirtek = $this->productRepository->getProductArticlesFromAirtek();
		$productListFromAirtek = $this->productRepository->getProductsFromAirtek();
		echo '<p>Парсим товары от ' . $startProductNumber . ' до ' . $finishProductNumber . '</p>';
		for ($i = $startProductNumber; $i < $finishProductNumber; $i++) {
			addToLog('');
			addToLog('Товар № ' . $i . ' до ' . $finishProductNumber);
//			message_to_telegram('Товар № ' . $i . ' до ' . $finishProductNumber . ' (из ' . $totalCount . ')');
			sleep(5);
			$product = new Product();
			$product->setParameters($this->productRepository->productList[$i]);
	
			if ($product->parseProduct() == 0) {
				addToLog('WARNING: не нашел фото товара или нет цены у товара');
				continue;
			}
			echo '<br>' . $product->printProduct($i + 1);
			if ($this->isProductInAirtek($product->rusklimatArticle, $productArticleListFromAirtek)) {
				$this->updateProduct($product, $productListFromAirtek);
			} else {
				$this->searchProductInAirtek($product);
			}

			$this->addProductTypeToCustomfield($product);
			$this->addSeriesToProductName($product);
			$this->productRepository->setNextProductNumber($i + 1);
			$this->unpublishElectroluxProducts($product);
			$report->generateProductReportLine($product, $i);                
			isStopScript();
		}
		$this->calculateCountNoChange($countProductsToday);

		$report->writeProductReportFile($this->productRepository, $countProductsToday);
		$sender = new Sender();
		$sender->sendScanProductsReportEmail($report, $countProductsToday);
		$sender->sendNewProductsToTelegram($this->productRepository);
	}

	function unpublishElectroluxProducts($product) {
		if (!empty(stripos($product->rusklimatTitle, 'electrolux'))) {
			addToLog('ProductService->unpublishElectroluxProducts: УБРАЛ Electrolux');
			$this->productRepository->updatePublishProduct($product->rusklimatArticle, 0);
		}
	}

	function addProductTypeToCustomfield($product) {
		$value = $this->getValueForCustomfieldType($product);
		if ($value != '') {
			$this->productRepository->insertQueryForCustomfields($product->airtekProductId, 71, $value);
		}
	}

	function getValueForCustomfieldType($product) {
		$title = $product->rusklimatTitle;
		$categoryId = $product->airtekCategoryId;
		if ($product->productType != '') {
			return $product->productType;
		}

		$typeList = [ 'Блок внутренний' , 'Блок наружный' ];    // 133 
		foreach ($typeList as $type) {
			if (stripos($title, $type) !== false) {
				return $type;
			}
		}
		return '';
	}

	function addSeriesToProductName($product) {
		$productId = $product->airtekProductId;
		$series = $this->productRepository->selectProductSeries($productId);
		$name = $product->rusklimatTitle . ', ' . $series;
		$this->productRepository->updateProductName($productId, $name);
	}

	function calculateFinishProductNumber($totalCount, $startProductNumber) {
		addToLog('ProductService->calculateFinishProductNumber: рассчитать крайний товар из перебираемых');
		$currentCount = floor($totalCount / floor($totalCount / $this->howManyProductsToParse));
		if (($totalCount - $startProductNumber) < $currentCount) {
			return $totalCount;
		}
		return $startProductNumber + $currentCount;
	}

	function isProductInAirtek($article, $array) {
		$this->productRepository->updatePublishProduct($article, 1);
		return in_array($article, $array);
	}

	function updateProduct($product, $productList) {
		addToLog('ProductService->updateProduct: обновляю товар в базе Аиртек');
		$currentProductFromDB = $productList[$product->rusklimatArticle];
		$product->airtekProductId = $currentProductFromDB->virtuemart_product_id;
		$product->airtekArticle = $currentProductFromDB->product_sku;
                $product->setStatus($this->productRepository, "без изменений");
		$product->airtekPrice = ceil($currentProductFromDB->product_price);
		$product->airtekSklad = $currentProductFromDB->customfield_value;
		if ($product->isPricesDiffer()) {
			$product->changePrice($this->productRepository);
		}
		$product->changeDelivery($this->productRepository);
	}


	function searchProductInAirtek($product) {
		addToLog('ProductService->searchProductInAirtek: ищу товар по артикулу Русклимата в базе Аиртека');
		if ($this->isHaveInAirtek($product) != 0) {
			$this->productRepository->publishProduct($product);
		} else {
			$this->insertProduct($product);
		}
	}

	function isHaveInAirtek($product) {		
		addToLog('ProductService->isHaveInAirtek: проверяю есть ли товар на Аиртеке');
		if ($this->productRepository->findProductByRusklimatArticle($product) == 0) {
			return 0;
		}
		return 1;
	}

	function insertProduct($product) {
		addToLog('ProductService->insertProduct: добавляю товар в базу Аиртек');
		$virtuemart_manufacturer_id = $product->getManufacturerId($this->productRepository);
		$this->productRepository->setNextIdForTables();
		$virtuemart_product_id = $this->productRepository->wxa43_virtuemart_products_nextid;
		$airtekArticle = "АТ-400" . $virtuemart_product_id;
		$airtekDescription = $this->generateDescription($product);
		$alias = $this->generateAlias($virtuemart_product_id, $product->rusklimatTitle);
		$file_title = $product->rusklimatTitle.". Фото.";
		$how_much_photo = $product->rusklimatHowMuchImages;
		if ($how_much_photo > 0) {
			$product->downloadProductImages($alias);
			$this->prepareCustomfields($virtuemart_product_id, $airtekDescription, $product->rusklimatTitle);
			$this->productRepository->insertProducts($airtekArticle, $product->rusklimatArticle);
			$this->productRepository->insertRuRu($airtekDescription, $product->rusklimatTitle, $alias);
			$this->productRepository->insertCustomfieldsSklad($product->rusklimatSklad);
			$this->productRepository->insertProductCategories($product->airtekCategoryId);
			if ($product->rusklimatManufacturer != -1) {
				$this->productRepository->insertManufacturers($virtuemart_manufacturer_id);
			}
			$this->productRepository->insertPrices($product->rusklimatAirtekNewPrice);
			$this->productRepository->insertImages($how_much_photo, $alias, $file_title);
		}
		$product->setStatus($this->productRepository, "нет на Аиртек");
	}

	function generateDescription($product) {
		addToLog('ProductService->generateDescription: редактирую описание товара');
		$airtekDescription = '<div style="text-align: justify;">' . $product->rusklimatDescrHeader . "</div>";
		$airtekDescription = str_replace('<article itemprop="description">', '', $airtekDescription);
		$airtekDescription = str_replace('</article>', '', $airtekDescription);
		if (strpos($airtekDescription, 'Бренд:')){
			$airtekDescription = $product->rusklimatDescrText;
		} else {
			$airtekDescription = $airtekDescription . $product->rusklimatDescrText;
		}
		$airtekDescription = str_replace('class="tbl tbl-char"', 'class="table-desc" style="margin: 0 auto"', $airtekDescription);
		$airtekDescription = str_replace(' class="sp"', '', $airtekDescription);
		$airtekDescription = str_replace(' class="ul"', '', $airtekDescription);
		$this->deleteLinkFromDescription($airtekDescription);
		$airtekDescription = str_replace('"', '""', $airtekDescription);
		return $airtekDescription;
	}

	function deleteLinkFromDescription($airtekDescription) {
		addToLog('ProductService->deleteLinkFromDescription: удаляю ссылки из описания');
		$poz1 = mb_stripos($airtekDescription, "<a ");
		while ($poz1 != "" ){
			$obrez2 = mb_substr($airtekDescription, $poz1);
			$poz3 = mb_stripos($obrez2, '">');		
			$poz4 = $poz1 + $poz3 + 2;
			$kusok1 = mb_substr($airtekDescription, 0, $poz1);
			$kusok2 = mb_substr($airtekDescription, $poz4, strlen($airtekDescription));
			$airtekDescription = $kusok1 . $kusok2;
			$poz1 = mb_stripos($airtekDescription, "<a ");
		}
		$airtekDescription = str_replace('</a>', '', $airtekDescription);
	}

	function calculateCountNoChange($countProductsToday) {
		addToLog('ProductService->calculateCountNoChange: вычисляю количество товаров без изменений');
		$changed_price = $this->productRepository->getParamByParamName('count_changed_price');
		$changed_delivery = $this->productRepository->getParamByParamName('count_changed_delivery');
		$changed_all = $this->productRepository->getParamByParamName('count_changed_all');
		$no_airtek = $this->productRepository->getParamByParamName('count_no_airtek');
		$no_change_value = $countProductsToday - $changed_price - $changed_delivery - $changed_all - $no_airtek;
		$this->productRepository->updateCounter("count_no_change", $no_change_value);
	}

	function generateAlias($virtuemart_product_id, $title) {
		addToLog('ProductService->generateAlias: редактирую алиас для фото товара');
		$alias = (string) $title;
		$alias = strip_tags($alias);
		$alias = str_replace(array("\n", "\r"), " ", $alias);
		$alias = preg_replace("/\s+/", ' ', $alias);
		$alias = trim($alias);
		$alias = function_exists('mb_strtolower') ? mb_strtolower($alias) : strtolower($alias);
		$alias = strtr($alias, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
		$alias = preg_replace("/[^0-9a-z-_ ]/i", "", $alias);
		$alias = str_replace(" ", "-", $alias)."-".$virtuemart_product_id;
		return $alias;
	}

	function prepareCustomfields($productId, $description, $productTitle) {
		$productCustomList = [];
		$queryList = [];
		addToLog('ProductService->prepareCustomfields: подготавливаю запросы для добавления характеристик товара в настраиваемые поля и фильтры');
		$customList = $this->productRepository->querySelectCustomFields();
		$description = htmlspecialchars_decode($description);
		$description = str_replace('<table class=""table-desc"" style=""margin: 0 auto""> <tr> <td colspan=""2"">&nbsp;</td> </tr><tr> <td colspan=""2"" style=""text-align:center;font-weight:bold;"">', '', $description);
		$description = str_replace('</td> </tr> </table>', '', $description);
		$description = str_replace('</td> </tr> </table></pre><br><pre>table class=""table-desc"" style=""margin: 0 auto"" tr  &nbsp; tr   style=""text-align:center;font-weight:bold;""', 'tlnenter', $description);
		$description = str_replace('</td> </tr><tr> <td colspan=""2"">&nbsp;</td> </tr><tr> <td colspan=""2"" style=""text-align:center;font-weight:bold;"">', 'tlnenter', $description);
		$description = str_replace('</td> </tr><tr> <td colspan=""2"">&nbsp;</td> </tr><tr> <td>', 'tlnenter', $description);
		$description = str_replace('</td> </tr><tr> <td colspan=""2"">&nbsp;</td>  </tr><tr> <td>', 'tlnenter', $description);
		$description = str_replace('</td> <td style=""width:160px"">', 'tlnspace', $description);
		$descrList = explode('tlnenter', $description);

		foreach($descrList as $descrLine) {
			$array = explode('tlnspace', $descrLine);
			$key = $array[0];
			
			$value = (count($array) > 1) ? $this->deleteTagA($array[1]) : NULL;
			if (isset($value) && isset($customList[$key])) {
				if ($key == 'Эффективен для помещ. площадью до') {
					$value = $this->convertSquareValue($value);
				} else if ($key == 'Уровень шума внутр. блока') {
					$value = $this->convertVolumeValue($value);
				} 
				$this->productRepository->insertQueryForCustomfields($productId, $customList[$key]['virtuemart_custom_id'], $value);
			}
		}
	}

	function deleteTagA($value) { 		// <a href=""/tyumen/konditsionery/split-sistemy/invertor-nastennye/electrolux/"">Electrolux</a>
		if (strpos($value, 'href')) {
			$value = htmlentities($value);
			$value = explode('&gt;', $value);
			if (isset(($value[2])) && !empty($value[2])) {
				$value = $value[2];
			} else {
				$value = $value[1];
			}
			$value = explode('&lt;', $value)[0];
		}
		return $value;
	}

	function convertSquareValue($customfield) {
		$customfield = explode(' м2', $customfield)[0];
		if (empty($customfield)) {
			return false;
		}
		$base = 10;
		while ($base <= 300) {
			if ($customfield <= $base) {
				return 'до ' . $base . ' м²';
			}           
			$base += 10;
		}
		return 'более 300 м²';
	}

	function convertVolumeValue($customfield) {
		$customfield = explode(' дБ', $customfield)[0];
		if (empty($customfield)) {
			return false;
		}
		if ($customfield <= 20) {
			return 'до 20 дБ (едва слышно)';
		} else if ($customfield <= 30) {
			return 'до 30 дБ (шепот)';
		} else if ($customfield <= 40) {
			return 'до 40 дБ (тихий разговор)';
		} else if ($customfield <= 55) {
			return 'до 55 дБ (разговор)';
		} else {
			return 'более 55 дБ';
		}
	}

}

?>