<?php

class CategoryRepository extends Repository {
	public $categoryList;

	function __construct() {
		addToLog('CategoryRepository: создал объект CategoryRepository');
		parent::__construct();
		$this->categoryList = $this->getCategoryList();
	}

	function getCategoryList() {
		$query = ' SELECT * FROM tln_rusklimat_category_list WHERE valid = 1 ';
		addToLog('CategoryRepository->getCategoryList: получаю список категорий, ' . $query);
		$this->db->setQuery($query);
		return $this->db->loadAssocList();
	}

	function resetProducts() {
		$query = ' UPDATE tln_rusklimat_products_from_rusklimat SET found = "0"; ';
		addToLog('CategoryRepository->resetProducts: обновил таблицу tln_rusklimat_products_from_rusklimat, ' . $query);
		$this->db->setQuery($query);
		$this->db->query();
	}

	function setProductCount($categoryId, $count) {
		$query = ' UPDATE tln_rusklimat_category_list SET product_count = "' . $count . '" WHERE category_id = "' . $categoryId . '"; ';
		addToLog('CategoryRepository->resetProducts: обновил таблицу tln_rusklimat_products_from_rusklimat, ' . $query);
		$this->db->setQuery($query);
		$this->db->query();
	}

	function unpublishNotFoundProducts() {
		$query = ' UPDATE wxa43_virtuemart_products SET published = "0" WHERE product_stockhandle != "found"; ';
		addToLog('CategoryRepository->unpublishNotFoundProducts: снять с публикации товары не найденные у поставщика, ' . $query);
		$this->db->setQuery($query);
		$this->db->query();
	}

	function addProduct($catId, $url, $article) {
		if ($this->isFoundProduct($article)) {
			$this->updateProduct($catId, $url, $article);
		} else {
			$this->insertProduct($catId, $url, $article);
		}
	}

	function isFoundProduct($article) {
		$query = ' SELECT * FROM tln_rusklimat_products_from_rusklimat WHERE product_article_rusklimat = "' . $article . '" ';
		addToLog('CategoryRepository->isFoundProduct: ищу товар в базе, ' . $query);
		$this->db->setQuery($query);
		return $this->db->loadColumn()[0];
	}

	function updateProduct($catId, $url, $article) {
		$query = ' UPDATE tln_rusklimat_products_from_rusklimat SET found = "1", airtek_category_id = "' . $catId . '", product_url = "' . $url . '" WHERE product_article_rusklimat = "' . $article . '"; ';
		addToLog('CategoryRepository->updateProduct: обновил таблицу tln_rusklimat_products_from_rusklimat, ' . $query);
		$this->db->setQuery($query);
		$this->db->query();
	}

	function insertProduct($catId, $url, $article) {
		$query = ' INSERT INTO tln_rusklimat_products_from_rusklimat (found, airtek_category_id, product_url, product_article_rusklimat) VALUES ("1", "' . $catId . '", "' . $url . '", "' . $article . '"); ';
		addToLog('CategoryRepository->insertProduct: добавил запись в таблицу tln_rusklimat_products_from_rusklimat, ' . $query);
		$this->db->setQuery($query);
		$this->db->query();
	}

	function checkFoundProductByArticle($article) {
		$query = ' UPDATE wxa43_virtuemart_products SET product_stockhandle = "found" WHERE product_gtin = "' . $article . '"; ';
		addToLog('CategoryRepository->checkFoundProductByArticle: поставить метку, что товар найден в категории, ' . $query);
		$this->db->setQuery($query);
		$this->db->query();
	}

}

?>