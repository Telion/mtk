<?php
require_once("configuration.php");
// https://www.tmk-airtek.ru/lofty/addCustomField.php

// ЗАДАЧА: спарсить из описания товара значение Настраиваемого поля
// 1. Получить список товаров с описанием из базы Аиртек
// 2. Перебрать товар:
//   3. Если в описании найдено нужное поле, то добавить значение в настраиваемые поля в базе.

$productRepository = new ProductRepository();
$customList = $productRepository->querySelectCustomFields();
                      
$currentSearchName = 'Толщина теплоизоляции'; // Бренд // Серия // Макс. охлаждаемая площадь // Уровень шума внутр. блока

$currentSearch = $customList[$currentSearchName]['custom_desc'];
printr($currentSearch);
$currentSearchId = $customList[$currentSearchName]['virtuemart_custom_id'];
$productList = $productRepository->querySelectVirtuemartProductsWithDescription($currentSearchId);

foreach ($productList as $product) { // virtuemart_product_id, product_desc
	$customfield = getValueFromDescription($currentSearch, $product['product_desc']);
	if ($currentSearchId == 52) {
		$customfield = convertSquareValue($customfield);
	}

	if (!empty($customfield)) {
		$customfield = html_entity_decode(htmlspecialchars_decode($customfield));
		$customfield = str_replace('"', "'", $customfield);
		$productRepository->queryInsertCustomField($product['virtuemart_product_id'], $currentSearchId, $customfield);
		echo '<hr>';
	} 

}

// ЗАДАЧА: встроить добавление настраиваемых полей в скрипт парсинга товара
// 1. 

function getValueFromDescription($search, $description) {
	$customfield = explode('<td>' . $search . '</td>', $description)[1];
	$customfield = explode('</td>', $customfield)[0];
	if (strpos($customfield, '<')) {
		$customfield = htmlentities($customfield); // html_entity_decode // htmlentities // htmlspecialchars // htmlspecialchars_decode
		$customfield = explode('&gt;', $customfield);
		if (isset($customfield[2])) {
			$customfield = $customfield[2];
		} else {
			$customfield = $customfield[1];
		}
		$customfield = explode('&lt;', $customfield)[0];
	}
	$customfield = trim($customfield);
	return $customfield;
}

function convertSquareValue($customfield) {
	$customfield = explode(' м2', $customfield)[0];
	if (empty($customfield)) {
		return false;
	}
	$base = 10;
	while ($base <= 300) {
		if ($customfield <= $base) {
			return 'до ' . $base . ' м²';
		}           
		$base += 10;
	}
	return 'более 300 м²';
}

?>
