<?php
require_once("configuration.php");
// https://www.tmk-airtek.ru/lofty/updateDescription.php



// ЗАДАЧА: обновить описание товаров на Airtek
// 1. Получить список товаров из Airtek
$productRepository = new ProductRepository();
$productRepository->productList = $productRepository->querySelectProductListWithoutLabel();

// 	В ЦИКЛЕ обойти каждый товар:
foreach ($productRepository->productList as $product) {  //[product_id] => 1 ; [found] => 1 ; [airtek_category_id] => 132 ; [product_url] => /tyumen/product--g-electrolux-eacm-13-hr-n3/ ; [product_article_rusklimat] => НС-1122327 ; [product_article_airtek] => 
	addToLog('');
	addToLog($product['rusklimat_product_url']);
	addToLog('Кол-во товаров на сегодня: ' . count($productRepository->productList));
	printr($product);

// 	2. Спарсить описание товара из Русклимата
	$parsingService = new ParsingService();
	$parsingService->downloadPagesFromRusklimat($product['rusklimat_product_url']);
//	$virtuemart_product_id = $productRepository->wxa43_virtuemart_products_nextid;
	$descriptionHeader = $parsingService->parseDescriptionHeader();
	$descriptionText = $parsingService->parseDescriptionText();
	$rusklimatTitle = $parsingService->parseTitleFromRusklimat();

// 	3. Отредактировать описание товара
	$productObj = new Product();
	$productObj->setParameters($product);
	$productObj->setDescription($descriptionHeader, $descriptionText);
	                       
	$productService = new ProductService();
	$newDescription = $productService->generateDescription($productObj);

//	$alias = $productService->generateAlias($virtuemart_product_id, $rusklimatTitle);
	

//	4. Вставить новое описание в базу Аиртек
	$virtuemart_product_id = $productRepository->querySelectVirtuemartProductIdByArticle($product['product_gtin']);

	$productRepository->updateProductDescription($virtuemart_product_id, $newDescription);
	$productRepository->queryUpdateProductCheckLabel($virtuemart_product_id);

//	5. Если в описании есть ключевые слова, типа "способен охладить помещение площадью до 16м²", то вытаскивать это в фильтры

// охладит помещение площадью до 20м²		// охладит или обогреет помещение площадью до 33м²	//
// Класс энергоэффективности "А";		// Класс энергоэффективности «A»;			//
// Макс. уровень шума	45 дБ			// Макс. уровень шума	44 дБ				// Макс. уровень шума внешнего блока	50 дБ
// Эффективен для помещ. площадью до	26 м2	// Эффективен для помещ. площадью до	33 м2		// Эффективен для помещ. площадью до	26 м2
// Класс энергоэффективности	A		// Класс энергоэффективности	A			// Класс энергоэффективности	A
// Бренд	Ballu				// Бренд	Electrolux				// Бренд	SHUFT


//	$customfield = explode('площадью до ', $descriptionHeader)[1];
//	$customfield = explode('м²', $customfield)[0];

/*	function insertCustomfields($rusklimatSklad) {
		$query = 'INSERT INTO wxa43_virtuemart_product_customfields (
				virtuemart_customfield_id, virtuemart_product_id, virtuemart_custom_id, customfield_value, 
				published, created_on, created_by, modified_on, modified_by 
			) VALUES (
				' . $this->wxa43_virtuemart_product_customfields_nextid . ', ' . $this->wxa43_virtuemart_products_nextid . ', 54, "' . $rusklimatSklad . '", 
				1, ' . $this->currentDate . ', 597, ' . $this->currentDate . ', 597)';
		addToLog('ProductRepository->insertCustomfields: добавил запись в таблицу wxa43_virtuemart_product_customfields, ' . $query);
		$this->insertQuery($query);
	} */





	         
//	printr($descriptionHeader);
echo '<hr>';
//	printr($customfield);
//	break;
}













?>
