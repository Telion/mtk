<?php
require_once("configuration.php");   
message_to_telegram('----- Airtek vs Rusklimat -----');

$categoryService = new CategoryService();

if ($categoryService->isRuningCategoryEnumeration()) {
	$categoryService->parseProductsFromCategoriesRusklimat();
} else {
	$productService = new ProductService();
	$productService->setParameters($howManyProductsToParse);
	$productService->parseProductData();
}

if (Product::$err_text != '') message_to_telegram(Product::$err_text);
message_to_telegram('----- Airtek завершил работу -----');
?>
