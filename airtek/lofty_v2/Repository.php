<?php
/* UPDATE wxa43_virtuemart_products SET published = 1 */

class Repository {
	public $db;
	public $wxa43_virtuemart_product_categories;
	public $virtuemart_media_id;
	public $virtuemart_customfield_id;
	public $wxa43_virtuemart_product_manufacturers;
	public $wxa43_virtuemart_product_medias;
	public $virtuemart_product_price_id;
	public $currentDate;

	function __construct() {
		$this->db = JFactory::getDBO();
		$this->wxa43_virtuemart_product_categories = $this->getNextId('SELECT MAX(id) FROM wxa43_virtuemart_product_categories');
		$this->virtuemart_media_id = $this->getNextId('SELECT MAX(virtuemart_media_id) FROM wxa43_virtuemart_medias');
		$this->virtuemart_customfield_id = $this->getNextId('SELECT MAX(virtuemart_customfield_id) FROM wxa43_virtuemart_product_customfields');
		$this->wxa43_virtuemart_product_manufacturers = $this->getNextId('SELECT MAX(id) FROM wxa43_virtuemart_product_manufacturers');
		$this->wxa43_virtuemart_product_medias = $this->getNextId('SELECT MAX(id) FROM wxa43_virtuemart_product_medias');
		$this->virtuemart_product_price_id = $this->getNextId('SELECT MAX(virtuemart_product_price_id) FROM wxa43_virtuemart_product_prices');
		$this->currentDate = '"'.date("Y-m-d")." ".date("H:i:s").'"';
	}

/*	function getArticles
$db->setQuery('SELECT wxa43_virtuemart_products.virtuemart_product_id, wxa43_virtuemart_products.published, wxa43_virtuemart_products.product_sku, wxa43_virtuemart_products.product_gtin, wxa43_virtuemart_product_prices.product_price, wxa43_virtuemart_product_customfields.customfield_value FROM wxa43_virtuemart_products, wxa43_virtuemart_product_prices, wxa43_virtuemart_product_customfields WHERE wxa43_virtuemart_product_prices.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id and wxa43_virtuemart_products.published=1 and wxa43_virtuemart_product_customfields.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id and wxa43_virtuemart_product_customfields.virtuemart_custom_id=54 ORDER BY wxa43_virtuemart_product_prices.virtuemart_product_id');
$result = $db->loadObjectList('product_gtin');
$arr_airtek_sku_rusklimat = $db->loadColumn(3); // все артикулы из аиртека*/

	function getProductArticlesFromAirtek() {
		$query = '
		    SELECT 
			wxa43_virtuemart_products.virtuemart_product_id, 
			wxa43_virtuemart_products.published, 
			wxa43_virtuemart_products.product_sku, 
			wxa43_virtuemart_products.product_gtin, 
			wxa43_virtuemart_product_prices.product_price, 
			wxa43_virtuemart_product_customfields.customfield_value 
		    FROM 
			wxa43_virtuemart_products, 
			wxa43_virtuemart_product_prices, 
			wxa43_virtuemart_product_customfields 
		    WHERE 
			wxa43_virtuemart_product_prices.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id 
			AND wxa43_virtuemart_products.published = 1 
			AND wxa43_virtuemart_product_customfields.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id 
			AND wxa43_virtuemart_product_customfields.virtuemart_custom_id = 54 
		    ORDER BY wxa43_virtuemart_product_prices.virtuemart_product_id
		';
		$this->db->setQuery($query);
		return $this->db->loadColumn(3);
	}

	function getProductsFromAirtek() {
		$query = '
		    SELECT 
			wxa43_virtuemart_products.virtuemart_product_id, 
			wxa43_virtuemart_products.published, 
			wxa43_virtuemart_products.product_sku, 
			wxa43_virtuemart_products.product_gtin, 
			wxa43_virtuemart_product_prices.product_price, 
			wxa43_virtuemart_product_customfields.customfield_value 
		    FROM 
			wxa43_virtuemart_products, 
			wxa43_virtuemart_product_prices, 
			wxa43_virtuemart_product_customfields 
		    WHERE 
			wxa43_virtuemart_product_prices.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id 
			AND wxa43_virtuemart_products.published = 1 
			AND wxa43_virtuemart_product_customfields.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id 
			AND wxa43_virtuemart_product_customfields.virtuemart_custom_id = 54 
		    ORDER BY wxa43_virtuemart_product_prices.virtuemart_product_id
		';
		$this->db->setQuery($query);
		return $this->db->loadObjectList('product_gtin');
	}

	function getManufacturers() {
		$this->db->setQuery('SELECT virtuemart_manufacturer_id, mf_name FROM wxa43_virtuemart_manufacturers_ru_ru');
		return $this->db->loadColumn(1);
	}

	function getManufacturerIdByName($rusklimat_manufacturer) {
		$this->db->setQuery("SELECT virtuemart_manufacturer_id, mf_name FROM wxa43_virtuemart_manufacturers_ru_ru WHERE mf_name LIKE '.$rusklimat_manufacturer.'");
		return $this->db->loadColumn(0)[0];
	}

	function getMaxProductId() {
		$this->db->setQuery('SELECT MAX(virtuemart_product_id) FROM wxa43_virtuemart_products');
		return $this->db->loadColumn()[0] + 1;
	}

	function getNextId($query) {
		$this->db->setQuery($query);
		return $this->db->loadColumn()[0] + 1;
	}

	function insertQuery($query) {
		$this->db->setQuery($query);
//		echo '<hr><pre>'; print_r($query); '</pre>';
		$this->db->query();
	}

	function insertProducts($virtuemart_product_id, $article_airtek, $article_rusklimat_tmn) {
		$query = 'INSERT INTO wxa43_virtuemart_products (
				virtuemart_product_id, product_sku, product_gtin, product_mpn, 
				product_available_date, product_unit, product_params, published, created_on, created_by, modified_on, modified_by
			) VALUES (
				' . $virtuemart_product_id . ', "' . $article_airtek . '", "' . $article_rusklimat_tmn . '", "made by bot (' . date("d-m-Y", time()) . ')", 
				' . $this->currentDate . ', "P", "min_order_level=0|max_order_level=0|", 1, ' . $this->currentDate . ', 597, ' . $this->currentDate . ', 597)';
		$this->insertQuery($query);
	}

	function insertRuRu($virtuemart_product_id, $descr_airtek, $airtek_title, $alias) {
		$descr_airtek = str_replace('"', '""', $descr_airtek);
		$query = 'INSERT INTO wxa43_virtuemart_products_ru_ru(virtuemart_product_id, product_desc, product_name, customtitle, slug
			) VALUES (' . $virtuemart_product_id . ', "' . $descr_airtek . '", "' . $airtek_title . '", "' . $airtek_title . '", "' . $alias . '")';
		$this->insertQuery($query);
	}

	function insertCustomfields($virtuemart_product_id, $sklad) {
		$query = 'INSERT INTO wxa43_virtuemart_product_customfields (
				virtuemart_customfield_id, virtuemart_product_id, virtuemart_custom_id, customfield_value, 
				published, created_on, created_by, modified_on, modified_by 
			) VALUES (
				' . $this->virtuemart_customfield_id . ', ' . $virtuemart_product_id . ', 54, "' . $sklad . '", 
				1, ' . $this->currentDate . ', 597, ' . $this->currentDate . ', 597)';
		$this->insertQuery($query);
	}

	function insertProductCategories($virtuemart_product_id, $cat_id) {
		$query = 'INSERT INTO wxa43_virtuemart_product_categories(id, virtuemart_product_id, virtuemart_category_id 
			) VALUES (' . $this->wxa43_virtuemart_product_categories . ', ' . $virtuemart_product_id . ', ' . $cat_id . ')';
		$this->insertQuery($query);
	}

	function insertImages($how_much_photo, $alias, $file_title, $file_description, $file_meta, $virtuemart_product_id) {
		for ($curr_photo = 0; $curr_photo < $how_much_photo; $curr_photo++){
			$alias_photo = $alias . '-' . $curr_photo;
			$path_img = '/images/stories/virtuemart/product/' . $alias_photo . '.jpg';
			$path_img_resize_for_db = 'images/stories/virtuemart/product/resized/' . $alias_photo . '_200x200.jpg';

			// Таблица wxa43_virtuemart_medias
			$query = 'INSERT INTO wxa43_virtuemart_medias (
					virtuemart_media_id, file_title, file_description, file_meta, file_mimetype, 
					file_type, file_url, file_url_thumb, file_is_product_image, published, 
					created_on, created_by, modified_on, modified_by
				) VALUES (
					' . $this->virtuemart_media_id . ', "' . $file_title . '", "' . $file_description . '", "' . $file_meta . '", "image/jpeg", 
					"product", "' . $path_img . '", "' . $path_img_resize_for_db . '", 1, 1, 
					' . $this->currentDate . ', 597, ' . $this->currentDate . ', 597)';
			$this->insertQuery($query);

			// Таблица wxa43_virtuemart_product_medias
			$query = 'INSERT INTO wxa43_virtuemart_product_medias(id, virtuemart_product_id, virtuemart_media_id, ordering
				) VALUES (' . $this->wxa43_virtuemart_product_medias . ', ' . $virtuemart_product_id . ', ' . $this->virtuemart_media_id . ', ' . $curr_photo . ')';
			$this->insertQuery($query);

			$this->virtuemart_media_id++;
			$this->wxa43_virtuemart_product_medias++;
			isStopScript();
		} // ЦИКЛ. Добавление записей в БД о картинках
	}

	function insertManufacturers($virtuemart_product_id, $virtuemart_manufacturer_id) {
		$query = 'INSERT INTO wxa43_virtuemart_product_manufacturers( id, virtuemart_product_id, virtuemart_manufacturer_id 
				) VALUES (' . $this->wxa43_virtuemart_product_manufacturers . ', ' . $virtuemart_product_id . ', ' . $virtuemart_manufacturer_id . ')';
		$this->insertQuery($query);
	}

	function insertPrices($virtuemart_product_id, $price_rusklimat_discount) {
		$query = 'INSERT INTO wxa43_virtuemart_product_prices (
				virtuemart_product_price_id, virtuemart_product_id, product_price, product_currency, 
				created_on, created_by, modified_on, modified_by
			) VALUES (
				' . $this->virtuemart_product_price_id . ', ' . $virtuemart_product_id . ', ' . $price_rusklimat_discount . ', 131, 
				' . $this->currentDate . ', 597, ' . $this->currentDate . ', 597)';
		$this->insertQuery($query);
	}
	
	function updatePrice($id_airtek, $price_rusklimat_discount) {
		$this->db->setQuery('UPDATE wxa43_virtuemart_product_prices SET product_price = "' . $price_rusklimat_discount . '" WHERE virtuemart_product_id = ' . (int) $id_airtek);
		$this->db->query();
	}

	function updateDelivery($id_airtek, $sklad) {
		$this->db->setQuery('UPDATE wxa43_virtuemart_product_customfields SET customfield_value = "' . $sklad . '" WHERE virtuemart_custom_id = 54 and virtuemart_product_id = ' . (int) $id_airtek);
		$this->db->query();
	}

	function updatePublishedDisabled($article_from_airtek) {
		$this->db->setQuery('UPDATE wxa43_virtuemart_products SET published = 0 WHERE product_gtin = "'.$article_from_airtek.'"');
		$this->db->query();
	}
}
?>