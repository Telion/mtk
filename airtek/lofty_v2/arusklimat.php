<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once("function.php");      
require_once("configuration.php");   
message_to_telegram('----- Airtek vs Rusklimat -----');

$counter = new Counter();
$repository = new Repository();
$show = new Show();
$report = new Report($fileReportName);

?>

<html>
<head>
<title>Worker: Airtek vs Rusklimat</title>
<link rel="shortcut icon" href="tln_myicon.ico">
</head>
<body>

<?php

$categoryList = file('tln_list_category.txt');
$currentCategory = file_get_contents('tln_current_category.txt');
$currentProductNumber = file_get_contents('tln_current_product.txt');
$how_much = count($category->productUrlList);
$howMuchCategoriesForDay = 1;
$howMuchCategories = count($categoryList);
$arr_all_articles_in_rusklimat = [];
$arr_articles_not_available_in_airtek = [];

$currentCategory = 0;
$how_much = 1;

echo 'Категория №' . ($currentCategory + 1) . ' из ' . count($categoryList) . '<br>';
$category = new Category($currentCategory, $categoryList[$currentCategory]);
$category->parseProductsFromRusklimat($howMuchCategoriesForDay, $categoryList);

$productArticlesFromAirtek = $repository->getProductArticlesFromAirtek();
$productsFromAirtek = $repository->getProductsFromAirtek();

// ЦИКЛ ОБХОДА СПИСКА СОБРАННЫХ ТОВАРОВ
echo "<br><br><hr><br><br> Перебираю товары из выбранной категории:<br>";
for ($n = 0; $n < $how_much; $n++) {

	$currentProduct = $category->productUrlList[$n];
	$show->currentTyumenUrl($currentProduct);


	$parseProduct = new ParseProduct($n, $currentProduct, $counter, $category->id);
	$parseProduct->printProduct();

	array_push($arr_all_articles_in_rusklimat, $parseProduct->getArticleFromRusklimatTyumen());

	if ($parseProduct->isProductInAirtek($productArticlesFromAirtek)) {
		$parseProduct->updateProductOnAirtek($productsFromAirtek, $repository);
	} else {
		$parseProduct->insertProductToAirtek($repository);
		array_push($arr_articles_not_available_in_airtek, $parseProduct->article_rusklimat_tmn);
	}

	// Добавление записи по текущему товару к HTML отчету
	$report->addLineToReport($parseProduct->generateReportLine($currentProductNumber));

	$currentProductNumber++;

	// если был переход категорий на начало	
	if ($category->countTemp != 0){
		if ($n > $category->countTemp - 2){
			$currentProductNumber = 1;
			$category->countTemp = 0;
			writeFileAllArticles($arr_all_articles_in_rusklimat);
			$arr_all_articles_in_rusklimat_extract = explode(";;", file_get_contents('tln_all_articles.txt'));
			$arr_all_articles_in_rusklimat = [];
			clearFileAllArticles();
		}
	}
	isStopScript();
}

//$count_1_no_change = $how_much - $count_2_changed_price - $count_3_changed_delivery - $count_4_changed_all - $count_5_no_airtek;
$counter->no_change = $how_much - $counter->changed_price - $counter->changed_delivery - $counter->changed_all - $counter->no_airtek;

// если закончил работу с последней категорией, то
echo '<h1>' . $category->number . ' >= ' . $howMuchCategories . '</h1>';
if ($category->number >= $howMuchCategories){
	echo "закончил работу с последней категорией, то".PHP_EOL;

	// Создаю файл с отчетом, для отправки на e-mail
	$reportEmail2 = '<html><head><title>Report2: Airtek vs Rusklimat (от '.date("d.m.Y").')</title><link rel="shortcut icon" href="tln_myicon.ico"></head><style>table {border: 1px solid #494540; border-collapse: collapse;} th {color: #fff; font-weight: 700; font-size: 13px; padding: 10px; width: 80px; border: solid 1px #494540; background: #494540;} td {border-left: solid 0; border-right: solid 0;font-size: 13px; color: #666; text-align: center; border: solid 1px #494540; padding: 6px;} .telion_post {margin-bottom: 300px;}</style><body><center><table class="zebra-table"><tr><th>№ п/п</th><th>Артикул Airtek</th><th>Артикул Rusklimat</th></tr>';
	$fileReportName2 = "tln_report2.html";
	$fdreport2 = fopen($fileReportName2, 'w') or die("не удалось создать файл");
	fwrite($fdreport2, $reportEmail2);
	fclose($fdreport2);
	// перебираю все товары аиртек и ищу их артикулы в ОБЩЕМ массиве артикулов русклимата
	$have_in_rusklimat = 0;	
	foreach ($productArticlesFromAirtek as $article_from_airtek){
		$productsFromAirtek_search = array_search($article_from_airtek, $arr_all_articles_in_rusklimat_extract); // ERROR
		// ищу товар с таким артикулом в базе Аиртека
		if ($productsFromAirtek_search != "") {
			$reportEmail2 = '<tr><td></td><td>'.$article_from_airtek.'</td><td>Есть в Русклимате</td></tr>';
			$productsFromAirtek_search = array_search($article_from_airtek, $productArticlesFromAirtek); // ERROR			
			$have_in_rusklimat++;	
			unset($productArticlesFromAirtek[$productsFromAirtek_search]); // из списка аиртека убираю артикулы, которые нашел в списке русклимата

			// Добавление записи по текущему товару к HTML отчету
			$fdreport2 = fopen("tln_report2.html", 'a') or die("не удалось создать файл");
			fwrite($fdreport2, $reportEmail2."\r\n");
			fclose($fdreport2);
		}
	}

	$i = 0;
	$not_have_in_rusklimat = 0;
	// перебираю не удаленные элементы массива артикулов из базы аиртека
	foreach ($productArticlesFromAirtek as $article_from_airtek){
		$i++;
		$reportEmail2 = '<tr style="background: #FEDDDE;"><td>'.$i.'</td><td>'.$article_from_airtek.'</td><td>Нет в Русклимате</td></tr>';
		$repository->updatePublishedDisabled($article_from_airtek);
		$not_have_in_rusklimat++;
		// Добавление записи по текущему товару к HTML отчету
		$fdreport2 = fopen("tln_report2.html", 'a') or die("не удалось создать файл");
		fwrite($fdreport2, $reportEmail2."\r\n");
		fclose($fdreport2);
	}
	if ($i > 0) message_to_telegram('Аиртек: снял с публикации '.$i.' товаров, в т.ч. '.$article_from_airtek);

	// если не нашел артикул, то
		// статус "нет на русклимат.ру", снимаю товар с этим артикулом с публикации на аиртеке.

	$fdreport2 = fopen("tln_report2.html", 'a') or die("не удалось создать файл");
	fwrite($fdreport2, '</table><center><p style="font-weight: 700; font-size: 16px; color: green;">Скрипт закончил работу</p>');
	fclose($fdreport2);
	sendEmail("tln_report2.html", "Airtek vs Rusklimat. ".$not_have_in_rusklimat." позиций нет в Русклимате");
}

writeAllArticles($arr_all_articles_in_rusklimat);
writeCurrentCategory($category->number);
writeCurrentProduct($currentProductNumber);

$counter->optimizedZeroValues();
$report->writeReportFile($fileReportName, $counter, $how_much);

sendEmail($fileReportName, "Airtek vs Rusklimat. ".$how_much." позиций");

if ($counter->no_airtek != 0) {
	message_to_telegram('Airtek vs Rusklimat от '.date("d.m.Y")."\r\nДобавлены новые товары: ".$counter->no_airtek." шт." . "\r\n" . "Проверь их отображение на сайте");
}

if (ParseProduct::$err_text != '') message_to_telegram(ParseProduct::$err_text);

message_to_telegram('----- Airtek завершил работу -----');
?>