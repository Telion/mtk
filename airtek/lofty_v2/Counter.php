<?php
class Counter {
	public $no_change;
	public $changed_price;
	public $changed_delivery;
	public $changed_all;
	public $no_airtek;

	function __construct() {
		$this->no_change = 0;
		$this->changed_price = 0;
		$this->changed_delivery = 0;
		$this->changed_all = 0;
		$this->no_airtek = 0;
	}

	function change_zero_value($value) {
		if ($value == 0) $value = '-';
		return $value;
	}

	function optimizedZeroValues() {
		$this->no_change = $this->change_zero_value($this->no_change);
		$this->changed_price = $this->change_zero_value($this->changed_price);
		$this->changed_delivery = $this->change_zero_value($this->changed_delivery);
		$this->changed_all = $this->change_zero_value($this->changed_all);
		$this->no_airtek = $this->change_zero_value($this->no_airtek);
	}

}
?>