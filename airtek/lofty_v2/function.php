<?php

function message_to_telegram($text) {
    $ch = curl_init();
    curl_setopt_array(
        $ch,
        array(
            CURLOPT_URL => 'https://api.telegram.org/bot' . TELEGRAM_TOKEN . '/sendMessage',
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_POSTFIELDS => array(
                'chat_id' => TELEGRAM_CHATID,
                'text' => $text,
            ),
        )
    );
    curl_exec($ch);
}

function sendEmail($fileReportName, $how_much) {
	$fdreport = file_get_contents($fileReportName);
	$to  = "Telion <www.neon@mail.ru>"; 
	$subject = "Airtek vs Rusklimat. ".$how_much." позиций"; 
	$message = $fdreport; 
	$headers  = "Content-type: text/html; charset=utf-8 \r\n";
	$headers .= "From: Mr. Airtek bot <postmaster@mirtelcom.nichost.ru>\r\n"; 
	mail($to, $subject, $message, $headers); 
}

function isStopScript() {
    if ( !file_exists('die.txt') ) {
	message_to_telegram('----- Airtek аварийное завершение работы -----');
	die();
    }
}

function createDieFile($currentDate) {
	editFile('die.txt', 'w', $currentDate."\r\nудали, для остановки скрипта");
}

function writeCurrentCategory($currentCategory) {
	editFile('tln_current_category.txt', 'w', $currentCategory);
}

function writeAllArticles() {
	editFile('tln_all_articles.txt', 'a', implode(';;', $arr_all_articles_in_rusklimat).";;");
}

function writeCurrentProduct($currentProduct) {
	editFile('tln_current_product.txt', 'w', $currentProduct);
}

function writeFileAllArticles($arr_all_articles_in_rusklimat) {
	editFile('tln_all_articles.txt', 'a', implode(';;', $arr_all_articles_in_rusklimat).";;");
}

function clearFileAllArticles() {
	editFile('tln_all_articles.txt', 'w', "");
}

function editFile($fileName, $operation, $content) {
	$f = fopen($fileName, $operation) or die("не удалось создать файл");
	fwrite($f, $content);
	fclose($f);
}
  
?>