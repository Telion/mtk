<?php
class ParseProduct {
	public static $err_text = '';
	private $index;
	private $product;
	private $html_tmn;
	private $html_msc;
	private $article_rusklimat_tmn;
	private $rusklimat_title;
	private $price_rusklimat_tmn;
	private $price_rusklimat_msk;
	private $price_rusklimat;
	private $price_rusklimat_discount;
	private $delivery_rusklimat;
	private $delivery_date;
	private $sklad;
	private $status;
	private $id_airtek;
	private $our_article_airtek;
	private $sklad_report;
	private $price_airtek;
	private $counter;
	private $delivery_airtek;
	private $airtek_title;
	private $rusklimat_manufacturer;
	private $descr_airtek_header;
	private $descr_airtek_text;
	private $categoryId;

	function __construct($index, $product, $counter, $categoryId) {
		$this->index = $index;
		$this->product = $product;
		$this->counter = $counter;
		$this->categoryId = $categoryId;
		$this->setProductFromRusklimatTyumen();
		$this->setProductFromRusklimatMoscow();
		$this->setArticleFromRusklimatTyumen();
		$this->setTitleFromRusklimatTyumen();
		$this->setArticleATag();
		$this->setPriceRusklimatTyumen();
		$this->setPriceRusklimatMoscow();
		$this->setPriceRusklimat();
		$this->setPriceAirtek();
		$this->setDeliveryRusklimat();
		$this->setDescriptionHeader();
		$this->setDescriptionText();
		$this->setDateDelivery();
		$this->changeDeliveryDateToAirtek();
		$this->chooseWarehouseByDeliveryDate(); 
	}

	function setStatus($status) {
		$this->status = $status;
	}

	function getStatus() {
		return $this->status;
	}

	function setProductFromRusklimatTyumen() {
		$this->html_tmn = file_get_html("https://www.rusklimat.ru" . $this->product);
	}

	function setProductFromRusklimatMoscow() {
		$this->html_msc = file_get_html("https://www.rusklimat.ru" . str_replace("/tyumen", "", $this->product));
	}

	function setArticleFromRusklimatTyumen() {
		$this->article_rusklimat_tmn = $this->html_tmn->find('div.article span b')[0]->plaintext;
	}

	function getArticleFromRusklimatTyumen() {
		return $this->article_rusklimat_tmn;
	}

	function setTitleFromRusklimatTyumen() {
		$this->rusklimat_title = $this->html_tmn->find('h1.ttl')[0];
	}

	function setDescriptionHeader() {
		$this->descr_airtek_header = $this->html_tmn->find('div.tab-hide article')[0];
	}

	function setDescriptionText() {
		$this->descr_airtek_text = $this->html_tmn->find('table.tbl')[0];
	}

	function setArticleATag() {
        	$article = '<a href="' . 'https://www.rusklimat.ru' . $this->product . '">' . $article_rusklimat_tmn . '</a><br>';
	}

	function setPriceRusklimatTyumen() {
		$this->price_rusklimat_tmn = 999999999;
		if ($this->html_tmn) {
			$this->price_rusklimat_tmn = $this->html_tmn->find('div.ctrl-restyled__price span')[0]->plaintext;
			$this->price_rusklimat_tmn = (int)preg_replace("/[^,.0-9]/", '', $this->price_rusklimat_tmn);
		}
	}

	function setPriceRusklimatMoscow() {
		$this->price_rusklimat_msk = 999999999;
		if ($this->html_msk){
			$this->price_rusklimat_msk = $this->html_msk->find('div.ctrl-restyled__price span')[0]->plaintext;
			$this->price_rusklimat_msk = (int)preg_replace("/[^,.0-9]/", '', $this->price_rusklimat_msk);
		}
	}

	function setPriceRusklimat() {
		$this->price_rusklimat = $this->price_rusklimat_msk;
		if ($this->price_rusklimat > $this->price_rusklimat_tmn) {
			$this->price_rusklimat = $this->price_rusklimat_tmn;
		}
	}	

	function setPriceAirtek() {
		$this->price_rusklimat_discount = (round($this->price_rusklimat / 100 - 0.5) * 100) - 10;
		$this->price_rusklimat_percent = ($this->price_rusklimat - $this->price_rusklimat_discount) / $this->price_rusklimat;
		if ($this->price_rusklimat < 300){
			$this->price_rusklimat_discount = $this->price_rusklimat;
		} elseif ($this->price_rusklimat < 5000) {
			if ($this->price_rusklimat_percent < 0.03) {
				$this->price_rusklimat_discount = $this->price_rusklimat_discount - 40;
			}
			if ($this->price_rusklimat_percent > 0.07) {
				$this->price_rusklimat_discount = $this->price_rusklimat_discount + 50;
			}
		} else {
			$this->price_rusklimat_discount = $this->price_rusklimat_discount - 100;
		}		
	}

	function setDeliveryRusklimat() {
		$this->delivery_rusklimat = $this->html_tmn->find('dl.ctrl-restyled__detayls-list')[0]; 
	}

	function setDateDelivery() {
		if (strpos($this->delivery_rusklimat, 'под заказ')) {
			$this->delivery_date = 0;
		} elseif (isset($this->html_tmn->find('div.ctrl-restyled__detayls-item')[2]->find('.ctrl-restyled__detayls-desc')[0])) {
			$this->delivery_date = $this->html_tmn->find('div.ctrl-restyled__detayls-item')[2]->find('.ctrl-restyled__detayls-desc')[0]->plaintext;
		} else {
			$this->delivery_date = 0;
		}
	}

	function changeDeliveryDateToAirtek() {
		if (empty($this->delivery_rusklimat)){
			$this->delivery_rusklimat = 7;
		} elseif ($this->delivery_date == 0) {
			$this->delivery_rusklimat = 0;
		} elseif ($this->delivery_date != "") {
			$this->delivery_date = preg_replace('/[^0-9]/', '', $this->delivery_date);
			if ($this->delivery_date > 9) {
				$this->delivery_rusklimat = 0;
			} elseif ($this->delivery_date > 6){
				$this->delivery_rusklimat = 7;
			} elseif ($this->delivery_date > 3){
				$this->delivery_rusklimat = 4;
			} else {
				$this->delivery_rusklimat = 1;
			}
		} else {
			$this->delivery_rusklimat = -1;
		}
	}

	function chooseWarehouseByDeliveryDate() {
		if ($this->delivery_rusklimat == -2){ $this->sklad = ''; }
		elseif ($this->delivery_rusklimat == 1) { $this->sklad = 'Тюмень'; }
		elseif ($this->delivery_rusklimat == 4) { $this->sklad = 'Екатеринбург'; }
		elseif ($this->delivery_rusklimat == 7) { $this->sklad = 'Москва'; }
		elseif ($this->delivery_rusklimat == 0) { $this->sklad = 'Под заказ'; }
		else {
			$this->sklad = 'Error';
			if (self::$err_text == '') self::$err_text .= 'Ошибки при парсинге Аиртек:\r\n ' . $this->article_rusklimat_tmn;
			else self::$err_text .= ', ' . $this->article_rusklimat_tmn;
		}
	}

	function printProduct() {
		echo '<br>
			Товар ' . ($this->index + 1) . ':<br> 
			&nbsp;&nbsp;&nbsp;Русклимат: 
				Артикул: ' . $this->article_rusklimat_tmn . ', 
				Цена: ' . $this->price_rusklimat . ', 
				Доставка: ' . $this->date_delivery . '<br> 
			&nbsp;&nbsp;&nbsp;Аиртек: 
				Цена: ' . $this->price_rusklimat_discount . ',
				Доставка: ' . $this->sklad . '<br>';
	}

	function isProductInAirtek($productArticlesFromAirtek) {
		return in_array($this->article_rusklimat_tmn, $productArticlesFromAirtek);
	}

	function getStatusStyle($status) {
		switch ($status) {
			case "без изменений":
				return ' style="background: white;" ';
			case "изменение цены":
				return ' style="background: #D0EECF;" ';
			case "изменение доставки":
				return ' style="background: #F9DAB9;" ';
			case "изменение цены и доставки":
				return ' style="background: #D7C6E1;" ';
		}
		return ' style="background: #FEDDDE;" ';
	}

	function generateReportLine($currentProductNumber) {
		$air_tr_style = $this->getStatusStyle($status);

		return
			'<tr' . $air_tr_style . '>
				<td>' . $currentProductNumber . '</td>
				<td>' . date("d.m.Y") . '  //  ' . date("H:i:s") . '</td>
				<td>' . $this->status . '</td>
				<td>' . $id_airtek . '</td>
				<td>' . $our_article_airtek . '</td>
				<td>' . $price_airtek . '</td>
				<td>' . $article . '</td>
				<td>' . $this->price_rusklimat_discount . '</td>
				<td>' . $sklad_report . '</td>
			</tr>';
	}

	function updateProductOnAirtek($productsFromAirtek, $repository) {
		$product = $productsFromAirtek[$this->article_rusklimat_tmn];
		$this->id_airtek = $product->virtuemart_product_id;
		$this->our_article_airtek = $product->product_sku;
		$this->setStatus("без изменений");
		$this->sklad_report = $product->customfield_value;
		$this->price_airtek = ceil($product->product_price);
		$this->delivery_airtek = $product->customfield_value;
		if ($this->isPricesDiffer()) {
			$this->changePrice($repository);
		}
		$this->changeDelivery($repository);
	}

	function isPricesDiffer() {
		return $this->price_airtek != $this->price_rusklimat_discount;
	}

	function changePrice($repository) {
		$this->setStatus("изменение цены");
		$repository->updatePrice($this->id_airtek, $this->price_rusklimat_discount);
		$this->counter->changed_price++;
	}

	function changeDelivery($repository) {
		if ($this->getStatus() != "без изменений") {
			$this->sklad_report = $this->delivery_airtek." -> ".$this->sklad;
		}

		if ($this->delivery_airtek == 'Тюмень') {
			$this->delivery_airtek = 1;
		} elseif ($this->delivery_airtek == 'Екатеринбург') {
			$this->delivery_airtek = 4;
		} elseif ($this->delivery_airtek == 'Москва') {
			$this->delivery_airtek = 7;
		} elseif ($this->delivery_airtek == 'Под заказ') {
			$this->delivery_airtek = 0;
		} else {
			$this->delivery_airtek = -1;
		}
		
		// Если доставки отличаются, то если статус "изменение цены", то станет "изменение цены и доставки", а если статус был "без изменений", то ставлю "изменение доставки". Также вношу изменения в базу аиртека
		if ($this->isDeliveryDiffer()){
			if ($this->getStatus() == "без изменений"){
				$this->setStatus("изменение доставки");
				$this->price_airtek = "";
				$this->price_rusklimat_discount = "";
				$this->counter->changed_delivery++;
			} else {
				$this->setStatus("изменение цены и доставки");
				$this->counter->changed_all++;
				$this->counter->changed_price--;
			}
			$repository->updateDelivery($this->id_airtek, $this->sklad);
		}

	}

	function isDeliveryDiffer() {
		return $this->delivery_rusklimat != $this->delivery_airtek;
	}

	function insertProductToAirtek($repository) {
		$this->airtek_title = $this->rusklimat_title->plaintext;

		$rusklimat_manufacturer = 0;
		$manufacturers = $repository->getManufacturers();
		foreach ($manufacturers as $manufacturer){
			if(strpos(strtolower($this->rusklimat_title), strtolower($manufacturer))){
				$this->rusklimat_manufacturer = $manufacturer;
			}
		}
		if ($this->rusklimat_manufacturer != 0){
			$virtuemart_manufacturer_id = $repository->getManufacturerIdByName($this->rusklimat_manufacturer);
		}

		$virtuemart_product_id = $repository->getMaxProductId();
		$article_airtek = "АТ-400" . $virtuemart_product_id;
		$descr_airtek = $this->generateDescription();
		$alias = $this->generateAlias($virtuemart_product_id);

		$file_title = $this->airtek_title.". Фото.";
		$file_description = $file_title;
		$file_meta = $file_title." Купить в Тюмени";

		$how_much_photo = $this->downloadProductImages($alias);

		$repository->insertProducts($virtuemart_product_id, $article_airtek, $this->article_rusklimat_tmn);
		$repository->insertRuRu($virtuemart_product_id, $descr_airtek, $this->airtek_title, $alias);
		$repository->insertCustomfields($virtuemart_product_id, $this->sklad);
		$repository->insertProductCategories($virtuemart_product_id, $this->categoryId);
		$repository->insertImages($how_much_photo, $alias, $file_title, $file_description, $file_meta, $virtuemart_product_id);
		if ($this->rusklimat_manufacturer != 0) {
			$repository->insertManufacturers($virtuemart_product_id, $virtuemart_manufacturer_id);
		}
		$repository->insertPrices($virtuemart_product_id, $this->price_rusklimat_discount);
		
		$this->setStatus("нет на Аиртек");
		$this->id_airtek = "";
		$this->our_article_airtek = "";
		$this->price_airtek = "";
		$this->price_rusklimat_discount = "";
		$this->sklad = "";
		$this->counter->no_airtek++;
	}

	function deleteLinkFromDescription($descr_airtek) {
		$poz1 = mb_stripos($descr_airtek, "<a ");
		while ($poz1 != "" ){
			$obrez2 = mb_substr($descr_airtek, $poz1);
			$poz3 = mb_stripos($obrez2, '">');		
			$poz4 = $poz1 + $poz3 + 2;
			$kusok1 = mb_substr($descr_airtek, 0, $poz1);
			$kusok2 = mb_substr($descr_airtek, $poz4, strlen($descr_airtek));
			$descr_airtek = $kusok1 . $kusok2;
			$poz1 = mb_stripos($descr_airtek, "<a ");
		}
		$descr_airtek = str_replace('</a>', '', $descr_airtek);
	}

	function generateDescription() {
		$descr_airtek = '<div style="text-align: justify;">' . $this->descr_airtek_header ."</div>";
		$descr_airtek = str_replace('<article itemprop="description">', '', $descr_airtek);
		$descr_airtek = str_replace('</article>', '', $descr_airtek);
		if (strpos($descr_airtek, 'Бренд:')){
				$descr_airtek = $this->descr_airtek_text;
		} else {
			$descr_airtek = $descr_airtek . $this->descr_airtek_text;
		}
		$descr_airtek = str_replace('class="tbl tbl-char"', 'class="table-desc" style="margin: 0 auto"', $descr_airtek);
		$descr_airtek = str_replace(' class="sp"', '', $descr_airtek);
		$descr_airtek = str_replace(' class="ul"', '', $descr_airtek);
		$this->deleteLinkFromDescription($descr_airtek);
		return $descr_airtek;
	}

	function downloadProductImages($alias) {
		$how_much_photo = count($this->html_tmn->find('div.catalog-element__photo'));
		for ($curr_photo = 0; $curr_photo < $how_much_photo; $curr_photo++){
			$image_url_rusklimat_tmn = 'https://www.rusklimat.ru' . $this->html_tmn->find('div.catalog-element__photo')[$curr_photo]->find('a')[0]->href;

			$explodeImagePath = explode('.', $image_url_rusklimat_tmn);
			$fileExtensionPartNumber = count($explodeImagePath) - 1;
        	        $fileExtension = $explodeImagePath[$fileExtensionPartNumber];

			if ($fileExtension == 'png') {
				$image = imagecreatefrompng($image_url_rusklimat_tmn); // получение изображения в переменную
			} elseif ($fileExtension == 'jpg') {
				$image = imagecreatefromjpeg($image_url_rusklimat_tmn); // получение изображения в переменную
			} else {
				message_to_telegram("Аиртек: Неизвестное расширение у картинки");
			}
			$image = imagescale($image, 1000); 
			$img_size_x = imagesx($image);
			$img_size_y = imagesy($image);

			$bg = imagecreatetruecolor($img_size_x, $img_size_y); // создание изображения с заданными размерами
			imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255)); // заливка изображения бг белым цветом
			imagealphablending($bg, TRUE); // сопряжение изображений
			imagecopy($bg, $image, 0, 0, 0, 0, $img_size_x, $img_size_y); // копирование исходного изображения (image) в созданную основу (bg)
			imagedestroy($image); // уничтожение исходного изображения
			$cropped_img = imagecropauto($bg , IMG_CROP_THRESHOLD, null, 16777215); // обрезание изображения
			imagedestroy($bg); // уничтожение исходного изображения

			$crp_w = ImageSX($cropped_img); // ширина обрезанного
			$crp_h = ImageSY($cropped_img); // высота обрезанного

			$crp_ratio = $crp_h / $crp_w;
			$ratio = 1.44;
			if ( $crp_ratio < $ratio ) {
				$new_h = $crp_w + $crp_w * 0.15;
				$new_w = $new_h;
			} else {
				$new_w = $crp_h + $crp_h * 0.15;
				$new_h = $new_w;
			}                   

			$dst_x = ($new_w - $crp_w) / 2;
			$dst_y = ($new_h - $crp_h) / 2;

			$canvas = imagecreatetruecolor($new_w, $new_h);
			$white = imagecolorallocate($canvas, 255, 255, 255);
			imagefilledrectangle($canvas, 0, 0, $new_w, $new_h, $white);

			imagecopyresampled($canvas, $cropped_img, $dst_x, $dst_y, 0, 0, $crp_w, $crp_h, $crp_w, $crp_h);
			if ($new_h < 550) {
				$canvas = imagescale($canvas, 500); 
			} else {
				$canvas = imagescale($canvas, 750); 
			}
			imagedestroy($cropped_img);

			$canvas_resize = imagescale($canvas, 200); 

			$alias_photo = $alias . '-' . $curr_photo;
			$path_img = '/images/stories/virtuemart/product/' . $alias_photo . '.jpg';
			$path_img_resize = '/images/stories/virtuemart/product/resized/' . $alias_photo . '_200x200.jpg';
			$path_img_resize_for_db = 'images/stories/virtuemart/product/resized/' . $alias_photo . '_200x200.jpg';
			$path_full_img = __DIR__ . "/.." . $path_img;
			$path_full_img_resize = __DIR__ . "/.." . $path_img_resize;

			echo '<span style="padding-left: 12px;"><b>Новый товар</b>: Путь к картинке: ' . $path_full_img . "</span><br>";

			try {
				imagejpeg($canvas, $path_full_img, 100);
				imagejpeg($canvas_resize, $path_full_img_resize, 100);
				file_put_contents("log.txt", "tln: " . date('l jS \of F Y h:i:s A') . ", " . $path_full_img . "\n");
			} catch (\Exception $e) {
				file_put_contents("log.txt", $e->getMessage() . "\n");
			}

			imagedestroy($canvas);
			imagedestroy($canvas_resize);
		}
		return $how_much_photo;
	}

	function generateAlias($virtuemart_product_id) {
		$alias = (string) $this->airtek_title;
		$alias = strip_tags($alias);
		$alias = str_replace(array("\n", "\r"), " ", $alias);
		$alias = preg_replace("/\s+/", ' ', $alias);
		$alias = trim($alias);
		$alias = function_exists('mb_strtolower') ? mb_strtolower($alias) : strtolower($alias);
		$alias = strtr($alias, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
		$alias = preg_replace("/[^0-9a-z-_ ]/i", "", $alias);
		$alias = str_replace(" ", "-", $alias)."-".$virtuemart_product_id;
		return $alias;
	}
	        
//todo: доделать переменные в generateReportLine



}


?>