<?php
class Category {
	public $id;
	public $number;
	public $url;
	public $productUrlList;
	public $countTemp;
	public $vendorURL = 'https://www.rusklimat.ru';

	function __construct($number, $lineFromFile) {
		$this->id = explode("; ", $lineFromFile)[1];
		$this->number = $number;
		$this->url = explode("; ", $lineFromFile)[0];;
		$this->productUrlList = [];
		$this->countTemp = 0;
	}

	function parseProductsFromRusklimat($howMuchCategoriesForDay) {
		echo "URL текущего каталога: " . $this->url . "<br>";
		for ($h = 0; $h < $howMuchCategoriesForDay; $h++){
			$temp = explode("; ", $this->url)[0];
			$html_tmn = file_get_html($temp);
			$cat_id = explode("; ", $this->url)[1];
		
			$curl = curl_init();
			   curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
			   curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
			   curl_setopt($curl, CURLOPT_HEADER, FALSE);
			   curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
			   curl_setopt($curl, CURLOPT_URL, $this->vendorURL);
			   curl_setopt($curl, CURLOPT_REFERER, $this->vendorURL);
			   curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
			   curl_setopt($curl, CURLOPT_COOKIESESSION, TRUE);
			   curl_setopt($curl, CURLOPT_POST, FALSE);
			   curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0 (Windows; U; Windows NT 5.0; En; rv:1.8.0.2) Gecko/20070306 Firefox/1.0.0.4");
			$str = curl_exec($curl);
			curl_close($curl);

			$urlFindLine = '.catalog-list__item .product-card__header .product-card__title a';
			$count_product_here = count($html_tmn->find($urlFindLine));
			echo "<br>Количество товаров в категории: ".$count_product_here."<br>"; 

			for ($i = 0; $i < $count_product_here; $i++) {
				$current_url = $html_tmn->find($urlFindLine, $i)->href;
				echo "Товар №: ".($i+1).' - <a href="https://www.rusklimat.ru'.$current_url.'" target="blank">'.$current_url."</a><br>";
				array_push($this->productUrlList, $current_url);
			}

			$countCategories = count($categoryList);
		        $this->number++;
			if ($this->number >= $countCategories){
				$this->number -= $countCategories;
			}

			if ($this->number == 0){
				$this->countTemp = count($this->productUrlList);
			}
		} 
		return $this;
	}
}
?>