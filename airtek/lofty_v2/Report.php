<?php

class Report {
	private $file;
	private $report;

	function __construct($file) {
		$this->file = $file;
		$this->generateReportHeader();
		$this->createReportFile();
	}

	function generateReportHeader() {
		$this->report = '
			<html><head>
				<title>Report: Airtek vs Rusklimat (от '.date("d.m.Y").')</title>
				<link rel="shortcut icon" href="favicon.ico">
				<link href="main.css" rel="stylesheet" type="text/css" />
				<style>table {border: 1px solid #494540; border-collapse: collapse;} th {color: #fff; font-weight: 700; font-size: 13px; padding: 10px; width: 80px; border: solid 1px #494540; background: #494540;} td {border-left: solid 0; border-right: solid 0;font-size: 13px; color: #666; text-align: center; border: solid 1px #494540; padding: 6px;} .telion_post {margin-bottom: 300px;}</style>
			<body>
				<div class="main-block-airtek">
				<div class="info-table">
				<table><tr><th>№ п/п</th><th style="width: 110px">Дата</th><th style="width: 110px">Тип операции</th><th>ID</th><th>Артикул Airtek</th><th>Цена Airtek</th><th>Артикул Rusklimat</th><th>Цена Rusklimat -скидка</th><th>Склад (был/стал)</th></tr>
		';
	}

	function createReportFile() {
		$fdreport = fopen($this->file, 'w') or die("не удалось создать файл");
		fwrite($fdreport, $this->report);
		fclose($fdreport);
	}

	function addLineToReport($reportLine) {
		$fdreport = fopen($this->file, 'a') or die("не удалось создать файл");
		fwrite($fdreport, $reportLine."\r\n");
		fclose($fdreport);
	}

	function writeReportFile($fileReportName, $counter, $how_much) {
		$fdreport = fopen($fileReportName, 'a') or die("не удалось создать файл");
		fwrite($fdreport, '
			</table></div>
			<div class="group-info"><table>
				<tr style="background: #fff;"><td class="td-text">Без изменений:</td><td style="width: 70px;">'.$counter->no_change.'</td></tr>
				<tr style="background: #D0EECF;"><td class="td-text">Изменение цены:</td><td>'.$counter->changed_price.'</td></tr>
				<tr style="background: #F9DAB9;"><td class="td-text">Изменение доставки:</td><td>'.$counter->changed_delivery.'</td></tr>
				<tr style="background: #D7C6E1;"><td class="td-text">Изменение цены и доставки:</td><td>'.$counter->changed_all.'</td></tr>
				<tr style="background: #FEDDDE;"><td class="td-text">Нет на Аиртек:</td><td>'.$counter->no_airtek.'</td></tr>
				<tr style="background: #333;">
					<td class="td-text" style="color: #ddd;"><span style="font-size: 15px;">Всего обработано, сегодня:</span></td>
					<td style="color: #ddd;"><span style="font-size: 15px;">'.$how_much.'</span></td></tr>
			</table></div></div>
		');
		fclose($fdreport);
	}

}

?>