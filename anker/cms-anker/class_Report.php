<?php

class Report {
	public $reportFile;
	public $reportName;
	

	function __construct($fileName, $reportName) {
		$currentDate = Date("d.m.Y", time());
		$this->reportFile = $fileName;
		$this->reportName = $reportName;
		$content = '<html><head>
				<title> AnkerSDS: '.$reportName.' от '.$currentDate.'</title>
				<style> 
					table { border-collapse: collapse; }
					th, td { border: solid 1px #888; }
					th { background: #bbb; padding: 7px 10px; }
					td { font-size: 14px; padding: 4px 7px; }
					.lightgreenBack { background: #b9ebb9; }
					.lightredBack { background: #f5d2d7; }
				</style></head>
				<body>
				<table>
				';
		$fdhtml = fopen($this->reportFile, 'w') or die("не удалось создать файл");
		fwrite($fdhtml, $content);
		fclose($fdhtml);
	}

	public function addLine($line) {
		$fdhtml = fopen($this->reportFile, 'a') or die("не удалось создать файл");
		fwrite($fdhtml, $line);
		fclose($fdhtml);
	}

	public function addHeader() {
		$content = "\r\n".'<tr><th>Артикул SDS</th><th>Название Anker</th><th>Количество<br>(было/стало)</th><th>Цена<br>(было/стало)</th></tr>';
		$this->addLine($content);
	}

	public function addCategory($category, $number, $totalAmount) {
		$content = "\r\n".'<tr><th colspan=4 style="background: #bae6f7;">Категория: '.$category.' ('.$number.' из '.$totalAmount.')</th></tr>';
		$this->addLine($content);
	}

	public function addProduct($product) {
		if ($product->productFromDB) {
			$productName = $product->productFromDB['product_name'];
			$productStockWas = $product->productFromDB['product_in_stock'];
			$productPriceWas = round($product->productFromDB['product_price'], 2);
		} else {
			$productName = '';
			$productStockWas = $productPriceWas = 0;
		}



		$lineColor = '';
		if (!$product->isHaveAnker()) {
			$lineColor = 'lightgreenBack';
		} else if ($product->remainFromSds == 0) {
			$lineColor = 'lightredBack';
		}

		$stockClass = '';
		if ($productStockWas > $product->remainFromSds) { // стало меньше
			$stockClass = 'lightredBack';
		} else if ($productStockWas < $product->remainFromSds) { // стало больше
			$stockClass = 'lightgreenBack';
		}
		$reportStock = $productStockWas . ' -> ' . $product->remainFromSds;

		$priceClass = '';
		$reportPrice = '';
		if (isset($product->options['sdsModifyPrice'])) {
			if ($productPriceWas > $product->options['sdsModifyPrice']) { // стало меньше
				$priceClass = 'lightredBack';
			} else if ($productPriceWas < $product->options['sdsModifyPrice']) { // стало больше
				$priceClass = 'lightgreenBack';
			}
			$reportPrice = $productPriceWas . ' -> ' . $product->options['sdsModifyPrice'];
		}

		$content = "\r\n".'<tr class="'.$lineColor.'">
			<td><a href="https://www.sds-group.ru/search.htm?search='.$product->options['article'].'" target="blank">'.$product->options['article'].'</a></td>
			<td><a href="https://www.anker72.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$product->options['virtuemart_product_id'].'" style="color: black;" target="blank">'.$productName.'</a></td>
			<td style="text-align: center;" class="'.$stockClass.'">' . $reportStock . '</td>
			<td style="text-align: center;" class="'.$priceClass.'">' . $reportPrice . '</td>
		</tr>';
		$this->addLine($content);
	}

	public function endTable() {
		$content = '</table>';
		$this->addLine($content);
	}

	public function printCountProductsWithIsNullUnit($database) {
		$countProductsWithIsNullUnit = $database->getCountProductsWithIsNullUnit();
		$content = '<div style="margin: 15px; background: #ff7f5033; padding: 15px; width: fit-content; border-radius: 8px; border: solid 2px coral;">product_unit isNull: '.$countProductsWithIsNullUnit.' шт.</div>';
		$this->addLine($content);
	}

}
?>