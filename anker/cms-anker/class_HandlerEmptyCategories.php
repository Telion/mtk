<?php

class HandlerEmptyCategories {

	private $categoryListAllProductsNotPublished;
	private $categoryListPublishedProducts;
	private $menuList;
	private $categoryList;
	private $db;


	function __construct($db) {
		$this->db = $db;
		$this->task();
		$this->categoryListAllProductsNotPublished = $db->getCategoriesWhereAllProductsNotPublished();
		$this->categoryListPublishedProducts = $db->getCategoriesPublishedProducts();
		$this->menuList = $db->getMenuList();
		$this->categoryList = $db->getCategoryList();
		$this->checkPublishedCategories();
		$this->searchCategoryWithoutPublishedProducts();
	}


	public function checkPublishedCategories() {

		foreach ($this->categoryListPublishedProducts as $value) {
			$currentMenuElement = $this->menuList[$value['category_name']];
			if ($currentMenuElement['published'] == -2) {
				$this->printr("Элемент меню: <" . $value['category_name'] . "> снова опубликован, в нем есть опубликованные товары.");
				$this->db->publishMenuElement($currentMenuElement['id']);
			}

			$currentCategoryElement = $this->categoryList[$value['virtuemart_category_id']];
			if ($currentCategoryElement['published'] == 0) {
				$this->printr("Категория: <" . $value['category_name'] . "> снова опубликована, в ней есть опубликованные товары.");
				$this->db->publishCategoryElement($currentCategoryElement['virtuemart_category_id']);
			}
		}
	}

	
	public function searchCategoryWithoutPublishedProducts() {

		foreach ($this->categoryListAllProductsNotPublished as $value) {
			$currentMenuElement = $this->menuList[$value['category_name']];
			if ($currentMenuElement['published'] == 1) {
				$this->printr("Элемент меню: <" . $value['category_name'] . "> снят с публикации, в нем нет опубликованных товаров.");
				$this->db->unpublishMenuElement($currentMenuElement['id']);
			}

			$currentCategoryElement = $this->categoryList[$value['virtuemart_category_id']];
			if ($currentCategoryElement['published'] == 1) {
				$this->printr("Категория: <" . $value['category_name'] . "> снята с публикации, в ней нет опубликованных товаров.");
				$this->db->unpublishCategoryElement($currentCategoryElement['virtuemart_category_id']);
			}
		}
	}


	public function task() {
	}


	public function printr($message) {
		echo "<hr><pre>";
		print_r($message);
		echo "</pre>";		
	}

}
?>