<?php 
require_once 'connectionDB.php';
require_once 'constants.php';

function printr($string) {
	echo "<pre>";
	print_r($string);
	echo "</pre>";
}

function printrhr($string) {
	echo "<hr><pre>";
	print_r($string);
	echo "</pre><hr>";
}

// функция считывает файл построчно и возвращает массив
function fileRead($fileName) {
	$handle = @fopen($fileName, "r");
	if ($handle) {
	    while (($buffer = fgets($handle)) !== false) {
		$result[] = $buffer;
	    }
	    if (!feof($handle)) {
	        echo "Ошибка: fgets() неожиданно потерпел неудачу\n";
	    }
	    fclose($handle);

	return $result;
	}
}

// Транслитерация кирилицы в латиницу
function translit($s) {
  $s = (string) $s; // преобразуем в строковое значение
  $s = strip_tags($s); // убираем HTML-теги
  $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
  $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
  $s = trim($s); // убираем пробелы в начале и конце строки
  $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
  $s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
  $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
  $s = str_replace("-", " ", $s); // заменяем минусы пробелами
  $s = mb_strtolower($s); // переводим в нижний регистр
  return $s; // возвращаем результат
}

// Перевод даты из строки неопределенного формата в мой формат.
function edit_date($date){
	if ($date == 0) {return '-';}
	return date("d.m.Y", strtotime($date));
}


function message_to_telegram($text, $telegram_chatid='440401693', $botid='914611381:AAH-o_IN7-9SEK7RDOLHeqYUgSCsbCFvwgE') {
	// Отправка сообщения ботом в чат телеграмм
	//
	// $text - текст сообщения
	// $telegram_chatid - ID получателя (440401693 - телион, 1016039173 - алия)

	$ch = curl_init();
	curl_setopt_array(
		$ch,
		array(
			CURLOPT_URL => 'https://api.telegram.org/bot' . $botid . '/sendMessage',
			CURLOPT_POST => TRUE,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_TIMEOUT => 10,
			CURLOPT_POSTFIELDS => array(
				'chat_id' => $telegram_chatid,
				'text' => $text,
			),
		)
	);
	curl_exec($ch);
}

function sendEmail($header, $content, $receiver, $sendFile=0, $sender=EMAIL_SENDER) {
	// Отправка E-Mail
	//
	// $header - Заголовок письма
	// $content - Содержимое письма (файл или текст)
	// $receiverName - имя получателя
	// $receiver - email получателя
	// $sendFile - отправка файла или текста, по умолчанию текст. 1 - файл, 0 - текст.
	// $sender - email отправителя
	// $senderName - имя отправителя

	if ($sendFile) $content = file_get_contents($content);
	$to = "4you<".$receiver.">";
	$subject = $header; 
	$headers  = "Content-type: text/html; charset=utf-8 \r\n";
	$headers .= "From: cmsAnker <".$sender.">\r\n"; 
	$error = mail($to, $subject, $content, $headers); 
}

?>