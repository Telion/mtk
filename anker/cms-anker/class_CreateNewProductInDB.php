<?php
include_once 'function.php';

class CreateNewProductInDB { // Получение данных из БД по запросу
	public $repository; // подключение к БД МТК
	public $options;
	public $productID;

	function __construct($options, $repository) { //
		echo '<p style="margin: 0;"> - - Получаю аналоги, фото и описание с СДС</p>';
		$this->repository = $repository;
		$this->options = $options;
		$this->correctName();
		$this->generateDescription();
		$this->options['createDate'] = '"'.date("Y-m-d")." ".date("H:i:s").'"';
		$this->options['newIdForTables'] = $this->generateNewIDsFromTables();
		$this->productID = $this->options['newIdForTables']['virtuemart_product_id'];
		$this->options['virtuemart_product_id'] = $this->productID;
		if (empty($this->options['photos'])) {
			echo "Нет фото для товара, товар в базу не добавлен";
		} else {
			$this->createEntriesInTables();
		}
	}

	public function createEntriesInTables() {
		if ($this->createValidation()) {
			$this->createEntryProduct();
			$this->createEntryProductsRuRu();
			$this->createEntryProductCategories();
			$this->createEntryPhotos();
			$this->createEntryProductPrices();
			message_to_telegram('Создан новый товар: ' . $this->productID);
		} else {
			message_to_telegram('товар не прошел валидацию: ' . $this->productID);
		}
	}

	private function createValidation() {
		if (
			!isset($this->productID) or
			!isset($this->options['article']) or
			!isset($this->options['createDate']) or
			!isset($this->options['unit']) or
			!isset($this->options['description']) or
			!isset($this->options['productName']) or
			!isset($this->options['alias']) or
			!isset($this->options['categoryID']) or
			!isset($this->options['photos']) or
			!isset($this->options['newIdForTables']['wxa43_virtuemart_product_categories']) or
			!isset($this->options['newIdForTables']['virtuemart_media_id']) or
			!isset($this->options['newIdForTables']['wxa43_virtuemart_product_medias']) or
			!isset($this->options['newIdForTables']['virtuemart_product_price_id'])
		) {
			return false;
		}
		return true;
	}

	public function getOptions() {
		return $this->options;
	}

	public function correctName() {
		$this->options['productName'] = preg_replace("/\s+/", ' ', $this->options['productName']); // удаляем повторяющие пробелы
		$this->options['productName'] = trim($this->options['productName']); // убираем пробелы в начале и конце строки
	}

	public function generateDescription() {
		$description = $this->options['description'][0]['attribute'];
		$result = '<p><b>Характеристики:</b></p><ul>';
		foreach ($description as $value) {
			$name = $value['characteristic'];
			$value1 = $value['value1'];
			$value2 = $value['value2'];
			$unit = $value['unit'];

			if ($value1 or $value2) {
				$result .= "<li>";
				if ($value2) {
					$result .= $name." - ".$value1."~".$value2." ".$unit;
				} else {
					$result .= $name." - ".$value1." ".$unit;
				}
				$result .= "</li>";
			}
		}

		$result = str_replace("\"", "'", $result); 
		$this->options['description'] = $result;
	}

	public function generateNewIDsFromTables() {
	        $listTableId = [];		
		$listTableId['virtuemart_product_id'] = $this->repository->getNextId('SELECT MAX(virtuemart_product_id) FROM wxa43_virtuemart_products');
		$listTableId['wxa43_virtuemart_product_categories'] = $this->repository->getNextId('SELECT MAX(id) FROM wxa43_virtuemart_product_categories');
		$listTableId['virtuemart_media_id'] = $this->repository->getNextId('SELECT MAX(virtuemart_media_id) FROM wxa43_virtuemart_medias');
		$listTableId['virtuemart_customfield_id'] = $this->repository->getNextId('SELECT MAX(virtuemart_customfield_id) FROM wxa43_virtuemart_product_customfields');
		$listTableId['wxa43_virtuemart_product_manufacturers'] = $this->repository->getNextId('SELECT MAX(id) FROM wxa43_virtuemart_product_manufacturers');
		$listTableId['wxa43_virtuemart_product_medias'] = $this->repository->getNextId('SELECT MAX(id) FROM wxa43_virtuemart_product_medias');
		$listTableId['virtuemart_product_price_id'] = $this->repository->getNextId('SELECT MAX(virtuemart_product_price_id) FROM wxa43_virtuemart_product_prices');

		return $listTableId;
	}

	public function createEntryProduct() {
		try {
			// Таблица wxa43_virtuemart_products
			$this->runQuery('INSERT INTO wxa43_virtuemart_products(virtuemart_product_id, product_sku, product_gtin, product_mpn, product_available_date, product_unit, product_params, published, created_on, created_by, modified_on, modified_by) 
				VALUES('.$this->productID.', '.$this->productID.', "'.$this->options['article'].'", "made by bot ('.date("d-m-Y", time()).')", '.$this->options['createDate'].', "'.$this->options['unit'].'", "min_order_level=0|max_order_level=0|", 1, '.$this->options['createDate'].', 427, '.$this->options['createDate'].', 427)'); // 427 - user PhpBot
		} catch (Exception $e){
			message_to_telegram("ANKER: error\r\ncreateEntryProduct()\r\nID: ".$this->productID);
		}
	}

	public function createEntryProductsRuRu() {
		// Таблица wxa43_virtuemart_products_ru_ru

		$virtuemart_product_id = $this->productID;
		$product_desc = '"'.$this->options['description'].'"';
		$product_name = '"'.$this->options['productName'].'"';
		$slug = '"'.$this->options['alias'].'"';

		try {
			$this->runQuery('INSERT INTO wxa43_virtuemart_products_ru_ru(virtuemart_product_id, product_desc, product_name, slug) 
				VALUES ('.$virtuemart_product_id.', '.$product_desc.', '.$product_name.', '.$slug.')');
		} catch (Exception $e) {
			message_to_telegram("ANKER: error\r\ncreateEntryProductsRuRu()\r\nID: ".$this->productID);
			printr("Ошибка базы данных, пробую вставить другой Алиас");
			$changeAlias = $this->options['alias']."-".$this->productID;
			$this->runQuery('INSERT INTO wxa43_virtuemart_products_ru_ru(virtuemart_product_id, product_desc, product_name, slug) 
				VALUES ('.$this->productID.', "'.$this->options['description'].'", "'.$this->options['productName'].'", "'.$changeAlias.'")');
		}
	}

	public function createEntryProductCategories() {
		try {
			// Таблица wxa43_virtuemart_product_categories
			$this->runQuery('INSERT INTO wxa43_virtuemart_product_categories(id, virtuemart_product_id, virtuemart_category_id) 
				VALUES ('.$this->options['newIdForTables']['wxa43_virtuemart_product_categories'].', '.$this->productID.', '.$this->options['categoryID'].')');
		} catch (Exception $e){
			message_to_telegram("ANKER: error\r\ncreateEntryProductCategories()\r\nID: ".$this->productID);
			message_to_telegram('INSERT INTO wxa43_virtuemart_product_categories(id, virtuemart_product_id, virtuemart_category_id) 
				VALUES ('.$this->options['newIdForTables']['wxa43_virtuemart_product_categories'].', '.$this->productID.', '.$this->options['categoryID'].')');
		}
	}

	public function createEntryPhotos() {
		try {
			$numberPhoto = 0;
			foreach ($this->options['photos'] as $photoPath) {
				$photoPathShort =  substr($photoPath, 1);

				$productName = str_replace('"', '\'', $this->options['productName']);
				
				// Таблица wxa43_virtuemart_medias
				$this->runQuery('INSERT INTO wxa43_virtuemart_medias(virtuemart_media_id, file_title, file_description, file_meta, file_mimetype, file_type, file_url, 
					file_url_thumb, created_on, created_by, modified_on, modified_by)
					VALUES ('.$this->options['newIdForTables']['virtuemart_media_id'].', "'.$this->options['alias'].'.jpg", "'.$productName.'", "'.$productName.'", "image/jpeg", "product", "'.$photoPath.'",
					"'.$photoPathShort.'", '.$this->options['createDate'].', 427, '.$this->options['createDate'].', 427)');

				// Таблица wxa43_virtuemart_product_medias
				$this->runQuery('INSERT INTO wxa43_virtuemart_product_medias(id, virtuemart_product_id, virtuemart_media_id, ordering) 
					VALUES ('.$this->options['newIdForTables']['wxa43_virtuemart_product_medias'].', '.$this->productID.', '.$this->options['newIdForTables']['virtuemart_media_id'].', '.$numberPhoto.')');

				$numberPhoto++;
				$this->options['newIdForTables']['virtuemart_media_id']++;
				$this->options['newIdForTables']['wxa43_virtuemart_product_medias']++;
			}
		} catch (Exception $e){
			message_to_telegram("ANKER: error\r\createEntryPhotos()\r\nID: ".$this->productID);
		}
	}

	public function createEntryProductPrices() {
		try {
			// Таблица wxa43_virtuemart_product_prices
			$this->runQuery('INSERT INTO wxa43_virtuemart_product_prices(virtuemart_product_price_id, virtuemart_product_id, product_price, product_currency, created_on, created_by, modified_on, modified_by)
				VALUES ('.$this->options['newIdForTables']['virtuemart_product_price_id'].', '.$this->productID.', 0, 131, '.$this->options['createDate'].', 427, '.$this->options['createDate'].', 427)');
		} catch (Exception $e){
			message_to_telegram("ANKER: error\r\createEntryProductPrices()\r\nID: ".$this->productID);
		}
	}

/*	public function createEntryProductManufacturers() {
		// Таблица wxa43_virtuemart_product_manufacturers
		if ($rusklimat_manufacturer != 0){
			$this->runQuery('INSERT INTO wxa43_virtuemart_product_manufacturers(id, virtuemart_product_id, virtuemart_manufacturer_id) 
				VALUES ('.$wxa43_virtuemart_product_manufacturers.', '.$this->productID.', '.$virtuemart_manufacturer_id.')');
		}
	}*/

/*	public function createEntryProductCustomfields() {
		// Таблица wxa43_virtuemart_product_customfields
		$this->runQuery('INSERT INTO wxa43_virtuemart_product_customfields(virtuemart_customfield_id, virtuemart_product_id, virtuemart_custom_id, customfield_value, customfield_price, disabler, override, 
			customfield_params, product_sku, product_gtin, product_mpn, published, created_on, created_by, modified_on, modified_by, locked_on, locked_by, ordering)
			VALUES ('.$virtuemart_customfield_id.', '.$this->productID.', 54, "'.$sklad.'", (NULL), 0, 0, (NULL), (NULL), (NULL), (NULL), 1, '.$current_date.', 597, '.$current_date.', 597, "0000-00-00 00:00:00", 0, 54000)');
	}*/

	public function runQuery($query) {
		$this->repository->db->setQuery($query);
		$query = htmlspecialchars($query);
		echo '<p style="font-size: 12px; font-family: monospace; margin: 4px 0;">' . $query . '</p>';
		$this->repository->db->query();
	}
}


?>