<?php
setlocale(LC_CTYPE, array('ru_RU.utf8', 'ru_UA.utf8')); 
setlocale(LC_ALL, array('ru_RU.utf8', 'ru_UA.utf8')); 

class sdsAPI {
	private static $token = 'Token 350f169dc59fd14a606c9c6a87c1ed1d7a4b21e1';

	public function queryToAPI($type, $method, $value) { // $method = article, name, categoryid
		$url = trim("http://193.106.69.207:8760/rexant/hs/api/v1/" . $type . "?" . $method . "=" . $value);

		$headers = [
			'Content-Type: application/json',
			'Accept: application/json',
			'Authorization: '.sdsAPI::$token
		];

		$curl = curl_init();
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($curl, CURLOPT_HEADER, FALSE);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_REFERER, $url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($curl, CURLOPT_COOKIESESSION, TRUE);
			curl_setopt($curl, CURLOPT_POST, 0);
			curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0 (Windows; U; Windows NT 5.0; En; rv:1.8.0.2) Gecko/20070306 Firefox/1.0.0.4");
		$startTime = time();

		$queryResult = json_decode(curl_exec($curl), TRUE)['result'];

		$info = curl_getinfo($curl);

		$timeOut = time() - $startTime;

		if ($timeOut > 2) {
			$color = 'lightsalmon';
		} else {
			$color = '#e7e7e7';
		}
		echo "<h4 style='background: ".$color."; margin: 0; margin-top: 7px;'>class_SDSapi.php -> queryToAPI(): Время обработки запроса: " . $timeOut . " сек (" . $url . ")</h4>";

		curl_close($curl);

		if (is_null($queryResult)) echo "<hr><span style='color: red';>isNull</span><hr>";

		return $queryResult;
	}

	public function getProducts($method, $value) {
		return $this->queryToAPI('product', $method, $value)['results'];
	}

	public function getPrice($method, $value) {
		return $this->queryToAPI('prices', $method, $value)['results']; // $method = productid, productid__article
	}

	public function getRemain($method, $value) {
		return $this->queryToAPI('remain', $method, $value)['results']; // $method = productid, productid__article
	}

	public function getAnalog($method, $value) {
		return $this->queryToAPI('analog', $method, $value)['results']; // $method = productid, productid__article
	}

	public function getPhoto($method, $value) {
		return $this->queryToAPI('photo', $method, $value)['results']; // $method = productid, productid__article
	}

	public function getDescription($method, $value) {
		return $this->queryToAPI('etimproduct', $method, $value)['results']; // $method = productid, productid__article
	}
}
?>