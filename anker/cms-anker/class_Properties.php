<?php
class Properties {
	public $fileName;
	public $arrayProperties = [];

	function __construct($fileName) {
		$this->fileName = $fileName;	
	}

	function readPropertiesFile() {
		$handle = @fopen($this->fileName, "r");
		if ($handle) {
			while (($buffer = fgets($handle, 4096)) !== false) {
				$line = explode('->', $buffer);
				if (isset($line[1])) {
					$this->arrayProperties[$line[0]] = $line[1];
				}
			}
			if (!feof($handle)) {
				echo "Ошибка: fgets() неожиданно потерпел неудачу\n";
			}
			fclose($handle);
			$this->arrayProperties = array_filter($this->arrayProperties);
		}
	}

	function writePropertiesFile() {
		$content = '';
		foreach ($this->arrayProperties as $key=>$value) {
			$content .= $key.'->'.$value."\r\n";
			}
		file_put_contents($this->fileName, $content);		
	}

	function getCurrentCategory() {
		return (int)$this->arrayProperties['currentCategoryID'] + 0;
	}

	function increaseCurrentCategory($value) {
		$this->arrayProperties['currentCategoryID'] = (int)$this->arrayProperties['currentCategoryID'] + $value;
	}

	function resetCurrentCategory() {
		$this->arrayProperties['currentCategoryID'] = 1;
	}


	function getCurrentProduct() {
		return (int)$this->arrayProperties['currentProduct'] + 0;
	}

	function increaseCurrentProduct($value) {
		$this->arrayProperties['currentProduct'] = (int)$this->arrayProperties['currentProduct'] + $value;
	}

	function resetCurrentProduct() {
		$this->arrayProperties['currentProduct'] = 0;	
	}

}

?>






