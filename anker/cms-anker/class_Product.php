<?php
include_once 'class_CreateNewProductInDB.php';

class Product {
	private $ModifyPercent = 87.5; 
	public static $arrArticlesFromAnker;
	public $repository;
	public $fromSDS;		// Массив
	public $productFromDB;		// Массив
	public $priceFromSds;
	public $remainFromSds;
	public $analogFromSds;		// Массив
	public $descriptionFromSds;	// Массив
	public $newIdForTables = [];
	public $options = [];

	public function getProductData($fromSDS, $sdsApi, $repository) {
		$this->fromSDS = $fromSDS;
		$this->repository = $repository;
		$this->options['article'] = $this->fromSDS['article'];
		$this->unitDefinition();
		$isProductHavePrice = true;
		$isProductHaveImage = true;

		if (!$this->isHaveAnker()) {
			echo '<p style="color: red; font-weight: 700;">Нет на Анкере. Добавить товар в базу </p>';
			$isProductHaveImage = $this->createProduct($sdsApi);
		} else {
			$this->repository->getProductsFromDB();
			if (!isset($this->repository->arrProductsFromDB[$this->options['article']])) {
				$isProductHavePrice = $this->deleteProductWithoutPrice($sdsApi);
			} else {
				$this->productFromDB = $this->repository->arrProductsFromDB[$this->options['article']];
				echo "<h3>Название на Анкере: ".$this->productFromDB['product_name']."</h3>";
				if (!isset($this->options['virtuemart_product_id'])) {
					$this->options['virtuemart_product_id'] = $this->productFromDB['virtuemart_product_id'];
				}
			}
		}

		if ($isProductHavePrice and $isProductHaveImage) {
			$this->updatePriceAndRemainFromSds($sdsApi);
		}
	}

	public function deleteProductWithoutPrice($sdsApi) {
		$productId = $this->repository->getProductWithoutPrice($this->options['article']);
		if (empty($this->getPriceFromSds($sdsApi))) {
			message_to_telegram('Нет цены у товара. Удалось получить цену');
			return true;
		}
		$this->repository->deleteProductFromProductTable($productId);
		$this->repository->deleteProductFromProductRuRuTable($productId);
		message_to_telegram('Нет цены у товара. Удалил записи из таблиц БД');
		return false;
	}

	public function updatePriceAndRemainFromSds($sdsApi) {
		echo "<p><b> - - Получаю цену и остатки с СДС <span style='color: red;'>".$this->options['article']."</span></b></p>";
		$this->priceFromSds = $this->getPriceFromSds($sdsApi);
		echo '<p style="margin: 0"> - - - - Цена СДС: ' . $this->priceFromSds . '</p>';
		$this->remainFromSds = $this->getRemainFromSds($sdsApi);
		echo '<p style="margin: 0"> - - - - Остатки СДС: ' . $this->remainFromSds . '</p>';
		echo '<p style="color: green; font-weight: 700;"> - - Есть на Анкере. Обновить цену и остатки </p>';
		$this->updateProduct();
	}

	// проверка наличия карточки товара на анкере
	public function isHaveAnker() {
		if (in_array($this->options['article'], Product::$arrArticlesFromAnker)) {
			return 1;
		}
		return 0;
	}

	// получение цены товара с СДС
	public function getPriceFromSds($sdsApi) {
		$arrayPrice = $sdsApi->getPrice('productid__article', $this->options['article']);
		if (!isset($arrayPrice[0])) {
			return 0;
		}
		$arrayPrice = $arrayPrice[0]['attribute'];
		foreach ($arrayPrice as $price) {
			if ($price['ratename'] == 'Субдилер') {
				return ceil($price['value']);
			}
		}
	}

	// получение остатков товара с СДС
	public function getRemainFromSds($sdsApi) {
		$remains = $sdsApi->getRemain('productid__article', $this->options['article']);
		if (!isset($remains[0])) {
			return 0;
		}

		$arrayRemain = $remains[0]['attribute'];
		foreach ($arrayRemain as $remain) {
			if ($remain['storagename'] == 'Химки') {
				return $remain['count'];
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function updateProduct() {
		$this->updatePrice();
		$this->updateStock();
		$this->updateProductUnit();
		$this->repository->checkMarkInDB($this->options['article'], 1);
		$this->updatePublished();
	}

	public function updatePrice() {
		$sdsModifyPrice = floor($this->priceFromSds * $this->ModifyPercent / 100 + $this->priceFromSds);
		$this->options['sdsModifyPrice'] = $sdsModifyPrice;
		$this->repository->setProductPrice($this->options['virtuemart_product_id'], $sdsModifyPrice);
	}

	public function updateStock() {
		$this->repository->setProductStock($this->options['virtuemart_product_id'], $this->remainFromSds);
	}

	public function updateProductUnit() {
		$this->repository->setProductUnit($this->options['virtuemart_product_id'], $this->options['unit']);
	}

	public function updatePublished() {
		$countImagesOfProduct = $this->repository->getCountImagesOfProduct($this->options['virtuemart_product_id']);
		if ($countImagesOfProduct == 0) {
			message_to_telegram('нет картинок у товара: ' . $this->options['virtuemart_product_id']);
		}


		if (($this->priceFromSds == 0) or ($this->remainFromSds == 0) or ($countImagesOfProduct == 0)) {
			$this->repository->setProductPublished($this->options['virtuemart_product_id'], 0);
		} else {
			$this->repository->setProductPublished($this->options['virtuemart_product_id'], 1);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	public function createProduct($sdsApi) {
//		message_to_telegram('Anker. Запущено создание нового товара, проверить описание, фото и аналог');
		$this->repository->db->checkMarkInDB($this->options['article'], 1);
		$this->options['productName'] = str_replace('"', '""', $this->fromSDS["name"]);
		$this->options['categoryID'] = $this->repository->getAnkerCategory($this->fromSDS['category']['categoryid']);
		$this->options['analog'] = $sdsApi->getAnalog('productid__article', $this->options['article']);
		$this->options['alias'] = $this->getAlias($this->options['productName']);
		$this->options['photos'] = $this->savePhotos($sdsApi);
		if (empty($this->options['photos'])) {
			return false;
		}
		$this->options['description'] = $sdsApi->getDescription('productid__article', $this->options['article']);
		$newProduct = new CreateNewProductInDB($this->options, $this->repository);
		$this->options = $newProduct->getOptions();
		return true;
	}

	public function getAlias($productName) {
		$alias = (string) $productName; // преобразуем в строковое значение
		$alias = strip_tags($alias); // убираем HTML-теги
		$alias = str_replace(array("\n", "\r"), " ", $alias); // убираем перевод каретки
		$alias = preg_replace("/\s+/", ' ', $alias); // удаляем повторяющие пробелы
		$alias = trim($alias); // убираем пробелы в начале и конце строки
		$alias = function_exists('mb_strtolower') ? mb_strtolower($alias) : strtolower($alias); // переводим строку в нижний регистр (иногда надо задать локаль)
		$alias = strtr($alias, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
		$alias = preg_replace("/[^0-9a-z-_ ]/i", "", $alias); // очищаем строку от недопустимых символов
		$alias = str_replace(" ", "-", $alias); // заменяем пробелы знаком минус

		return $alias;
	}


	public function saveTempImage($pathPhoto, $pathTempFile) {
		$curl = curl_init($pathPhoto);
		    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		    curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
		    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$content = curl_exec($curl);

		$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		if ($httpCode == 404) {
			return $httpCode;
		}

		curl_close($curl);
		if (file_exists($pathTempFile)) :
		    unlink($pathTempFile);
		endif;
		try {
			$fp = fopen($pathTempFile,'x');
			fwrite($fp, $content);
			fclose($fp);
		} catch (Exception $e) {
			return 0;
		}

		return file_exists($pathTempFile);
	}

	public function check_domain_availible($domain) {
		if (!filter_var($domain, FILTER_VALIDATE_URL)) {
			return false;
		}
		$curlInit = curl_init($domain);
		curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($curlInit, CURLOPT_HEADER, true);
		curl_setopt($curlInit, CURLOPT_NOBODY, true);
		curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($curlInit);
		curl_close($curlInit);

		if ($response) {
			return true;
		}
		return false;
	}

	public function savePhotos($sdsApi) {
		$photosFromSds = $sdsApi->getPhoto('productid__article', $this->options['article']);
		$arrPathPhotos = [];
		$i = 0;
		foreach ($photosFromSds as $photo) {
			$i++;
			$pathPhotoArray = explode('/', $photo["filelink"]);
			$pathPhoto = $pathPhotoArray[0] . '/' . $pathPhotoArray[1] . '/'  . $pathPhotoArray[2] . '/' . $pathPhotoArray[3] . '/' . $photo['filetype'] . '/' . $pathPhotoArray[4];

			$alias_photo = $this->options['alias'].'-'.$i.".".$photo["filetype"];
			$pathTempFile = "tempImage.jpg";

printr($pathPhoto);
printr($this->saveTempImage($pathPhoto, $pathTempFile));
//die();
			if (($this->saveTempImage($pathPhoto, $pathTempFile) !== 404) and ($this->check_domain_availible($pathPhoto))) {
				$explodeImagePath = explode('.', $pathPhoto);
				$fileExtensionPartNumber = count($explodeImagePath) - 1;
        		        $fileExtension = $explodeImagePath[$fileExtensionPartNumber];
				$imageSize = getimagesize($pathPhoto);
				$required_memory = round($imageSize[0] * $imageSize[1] * $imageSize['bits'] * 1.4);
				if (($required_memory > 100000000) AND ($required_memory < 400000000)) {
					printr($required_memory);
					$new_limit = memory_get_usage() + $required_memory;
					ini_set("memory_limit", $new_limit);
				}
				if ($fileExtension == 'png') {
					$image = imagecreatefrompng($pathPhoto);
					if (empty($image)) {
						$image = imagecreatefromjpeg($pathPhoto);
					}
				} elseif ($fileExtension == 'jpg') {
					$image = imagecreatefromjpeg($pathPhoto);
					if (empty($image)) {
						$image = imagecreatefrompng($pathPhoto);
					}
				} else {
					message_to_telegram("Аиртек: Неизвестное расширение у картинки");
				}
				$image = imagescale($image, 1000); 
				ini_restore("memory_limit");

				$img_size_x = imagesx($image);
				$img_size_y = imagesy($image);

				$bg = imagecreatetruecolor($img_size_x, $img_size_y); // создание изображения с заданными размерами
				imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255)); // заливка изображения бг белым цветом
				imagealphablending($bg, TRUE); // сопряжение изображений
				imagecopy($bg, $image, 0, 0, 0, 0, $img_size_x, $img_size_y); // копирование исходного изображения (image) в созданную основу (bg)
				imagedestroy($image); // уничтожение исходного изображения

				$cropped_img = imagecropauto($bg , IMG_CROP_THRESHOLD, null, 16777215); // обрезание изображения
				imagedestroy($bg); // уничтожение исходного изображения

				$crp_w = ImageSX($cropped_img); // ширина обрезанного
				$crp_h = ImageSY($cropped_img); // высота обрезанного

				$crp_ratio = $crp_h / $crp_w;
				$ratio = 1.44;
				if ( $crp_ratio < $ratio ) {
					$new_h = $crp_w + $crp_w * 0.15;
					$new_w = $new_h;
				} else {
					$new_w = $crp_h + $crp_h * 0.15;
					$new_h = $new_w;
				}                   

				// выравнивание картинки по центру
				$dst_x = ($new_w - $crp_w) / 2;
				$dst_y = ($new_h - $crp_h) / 2;

				// создаем основу
				$canvas = imagecreatetruecolor($new_w, $new_h);
				$white = imagecolorallocate($canvas, 255, 255, 255);
				imagefilledrectangle($canvas, 0, 0, $new_w, $new_h, $white);

				// сливаем картинки
				imagecopyresampled($canvas, $cropped_img, $dst_x, $dst_y, 0, 0, $crp_w, $crp_h, $crp_w, $crp_h);
				if ($new_h < 550) {
					$canvas = imagescale($canvas, 500); 
				} else {
					$canvas = imagescale($canvas, 750); 
				}
				imagedestroy($cropped_img);

				$canvas_resize = imagescale($canvas, 200); 

				$alias_photo = $this->options['alias'].'-'.$i;
				$path_img = '/images/virtuemart/product/'.$alias_photo.'.jpg';
				$path_img_resize = '/images/virtuemart/product/resized/'.$alias_photo.'_200x200.jpg';
				$path_img_resize_for_db = 'images/virtuemart/product/resized/'.$alias_photo.'_200x200.jpg';
				$path_full_img = __DIR__."/..".$path_img;
				$path_full_img_resize = __DIR__."/..".$path_img_resize;
				$arrPathPhotos[] = $path_img;

				echo '<p style="padding-left: 12px; margin: 0;"><b>Новый товар</b>: Путь к картинке: '.$path_full_img."</p>";
		
				try {
					imagejpeg($canvas, $path_full_img, 100);
					imagejpeg($canvas_resize, $path_full_img_resize, 100);
				} catch (\Exception $e) {
					file_put_contents("log.txt", $e->getMessage() . "\n");
				}
		
				imagedestroy($canvas);
				imagedestroy($canvas_resize);
	                }
		}
		return $arrPathPhotos;
	}

	public function unitDefinition() {
		try {
			switch ($this->fromSDS['unit']) {
				case "шт": case "бухта": case "рулон": case "наб.": case "кг": case "пар":  case "рол.":
					$result = 'P';
					break;
				case "метр": case "пог. м":
					$result = 'M';
					break;
				case "упак": case "блистер": case "комплект":
					$result = 'FOOT';
					break;
				default: 
					$result = $this->fromSDS['unit'];
					$message = 'Найдена неизвестная единица измерения: '.$this->fromSDS['unit'].', артикул: https://www.sds-group.ru/search.htm?search='.$this->fromSDS['article'];
					printr($message);	
					message_to_telegram($message);	
					break;
			}
			$this->options['unit'] = $result;
		} catch (Exception $e) {
			echo $e;
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function getOurCategory($sdsCategory) { // метод заполняет таблицу соответствия категорий СДС и Анкера
		echo "<h4>если товар есть в базе, то получить его категорию и записать в таблицу соответствия наших категорий и категорий сдс</h4>";
		$this->productFromDB = $this->repository->arrProductsFromDB[$this->options['article']];
		$result = in_array($this->options['article'], Product::$arrArticlesAnker);
		if ($result) {
			$ankerCategory = $this->repository->getCategoryProduct($this->productFromDB['virtuemart_product_id']);
			$this->repository->setCategoryAnkerSDS($ankerCategory, $sdsCategory);
		}
		
		return 1;
	}

}
?>