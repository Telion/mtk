<?php
class Repository { // Получение данных из БД по запросу
	public $db; // подключение к БД МТК
	public $arrProductsFromDB;

	function __construct($db) { //
		$this->db = $db;
		$this->getProductsFromDB();
	}

	// получение списка соответствия категорий Анкер и СДС
	public function getListOfCategories() {
		$this->db->setQuery('SELECT * FROM cmsanker_category_sds ORDER BY cmsanker_category_sds.kod ASC');
		return $this->db->loadAssocList('category_sds');
	}

	public function getCategoryByKod($categoryKod) {
		$query = 'SELECT * FROM cmsanker_category_sds WHERE kod='.$categoryKod;
		$this->db->setQuery($query);
		return $this->db->loadAssocList();
	}

	public function getAnkerCategory($sdsCategoryID) {
		$this->db->setQuery('SELECT category_anker FROM cmsanker_category_sds WHERE category_sds = "'.$sdsCategoryID.'"');
		return $this->db->loadResult();
	}

	public function getProductArticle() {
		$this->db->setQuery('SELECT product_gtin FROM wxa43_virtuemart_products WHERE product_gtin !="" ORDER BY product_gtin');
		return $this->db->loadColumn();
	}

	public function getCountProductsWithIsNullUnit() {
		$this->db->setQuery('SELECT count(virtuemart_product_id) FROM wxa43_virtuemart_products WHERE product_unit is NULL');
		return $this->db->loadResult();
	}

	public function getProductWithoutPrice($article) {
		$this->db->setQuery('
			SELECT product.virtuemart_product_id, price.virtuemart_product_price_id 
			FROM wxa43_virtuemart_products as product 
			LEFT JOIN wxa43_virtuemart_products_ru_ru as ru_ru ON product.virtuemart_product_id = ru_ru.virtuemart_product_id 
			LEFT JOIN wxa43_virtuemart_product_prices as price ON product.virtuemart_product_id = price.virtuemart_product_id 
			WHERE price.virtuemart_product_price_id IS NULL
				AND product.product_gtin = "' . $article . '";
		');
		return $this->db->loadResult('virtuemart_product_id');
	}


	// получение товаров из БД анкера
	public function getProductsFromDB() {
		$this->db->setQuery('
			SELECT 
				wxa43_virtuemart_products.virtuemart_product_id, 
				wxa43_virtuemart_products.published, 
				wxa43_virtuemart_products_ru_ru.product_name, 
				wxa43_virtuemart_products.product_gtin, 
				wxa43_virtuemart_products.product_in_stock, 
				wxa43_virtuemart_product_prices.product_price
			FROM 
				wxa43_virtuemart_products_ru_ru, 
				wxa43_virtuemart_products,
				wxa43_virtuemart_product_prices
			WHERE 
				wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id 
				AND wxa43_virtuemart_product_prices.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id
		');
		$this->arrProductsFromDB = $this->db->loadAssocList('product_gtin');
	}

	// обновление данных в таблице БД
	public function updateTable($query) {
		echo '<p style="font-size: 12px; font-family: monospace; margin: 0;">' . $query . '</p>';
		$this->db->setQuery($query);
		$this->db->query();
	}

	public function checkMarkInDB($article, $value) {
		$this->updateTable('UPDATE wxa43_virtuemart_products SET metaauthor = '.$value.' WHERE product_gtin = "'.$article.'"');
	}

	// обновление цены товара
	public function setProductPrice($id, $value) {
		$this->updateTable('UPDATE wxa43_virtuemart_product_prices SET product_price = '.$value.' WHERE virtuemart_product_id = '.$id);
	}

	// обновление остатка товара
	public function setProductStock($id, $value) {
		$this->updateTable('UPDATE wxa43_virtuemart_products SET product_in_stock = '.$value.' WHERE virtuemart_product_id = '.$id);
	}

	// обновление статуса опубликован товар или нет
	public function setProductPublished($id, $value) {
		echo '<div style="background: ' . (($value == 1) ? 'lightgreen' : 'lightcoral') . '; width: max-content">';
		$this->updateTable('UPDATE wxa43_virtuemart_products SET published = '.$value.' WHERE virtuemart_product_id = '.$id);
		echo '</div>';
	}

	// обновление единицы измерения
	public function setProductUnit($id, $value) {
		$this->updateTable('UPDATE wxa43_virtuemart_products SET product_unit = "'.$value.'" WHERE virtuemart_product_id = '.$id);
	}

	public function getNextId($query) {
		$this->db->setQuery($query);
		return $this->db->loadColumn()[0] + 1;
	}	

/*	// получить список товаров, не найденных на СДС
	public function getMissingProductsFromDB() {
		$this->db->setQuery('
			SELECT 
				wxa43_virtuemart_products.virtuemart_product_id, 
				wxa43_virtuemart_products.published, 
				wxa43_virtuemart_products_ru_ru.product_name, 
				wxa43_virtuemart_products.product_gtin, 
				wxa43_virtuemart_products.product_in_stock, 
				wxa43_virtuemart_product_prices.product_price
			FROM 
				wxa43_virtuemart_products_ru_ru, 
				wxa43_virtuemart_products,
				wxa43_virtuemart_product_prices
			WHERE 
				wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id 
				AND wxa43_virtuemart_product_prices.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id
		');
		$this->arrProductsFromDB = $this->db->loadAssocList('product_gtin');
	}	*/

	public function getCategoriesWhereAllProductsNotPublished() { // Получить категории, в которых 0 опубликованных товаров
		$this->db->setQuery('
			SELECT wxa43_virtuemart_categories.virtuemart_category_id, wxa43_virtuemart_categories_ru_ru.category_name
			FROM wxa43_virtuemart_categories, wxa43_virtuemart_categories_ru_ru
			WHERE 
			    wxa43_virtuemart_categories.virtuemart_category_id = wxa43_virtuemart_categories_ru_ru.virtuemart_category_id AND
			    wxa43_virtuemart_categories.virtuemart_category_id NOT IN (
			        SELECT wxa43_virtuemart_categories.virtuemart_category_id
				    FROM wxa43_virtuemart_products, wxa43_virtuemart_product_categories, wxa43_virtuemart_categories
				    WHERE 
				        wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_product_categories.virtuemart_product_id AND
				        wxa43_virtuemart_product_categories.virtuemart_category_id = wxa43_virtuemart_categories.virtuemart_category_id AND
			    	        wxa43_virtuemart_products.published = 1
				    GROUP BY wxa43_virtuemart_categories.virtuemart_category_id) AND
			    wxa43_virtuemart_categories.virtuemart_category_id NOT IN (
			        SELECT category_parent_id FROM wxa43_virtuemart_categories GROUP BY wxa43_virtuemart_categories.category_parent_id ASC)
			ORDER BY wxa43_virtuemart_categories.virtuemart_category_id ASC
		');
		$categoriesWithNotPublishedElementsList = $this->db->loadAssocList('virtuemart_category_id');
		$resultLowerCategoriesWithNotPublishedElementsList = [];

		foreach ($categoriesWithNotPublishedElementsList as $categoriesElement) {
			$lowerCategoryName = mb_strtolower($categoriesElement['category_name']);
			$resultLowerCategoriesWithNotPublishedElementsList[$lowerCategoryName] = $categoriesElement;
			$resultLowerCategoriesWithNotPublishedElementsList[$lowerCategoryName]['category_name'] = $lowerCategoryName;
		}


		return $resultLowerCategoriesWithNotPublishedElementsList;
	}

	public function getCategoriesPublishedProducts() { // Получить категории, в которых есть хотя бы один опубликованный товар
		$this->db->setQuery('
			SELECT wxa43_virtuemart_categories.virtuemart_category_id, wxa43_virtuemart_categories_ru_ru.category_name
			FROM wxa43_virtuemart_products, wxa43_virtuemart_product_categories, wxa43_virtuemart_categories, wxa43_virtuemart_categories_ru_ru
			WHERE 
                    	    wxa43_virtuemart_products.published = 1 AND
			    wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_product_categories.virtuemart_product_id AND
                            wxa43_virtuemart_categories.virtuemart_category_id = wxa43_virtuemart_categories_ru_ru.virtuemart_category_id AND
			    wxa43_virtuemart_product_categories.virtuemart_category_id = wxa43_virtuemart_categories.virtuemart_category_id AND
                            wxa43_virtuemart_categories.virtuemart_category_id NOT IN (
			        SELECT category_parent_id FROM wxa43_virtuemart_categories GROUP BY wxa43_virtuemart_categories.category_parent_id ASC)
			GROUP BY wxa43_virtuemart_categories.virtuemart_category_id
		');
		$categoriesElementsList = $this->db->loadAssocList('virtuemart_category_id');
		$resultLowerCategoriesList = [];

		foreach ($categoriesElementsList as $categoriesElement) {
			$lowerCategoryName = mb_strtolower($categoriesElement['category_name']);
			$resultLowerCategoriesList[$lowerCategoryName] = $categoriesElement;
			$resultLowerCategoriesList[$lowerCategoryName]['category_name'] = $lowerCategoryName;
		}


		return $resultLowerCategoriesList;
	}

	public function getMenuList() {
		$this->db->setQuery('
			SELECT id, title, published FROM wxa43_menu 
			WHERE menutype = "category"
		');
		$menuElementsList = $this->db->loadAssocList('title');
		$resultLowerTitleList = [];


		foreach ($menuElementsList as $menuElement) {
			$lowerTitle = mb_strtolower($menuElement['title']);
			$resultLowerTitleList[$lowerTitle] = $menuElement;
			$resultLowerTitleList[$lowerTitle]['title'] = $lowerTitle;
		}

		return $resultLowerTitleList;
	}

	public function getCategoryList() {
		$this->db->setQuery('
			SELECT virtuemart_category_id, published
			FROM wxa43_virtuemart_categories
			WHERE 
			    wxa43_virtuemart_categories.virtuemart_category_id NOT IN (
			        SELECT category_parent_id FROM wxa43_virtuemart_categories GROUP BY wxa43_virtuemart_categories.category_parent_id ASC)
			GROUP BY wxa43_virtuemart_categories.virtuemart_category_id
		');
		return $this->db->loadAssocList('virtuemart_category_id');
	}

	public function publishMenuElement($id) {
		$this->updateTable('UPDATE wxa43_menu SET published = 1 WHERE id = "'.$id.'"');
	}

	public function publishCategoryElement($id) {
		$this->updateTable('UPDATE wxa43_virtuemart_categories SET published = 1 WHERE virtuemart_category_id = "'.$id.'"');
	}

	public function unpublishMenuElement($id) {
		$this->updateTable('UPDATE wxa43_menu SET published = -2 WHERE id = "'.$id.'"');
	}

	public function unpublishCategoryElement($id) {
		$this->updateTable('UPDATE wxa43_virtuemart_categories SET published = 0 WHERE virtuemart_category_id = "'.$id.'"');
	}

	public function getProductsWithoutImagesList() {
		$this->db->setQuery('
			SELECT wxa43_virtuemart_products.virtuemart_product_id, product_gtin, published, wxa43_virtuemart_products_ru_ru.slug
			FROM wxa43_virtuemart_products, wxa43_virtuemart_products_ru_ru
			WHERE wxa43_virtuemart_products.virtuemart_product_id NOT IN 
				(SELECT virtuemart_product_id FROM wxa43_virtuemart_product_medias GROUP BY virtuemart_product_id) AND
		                wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id
			ORDER BY `wxa43_virtuemart_products`.`virtuemart_product_id` DESC
		');
		return $this->db->loadAssocList('virtuemart_product_id');
	}

	public function getMediasStartID() {
		$this->db->setQuery('
			SELECT virtuemart_media_id FROM wxa43_virtuemart_medias  
			ORDER BY wxa43_virtuemart_medias.virtuemart_media_id  DESC
			LIMIT 1
		');
		return $this->db->loadResult() + 1;
	}

	public function getProductMediasStartID() {
		$this->db->setQuery('
			SELECT id FROM wxa43_virtuemart_product_medias
			ORDER BY wxa43_virtuemart_product_medias.id  DESC
			LIMIT 1
		');
		return $this->db->loadResult() + 1;
	}

	public function getCountImagesOfProduct($productId) {
		$query = 'SELECT COUNT(*) FROM wxa43_virtuemart_product_medias WHERE virtuemart_product_id = ' . $productId;
		$this->db->setQuery($query);
		return $this->db->loadResult() + 1;
	}

	public function deleteProductFromProductTable($productId) {
		$this->db->setQuery('DELETE FROM wxa43_virtuemart_products WHERE virtuemart_product_id = ' . $productId);
		$this->db->query();
	}

	public function deleteProductFromProductRuRuTable($productId) {
		$this->db->setQuery('DELETE FROM wxa43_virtuemart_products_ru_ru WHERE virtuemart_product_id = ' . $productId);
		$this->db->query();

	}

}
?>






