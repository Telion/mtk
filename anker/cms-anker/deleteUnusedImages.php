<?php

ini_set('display_errors', 1);
require_once("connectionDB.php"); 

$arrayWithArticles = [];
$allProductList = getAllPhotos($db);

$dirPath = "../images/virtuemart/product";
$allFilesInDir = scandir($dirPath);

$fileNamesFromDB = renamePathInArray($allProductList);
$fileNamesFromDB[] = '.';
$fileNamesFromDB[] = '..';
$fileNamesFromDB[] = '.htaccess';

deleteUnusedFiles($fileNamesFromDB, $allFilesInDir);



function getAllPhotos($db) {
	$query = ' SELECT file_url, file_url_thumb FROM wxa43_virtuemart_medias WHERE file_type = "product"';
	$db->setQuery($query);
	return $db->loadAssocList("file_url");
}

function renamePathInArray($allProductList) {
	$result = [];
	$fullSizeOfFiles = 0;
	foreach ($allProductList as $product) {
		$ImgPathArray = explode('/', $product['file_url']);
		$file = $ImgPathArray[count($ImgPathArray)-1];
		$result[] = $file;
		$fullSizeOfFiles += filesize("../images/virtuemart/product/" . $file);
	}
	echo '<hr><strong>FULL SIZE: ' . round($fullSizeOfFiles / 1024 / 1024, 2) . ' Mb</strong>';
	return $result;
}

function deleteUnusedFiles($fileNamesFromDB, $allFilesInDir) {
	$count = 0;
	$sizeOfFiles = 0;
	foreach ($allFilesInDir as $file) {
		if (!in_array($file, $fileNamesFromDB)) {
			echo "Нет в базе, удалить: " .  $file . "<br>";
			$sizeOfFiles += filesize("../images/virtuemart/product/" . $file);
			$count++;
		}
	}
	echo '<hr><strong>ИТОГО: ' . $count . ' шт';
	echo ' / ' . round($sizeOfFiles / 1024 / 1024, 2) . ' Mb</strong>';
}
                         
?>
