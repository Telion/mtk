<?php
ini_set('display_errors', 1);
require_once 'function.php';

require_once 'class_Repository.php';
require_once 'class_Product.php';
require_once 'class_Report.php';
require_once 'class_SDSapi.php';
require_once 'class_Properties.php';
require_once 'class_HandlerEmptyCategories.php';

$dieFile = createDieFile(__FILE__);
$report = new Report('3-report-anker.html', 'Главный отчет');
$report->addHeader();

$repository = new Repository($db);
$sdsApi = new sdsAPI();
$properties = new Properties("checkSDS.properties");

Product::$arrArticlesFromAnker = $repository->getProductArticle();
$arrCategories = $repository->getListOfCategories();

$howProducts = 100; // сколько запросов за 1 запуск скрипта
$workCount = 1; // № текущего запроса

while ($workCount <= $howProducts) {

	$properties->readPropertiesFile();
	$currentCategoryID = (int) $properties->arrayProperties['currentCategoryID'] + 0;
	$category = $repository->getCategoryByKod($currentCategoryID)[0];
	$report->addCategory($category['category_sds_name'], $currentCategoryID, count($arrCategories));
	$arrCurrentCategoryProducts = $sdsApi->getProducts('categoryid', $category['category_sds']);
	$countProducts = count($arrCurrentCategoryProducts);

	echo "<h3 style='background: darkslateblue; color: white; padding: 4px 10px; margin: 0;'>Категория в пропетис: " . $currentCategoryID . ", кол-во товаров в категории: " . $countProducts . "</h3>";
	$startNumber = (int) $properties->arrayProperties['currentProduct'] + 0;
	$startNumber = ($startNumber > $countProducts) ? $countProducts : $startNumber;

	for ($i = $startNumber; $i < $countProducts; $i++) {

		sleep(1);
		echo "<h3 style='background: lightblue; padding: 4px 10px; margin: 0;'>№ товаров в категории: " . ($i + 1) . " из " . $countProducts . " (текущий запуск скрипта: " . $workCount . " из " . $howProducts . ")</h3>";

		$currentProduct = new Product();
		$currentProduct->getProductData($arrCurrentCategoryProducts[$i], $sdsApi, $repository);

		if (isset($currentProduct->virtuemart_product_id)) {
			$report->addProduct($currentProduct);
			echo "<hr>";
		}

		$properties->arrayProperties['currentProduct'] = (int) $properties->arrayProperties['currentProduct'] + 1;
		$properties->arrayProperties['counter'] = $workCount;
		$properties->writePropertiesFile();

		$workCount++;
		if ($workCount > $howProducts) break;
	}


	// Запоминаем следующую категорию
	if ($properties->arrayProperties['currentProduct'] >= $countProducts) {
		$properties->arrayProperties['currentCategoryID'] = (int) $properties->arrayProperties['currentCategoryID'] + 1;	
		$properties->arrayProperties['currentProduct'] = 0;	
	} else if ($currentCategoryID >= count($arrCategories)) {
		$properties->arrayProperties['currentCategoryID'] = 1;
		// TODO: ОТЧЕТ Об отсутствующих товарах в СДС
		$reportMissingProducts = new Report('reportMissingProducts.html', 'Главный отчет');		
	}
	
	$properties->writePropertiesFile();
} 

$handlerEmptyCategories = new HandlerEmptyCategories($repository); // Публикация/снятие с публикации пустых категорий

// TODO: Общий файл-отчет, в котором содержаться все обработанные товары с первого до последнего, хранить все версии этого отчета

$report->endTable();
$report->printCountProductsWithIsNullUnit($repository);

sendEmail('From: Mr. Anker72 bot', $report->reportFile, EMAIL_TLN, 1, $sender=EMAIL_SENDER);

function createDieFile($fileName) {
    $dieFile = __FILE__ . '_die.txt';
    file_put_contents($dieFile, date("d.m.Y H:i:s", time()) . " Скрипт начал работу. Удали для остановки скрипта");

    return $dieFile;
}

?>