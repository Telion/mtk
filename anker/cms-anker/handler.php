<?php
require_once 'function.php';
require_once 'class_Repository.php';
require_once 'class_Product.php';
require_once 'class_Report.php';
require_once 'class_SDSapi.php';
require_once 'class_Properties.php';
require_once 'class_HandlerEmptyCategories.php';

$dieFile = createDieFile(__FILE__);
$report = new Report('3-report-anker.html', 'Главный отчет');
$report->addHeader();
$repository = new Repository($db);
$sdsApi = new sdsAPI();
$properties = new Properties("checkSDS.properties");

Product::$arrArticlesFromAnker = $repository->getProductArticle();
$arrCategories = $repository->getListOfCategories();
echo "<h3 style='background: lightblue;'>Количество SDS категорий: ".count($arrCategories)."</h3>";

$howProducts = 5; // сколько запросов за 1 запуск скрипты
$workCount = 1; // № текущего запроса

while ($workCount <= $howProducts) {
	$properties->readPropertiesFile();
	$currentCategoryID = $properties->getCurrentCategory();

	$category = $repository->getCategoryByKod($currentCategoryID)[0];
	$report->addCategory($category['category_sds_name'], $currentCategoryID, count($arrCategories));

	$arrCurrentCategoryProducts = $sdsApi->getProducts('categoryid', $category['category_sds']);
	$countProducts = $arrCurrentCategoryProducts['count'] ?: 0;

	echo "<h1 style='background: lightcoral; padding: 10px 20px;'>Количество товаров в категории с id".$currentCategoryID.": ".$countProducts."</h1>";

printr($arrCurrentCategoryProducts);
die();

	$startNumber = $properties->getCurrentProduct;
	$startNumber = ($startNumber > $countProducts) ? $countProducts : $startNumber;

	for ($i = $startNumber; $i < $countProducts; $i++) {

		echo "<h2 style='background: lightblue; padding: 10px 20px;'>№ товаров в категории: ".($i+1)." из ".$countProducts." (текущий товар ".$workCount." из ".$howProducts.")</h2>";

		$currentProduct = new Product();
		$currentProduct->getProductData($arrCurrentCategoryProducts['results'][$i], $sdsApi, $repository);

		$report->addProduct($currentProduct);
		echo "<hr>";

		$properties->increaseCurrentProduct(1);
		$properties->writePropertiesFile();

		$workCount++;
		if ($workCount > $howProducts) break;
	}

	// Запоминаем следующую категорию
	if ($properties->arrayProperties['currentProduct'] >= $countProducts) {
		$properties->increaseCurrentCategory(1);
		$properties->resetCurrentProduct();
	} else if ($currentCategoryID >= count($arrCategories)) {
		$properties->resetCurrentCategory();
		// TODO: ОТЧЕТ Об отсутствующих товарах в СДС
		$reportMissingProducts = new Report('reportMissingProducts.html', 'Главный отчет');		
	}
	
	$properties->writePropertiesFile();
}

$handlerEmptyCategories = new HandlerEmptyCategories($repository); // Публикация/снятие с публикации пустых категорий

// TODO: Общий файл-отчет, в котором содержаться все обработанные товары с первого до последнего, хранить все версии этого отчета

$report->endTable();
$report->printCountProductsWithIsNullUnit($repository);

sendEmail('From: Mr. Anker72 bot', $report->reportFile, EMAIL_TLN, 1, $sender=EMAIL_SENDER);

function createDieFile($fileName) {
    $dieFile = __FILE__.'_die.txt';
    file_put_contents($dieFile, date("d.m.Y H:i:s", time()). " Скрипт начал работу. Удали для остановки скрипта");

    return $dieFile;
}
?>