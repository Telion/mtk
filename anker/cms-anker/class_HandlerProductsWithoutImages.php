<?php

class HandlerProductsWithoutImages {

	private $db;
	private $sdsApi;
	private $productsWithoutImagesList;

	function __construct($db, $sdsApi) {
		$this->db = $db;
		$this->sdsApi = $sdsApi;
	}

	public function runProductsHandler() {
		$this->task();

		$this->productsWithoutImagesList = $this->db->getProductsWithoutImagesList();

		$productMediasStartID = $this->db->getProductMediasStartID();
		$mediasStartID = $this->db->getMediasStartID();

		foreach ($this->productsWithoutImagesList as $product) {
			$this->printr($product);
			$this->savePhotos($this->sdsApi, $product);

				// Таблица wxa43_virtuemart_medias
				$this->runQuery('INSERT INTO wxa43_virtuemart_medias(
					virtuemart_media_id, 
					file_title, 
					file_description, 
					file_meta, 
					file_mimetype, 
					file_type, 
					file_url, 
					file_url_thumb, 
					created_on, 
					created_by, 
					modified_on, 
					modified_by
					)
					VALUES (
					'.$this->options['newIdForTables']['virtuemart_media_id'].', 
					"'.$this->options['alias'].'.jpg", 
					"'.$productName.'", 
					"'.$productName.'", 
					"image/jpeg", 
					"product", 
					"'.$photoPath.'",
					"'.$photoPathShort.'", 
					'.$this->options['createDate'].', 
					427, 
					'.$this->options['createDate'].', 
					427
					)');

				// Таблица wxa43_virtuemart_product_medias
				$this->runQuery('INSERT INTO wxa43_virtuemart_product_medias(id, virtuemart_product_id, virtuemart_media_id, ordering) 
					VALUES ('.$this->options['newIdForTables']['wxa43_virtuemart_product_medias'].', '.$this->productID.', '.$this->options['newIdForTables']['virtuemart_media_id'].', '.$numberPhoto.')');

			echo "OK";

break;
			$productMediasStartID++;
			$mediasStartID++;
		}    



	}

	public function createEntryPhotos() {
		try {
			$numberPhoto = 0;
			foreach ($this->options['photos'] as $photoPath) {
				$photoPathShort =  substr($photoPath, 1);

				$productName = str_replace('"', '\'', $this->options['productName']);
				
				// Таблица wxa43_virtuemart_medias
				$this->runQuery('INSERT INTO wxa43_virtuemart_medias(virtuemart_media_id, file_title, file_description, file_meta, file_mimetype, file_type, file_url, 
					file_url_thumb, created_on, created_by, modified_on, modified_by)
					VALUES ('.$this->options['newIdForTables']['virtuemart_media_id'].', "'.$this->options['alias'].'.jpg", "'.$productName.'", "'.$productName.'", "image/jpeg", "product", "'.$photoPath.'",
					"'.$photoPathShort.'", '.$this->options['createDate'].', 427, '.$this->options['createDate'].', 427)');

				// Таблица wxa43_virtuemart_product_medias
				$this->runQuery('INSERT INTO wxa43_virtuemart_product_medias(id, virtuemart_product_id, virtuemart_media_id, ordering) 
					VALUES ('.$this->options['newIdForTables']['wxa43_virtuemart_product_medias'].', '.$this->productID.', '.$this->options['newIdForTables']['virtuemart_media_id'].', '.$numberPhoto.')');

				$numberPhoto++;
				$this->options['newIdForTables']['virtuemart_media_id']++;
				$this->options['newIdForTables']['wxa43_virtuemart_product_medias']++;
			}
		} catch (Exception $e){
			message_to_telegram("ANKER: error\r\createEntryPhotos()\r\nID: ".$this->productID);
		}
	}

	public function saveTempImage($pathPhoto, $pathTempFile) {
		$curl = curl_init($pathPhoto);
		    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		    curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
		    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$content = curl_exec($curl);

		$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		if ($httpCode == 404) {
			return $httpCode;
		}

		curl_close($curl);
		if (file_exists($pathTempFile)) :
		    unlink($pathTempFile);
		endif;
		$fp = fopen($pathTempFile,'x');
		fwrite($fp, $content);
		fclose($fp);

		return file_exists($pathTempFile);
	}


	public function savePhotos($sdsApi, $product) {
		$photosFromSds = $sdsApi->getPhoto('productid__article', $product['product_gtin']);
		print_r($product['product_gtin']);
		$arrPathPhotos = [];
		$i = 0;
		foreach ($photosFromSds as $photo) {
			$i++;
			$pathPhoto = $photo["filelink"];
			$alias_photo = $product['slug'].'-'.$i.".".$photo["filetype"];
			$pathTempFile = "tempImage.jpg";
			if ($this->saveTempImage($pathPhoto, $pathTempFile) !== 404) {
				$image = imagecreatefromjpeg($pathTempFile); // получение изображения в переменную
				$img_size_x = imagesx($image);
				$img_size_y = imagesy($image);

				$bg = imagecreatetruecolor($img_size_x, $img_size_y); // создание изображения с заданными размерами
				imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255)); // заливка изображения бг белым цветом
				imagealphablending($bg, TRUE); // сопряжение изображений
				imagecopy($bg, $image, 0, 0, 0, 0, $img_size_x, $img_size_y); // копирование исходного изображения (image) в созданную основу (bg)
				imagedestroy($image); // уничтожение исходного изображения

				$cropped_img = imagecropauto($bg , IMG_CROP_THRESHOLD, null, 16777215); // обрезание изображения
				imagedestroy($bg); // уничтожение исходного изображения

				$crp_w = ImageSX($cropped_img); // ширина обрезанного
				$crp_h = ImageSY($cropped_img); // высота обрезанного

				$crp_ratio = $crp_h / $crp_w;
				$ratio = 1.44;
				if ( $crp_ratio < $ratio ) {
					$new_h = $crp_w + $crp_w * 0.15;
					$new_w = $new_h;
				} else {
					$new_w = $crp_h + $crp_h * 0.15;
					$new_h = $new_w;
				}                   

				// выравнивание картинки по центру
				$dst_x = ($new_w - $crp_w) / 2;
				$dst_y = ($new_h - $crp_h) / 2;

				// создаем основу
				$canvas = imagecreatetruecolor($new_w, $new_h);
				$white = imagecolorallocate($canvas, 255, 255, 255);
				imagefilledrectangle($canvas, 0, 0, $new_w, $new_h, $white);

				// сливаем картинки
				imagecopyresampled($canvas, $cropped_img, $dst_x, $dst_y, 0, 0, $crp_w, $crp_h, $crp_w, $crp_h);
				if ($new_h < 550) {
					$canvas = imagescale($canvas, 500); 
				} else {
					$canvas = imagescale($canvas, 750); 
				}
				imagedestroy($cropped_img);

				$canvas_resize = imagescale($canvas, 200); 

				$alias_photo = $product['slug'].'-'.$i;
				$path_img = '/images/virtuemart/product/'.$alias_photo.'.jpg';
				$path_img_resize = '/images/virtuemart/product/resized/'.$alias_photo.'_200x200.jpg';
				$path_img_resize_for_db = 'images/virtuemart/product/resized/'.$alias_photo.'_200x200.jpg';
				$path_full_img = __DIR__."/..".$path_img;
				$path_full_img_resize = __DIR__."/..".$path_img_resize;
				$arrPathPhotos[] = $path_img;

				echo '<p style="padding-left: 12px;"><b>Новый товар</b>: Путь к картинке: '.$path_full_img."</p>";
		
				try {
					imagejpeg($canvas, $path_full_img, 100);
					imagejpeg($canvas_resize, $path_full_img_resize, 100);
				} catch (\Exception $e) {
					file_put_contents("log.txt", $e->getMessage() . "\n");
				}
		
				imagedestroy($canvas);
				imagedestroy($canvas_resize);
	                }
		}
		return $arrPathPhotos;
	}


	public function getAlias($productName) {
		$alias = (string) $productName; // преобразуем в строковое значение
		$alias = strip_tags($alias); // убираем HTML-теги
		$alias = str_replace(array("\n", "\r"), " ", $alias); // убираем перевод каретки
		$alias = preg_replace("/\s+/", ' ', $alias); // удаляем повторяющие пробелы
		$alias = trim($alias); // убираем пробелы в начале и конце строки
		$alias = function_exists('mb_strtolower') ? mb_strtolower($alias) : strtolower($alias); // переводим строку в нижний регистр (иногда надо задать локаль)
		$alias = strtr($alias, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
		$alias = preg_replace("/[^0-9a-z-_ ]/i", "", $alias); // очищаем строку от недопустимых символов
		$alias = str_replace(" ", "-", $alias); // заменяем пробелы знаком минус

		return $alias;
	}


	public function task() {
		?>
		<h2>Добавить картинки к товарам без картинок, если картинки найти не удается, значит снимаем товар с публикации.</h2>
		<ol>
			<li>Получить список товаров без картинок</li>
			<li>По АПИ попытаться скачать картинки и прописать их к товарам</li>
			<li>Если нет картинок, то снять товар с публикации</li>
		</ol>
		<?php
	}


	public function printr($message) {
		echo "<hr><pre>";
		print_r($message);
		echo "</pre>";		
	}

}
?>