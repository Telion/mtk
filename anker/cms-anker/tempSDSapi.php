<?php
class sdsAPI {
	private static $token = '350f169dc59fd14a606c9c6a87c1ed1d7a4b21e1';

	public function queryToAPI($type, $method, $value) { // $method = article, name, categoryid
		$url = trim("https://api-rexant.ru/api/v1/".$type."?".$method."=".$value);
		$opts = array(
		  'http' => array(
		    'method' => "GET",
		    'header' => "Accept: application/json\r\nAuthorization: Token ".sdsAPI::$token."\r\n"
		  ),
		    'ssl' => array(
		      'verify_peer' => false,
		      'verify_peer_name' => false
		  )
		);
		$context = stream_context_create($opts);
		$res = file_get_contents($url, false, $context);
		$result = json_decode($res, true);

		return $result['results'];
	}

	public function getProducts($method, $value) {
		$result = $this->queryToAPI('product', $method, $value);

		return $result;
	}

	public function getPrice($method, $value) {
		$result = $this->queryToAPI('prices', $method, $value); // $method = productid, productid__article

		return $result;
	}

	public function getRemain($method, $value) {
		$result = $this->queryToAPI('remain', $method, $value); // $method = productid, productid__article

		return $result;
	}

	public function getAnalog($method, $value) {
		$result = $this->queryToAPI('analog', $method, $value); // $method = productid, productid__article

		return $result;
	}

	public function getPhoto($method, $value) {
		$result = $this->queryToAPI('photo', $method, $value); // $method = productid, productid__article

		return $result;
	}

	public function getDescription($method, $value) {
		$result = $this->queryToAPI('etimproduct', $method, $value); // $method = productid, productid__article

		return $result;
	}

}
	$sdsApi = new sdsAPI();
	$arrayPrice = $sdsApi->getPrice('productid__article', '01-2431-2');
	print_r($arrayPrice);
?>