<?php

class Report {
	public $file;
	public $report;
	public $reportEmail;

	function __construct($reportType = "product", $file = "report/4-report-airtek.html") {
		addToLog('Report: создал объект Report');
		$this->file = $file;
		$this->generateReportHeader();
		if ($reportType == "product") {
			$this->generateProductReportTable();
		} elseif ($reportType == "category") {
			$this->generateCategoryReportTable();
		}
		$this->createReportFile();
	}

	function generateReportHeader() {
		addToLog('Report->generateReportHeader: основная шапка для отчетов');
		$this->report = '
			<html><head>
				<title>Report: Airtek vs Rusklimat (от '.date("d.m.Y").')</title>
				<link rel="shortcut icon" href="favicon.ico">
				<link href="main.css" rel="stylesheet" type="text/css" />
				<style>table {border: 1px solid #494540; border-collapse: collapse;} th {color: #fff; font-weight: 700; font-size: 13px; padding: 10px; width: 80px; border: solid 1px #494540; background: #494540;} td {border-left: solid 0; border-right: solid 0;font-size: 13px; color: #666; text-align: center; border: solid 1px #494540; padding: 6px;} .telion_post {margin-bottom: 300px;}</style>
			<body>
				<div class="main-block-airtek">
				<div class="info-table">
		';
	}

	function generateProductReportTable() {
		addToLog('Report->generateProductReportTable: шапка для таблицы парсинга товаров');
		$style = ' style ="color: #ffffff; background-color: #494540; font-weight: 700; font-size: 13px; padding: 10px;" ';
		$this->report .= '
			<table> <tr>
				<th ' . $style . '>№ п/п</th>
				<th ' . $style . '>Дата</th>
				<th ' . $style . '>Тип операции</th>
				<th ' . $style . '>ID</th>
				<th ' . $style . '>Артикул Airtek</th>
				<th ' . $style . '>Цена Airtek</th>
				<th ' . $style . '>Артикул Rusklimat</th>
				<th ' . $style . '>Цена Rusklimat -скидка</th>
				<th ' . $style . '>Склад (был/стал)</th>
			</tr>
		';
	}

	function generateCategoryReportTable() {
		addToLog('Report->generateCategoryReportTable: шапка для таблицы парсинга категорий');
		$style = ' style ="color: #ffffff; background-color: #494540; font-weight: 700; font-size: 13px; padding: 10px;" ';
		$this->report .= '
			<table> <tr>
				<th ' . $style . '>№ п/п</th>
				<th ' . $style . '>Дата</th>
				<th ' . $style . '>URL категории</th>
				<th ' . $style . '>Кол-во товара в категории</th>
			</tr>
		';
	}

	function createReportFile() {
		addToLog('Report->createReportFile: создать файл отчета');
		$fdreport = fopen($this->file, 'w') or die("не удалось создать файл");
		fwrite($fdreport, $this->report);
		fclose($fdreport);
	}

	function generateCategoryReportLine($url, $countProductHere, $num) {
		$urlCute = str_replace("https://www.rusklimat.ru/tyumen/", "", $url);
		$urlCute = str_replace("/?COUNT=100", "", $urlCute);
		addToLog('Report->generateCategoryReportLine: добавляю запись о категории: ' . $urlCute);
		$generateUrlTag = '<a href="' . $url . '">' . $urlCute . '</a>';
		$content = 
			'<tr>
				<td style="text-align: right; padding-right: 10px;">' . $num . '</td>
				<td>' . date("d.m.Y") . '  //  ' . date("H:i:s") . '</td>
				<td>' . $generateUrlTag . '</td>
				<td style="text-align: right; padding-right: 10px;">' . $countProductHere . '</td>
			</tr>';
		$this->addLineToReport($content);
	}

	function writeCategoryReportFile() {
		$fdreport = fopen($this->file, 'a') or die("не удалось создать файл");
		fwrite($fdreport, '</table></div>');
		fclose($fdreport);
	}

	function generateProductReportLine($product, $currentProductNumber) {
		addToLog('Report->generateProductReportLine: добавить запись в отчет');
		$air_tr_style = $this->getStatusStyle($product->status);
		if ($product->airtekSklad != $product->rusklimatSklad) {
			$sklad = $product->airtekSklad . ' -> ' . $product->rusklimatSklad;
		} else {
			$sklad = $product->rusklimatSklad;
		}
		$content = 
			'<tr' . $air_tr_style . '>
				<td>' . $currentProductNumber . '</td>
				<td>' . date("d.m.Y") . '  //  ' . date("H:i:s") . '</td>
				<td>' . $product->status . '</td>
				<td>' . $product->airtekProductId . '</td>
				<td>' . $product->airtekArticle . '</td>
				<td>' . $product->airtekPrice . '</td>
				<td>' . $product->rusklimatArticle . '</td>
				<td>' . $product->rusklimatAirtekNewPrice . '</td>
				<td>' . $sklad . '</td>
			</tr>';
		$this->addLineToReport($content);
	}

	function getStatusStyle($status) {
		switch ($status) {
			case "без изменений":
				return ' style="background: white;" ';
			case "изменение цены":
				return ' style="background: #D0EECF;" ';
			case "изменение доставки":
				return ' style="background: #F9DAB9;" ';
			case "изменение цены и доставки":
				return ' style="background: #D7C6E1;" ';
		}
		return ' style="background: #FEDDDE;" ';
	}

	function addLineToReport($content) {
		addToLog('Report->addLineToReport: добавить запись в файл отчета');
		$fdreport = fopen($this->file, 'a') or die("не удалось создать файл");
		fwrite($fdreport, $content."\r\n");
		fclose($fdreport);
	}

	function writeProductReportFile($repository, $countProductsToday) {
		addToLog('Report->writeProductReportFile: добавить блок со статистикой изменений товаров в отчет');
		$fdreport = fopen($this->file, 'a') or die("не удалось создать файл");
		fwrite($fdreport, '
			</table></div>
			<div class="group-info"><table>
				<tr style="background: #fff;"><td class="td-text">Без изменений:</td><td style="width: 70px;">' . $repository->getParamByParamName('count_no_change') . '</td></tr>
				<tr style="background: #D0EECF;"><td class="td-text">Изменение цены:</td><td>' . $repository->getParamByParamName('count_changed_price') . '</td></tr>
				<tr style="background: #F9DAB9;"><td class="td-text">Изменение доставки:</td><td>' . $repository->getParamByParamName('count_changed_delivery') . '</td></tr>
				<tr style="background: #D7C6E1;"><td class="td-text">Изменение цены и доставки:</td><td>' . $repository->getParamByParamName('count_changed_all') . '</td></tr>
				<tr style="background: #FEDDDE;"><td class="td-text">Нет на Аиртек:</td><td>' . $repository->getParamByParamName('count_no_airtek') . '</td></tr>
				<tr style="background: #333;">
					<td class="td-text" style="color: #ddd;"><span style="font-size: 15px;">Всего обработано, сегодня:</span></td>
					<td style="color: #ddd;"><span style="font-size: 15px;">'.$countProductsToday.'</span></td></tr>
			</table></div></div>
		');
		fclose($fdreport);
	}

}

?>