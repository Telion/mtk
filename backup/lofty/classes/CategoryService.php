<?php
class CategoryService {
	public $categoryRepository;
	public $categoryList;
	public $vendorURL = 'https://www.rusklimat.ru';

	function __construct() {
		addToLog('CategoryService: создал объект CategoryService');
		$this->categoryRepository = new CategoryRepository();
		$this->categoryList = $this->categoryRepository->categoryList;
	}

	function isRuningCategoryEnumeration() {
		addToLog('CategoryService->isRuningCategoryEnumeration: проверка - запустить парсинг списка товаров из категорий?');
		if ($this->categoryRepository->getNextProductNumber() == -1) {
			return 1;
		}
		return 0;
	}

	function parseProductsFromCategoriesRusklimat() {
		addToLog('CategoryService->parseProductsFromCategoriesRusklimat: цикл парсинга категорий, собираю товары в базу');
		$report = new Report('category');
		$this->categoryRepository->resetProducts();
		$num = 0;
		foreach ($this->categoryList as $category) {
			$num++;
			$url = $category['category_url'];
			echo "<hr><details><summary>Category: URL текущего каталога: " . $url  . "</summary>";



//			$html = file_get_html($url);

			$str = $this->curlQuery();
			$html = str_get_html($str);

			$urlFindProduct = '.catalog-list__item .product-card__header .product-card__title a';
			$articleFindProduct = 'article > div.product-card__header > div.product-card__info > div > div.product-card__product-id > output';
			$countProductHere = count($html->find($urlFindProduct));

die();
// КОЛИЧЕСТВО ТОВАРОВ В КАТЕГОРИИ
			echo "<p>Category: Количество товаров в категории: " . $countProductHere . "</p>"; 
			$this->categoryRepository->setProductCount($category['category_id'], $countProductHere);

// ПЕРЕБИРАЮ ТОВАРЫ КАТЕГОРИИ
			for ($i = 0; $i < $countProductHere; $i++) {

// ТЕКУЩИЙ ТОВАР
				$currentProductUrl = $html->find($urlFindProduct, $i)->href;
// АРТИКУЛ ТОВАРА
				$currentProductArticle = $html->find($articleFindProduct, $i)->innertext;
				echo "<p>Category: Товар №: " . ($i + 1) . ' - <a href="https://www.rusklimat.ru' . $currentProductUrl . '" target="blank">' . $currentProductUrl . "</a></p>";
				$this->categoryRepository->checkFoundProductByArticle($currentProductArticle);
				$this->categoryRepository->addProduct($category['id_airtek_category'], $currentProductUrl, $currentProductArticle);
			}

			echo "</details>";
			$report->generateCategoryReportLine($url, $countProductHere, $num);
//			message_to_telegram($url . ": " . $countProductHere);
		}
		$this->categoryRepository->unpublishNotFoundProducts();
		$this->categoryRepository->setNextProductNumber(0);

		$report->writeCategoryReportFile();
		$sender = new Sender();
		$sender->sendScanCategoriesReportEmail($report, count($this->categoryList));
	}

	function curlQuery() {
		addToLog('CategoryService->curlQuery: CURL запрос на сайт вендора');

		$curl = curl_init();
		   curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		   curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
		   curl_setopt($curl, CURLOPT_HEADER, FALSE);
		   curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
		   curl_setopt($curl, CURLOPT_URL, $this->vendorURL);
		   curl_setopt($curl, CURLOPT_REFERER, $this->vendorURL);
		   curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		   curl_setopt($curl, CURLOPT_COOKIESESSION, TRUE);
		   curl_setopt($curl, CURLOPT_POST, FALSE);
//		   curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0 (Windows; U; Windows NT 5.0; En; rv:1.8.0.2) Gecko/20070306 Firefox/1.0.0.4");
		   curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36");

		$str = curl_exec($curl);
		curl_close($curl);
		return $str;
	}

}

?>