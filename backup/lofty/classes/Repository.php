<?php

class Repository {
	public $db;
	public $currentDate;

	function __construct() {
		addToLog('Repository: создал объект Repository');
		$this->db = JFactory::getDBO();
		$this->currentDate = '"'.date("Y-m-d")." ".date("H:i:s").'"';
	}

	function getNextProductNumber() {
		addToLog('Repository->getNextProductNumber: получил номер следующего товара');
		return $this->getCurrentOption('next_product');
	}

	function getCurrentOption($paramName) {
		$query = ' SELECT param_value FROM tln_rusklimat_config WHERE param_name = "' . $paramName . '" ';
		addToLog('Repository->getCurrentOption: из базы получил сохраненные параметры, ' . $query);
		$this->db->setQuery($query);
		return $this->db->loadResult();
	}

	function setNextProductNumber($value) {
		if ($value == $this->getCountProducts()) {
			$value = -1;
		}
		addToLog('Repository->setNextProductNumber: установить новое значение в поле next_product, ' . $value);
		$this->setCurrentOption('next_product', $value);
	}

	function getCountProducts() {
		$query = ' SELECT COUNT(virtuemart_product_id) as count FROM wxa43_virtuemart_products; ';
		addToLog('ProductRepository->getCountProducts: получаю количество активных товаров для парсинга, ' . $query);
		$this->db->setQuery($query);
		return $this->db->loadColumn()[0];
	}

	function setCurrentOption($paramName, $value) {
		$query = ' UPDATE tln_rusklimat_config SET param_value = "' . $value . '" WHERE param_name = "' . $paramName . '" ';
		addToLog('Repository->setCurrentOption: обновил таблицу tln_rusklimat_config, ' . $query);
		$this->db->setQuery($query);
		$this->db->query();
	}

}

?>