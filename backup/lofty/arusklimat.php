<?php
require_once("configuration.php");   
message_to_telegram('----- Airtek vs Rusklimat ----- ' . "\r\n" . '<b>должен прислать уведомление о завершении работы</b>');

$categoryService = new CategoryService();

if ($categoryService->isRuningCategoryEnumeration()) {
	message_to_telegram('----- Парсинг категорий -----');
	$categoryService->parseProductsFromCategoriesRusklimat();
} else {
	message_to_telegram('----- Парсинг товаров (' . $howManyProductsToParse . ') -----');
	$productService = new ProductService();
	$productService->setParameters($howManyProductsToParse);
	$productService->parseProductData();
}

if (Product::$err_text != '') message_to_telegram(Product::$err_text);
message_to_telegram('----- Airtek завершил работу -----');
?>
