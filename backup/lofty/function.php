<?php

function cleanLog() {
	editFile('functions.log', 'w', '');
}

function createDieFile($currentDate) {
	editFile('die.txt', 'w', $currentDate."\r\nудали, для остановки скрипта");
}

function editFile($fileName, $operation, $content) {
	$f = fopen($fileName, $operation) or die("не удалось создать файл");
	fwrite($f, $content);
	fclose($f);
}

function addToLog($message) {
	$parts = explode(': ', $message);
	$firstPart = (isset($parts[0])) ? $parts[0] : '';
	$secondPart = (isset($parts[1])) ? $parts[1] : '';
	$countSpaces = 55 - strlen($firstPart);
	$spaces = '';
	for ($i = 0; $i < $countSpaces; $i++) {
		$spaces .= ' ';
	}
	$date = getCurrentDate();
	$message = $date . '    ' . $firstPart . $spaces . $secondPart;
	editFile('functions.log', 'a', $message . "\r\n");
}

function isStopScript() {
    if ( !file_exists('die.txt') ) {
	message_to_telegram('----- Airtek аварийное завершение работы -----');
	die();
    }
}

function message_to_telegram($text) {	// хз
    $ch = curl_init();
    curl_setopt_array(
        $ch,
        array(
            CURLOPT_URL => 'https://api.telegram.org/bot' . TELEGRAM_TOKEN . '/sendMessage',
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_POSTFIELDS => array(
                'chat_id' => TELEGRAM_CHATID,
                'text' => $text,
                'parse_mode' => 'html'
            ),
        )
    );
    curl_exec($ch);
}



function writeFileAllArticles($arr_all_articles_in_rusklimat) {	// хз
	editFile('tln_all_articles.txt', 'a', implode(';;', $arr_all_articles_in_rusklimat).";;");
}

function clearFileAllArticles() {	// хз
	editFile('tln_all_articles.txt', 'w', "");
}

function printr($message) {	// хз
	echo '<pre>';
	print_r($message);
	echo '</pre><br>';
}

function getCurrentDate() {	// хз
	return date("d.m.Y H:i:s");
}
  
?>