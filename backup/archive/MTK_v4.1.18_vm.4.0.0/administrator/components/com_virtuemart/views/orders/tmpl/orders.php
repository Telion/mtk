<?php
/**
 *
 * Description
 *
 * @package    VirtueMart
 * @subpackage
 * @author VirtueMart Team, Max Milbers
 * @link https://virtuemart.net
 * @copyright Copyright (c) 2004 - 2022 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: orders.php 10649 2022-05-05 14:29:44Z Milbo $
 */
// Check to ensure this file is included in Joomla!
defined ('_JEXEC') or die('Restricted access');
AdminUIHelper::startAdminArea ($this);

$styleDateCol = 'style="width:5%;min-width:110px"';

	/* Telion #1.11.3 add. cmsMTK reservDate. Получить даты резервов из базы */
	$db = JFactory::getDBO();
	$query = 'SELECT virtuemart_order_id, max(reserv_date) as reserv_date FROM wxa43_virtuemart_order_histories GROUP BY virtuemart_order_id';
	$db->setQuery($query);
	$reservDates = $db->loadObjectList('virtuemart_order_id');

?>

<form action="index.php?option=com_virtuemart&view=orders" method="post" name="adminForm" id="adminForm">
	<div id="header">
		<div id="filterbox">
			<table>
				<tr>
					<td align="left" style="min-width:420px;width:17%;">
						<?php echo $this->displayDefaultViewSearch ('COM_VIRTUEMART_ORDER_PRINT_NAME'); ?>

			<!-- Telion #1.11.4 change. cmsMTK reservDate. Скрыть количество заказов: Показано 1 -30 из 94 -->
                        <div id="resultscounter" style="display: none;"><?php echo $this->pagination->getResultsCounter (); ?></div>
			<!-- base
                        <div id="resultscounter"><?php echo $this->pagination->getResultsCounter (); ?></div> -->

					</td>
					<td align="left" style="min-width:190px;width:21%;">
						<?php echo vmText::_ ('COM_VIRTUEMART_ORDERSTATUS') . ':' . $this->lists['state_list']; ?>
                    </td>

					<!-- Telion #1.11.5 change. cmsMTK reservDate. Скрыть поле фильтрации -->
 					<td align="right" style="min-width:190px;width:25%;max-width:300px;border-style:solid none solid solid;border-width:1px;display: none;">
					<!-- base
 					<td align="right" style="min-width:190px;width:25%;max-width:300px;border-style:solid none solid solid;border-width:1px;"> -->

						<span style="text-align:left"><?php echo vmText::_ ('COM_VIRTUEMART_BULK_ORDERSTATUS') . $this->lists['bulk_state_list']; ?></span>
					</td>

					<!-- Telion #1.11.6 change. cmsMTK reservDate. Скрыть поле фильтрации -->
					<td align="left" style="min-width:330px;width:22%;border-style:solid solid solid none;border-width:1px; display: none;">
					<!-- base
					<td align="left" style="min-width:330px;width:22%;border-style:solid solid solid none;border-width:1px;"> -->

						<?php echo VmHTML::checkbox ('customer_notified', 0) . vmText::_ ('COM_VIRTUEMART_ORDER_LIST_NOTIFY'); ?> <br>
						<?php echo VmHTML::checkbox ('customer_send_comment', 1) . vmText::_ ('COM_VIRTUEMART_ORDER_HISTORY_INCLUDE_COMMENT'); ?>
						<?php echo VmHTML::checkbox ('update_lines', 1) . vmText::_ ('COM_VIRTUEMART_ORDER_UPDATE_LINESTATUS'); ?>
						<textarea class="element-hidden vm-order_comment vm-showable" name="comments" cols="5" rows="5"></textarea>
						<?php echo JHtml::_ ('link', '#', vmText::_ ('COM_VIRTUEMART_ADD_COMMENT'), array('class' => 'show_comment')); ?>
					</td>
					<td align="right" style="min-width:220px;width:14%;">
						<?php echo $this->lists['vendors'] ?>
					</td>
				</tr>
			</table>
		</div>

	</div>
<div style="text-align: left;">
	<table class="adminlist table table-striped" cellspacing="0" cellpadding="0">
		<thead>
		<tr>
			<th class="admin-checkbox"><input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this)"/></th>
			<th width="8%"><?php echo $this->sort ('order_number', 'COM_VIRTUEMART_ORDER_LIST_NUMBER')  ?> / <?php echo vmText::_('COM_VIRTUEMART_INVOICE') ?></th>
			<th width="26%"><?php echo $this->sort ('order_name', 'COM_VIRTUEMART_ORDER_PRINT_NAME').' / '; echo $this->sort ('order_email', 'COM_VIRTUEMART_EMAIL')  ?></th>

			<!-- Telion #13.10 -->
			<th>Комментарий</th>
			<th><?php echo $this->sort ('order_status', 'COM_VIRTUEMART_STATUS')  ?></th>

			<!-- Telion #1.11.7 add. cmsMTK reservDate. Заголовок столбца с датой резерва -->
			<th><?php echo $this->sort ('reserv_date', "Резерв")  ?></th>

			<th width="18%"><?php echo $this->sort ('payment_method', 'COM_VIRTUEMART_ORDER_PRINT_PAYMENT_LBL')  ?></th>
			<th width="18%"><?php echo $this->sort('shipment_method', 'COM_VIRTUEMART_ORDER_PRINT_SHIPMENT_LBL') ?></th>

			<!-- Telion #1.11.8 remove. cmsMTK reservDate. Скрыть столбец "Вид для печати"
			<th style="min-width:100px;width:5%;"><?php echo vmText::_ ('COM_VIRTUEMART_PRINT_VIEW'); ?></th>  -->

			<th class="admin-dates"><?php echo $this->sort ('created_on', 'COM_VIRTUEMART_ORDER_CDATE')  ?></th>

			<!-- Telion #1.11.9 remove. cmsMTK reservDate. Скрыть столбец "Дата последнего изменения"
			<th class="admin-dates"><?php echo $this->sort ('modified_on', 'COM_VIRTUEMART_ORDER_LIST_MDATE')  ?></th>  -->

			<!-- Telion #1.11.10 remove. cmsMTK reservDate. Скрыть столбец "Paid"
           		<th><?php echo $this->sort ('paid', 'COM_VM_ORDER_PAID')  ?></th> -->

			<!-- Telion #1.11.10 remove. cmsMTK reservDate. Скрыть столбец "Уведомлять покупателя"
			<th style="min-width:130px;width:5%;"><?php echo vmText::_ ('COM_VIRTUEMART_ORDER_LIST_NOTIFY'); ?></th>  -->

			<!-- Telion #1.11.10 remove. cmsMTK reservDate. Скрыть столбец "Итого"
			<th><?php echo $this->sort ('order_total', 'COM_VIRTUEMART_TOTAL')  ?></th> -->

			<!-- Telion #1.11.10 remove. cmsMTK reservDate. Скрыть столбец "ID заказа"
			<th><?php echo $this->sort ('virtuemart_order_id', 'COM_VIRTUEMART_ORDER_LIST_ID')  ?></th> -->

		</tr>
		</thead>
		<tbody>
		<?php
		if (count ($this->orderslist) > 0) {
			$i = 0;
			$k = 0;
			$keyword = vRequest::getCmd ('keyword');

			foreach ($this->orderslist as $key => $order) {
				$checked = JHtml::_ ('grid.id', $i, $order->virtuemart_order_id);

				//vmdebug('My order',$order);
				?>
			<tr class="row<?php echo $k . ' status-'. strtolower($order->order_status); ?>">
				<!-- Checkbox -->
				<td class="admin-checkbox"><?php echo $checked; ?></td>
				<!-- Order id -->
				<?php
				$link = 'index.php?option=com_virtuemart&view=orders&task=edit&virtuemart_order_id=' . $order->virtuemart_order_id;
				?>
				<td><?php echo JHtml::_ ('link', JRoute::_ ($link, FALSE), $order->order_number, array('title' => vmText::_ ('COM_VIRTUEMART_ORDER_EDIT_ORDER_NUMBER') . ' ' . $order->order_number));
				echo '<br>';
				echo implode('<br>',$order->invoiceNumbers); ?>
				</td>
				<td>
					<?php
					$orderName = html_entity_decode($order->order_name);
					if ($order->virtuemart_user_id) {
						$userlink = JRoute::_ ('index.php?option=com_virtuemart&view=user&task=edit&virtuemart_user_id[]=' . $order->virtuemart_user_id, FALSE);
						echo JHtml::_ ('link', $userlink, $orderName, array('title' => vmText::_ ('COM_VIRTUEMART_ORDER_EDIT_USER') . ' ' .  $orderName) );
					} else {
						echo $orderName;
					}
					echo '<br>';
					echo $order->order_email;
					?>
				</td>

				<!-- Telion #13.10 -->
				<td style="max-width: 500px;"> <?= $order->customer_note ?> </td>

				<td style="position:relative;<?php echo $colorStyle ?>">

				<!-- Telion #1.8.6.1 POINTS -->
				<?php
				switch ($order->order_status) {
				    case "U": echo vmText::_ ('COM_VIRTUEMART_ORDER_STATUS_CONFIRMED_BY_SHOPPER'); break;
				    case "C": echo vmText::_ ('COM_VIRTUEMART_ORDER_STATUS_CONFIRMED'); break;
				    case "P": echo vmText::_ ('COM_VIRTUEMART_ORDER_STATUS_PENDING'); break;
				    case "X": echo vmText::_ ('COM_VIRTUEMART_ORDER_STATUS_CANCELLED'); break;
				    case "R": echo vmText::_ ('COM_VIRTUEMART_ORDER_STATUS_REFUNDED'); break;
				    case "S": echo vmText::_ ('COM_VIRTUEMART_ORDER_STATUS_SHIPPED'); break;
				    case "F": echo vmText::_ ('COM_VIRTUEMART_ORDER_STATUS_COMPLETED'); break;
				    case "D": echo vmText::_ ('COM_VIRTUEMART_ORDER_STATUS_DENIED'); break;
				}
				 ?>

<!-- Telion -->
<!--					<?php

					if($order->paid < $order->order_total){
						$orderStati = $this->orderStatesUnpaid;
					} else {
						$orderStati = $this->orderstatuses;
					}
					echo JHtml::_ ('select.genericlist', $orderStati, "orders[" . $order->virtuemart_order_id . "][order_status]", 'class="orderstatus_select" style="width:180px;"', 'order_status_code', 'order_status_name', $order->order_status, 'order_status' . $i, TRUE); ?>
					<input type="hidden" name="orders[<?php echo $order->virtuemart_order_id; ?>][current_order_status]" value="<?php echo $order->order_status; ?>"/>
		<?php /*  	<input type="hidden" name="orders[<?php echo $order->virtuemart_order_id; ?>][coupon_code]" value="<?php echo $order->coupon_code; ?>"/>    //Coupon code??	*/?>
                    <br/>
					<textarea class="element-hidden vm-order_comment vm-showable" name="orders[<?php echo $order->virtuemart_order_id; ?>][comments]" cols="5" rows="5"></textarea>
					<?php echo JHtml::_ ('link', '#', vmText::_ ('COM_VIRTUEMART_ADD_COMMENT'), array('class' => 'show_comment')); ?> -->
				</td>
				<!-- Update -->
<!-- Telion -->
<!--				<td><?php echo VmHTML::checkbox ('orders[' . $order->virtuemart_order_id . '][customer_notified]', 0) . vmText::_ ('COM_VIRTUEMART_ORDER_LIST_NOTIFY'); ?>
					<br/>
					<?php echo VmHTML::checkbox ('orders[' . $order->virtuemart_order_id . '][customer_send_comment]', 1) . vmText::_ ('COM_VIRTUEMART_ORDER_HISTORY_INCLUDE_COMMENT'); ?>
					<br/>
					<?php echo VmHTML::checkbox ('orders[' . $order->virtuemart_order_id . '][update_lines]', 1) . vmText::_ ('COM_VIRTUEMART_ORDER_UPDATE_LINESTATUS'); ?>
				</td> -->

	<!-- Telion #1.11.11 add. cmsMTK reservDate. добавить ячейки в столбец "Дата резерва" -->
	<td>
		<?php 
		$rd = strtotime($reservDates[$order->virtuemart_order_id]->reserv_date);
		if (($rd > 0) and (($order->order_status == "U") or ($order->order_status == "C") or ($order->order_status == "P"))) {
			$yet = ceil(($rd - time()) / (60*60*24));
			if ($yet > 0) {
				$symbol = '+';
				$color = 'green';
			} else {
				$symbol = '';
				$color = 'red';
			}
			echo "<span style='color: ".$color.";'>до ".date("d.m.Y", $rd)." (".$symbol.$yet." дн)</span>"; 
		}
		?>
	</td>

				<!-- Payment method -->
				<td><?php echo $order->payment_method; ?></td>
				<!-- Shipment method -->
<!--				quorvia-->
				<?php $shipmentcolorStyle = '';
				if (!empty($this->shipmentColors[$order->virtuemart_shipmentmethod_id])) {
					$shipmentcolorStyle = "background-color:" . $this->shipmentColors[$order->virtuemart_shipmentmethod_id];
				}
				?>
				<td style="<?php echo $shipmentcolorStyle ?>">
				<?php echo $order->shipment_method; ?></td>

				<!-- Print view -->
				<?php
					$print_link = ''; $deliverynote_link = ''; $invoice_link = '';
					$this->createPrintLinks($order,$print_link,$deliverynote_link,$invoice_link);
				?>

				<!-- Telion
				<td><?php echo $print_link; echo $deliverynote_link; echo $invoice_link; ?></td> -->

				<!-- Order date -->
				<td><?php echo vmJsApi::date ($order->created_on, 'LC2', TRUE); ?></td>

				<!-- Last modified -->
				<!-- Telion
				<td><?php echo vmJsApi::date ($order->modified_on, 'LC2', TRUE); ?></td> -->

				<!-- Status -->
				<!-- Telion -->
		                <td style="display: none;"><?php echo $this->toggle($order->paid, $i,'toggle.paid'); ?></td>
				<!-- base
		                <td><?php echo $this->toggle($order->paid, $i,'toggle.paid'); ?></td> -->

				<?php
				$colorStyle = '';
				if (!empty($this->orderStatesColors[$order->order_status])) {
					$colorStyle = "background-color:" . $this->orderStatesColors[$order->order_status];
				}
				?>

				<!-- Total -->

				<!-- Telion -->
				<td style="display: none;"><?php echo $order->order_total; ?></td>
				<td style="display: none;"><?php echo JHtml::_ ('link', JRoute::_ ($link, FALSE), $order->virtuemart_order_id, array('title' => vmText::_ ('COM_VIRTUEMART_ORDER_EDIT_ORDER_ID') . ' ' . $order->virtuemart_order_id)); ?></td>
				<!-- base
				<td><?php echo $order->order_total; ?></td>
				<td><?php echo JHtml::_ ('link', JRoute::_ ($link, FALSE), $order->virtuemart_order_id, array('title' => vmText::_ ('COM_VIRTUEMART_ORDER_EDIT_ORDER_ID') . ' ' . $order->virtuemart_order_id)); ?></td> -->

			</tr>
				<?php
				$k = 1 - $k;
				$i++;
			}
		}
		?>
		</tbody>
		<tfoot>
		<tr>
			<td colspan="12">
				<?php echo $this->pagination->getListFooter (); ?>
			</td>
		</tr>
		</tfoot>
	</table>
</div>
	<!-- Hidden Fields -->
	<?php echo $this->addStandardHiddenToForm (); ?>
</form>
<?php AdminUIHelper::endAdminArea ();

$orderstatusForShopperEmail = VmConfig::get('email_os_s',array('U','C','S','R','X'));
if(!is_array($orderstatusForShopperEmail)) $orderstatusForShopperEmail = array($orderstatusForShopperEmail);
$jsOrderStatusShopperEmail = vmJsApi::safe_json_encode($orderstatusForShopperEmail);

$j = 'if (typeof Virtuemart === "undefined")
	var Virtuemart = {};
	Virtuemart.orderstatus = '.$jsOrderStatusShopperEmail.';
	jQuery(document).ready(function() {
		//Virtuemart.onReadyOrderItems();
		Virtuemart.onReadyOrderStatus()
	});';
vmJsApi::addJScript('onReadyOrders',$j);

vmJsApi::addJScript('/administrator/components/com_virtuemart/assets/js/orders.js',false,false);
?>