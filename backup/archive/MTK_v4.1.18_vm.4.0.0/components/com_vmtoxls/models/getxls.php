<?php defined('_JEXEC') or die('Restricted access');
/**
* @package		VMtoXLS
* @author		Denis Mukhin
* @website	 	http://e-commerce24.ru
* @email 		info@e-commerce24.ru
* @copyright	Copyright © 2016 Denis Mukhin. All rights reserved.
* @license		GNU/GPL http://www.gnu.org/licenses/gpl.html
* @version		1.7 (Custom)     
**/
jimport('joomla.application.application');
jimport('joomla.application.component.modelitem');
jimport('joomla.application.component.controller');
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
jimport('joomla.environment.uri');

require_once(JPATH_ROOT.DS.'components'.DS.'com_vmtoxls'.DS.'assets'.DS.'lib'.DS.'PHPExcel'.DS.'Classes'.DS.'PHPExcel.php');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'config.php');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'vobject.php');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'calculationh.php');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'vmmodel.php');
require_once(JPATH_ROOT.DS.'components'.DS.'com_vmtoxls'.DS.'helpers'.DS.'resize.php');

JFactory::getLanguage()->load('com_virtuemart_countries', JPATH_ADMINISTRATOR);

// Подключаем кеширование
$cacheMethod 	= PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
$cacheSettings 	= array('memoryCacheSize' => '64MB');
PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

VmConfig::loadConfig();

class VMtoXLSModelGetXLS extends JModelItem {

	function getItem($id = null) {
		$params = JComponentHelper::getParams('com_vmtoxls');
		$file = './media/com_vmtoxls/price.xlsx';
		$id = (!empty($id)) ? $id : (int) $this->getState('message.id');
		
		
		if (!isset($this->_item)) {
			switch ($id) {
				case 1:
					$this->_item = $this->getProducts();
					break;
				case 2:
				default:
					if ($params->get('dynamicPrice') == 0) {
						$this->_item = $this->getFile($file);
					}
					elseif ($params->get('dynamicPrice') == 1) {
						$this->_item = $this->getXLS($file);
					}
				break;
			}
		}
		return $this->_item;
	}
	
	/**
    * Метод для авто-заполнения состояния модели.
    * Заметка. Вызов метода getState в этом методе приведет к рекурсии.
    * @return  void
    */
    protected function populateState() {
		$app = JFactory::getApplication();
		$id = $app->input->getInt('id', 0);
        $this->setState('message.id', $id);

        parent::populateState();
    }
	
	// Обновляем прайс-лист
	function updPrice() {
		JFactory::getLanguage()->load('com_vmtoxls', JPATH_SITE);
		VmConfig::loadJLang('com_virtuemart_countries');
		$file = '../media/com_vmtoxls/price_' . date("d-m-Y") . '.xlsx';
		$this->getXLS($file);
		
		return true;
	}
	
	// Выгружаем каталог в файл
	function getFile($file) {
		$referer = JRequest::getString('HTTP_REFERER', null, 'SERVER');
		if (JFile::exists($file)) {
			// Контент-тип означающий скачивание
			header("Content-Type: application/octet-stream");
			// Размер в байтах
			header("Accept-Ranges: bytes");
			// Размер файла
			header("Content-Length: ".filesize($file)); 
			// Расположение скачиваемого файла
			header("Content-Disposition: attachment; filename=" . JFile::stripExt(JFile::getName($file)) . '_' . date("d-m-Y") . '.' . JFile::getExt($file));  
			readfile($file);
		}
		else {
				JFactory::getApplication()->redirect($referer, JText::_('COM_VMTOXLS_NOTICE_FILE_NOT_EXISTS'), error);
		}
		return true;
	}
	
	function getXLS($file) {
		$objPHPExcel 	= new PHPExcel();
		$app 			= JFactory::getApplication();
		$router 		= $app::getRouter("site");
		$router			->setMode(JROUTER_MODE_SEF);
		$params 		= JComponentHelper::getParams('com_vmtoxls');
		$products		= $this->getProducts();
		$cName			= $this->getCategory();
		$vendor			= $this->getVendor();
		
		$lett = 'C';
		if ($params->get('residueControl') == 1)
			$lett++;
		if ($params->get('inStock') == 1)
			$lett++;
		if ($params->get('addSKU') == 1)
			$lett++;
		if ($params->get('addMANUF') == 1)
			$lett++;
		if ($params->get('addPHOTO') == 1)
			$lett++;
		if ($params->get('addWEIGHT') == 1)
			$lett++;
		
		$rows = 5;
		$mRows = 5;
		if ($vendor['phone_1'] != null)
			$mRows++;
		if ($vendor['phone_2'] != null)
			$mRows++;
		if ($vendor['fax'] != null)
			$mRows++;
		$l = 'A';
		
		$i = 0;
		
		$objPHPExcel->setActiveSheetIndex(0);
		$active_sheet = $objPHPExcel->getActiveSheet();
		
		//Ориентация страницы и  размер листа
		$active_sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
		$active_sheet->getPageSetup()->SetPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		//Поля документа       
		$active_sheet->getPageMargins()->setTop(1);
		$active_sheet->getPageMargins()->setRight(0.75);
		$active_sheet->getPageMargins()->setLeft(0.75);
		$active_sheet->getPageMargins()->setBottom(1);
		
		//Название листа
		$active_sheet->setTitle(JText::_('COM_VMTOXLS_DEFAULT_PAGE_TITLE'));  
		
		// Футер
		$active_sheet->getHeaderFooter()->setOddFooter('&L&B'.JText::_('COM_VMTOXLS_HEADING_PRICE_DATE').date("d.m.Y").'&R'.JText::_('COM_VMTOXLS_HEADING_PRICE_PAGE').'&P'.JText::_('COM_VMTOXLS_HEADING_PRICE_PAGE_FROM').'.&N');
		
		//Настройки шрифта
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
		$objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
		
		// Стили прайс-листа excel
		$style_wrap = array(
			'borders'=>array(
				'allborders'=>array(
					'style'=>PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb'=>'696969')
				)
			)
		);
		
		$style_header = array(
			//Шрифт
			'font'=>array(
				'bold' => true,
				'name' => 'Arial',
				'size' => 20
			),
			//Выравнивание
			'alignment' => array(
				'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
			),
			//заполнение цветом
			'fill' => array(
				'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
				'color'=>array(
					'rgb' => 'C0C0C0'
				),
			),
		);
		
		$styleCenterCenter = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			),
		);
		
		$styleLeftCenter = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			),
		);
		
		$styleRightCenter = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			),
		);
		
		$style_vendor = array(
			//Шрифт
			'font'=>array(
				'bold' => true,
			),
		);
		
		$style_addr = array(
			//Выравнивание
			'alignment' => array(
				'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_LEFT,
				'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_TOP,
			),
		);
		
		$style_url = array(
			// Шрифт
			'font'=>array(
				'underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE,
				'color' => array(
					'rgb' => '0000FF'
				),
			),
		);
		
		$style_t_header = array(
			//Шрифт
			'font'=>array(
				'bold' => true,
			),
			//Выравнивание
			'alignment' => array(
				'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
			),
			//заполнение цветом
			'fill' => array(
				'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
				'color'=>array(
					'rgb' => 'C0C0C0'
				),
			),
		);
		
		//Выводим название магазина
		$active_sheet->mergeCells('A1:'.$lett.'1');
		$active_sheet->getRowDimension('1')->setRowHeight(40);
		$active_sheet->setCellValue('A1', $vendor['vendor_store_name']);
		$active_sheet->getStyle('A1:'.$lett.'1')->applyFromArray($style_header);
		
		
		// Выводим логотип и контактную информацию
		if ($lett <= 'C')
			$colL = 'B';
		else
			$colL = 'C';
		$active_sheet->mergeCells('A2:'.$colL.$mRows);
		if ($app->isAdmin()) {
			$l_file = '../' . $vendor['file_url'];
		}
		else {
			$l_file = './' . $vendor['file_url'];
		}
		if (JFile::exists($l_file)) {
			$logo = new PHPExcel_Worksheet_Drawing();
			$logo->setPath($l_file);
			$logo->setCoordinates('A2');             

			/* Telion #2.7.1 */
			$logo->setOffsetX(100);
			$logo->setOffsetY(100);    
			$logo->setWidth(45);
			$logo->setHeight(45);

			$logo->setWorksheet($active_sheet);
		}
		
		$colL++;
		
		$active_sheet->mergeCells($colL.'2:'.$lett.'2');
		$active_sheet->setCellValue($colL.'2', $vendor['vendor_name']);
		$active_sheet->getStyle($colL.'2')->applyFromArray($style_vendor);
		$active_sheet->mergeCells($colL.'3:'.$lett.'3');

		/* Telion #2.7.2 */
		$active_sheet->setCellValue($colL.'3', ($vendor['country'] . $vendor['city'] . ', ' . $vendor['address_1']));
		/* base 
		$active_sheet->setCellValue($colL.'3', ($vendor['zip'] . ', ' . $vendor['country'] . ', ' . $vendor['state_name'] . ', ' . JText::_('COM_VMTOXLS_HEADING_PRICE_PRE_CITY') . $vendor['city'] . ', ' . $vendor['address_1'])); */

		$active_sheet->getStyle($colL.'3')->getAlignment()->setWrapText(true);

		/* Telion #2.7.3 remove */
//		$active_sheet->getRowDimension(3)->setRowHeight(0);

		$active_sheet->getStyle($colL.'3')->applyFromArray($style_addr);
		
		$active_sheet->mergeCells($colL.'4:'.$lett.'4');
		$active_sheet->setCellValue($colL.'4', $vendor['vendor_url']);
		$active_sheet->getCell($colL.'4')->getHyperlink()->setUrl($vendor['vendor_url']);
		$active_sheet->getStyle($colL.'4')->applyFromArray($style_url);
		
		$active_sheet->mergeCells($colL.'5:'.$lett.'5');
		$active_sheet->setCellValue($colL.'5', $vendor['email']);
		$active_sheet->getCell($colL.'5')->getHyperlink()->setUrl('mailto:'.$vendor['email']);
		$active_sheet->getStyle($colL.'5')->applyFromArray($style_url);
		
		
		if ($vendor['phone_1'] != null) {
			$rows++;
			$active_sheet->mergeCells($colL.$rows.':'.$lett.$rows);
			$active_sheet->setCellValue($colL.$rows, (JText::_('COM_VMTOXLS_HEADING_PRICE_TEL') . $vendor['phone_1']));
		}
		
		if ($vendor['phone_2'] != null) {
			$rows++;
			$active_sheet->mergeCells($colL.$rows.':'.$lett.$rows);
			$active_sheet->setCellValue($colL.$rows, (JText::_('COM_VMTOXLS_HEADING_PRICE_TEL') . $vendor['phone_2']));
		}
		
		if ($vendor['fax'] != null) {
			$rows++;
			$active_sheet->mergeCells($colL.$rows.':'.$lett.$rows);
			$active_sheet->setCellValue($colL.$rows, (JText::_('COM_VMTOXLS_HEADING_PRICE_FAX') . $vendor['fax']));
		}
		
		$rows++;
		
		$active_sheet->mergeCells('A'.$rows.':'.$lett.$rows);
		$active_sheet->setCellValue('A'.$rows, (JText::_('COM_VMTOXLS_HEADING_PRICE_DATE') . date("d.m.Y")));
		$active_sheet->getRowDimension($rows)->setRowHeight(20);
		$active_sheet->getStyle('A'.$rows)->applyFromArray($style_t_header);
		
		$rows++;
		
		$active_sheet->getRowDimension($rows)->setRowHeight(20);
		
		$active_sheet->setCellValue($l.$rows, JText::_('COM_VMTOXLS_THEADER_PRODUCT_NUMBER'));
		$active_sheet->getColumnDimension($l)->setAutoSize(true);
		$active_sheet->getStyle($l.$rows)->applyFromArray($style_t_header);
		$l++;
		
		if ($params->get('addPHOTO') == 1) {
			$active_sheet->setCellValue($l.$rows, JText::_('COM_VMTOXLS_THEADER_PRODUCT_PHOTO'));
			$active_sheet->getStyle($l.$rows)->applyFromArray($style_t_header);
			$l++;
		}
		
		if ($params->get('addSKU') == 1) {
			$active_sheet->setCellValue($l.$rows, JText::_('COM_VMTOXLS_THEADER_PRODUCT_SKU'));
			$active_sheet->getColumnDimension($l)->setAutoSize(true);
			$active_sheet->getStyle($l.$rows)->applyFromArray($style_t_header);
			$l++;
		}

		$active_sheet->setCellValue($l.$rows, JText::_('COM_VMTOXLS_THEADER_PRODUCT_NAME'));
		$active_sheet->getColumnDimension($l)->setAutoSize(true);
		$active_sheet->getStyle($l.$rows)->applyFromArray($style_t_header);
		$l++;
		
		if ($params->get('addMANUF') == 1) {
			$active_sheet->setCellValue($l.$rows, JText::_('COM_VMTOXLS_THEADER_PRODUCT_MANUF'));
			$active_sheet->getColumnDimension($l)->setAutoSize(true);
			$active_sheet->getStyle($l.$rows)->applyFromArray($style_t_header);
			$l++;
		}
		
		if ($params->get('addWEIGHT') == 1) {
			$active_sheet->setCellValue($l.$rows, JText::_('COM_VMTOXLS_THEADER_PRODUCT_WEIGHT'));
			$active_sheet->getColumnDimension($l)->setAutoSize(true);
			$active_sheet->getStyle($l.$rows)->applyFromArray($style_t_header);
			$l++;
		}
		
		$active_sheet->setCellValue($l.$rows, JText::_('COM_VMTOXLS_THEADER_PRODUCT_PRICE'));
		$active_sheet->getColumnDimension($l)->setAutoSize(true);
		$active_sheet->getStyle($l.$rows)->applyFromArray($style_t_header);
		$l++;
		
		if ($params->get('inStock') == 1) {
			$active_sheet->setCellValue($l.$rows, JText::_('COM_VMTOXLS_THEADER_PRODUCT_IN_STOCK'));
			$active_sheet->getColumnDimension($l)->setAutoSize(true);
			$active_sheet->getStyle($l.$rows)->applyFromArray($style_t_header);
			$l++;
		}
		if ($params->get('residueControl') == 1) {
			$active_sheet->setCellValue($l.$rows, JText::_('COM_VMTOXLS_THEADER_PRODUCT_AVAILABILITY'));
			$active_sheet->getColumnDimension($l)->setAutoSize(true);
			$active_sheet->getStyle($l.$rows)->applyFromArray($style_t_header);
		}
		
		// Выводим товары по категориям
		$rows++;
		foreach($cName as $cat => &$c) {
			$active_sheet->mergeCells('A'.$rows.':'.$lett.$rows);
			$active_sheet->setCellValue('A'.$rows, $c['category_name']);
			$active_sheet->getRowDimension($rows)->setRowHeight(20);
			$active_sheet->getStyle('A'.$rows)->applyFromArray($style_t_header);
			$rows++;
			foreach($products as $product) {
				$l = 'A';
				if (($product->category_title == $c['category_name']) and ($product->price != 0)) {
					$i++;
					
					$active_sheet->setCellValue($l.$rows, $i);
					$l++;
					
					if ($params->get('addPHOTO') == 1) {
						$p_file = JPATH_ROOT . DS . $product->photo_url;
						
						// сжимаем картинку
						if (!JFile::exists(JPATH_ROOT . DS . 'images/vmtoxls' . DS . JFile::getName($p_file))) {
							ComVMtoXLSHelper::picResize($product->photo_url);
						}
						
						$pr_pic = new PHPExcel_Worksheet_Drawing();
						$pr_pic->setPath(JPATH_ROOT . DS . 'images/vmtoxls' . DS . JFile::getName($p_file));
						$pr_pic->setCoordinates($l.$rows);   
						$pr_pic->setResizeProportional(true);;
						$pr_pic->setHeight(60);							
						$pr_pic->setOffsetX(5);
						$pr_pic->setOffsetY(5);    
						$pr_pic->setWorksheet($active_sheet);
						$active_sheet->getCell($l.$rows)->getHyperlink()->setUrl(JURI::ROOT() . $product->photo_url);
						$active_sheet->getRowDimension($rows)->setRowHeight(52.5);
						$active_sheet->getColumnDimension($l)->setWidth(10);
						$active_sheet->getStyle($l.$rows)->applyFromArray($styleLeftCenter);
						$l++;
					}
					
					if ($params->get('addSKU') == 1) {
						$active_sheet->setCellValue($l.$rows, $product->product_sku);

						/* Telion #2.7.4 */
						$active_sheet->getColumnDimension($l)->setAutoSize(false);
						$active_sheet->getColumnDimension($l)->setWidth(16);
						/* base
						$active_sheet->getColumnDimension($l)->setAutoSize(true); */

						$l++;
					}
					
					if ($params->get('addURL') == 1) {
						if ($app->isAdmin()) {
							$uri = $router->build(JURI::root() . JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id, true));
							$path = str_replace(array('/administrator', '/index.php'), array('', ''), $uri->getPath());
							$uri->setPath($path);
							$url = JURI::root() . $uri->toString(array('path', 'query', 'fragment'));
						}
						else {
							$uri = JURI::root() . JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id, true);
							$url = str_replace(array('//index.php',), array(''), $uri);
						}
						$active_sheet->setCellValue($l.$rows, htmlspecialchars_decode($product->product_name), ENT_QUOTES);
						$active_sheet->getCell($l.$rows)->getHyperlink()->setUrl($url);
						$active_sheet->getCell($l.$rows)->getHyperlink()->setTooltip(JText::_('COM_VMTOXLS_NAVIGATE_TO_PRODUCT_PAGE'));

						/* Telion #2.7.5 */
						$active_sheet->getColumnDimension($l)->setAutoSize(false);
						$active_sheet->getColumnDimension($l)->setWidth(90);
						/* base
						$active_sheet->getColumnDimension($l)->setAutoSize(true); */

						$active_sheet->getStyle($l.$rows)->applyFromArray($style_url);
						$l++;
					}
					else {
						$active_sheet->setCellValue($l.$rows, htmlspecialchars_decode($product->product_name), ENT_QUOTES);
						$active_sheet->getColumnDimension($l)->setAutoSize(true);
						$l++;
					}
					
					if ($params->get('addMANUF') == 1) {
						
						/* Telion #2.7.6 */
						$productManuf = ($product->mf_name == "") ? " " : $product->mf_name;
						$active_sheet->setCellValue($l.$rows, $productManuf);
						/* base
						$active_sheet->setCellValue($l.$rows, $product->mf_name); */

						$active_sheet->getColumnDimension($l)->setAutoSize(true);
						$l++;
					}
					
					if ($params->get('addWEIGHT') == 1) {
						if(isset($product->product_weight) && $product->product_weight != null) {
							$active_sheet->setCellValue($l.$rows, round($product->product_weight, 2) . ' ' . $product->product_weight_uom);
						}
						else {
							$active_sheet->setCellValue($l.$rows, JText::_('COM_VMTOXLS_PARAM_WEIGHT_UNKNOWN'));
						}
						$active_sheet->getColumnDimension($l)->setAutoSize(true);
						$l++;
					}
					
					$active_sheet->setCellValue($l.$rows, $product->price);
					$active_sheet->getStyle($l.$rows)->getNumberFormat()->setFormatCode('_-* #,##0.00\ [$₽-419]_-');
					$active_sheet->getColumnDimension($l)->setAutoSize(true);
					$l++;
					
					if ($params->get('inStock') == 1) {
						$active_sheet->setCellValue($l.$rows, ($product->product_in_stock));
						$active_sheet->getStyle($l.$rows)->getNumberFormat()->setFormatCode('_-* #0\ [$'.JText::_('COM_VMTOXLS_PRODUCT_IN_STOCK_PC').']_-');
						$active_sheet->getColumnDimension($l)->setAutoSize(true);
						$l++;
					}
					
					if ($params->get('residueControl') == 1) {
						$active_sheet->setCellValue($l.$rows, $product->availability);
						$active_sheet->getColumnDimension($l)->setAutoSize(true);
					}
					$rows++;
				}
			}
		}
		
		$active_sheet->getStyle('A1:'.$l.$rows)->applyFromArray($style_wrap);
		
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		
		if (($params->get('dynamicPrice') == 1) and ($app->isSite())) {
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header("Content-Disposition:attachment;filename=" . JFile::stripExt(JFile::getName($file)) . '_' . date("d-m-Y") . '.' . JFile::getExt($file));
			header('Cache-Control: max-age=0');
			$objWriter->setPreCalculateFormulas(false);
			ob_end_clean();
			$objWriter->save('php://output');
			exit;
		}
		elseif (($params->get('dynamicPrice') == 0) or ($app->isAdmin())) { 
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			ob_end_clean();
			$objWriter->save($file);
		}
		
		return true;
	}
	
	function getProducts() {
		// Обновляем содержимое таблицы
		$app 	= 	JFactory::getApplication();
		$params = JComponentHelper::getParams('com_vmtoxls');
		// Подключаем калькулятор цен и валют
		$model 		= 	new VirtueMartModelProduct();
		$calculator = 	calculationHelper::getInstance();

		// Получаем список товаров
		$db 	= 	JFactory::getDBO();
		$query 	= 	$db->getQuery(true);
		$app 	= 	JFactory::getApplication();
		
		$query	->	select($this->getState('list.select', 'pl.virtuemart_product_id, pc.virtuemart_category_id, pl.product_name, p.product_parent_id'
											. ', p.product_sku, p.product_in_stock, p.product_ordered, pp.product_price, pp.product_currency, pp.product_tax_id'
											. ', pp.product_discount_id, pp.product_price, pp.product_override_price, pp.override'
											. ', pm.virtuemart_manufacturer_id, m.mf_name, me.virtuemart_media_id, p.product_weight, p.product_weight_uom'));									
		$query	->	from($db->quoteName('#__virtuemart_products_' . VmConfig::$vmlang, 'pl'));
		$query	->	join('INNER', $db->quoteName('#__virtuemart_products', 'p') . ' ON (' . $db->quoteName('p.virtuemart_product_id') . ' = ' . $db->quoteName('pl.virtuemart_product_id') . ')');
		$query	->	join('LEFT', $db->quoteName('#__virtuemart_product_prices', 'pp') . ' ON (' . $db->quoteName('pp.virtuemart_product_id') . ' = ' . $db->quoteName('pl.virtuemart_product_id') . ')');
		$query	->	join('LEFT', $db->quoteName('#__virtuemart_product_categories', 'pc') . ' ON (' . $db->quoteName('pl.virtuemart_product_id') . ' = ' . $db->quoteName('pc.virtuemart_product_id') . ')');
		$query	->	join('LEFT', $db->quoteName('#__virtuemart_product_manufacturers', 'pm') . ' ON (' . $db->quoteName('pl.virtuemart_product_id') . ' = ' . $db->quoteName('pm.virtuemart_product_id') . ')');
		$query	->	join('LEFT', $db->quoteName('#__virtuemart_manufacturers_' . VmConfig::$vmlang, 'm') . ' ON (' . $db->quoteName('pm.virtuemart_manufacturer_id') . ' = ' . $db->quoteName('m.virtuemart_manufacturer_id') . ')');
		$query	->	join('LEFT', $db->quoteName('#__virtuemart_product_medias', 'me') . ' ON (' . $db->quoteName('pl.virtuemart_product_id') . ' = ' . $db->quoteName('me.virtuemart_product_id') . ')');
		
		// Присоединяем таблицу категорий.
		$query		->	select($db->quoteName('pcn.category_name', 'category_title'));
		$query		->	join('LEFT', $db->quoteName('#__virtuemart_categories_' . VmConfig::$vmlang, 'pcn'). 'ON (' . $db->quoteName('pcn.virtuemart_category_id') . ' = ' . $db->quoteName('pc.virtuemart_category_id') . ')');

		if ($params->get('onlyAvailable') == 1) {
			$query -> where($db->quoteName('p.product_in_stock') . ' > 0');
			$query -> where($db->quoteName('p.product_in_stock') . ' > ' . $db->quoteName('p.product_ordered'));
		}
		
		if ($params->get('loadOnOrder') == 0) {
			$query -> where(($db->quoteName('p.product_availability') . ' NOT LIKE ' . $db->quote('%on-order%')) . ' OR ' . ($db->quoteName('p.product_availability') . ' IS NULL'));
		}
		
		$query	->	where($db->quoteName('p.published') . ' = 1');
		
		$query	->	order($db->quoteName('pl.product_name') . ' ASC');
		$query	->	group($db->quoteName('pl.virtuemart_product_id'));
		$query	->	group($db->quoteName('pc.virtuemart_category_id'));
		
		$db		->	setQuery($query);
		
		$products = $db->loadObjectList();
		
		foreach ($products as $product) {
			$parentID = $product->virtuemart_product_id;
			// Получаем цену
			$product->categories = array($product->virtuemart_category_id);
			$model->getRawProductPrices($product, 0, array(1), 1);
			$prices = $calculator->getProductPrices($product);
			
			if (isset($prices['product_override_price']) && $prices['product_override_price'] != 0) {
				$product->price = round($prices['product_override_price'], 2);
			}
			else {
				$product->price = round($prices['salesPrice'], 2);
			}
			
			// Получаем валюту
			$pr_c = $product->product_currency;
			$product->currency = $this->getCurrencySymbol($pr_c);
			
			// Получаем доступность
			if ($params->get('residueControl') == 1) {
				$pr_id = $product->virtuemart_product_id;
				$product->availability = $this->getAvailability($pr_id);
			}
			
			// Получаем родительскую категорию
			if ($product->category_title == null) {
				$pCat = $this->getParentCat($parentID);
				if ($pCat != null) {
					$product->category_title = $pCat;
				}
			}
			
			// Получаем производителя родительского товара
			if ($params->get('addMANUF') == 1) {
				if (($product->virtuemart_manufacturer_id == null) and ($product->product_parent_id != null)) {
					$product->mf_name = $this->getPManuf($product->product_parent_id);
				}
			}
			// Получаем фото товаров
			if ($params->get('addPHOTO') == 1) {
				if ($product->virtuemart_media_id != null) {
					$product->photo_url = $this->getPhotoUrl($product->virtuemart_media_id);
				}
				else {
					$product->photo_url = 'components/com_virtuemart/assets/images/vmgeneral/noimage.gif';
				}
			}			
			
			// Обрабатываем вес
			
			if ($params->get('addWEIGHT') == 1) {
				if(isset($product->product_weight_uom)) {
					switch ($product->product_weight_uom) {
						case "MG":
							$product->product_weight_uom = JText::_('COM_VMTOXLS_PARAM_WEIGHT_UOM_MG');
							break;
						case "G":
							$product->product_weight_uom = JText::_('COM_VMTOXLS_PARAM_WEIGHT_UOM_G');
							break;
						case "KG":
							$product->product_weight_uom = JText::_('COM_VMTOXLS_PARAM_WEIGHT_UOM_KG');
							break;
						case "LB":
							$product->product_weight_uom = JText::_('COM_VMTOXLS_PARAM_WEIGHT_UOM_LB');
							break;
						case "OZ":
							$product->product_weight_uom = JText::_('COM_VMTOXLS_PARAM_WEIGHT_UOM_OZ');
							break;
					}
				}
			}
		}
		return $products;
	}
	
	// Получаем названия категорий
	function getCategory() {
		$db		= 	$this->getDBO();
		$query	=	$db->getQuery(true);
		$query	->	select(array('vcl.virtuemart_category_id, vcl.category_name'));
		$query	->	from($db->quoteName('#__virtuemart_categories_' . VmConfig::$vmlang, 'vcl'));
		$query	->	order($db->quoteName('vcl.category_name') . ' ASC');
		$db		->	setQuery($query);
		$cName	=	$db->loadAssocList();

		return $cName;
	}
	
	// Получаем название магазина и название компании из настроек VirtueMart
	function getVendor() {
		$lang = JFactory::getLanguage();
		$prefix="COM_VIRTUEMART_COUNTRY_";
		
		$db		= 	$this->getDBO();
		$query	=	$db->getQuery(true);
		$query	->	select(array('vmu.virtuemart_user_id, v.vendor_name, vl.vendor_store_name, vl.vendor_url, vme.virtuemart_media_id'
								. ', m.file_url, u.email, vui.zip, vui.virtuemart_country_id, vc.country_name, vc.country_3_code'
								. ', vui.virtuemart_state_id, vs.state_name, vui.city, vui.address_1, vui.phone_1, vui.phone_2, vui.fax'));
		$query	->	from($db->quoteName('#__virtuemart_vmusers', 'vmu'));
		$query	->	join('LEFT', $db->quoteName('#__virtuemart_vendors_' . VmConfig::$vmlang, 'vl') . ' ON (' . $db->quoteName('vl.virtuemart_vendor_id') . ' = ' . $db->quoteName('vmu.virtuemart_vendor_id') . ')');
		$query	->	join('LEFT', $db->quoteName('#__virtuemart_vendors', 'v') . ' ON (' . $db->quoteName('v.virtuemart_vendor_id') . ' = ' . $db->quoteName('vmu.virtuemart_vendor_id') . ')');
		$query	->	join('LEFT', $db->quoteName('#__virtuemart_vendor_medias', 'vme') . ' ON (' . $db->quoteName('vme.virtuemart_vendor_id') . ' = ' . $db->quoteName('vmu.virtuemart_vendor_id') . ')');
		$query	->	join('LEFT', $db->quoteName('#__users', 'u') . ' ON (' . $db->quoteName('u.id') . ' = ' . $db->quoteName('vmu.virtuemart_user_id') . ')');
		$query	->	join('LEFT', $db->quoteName('#__virtuemart_medias', 'm') . ' ON (' . $db->quoteName('m.virtuemart_media_id') . ' = ' . $db->quoteName('vme.virtuemart_media_id') . ')');
		$query	->	join('LEFT', $db->quoteName('#__virtuemart_userinfos', 'vui') . ' ON (' . $db->quoteName('vui.virtuemart_user_id') . ' = ' . $db->quoteName('vmu.virtuemart_user_id') . ')');
		$query	->	join('LEFT', $db->quoteName('#__virtuemart_countries', 'vc') . ' ON (' . $db->quoteName('vc.virtuemart_country_id') . ' = ' . $db->quoteName('vui.virtuemart_country_id') . ')');
		$query	->	join('LEFT', $db->quoteName('#__virtuemart_states', 'vs') . ' ON (' . $db->quoteName('vs.virtuemart_state_id') . ' = ' . $db->quoteName('vui.virtuemart_state_id') . ')');
		$query	->	where($db->quoteName('vmu.user_is_vendor') . ' = 1');
		$db		->	setQuery($query);
		$vendor	=	$db->loadAssoc();
		
		foreach ($vendor as &$v) {
			$vendor['country'] = vmText::_($prefix.$vendor['country_3_code']);
		}
		
		return $vendor;
	}
	
	// Наследуем родительскую категорию
	function getParentCat($parentID) {
		$db		=	$this->getDBO();
		$query	=	$db->getQuery(true);
		$query	->	select('cl.category_name');
		$query	->	from($db->quoteName('#__virtuemart_categories_' . VmConfig::$vmlang, 'cl'));
		$query	->	join('LEFT', $db->quoteName('#__virtuemart_product_categories', 'pc') . ' ON (' . $db->quoteName('pc.virtuemart_category_id') . ' = ' . $db->quoteName('cl.virtuemart_category_id') . ')');
		$query	->	where($db->quoteName('pc.virtuemart_product_id') . ' = ' . $parentID);
		$db		->	setQuery($query);
		$pCat	= 	$db->loadResult();
		
		return $pCat;
	}
	
	// Получаем производителя родительского товара
	function getPManuf($parentID) {
		$db		=	$this->getDBO();
		$query	=	$db->getQuery(true);
		$query	->	select('m.mf_name');
		$query	->	from($db->quoteName('#__virtuemart_manufacturers_' . VmConfig::$vmlang, 'm'));
		$query	->	join('LEFT', $db->quoteName('#__virtuemart_product_manufacturers', 'pm') . ' ON (' . $db->quoteName('pm.virtuemart_manufacturer_id') . ' = ' . $db->quoteName('m.virtuemart_manufacturer_id') . ')');
		$query	->	where($db->quoteName('pm.virtuemart_product_id') . ' = ' . $parentID);		
		$db		->	setQuery($query);
		$pManuf	= 	$db->loadResult();
		
		return $pManuf;
	}
	
	// Получаем валюту
	function getCurrencySymbol($pr_c) {
		$db 	= 	JFactory::getDBO();
		$query 	= 	$db->getQuery(true);
		$query	->	select($db->quoteName('vmc.currency_symbol'));
		$query	->	from($db->quoteName('#__virtuemart_currencies', 'vmc'));
		$query	->	join('LEFT', $db->quoteName('#__virtuemart_vendors', 'vmv') . 'ON (' . $db->quoteName('vmc.virtuemart_currency_id') . ' = ' . $db->quoteName('vmv.vendor_currency') .')');
		$query	->	where($db->quoteName('vmv.virtuemart_vendor_id') . ' = 1');
		$db		->	setQuery($query);
		$curr	=	$db->loadResult();

		return $curr;
	}
	
	// Получаем сведения о наличии товара
	function getAvailability($pr_id) {
		$stockhandle = VmConfig::get('stockhandle');
		$availability = VmConfig::get('rised_availability');
		$params = JComponentHelper::getParams('com_vmtoxls');
		if ($params->get('residueControl') == 1) {
			$db 	= 	JFactory::getDBO();
			$query 	= 	$db->getQuery(true);
			$query	->	select(array('p.product_availability', 'p.product_available_date', 'p.product_in_stock', 'p.product_ordered'));
			$query	->	from($db->quoteName('#__virtuemart_products', 'p'));
			$query	->	where($db->quoteName('p.virtuemart_product_id') . ' = ' . $pr_id);
			$db		->	setQuery($query);
			$avail	=	$db->loadAssoc();
			
			if (($avail['product_in_stock'] - $avail['product_ordered']) < 1) {
				if ((date("Y-m-d", strtotime($avail['product_available_date'])) > date("Y-m-d")) and ($avail['product_availability'] != null)) {
					$prAvailability = JText::_('COM_VMTOXLS_AVAIL_PRODUCT_EXPECTED') . date("d.m.Y", strtotime($avail['product_available_date']));
				}
				elseif (($stockhandle == 'risetime') and ($availability != null) and ($avail['product_availability'] == null)) {
					$prAvailability = $availability;
				}
				elseif (($stockhandle == 'risetime') and ($availability == null) and ($avail['product_availability'] == null)) {
					$prAvailability = JText::_('COM_VMTOXLS_AVAIL_PRODUCT_NO_DATA');
				}
				elseif (($stockhandle == 'disableadd') and ($avail['product_availability'] == null)) {
					$prAvailability = JText::_('COM_VMTOXLS_AVAIL_PRODUCT_NOT_AVAILABLE');
				}
				elseif (($stockhandle == 'none') and ($avail['product_availability'] == null)) {
					$prAvailability = JText::_('COM_VMTOXLS_AVAIL_PRODUCT_DETAILS_IN_STORE');
				}
				elseif ($stockhandle == 'disableit') {
					$prAvailability = JText::_('COM_VMTOXLS_AVAIL_PRODUCT_NOT_AVAILABLE');
				}
				elseif ($avail['product_availability'] != null) {
					$prAvailability = $avail['product_availability'];
				}
			}
			elseif (($avail['product_in_stock'] - $avail['product_ordered']) >= 1) {
				$prAvailability = JText::_('COM_VMTOXLS_AVAIL_PRODUCT_IN_STOCK');
			}
			
			switch ($prAvailability) {
				case "on-order.gif":
					$prAvailability = JText::_('COM_VMTOXLS_AVAIL_PRODUCT_ON_ORDER');
					break;
				case "not_available.gif":
					$prAvailability = JText::_('COM_VMTOXLS_AVAIL_PRODUCT_NOT_AVAILABLE');
					break;
				case "7d.gif":
					$prAvailability = JText::_('COM_VMTOXLS_AVAIL_PRODUCT_EXPECTED_PRE') . JText::_('COM_VMTOXLS_AVAIL_PRODUCT_EXPECTED_7D');
					break;
				case "48h.gif":
					$prAvailability = JText::_('COM_VMTOXLS_AVAIL_PRODUCT_EXPECTED_PRE') . JText::_('COM_VMTOXLS_AVAIL_PRODUCT_EXPECTED_48H');
					break;
				case "3-5d.gif":
					$prAvailability = JText::_('COM_VMTOXLS_AVAIL_PRODUCT_EXPECTED_PRE') . JText::_('COM_VMTOXLS_AVAIL_PRODUCT_EXPECTED_3_5D');
					break;
				case "24h.gif":
					$prAvailability = JText::_('COM_VMTOXLS_AVAIL_PRODUCT_EXPECTED_PRE') . JText::_('COM_VMTOXLS_AVAIL_PRODUCT_EXPECTED_24H');
					break;
				case "2-3d.gif":
					$prAvailability = JText::_('COM_VMTOXLS_AVAIL_PRODUCT_EXPECTED_PRE') . JText::_('COM_VMTOXLS_AVAIL_PRODUCT_EXPECTED_2_3D');
					break;
				case "14d.gif":
					$prAvailability = JText::_('COM_VMTOXLS_AVAIL_PRODUCT_EXPECTED_PRE') . JText::_('COM_VMTOXLS_AVAIL_PRODUCT_EXPECTED_14D');
					break;
				case "1-4w.gif":
					$prAvailability = JText::_('COM_VMTOXLS_AVAIL_PRODUCT_EXPECTED_PRE') . JText::_('COM_VMTOXLS_AVAIL_PRODUCT_EXPECTED_1_4W');
					break;
				case "1-2m.gif":
					$prAvailability = JText::_('COM_VMTOXLS_AVAIL_PRODUCT_EXPECTED_PRE') . JText::_('COM_VMTOXLS_AVAIL_PRODUCT_EXPECTED_1_2M');
					break;
				case null:
					$prAvailability = JText::_('COM_VMTOXLS_AVAIL_PRODUCT_NO_DATA');
					break;
			}
		}
		
		return $prAvailability;
	}
	
	// Получаем ссылку на миниатюру товара
	function getPhotoUrl($meID) {
		$db 	= 	JFactory::getDBO();
		$query 	= 	$db->getQuery(true);
		$query 	->	select($db->quoteName('vmm.file_url'));
		$query	->	from($db->quoteName('#__virtuemart_medias', 'vmm'));
		$query	->	where($db->quoteName('vmm.virtuemart_media_id') . ' = ' . $meID);	
		$query	->	where($db->quoteName('vmm.file_type') . ' LIKE ' . $db->quote('product'));
		$db		->	setQuery($query);
		$ph_url	=	$db->loadResult();
		if ($ph_url == null)
			$ph_url = 'components/com_virtuemart/assets/images/vmgeneral/noimage.gif';
		
		return $ph_url;		
	}
}