<?php
/**
 *
 * Layout for the order email
 * shows the chosen adresses of the shopper
 * taken from the stored order
 *
 * @package	VirtueMart
 * @subpackage Order
 * @author Max Milbers,   Valerie Isaksen
 *
 * @link https://virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 *
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

	/* Telion */
	$db = JFactory::getDBO();
	$db->setQuery('SELECT reserv_date FROM wxa43_virtuemart_order_histories WHERE virtuemart_order_id='.$this->orderDetails['details']['BT']->virtuemart_order_id.' ORDER BY virtuemart_order_history_id ASC LIMIT 1');
	$reservDate = $db->loadColumn();

	$shipmentMethods = array(
		'3' => 'Самовывоз',
		'9' => 'Доставка курьером по Тюмени',
		'10' => 'Экспресс-доставка по Тюмени',
		'11' => 'ТК "СДЕК"',
		'12' => 'ТК "Деловые линии"',
		'13' => 'Прочие ТК',
		'14' => 'Авиа доставка'
	);

	$paymentMethods = array(
		'2' => 'Оплата в магазине',
		'5' => 'Безналичная оплата'
	);

?>

<table width="600" align="center" cellpadding="10" cellspacing="0" border="0" bgcolor="#fff" style="border-collapse: collapse;font-family: Arial, Helvetica, sans-serif;">
<tr>
	<td style="vertical-align: top; max-width: 300px;">
	<p style="font-size: 14px; font-weight: 700; padding: 0 10px 0; margin: 0;">Информация о заказе:</p>
	<ul style="font-size: 13px; margin-bottom: 2px;">
		<li style="padding-bottom: 3px;">Номер заказа: <a target="_blank" href="https://www.mirtelcom.ru/index.php?option=com_virtuemart&amp;view=orders&amp;layout=details&amp;order_number=<?php echo $this->orderDetails['details']['BT']->order_number; ?>&amp;order_pass=<?php echo $this->orderDetails['details']['BT']->order_pass; ?>" rel=" noopener noreferrer" style="color: #0087c7; text-decoration: none;">
			<?php echo $this->orderDetails['details']['BT']->order_number; ?></a></li>

		<li style="padding-bottom: 3px;">Статус заказа: <span style="text-transform: lowercase; color: firebrick;"><?php echo vmText::_($this->orderDetails['details']['BT']->order_status_name); ?></span></li>
		<li style="padding-bottom: 3px;">Пароль: <?php echo $this->orderDetails['details']['BT']->order_pass; ?></li>
		<li style="padding-bottom: 3px;">Доставка: <?php echo $shipmentMethods[$this->orderDetails['details']['ST']->virtuemart_shipmentmethod_id]; ?></li>
		<li style="padding-bottom: 3px;">Оплата: <?php echo $paymentMethods[$this->orderDetails['details']['ST']->virtuemart_paymentmethod_id]; ?></li>

		<!-- Telion -->
		<?php if ($reservDate != "0000-00-00") { ?>
			<li style="padding-bottom: 3px;">Дата резерва: <span style="color: firebrick;"><?php echo Date("d.m.Y", strtotime($reservDate[0])); ?></span></li>
		<?php } ?>

	</ul>
	</td>

	<td style="vertical-align: top; max-width: 300px;">
	<p style="font-size: 14px; font-weight: 700; padding: 0 10px 0; margin: 0;">Информация о покупателе:</p>
	<ul style="font-size: 13px; margin-bottom: 2px;">

		<li style="padding-bottom: 3px;">Покупатель: <?php echo $this->userfields['fields']['first_name']['value']; ?></li>

		<li style="padding-bottom: 3px; text-decoration: none;">E-mail: <?php echo '<a href="mailto:'.$this->userfields['fields']['email']['value'].'" style="color: #0087c7">'.$this->userfields['fields']['email']['value'].'</a>'; ?></li>

		<?php if ($this->userfields['fields']['phone_1']['value']) { ?>
			<li style="padding-bottom: 3px;">Телефон: <?php echo $this->userfields['fields']['phone_1']['value']; ?></li>
		<?php } ?>

		<?php if ($this->userfields['fields']['address_1']['value']) { ?>
			<li style="padding-bottom: 3px;">Адрес: <?php echo $this->userfields['fields']['address_1']['value']; ?></li>
		<?php } ?>

	</ul>
	</td>
</tr>
</table>