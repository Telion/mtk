<?php
/**
*
* Order items view
*
* @package	VirtueMart
* @subpackage Orders
* @author Oscar van Eijk, Valerie Isaksen
* @link https://virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: details_items.php 10033 2019-03-26 13:31:15Z Milbo $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

/* Telion #8.3.2 add */
$discount_p = 0;

/* Telion add */
$sumOrdersWithoutSale = $this->orderdetails['details']['BT']->order_shipment;
foreach ($this->orderdetails['items'] as $item) {
	$item->product_item_price = $item->allPrices[0]['product_price'];
	$sumOrdersWithoutSale += $item->allPrices[0]['product_price'] * $item->product_quantity;
}

if($this->format == 'pdf'){
	$widthTable = '100';
	$widthTitle = '27';
} else {
	$widthTable = '100';
	$widthTitle = '30';
}
?>

<table width="<?php echo $widthTable ?>%" cellspacing="0" cellpadding="0" border="0">
	<tr style="text-align: left;" class="sectiontableheader">

		<!-- Telion #10.6.1 change. Add class name -->
		<th class="telion-order-details-th-left-sku" width="10%"><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_SKU') ?></th>
		<th class="telion-order-details-th-left" colspan="5" width="<?php echo $widthTitle ?>%" ><?php echo vmText::_('COM_VIRTUEMART_PRODUCT_NAME_TITLE') ?></th>
		<th class="telion-order-details-th-right" width="10%" ><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PRICE') ?></th>
		<th class="telion-order-details-th-right" width="10%"><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_QTY') ?></th>
		<?php if ( VmConfig::get('show_tax')) { ?>
		<th class="telion-order-details-th-right" width="10%" ><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PRODUCT_TAX') ?></th>
		  <?php } ?>
		<th class="telion-order-details-th-right" width="12%"><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_TOTAL') ?></th>
		<?php /* base
		<th style="text-align: left;" width="10%"><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_SKU') ?></th>
		<th style="text-align: left;" colspan="2" width="<?php echo $widthTitle ?>%" ><?php echo vmText::_('COM_VIRTUEMART_PRODUCT_NAME_TITLE') ?></th>
		<th style="text-align: center;" width="10%"><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PRODUCT_STATUS') ?></th>
		<th style="text-align: right;" width="10%" ><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PRICE') ?></th>
		<th style="text-align: right;" width="6%"><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_QTY') ?></th>
		<?php if ( VmConfig::get('show_tax')) { ?>
		<th style="text-align: right;" width="11%" ><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PRODUCT_TAX') ?></th>
		  <?php } ?>
		<th style="text-align: right;" width="11%"><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_SUBTOTAL_DISCOUNT_AMOUNT') ?></th>
		<th style="text-align: right;" width="12%"><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_TOTAL') ?></th> */ ?>

		<!-- TELION #1.8.4.1 POINTS -->
		<?php $arr_usergroups = explode(',', $this->orderdetails["details"]["BT"]->user_shoppergroups); 
		if (in_array(7, $arr_usergroups)) { ?>
			<th class="telion-order-details-th-right" width="8%" style="color: #4d95e6;"><?php echo vmText::_('COM_VIRTUEMART_ORDER_LOGIN_PRINT_TLN_POINTS');?></th>
		<?php } ?>

	</tr>
<?php

/* TELION #1.8.4.2 POINTS */
if (in_array(7, $arr_usergroups)) { 

	$db = JFactory::getDBO();
	$db->setQuery('
		SELECT 
        	    wxa43_virtuemart_order_items_points.virtuemart_order_item_id,
		    wxa43_virtuemart_order_items_points.increase_points
		FROM 
		    wxa43_virtuemart_order_items,
		    wxa43_virtuemart_order_items_points
		WHERE
		    wxa43_virtuemart_order_items.virtuemart_order_item_id = wxa43_virtuemart_order_items_points.virtuemart_order_item_id
	            and wxa43_virtuemart_order_items.virtuemart_order_id = '.$this->orderdetails['details']['BT']->virtuemart_order_id.'
	');
	$arr_result = $db->loadAssocList('virtuemart_order_item_id');

	$arr_points = array();
	foreach ($arr_result as $key => $value) {
		$arr_points[$key] = $value["increase_points"];
	}

	$summ_points = 0;
}

foreach($this->orderdetails['items'] as $item) {
	$qtt = $item->product_quantity ;
	$_link = JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_category_id=' . $item->virtuemart_category_id . '&virtuemart_product_id=' . $item->virtuemart_product_id, FALSE);
?>
	<tr style="vertical-align: top; ">

		<!-- Telion #10.6.2 change -->
		<td class="telion-order-details-td-left-sku" style="text-align: left; vertical-align: middle;">
			<?php echo $item->order_item_sku; ?>
		</td>
		<td style="text-align: left; vertical-align: middle;" colspan="5" >
		<?php /* base
		<td style="text-align: left;">
			<?php echo $item->order_item_sku; ?>
		</td>
		<td style="text-align: left;" colspan="2" > */ ?>

			<div><a href="<?php echo $_link; ?>"><?php echo $item->order_item_name; ?></a></div>
			<?php
				$product_attribute = VirtueMartModelCustomfields::CustomsFieldOrderDisplay($item,'FE');
				echo $product_attribute;
			?>
		</td>

		<!-- Telion #10.6.3 remove -->
		<?php /* <td style="text-align: center;">
			<?php echo $this->orderstatuses[$item->order_status]; ?>
		</td> */ ?>

		<td style="text-align: right; vertical-align: middle;" class="priceCol" >
			<?php
			$item->product_discountedPriceWithoutTax = (float) $item->product_discountedPriceWithoutTax;

			/* Telion #8.3.3 change */
			if(($item->product_item_price - $item->product_final_price)!=0) {
				echo '<span class="line-through">'.$this->currency->priceDisplay($item->product_item_price, $this->user_currency_id) .'</span><br />';
				echo '<span >'.$this->currency->priceDisplay($item->product_discountedPriceWithoutTax, $this->user_currency_id) .'</span><br />';
			} elseif (!empty($item->product_priceWithoutTax) && $item->product_discountedPriceWithoutTax != $item->product_priceWithoutTax) {
				echo '<span class="line-through">'.$this->currency->priceDisplay($item->product_item_price, $this->user_currency_id) .'</span><br />';
				echo '<span >'.$this->currency->priceDisplay($item->product_discountedPriceWithoutTax, $this->user_currency_id) .'</span><br />';
			} else {
				echo '<span >'.$this->currency->priceDisplay($item->product_item_price, $this->user_currency_id) .'</span><br />';
			}
			/* base
			if (!empty($item->product_priceWithoutTax) && $item->product_discountedPriceWithoutTax != $item->product_priceWithoutTax) {
				echo '<span class="line-through">'.$this->currency->priceDisplay($item->product_item_price, $this->user_currency_id) .'</span><br />';
				echo '<span >'.$this->currency->priceDisplay($item->product_discountedPriceWithoutTax, $this->user_currency_id) .'</span><br />';
			} else {
				echo '<span >'.$this->currency->priceDisplay($item->product_item_price, $this->user_currency_id) .'</span><br />';
			} */

			?>
		</td>
		<td style="text-align: right; vertical-align: middle;" >
			<?php echo $qtt; ?>
		</td>

		<? /* Telion delete 
		<!-- Telion #8.3.4 change -->
		<td style="text-align: right;" class="priceCol">
			<?php echo $this->currency->priceDisplay(($item->product_item_price - $item->product_final_price) * $qtt ,$this->user_currency_id);
			$discount_p += ($item->product_item_price - $item->product_final_price) * $qtt; ?>
		</td>
		<?php /* base
		<?php if ( VmConfig::get('show_tax')) { ?>
		<td style="text-align: right;" class="priceCol"><?php echo "<span class='priceColor2'>".$this->currency->priceDisplay($item->product_tax ,$this->user_currency_id, $qtt)."</span>" ?></td>
		<?php } ?>
		<td style="text-align: right;" class="priceCol" >
			<?php echo  $this->currency->priceDisplay( $item->product_subtotal_discount ,$this->user_currency_id);  //No quantity is already stored with it ?>
		</td> */ ?>

		<td style="text-align: right; vertical-align: middle;"  class="priceCol">
			<?php
			$item->product_basePriceWithTax = (float) $item->product_basePriceWithTax;
			$class = '';

			/* Telion #8.3.5 change */
                        if (($item->product_item_price - $item->product_final_price) != 0) {
				echo '<span class="line-through">' . $this->currency->priceDisplay($item->product_item_price,$this->user_currency_id, $qtt) . '</span><br />';
			} elseif (!empty($item->product_basePriceWithTax) && $item->product_basePriceWithTax != $item->product_final_price ) {
				echo '<span class="line-through" >'.$this->currency->priceDisplay($item->product_basePriceWithTax,$this->user_currency_id, $qtt) .'</span><br />' ;
			}
			elseif (empty($item->product_basePriceWithTax) && $item->product_item_price != $item->product_final_price) {
				echo '<span class="line-through">' . $this->currency->priceDisplay($item->product_item_price,$this->user_currency_id, $qtt) . '</span><br />';
			}
			/* base
			if(!empty($item->product_basePriceWithTax) && $item->product_basePriceWithTax != $item->product_final_price ) {
				echo '<span class="line-through" >'.$this->currency->priceDisplay($item->product_basePriceWithTax,$this->user_currency_id,$qtt) .'</span><br />' ;
			}
			elseif (empty($item->product_basePriceWithTax) && $item->product_item_price != $item->product_final_price) {
				echo '<span class="line-through">' . $this->currency->priceDisplay($item->product_item_price,$this->user_currency_id,$qtt) . '</span><br />';
			} */

			echo $this->currency->priceDisplay(  $item->product_subtotal_with_tax ,$this->user_currency_id); //No quantity or you must use product_final_price ?>
		</td>

		<!-- TELION 1.8.4.3 POINTS -->
		<?php if (in_array(7, $arr_usergroups)) { ?>
		<td style="text-align: right; color: #4d95e6;" class="priceCol"><?php
			if (isset($arr_points[$item->virtuemart_order_item_id])){
			    echo $arr_points[$item->virtuemart_order_item_id];
			    $summ_points += $arr_points[$item->virtuemart_order_item_id];
			} else echo "-";
		?></td>
		<?php } ?>

	</tr>
<?php } ?>

	<? /* Telion #8.3.6 delete. "Промежуточный итог" 
	<tr class="sectiontableentry1">
		<td colspan="6" style="text-align: right;"><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PRODUCT_PRICES_TOTAL'); ?></td>
		<?php if ( VmConfig::get('show_tax')) { ?>
		<td style="text-align: right;"><?php echo "<span  class='priceColor2'>".$this->currency->priceDisplay($this->orderdetails['details']['BT']->order_tax,$this->user_currency_id)."</span>" ?></td>
		<?php } ?>
		<td style="text-align: right;"><?php echo "<span  class='priceColor2'>".$this->currency->priceDisplay($this->orderdetails['details']['BT']->order_discountAmount,$this->user_currency_id)."</span>" ?></td>  ?>

		<td style="text-align: right;"><?php echo $this->currency->priceDisplay($this->orderdetails['details']['BT']->order_salesPrice,$this->user_currency_id) ?></td>
	</tr> */ ?>

	<tr style="border-top: solid 1px #ccc;">
<?php
if ($this->orderdetails['details']['BT']->coupon_discount <> 0.00) {
	$coupon_code=$this->orderdetails['details']['BT']->coupon_code?' ('.$this->orderdetails['details']['BT']->coupon_code.')':'';
?>
		<td style="text-align: right;" class="pricePad" colspan="8"><?php echo vmText::_('COM_VIRTUEMART_COUPON_DISCOUNT').$coupon_code ?></td>
		<?php if ( VmConfig::get('show_tax')) { ?>
		<td style="text-align: right;">&nbsp;</td>
		<?php } ?>
		<td style="text-align: right;"><?php echo $this->currency->priceDisplay($this->orderdetails['details']['BT']->coupon_discount,$this->user_currency_id); ?></td>
	</tr>
<?php } ?>

<?php
foreach($this->orderdetails['calc_rules'] as $rule){
	if ($rule->calc_kind== 'DBTaxRulesBill') { ?>
	<tr>
		<td colspan="6"  style="text-align: right;" class="pricePad"><?php echo $rule->calc_rule_name ?> </td>
		<?php if ( VmConfig::get('show_tax')) { ?>
		<td style="text-align: right;">&nbsp;</td>
		<?php } ?>
		<td style="text-align: right;"><?php echo  $this->currency->priceDisplay($rule->calc_amount,$this->user_currency_id);  ?> </td>
		<td style="text-align: right;"><?php echo  $this->currency->priceDisplay($rule->calc_amount,$this->user_currency_id);  ?> </td>
	</tr>
	<?php
	} elseif ($rule->calc_kind == 'taxRulesBill') { ?>
	<tr>
		<td colspan="6"  style="text-align: right;" class="pricePad"><?php echo $rule->calc_rule_name ?> </td>
		<?php if ( VmConfig::get('show_tax')) { ?>
		<td style="text-align: right;"><?php echo $this->currency->priceDisplay($rule->calc_amount,$this->user_currency_id); ?> </td>
		 <?php } ?>
		<td style="text-align: right;">&nbsp;</td>
		<td style="text-align: right;"><?php echo $this->currency->priceDisplay($rule->calc_amount,$this->user_currency_id); ?> </td>
	</tr>
	<?php
	} elseif ($rule->calc_kind == 'DATaxRulesBill') { ?>
	<tr>
		<td colspan="6" style="text-align: right;" class="pricePad"><?php echo $rule->calc_rule_name ?> </td>
		<?php if ( VmConfig::get('show_tax')) { ?>
		<td style="text-align: right;">&nbsp;</td>
		 <?php } ?>
		<td style="text-align: right;"><?php  echo   $this->currency->priceDisplay($rule->calc_amount,$this->user_currency_id);  ?> </td>
		<td style="text-align: right;"><?php echo $this->currency->priceDisplay($rule->calc_amount,$this->user_currency_id);  ?> </td>
	</tr>
	<?php
	}
} ?>


	<!-- Telion #8.3.6a change. Расширил ячейку "Стоимость доставки" -->
	<?php if ($this->orderdetails['details']['BT']->coupon_discount <> 0.00) { ?>
	<tr>
	<?php } ?>
		<td style="text-align: right;" class="pricePad" colspan="8"><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_SHIPPING') ?></td>
		<?php /* base
		<td style="text-align: right;" class="pricePad" colspan="6"><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_SHIPPING') ?></td> 
		<?php if ( VmConfig::get('show_tax')) { ?>
		<td style="text-align: right;"><?php echo "<span  class='priceColor2'>".$this->currency->priceDisplay($this->orderdetails['details']['BT']->order_shipment_tax, $this->user_currency_id)."</span>" ?></td>
		<?php } ?>
		<td style="text-align: right;">&nbsp;</td> */ ?>
		<td style="text-align: right;"><?php echo $this->currency->priceDisplay($this->orderdetails['details']['BT']->order_shipment+ $this->orderdetails['details']['BT']->order_shipment_tax, $this->user_currency_id); ?></td>
	</tr>

	<!-- TELION #1.8.4.4 POINTS -->
	<?php if (in_array(7, $arr_usergroups)) { ?>
	<tr>
		<!-- Начислено баллов -->
		<td style="text-align: right; color: #4d95e6;" class="pricePad" colspan="8"><?php echo vmText::_('COM_VIRTUEMART_ORDER_LOGIN_PRINT_TLN_POINTS_TOTAL');?></td>
		<td style="text-align: right; color: #4d95e6; padding-right: 14px;"><?php echo $summ_points; ?></td>
	</tr>
	<?php } ?>

	<!-- Telion #10.6.4 remove -->
	<?php /* <tr>
		<td style="text-align: right;" class="pricePad" colspan="6"><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PAYMENT') ?></td>
		<?php if ( VmConfig::get('show_tax')) { ?>
		<td style="text-align: right;"><?php echo "<span  class='priceColor2'>".$this->currency->priceDisplay($this->orderdetails['details']['BT']->order_payment_tax, $this->user_currency_id)."</span>" ?></td>
		<?php } ?>
		<td style="text-align: right;">&nbsp;</td>
		<td style="text-align: right;"><?php echo $this->currency->priceDisplay($this->orderdetails['details']['BT']->order_payment+ $this->orderdetails['details']['BT']->order_payment_tax, $this->user_currency_id); ?></td>
	</tr> */ ?>

	<!-- Telion add -->
	<tr>
		<td style="text-align: right;" class="pricePad" colspan="8"><?php echo "Итого (<span style='color: #b9b0a2; font-size: small;'>без учета скидки</span>)" ?></td>
		<td style="text-align: right;"><span  class='priceColor2'>
			<?php echo $this->currency->priceDisplay($sumOrdersWithoutSale, $this->user_currency_id); ?>
	</tr>

	<tr>
		<!-- Telion #8.3.6b change. Расширил ячейку "Всего" -->
		<td style="text-align: right;" class="pricePad" colspan="8"><strong><?php echo "Всего к оплате" ?></strong></td>
		<?php /* base
		<td style="text-align: right;" class="pricePad" colspan="6"><strong><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_TOTAL') ?></strong></td> */ ?>

		<?php /* Telion delete
		<?php if ( VmConfig::get('show_tax')) {  ?>
		<!-- Telion #8.3.7 change -->
		<td style="text-align: right;"><span  class='priceColor2'>
			<?php echo $this->currency->priceDisplay($this->orderdetails['details']['BT']->order_billTaxAmount + $discount_p, $this->user_currency_id); ?>
			</span></td>
		<?php } ?>
		<td style="text-align: right;"><span  class='priceColor2'>
			<?php echo $this->currency->priceDisplay($discount_p, $this->user_currency_id); ?></span></td>
		<?php /* base
		<td style="text-align: right;"><span  class='priceColor2'>
			<?php echo $this->currency->priceDisplay($this->orderdetails['details']['BT']->order_billTaxAmount, $this->user_currency_id); ?></span></td>
		<?php } ?>
		<td style="text-align: right;"><span  class='priceColor2'>
			<?php echo $this->currency->priceDisplay($this->orderdetails['details']['BT']->order_billDiscountAmount, $this->user_currency_id); ?>
			</span></td> */ ?>

		<td style="text-align: right;"><strong><?php echo $this->currency->priceDisplay($this->orderdetails['details']['BT']->order_total, $this->user_currency_id); ?></strong></td>
	</tr>

</table>