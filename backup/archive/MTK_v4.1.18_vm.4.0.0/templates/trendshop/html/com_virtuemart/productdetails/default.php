<?php
/**
 *
 * Show the product details page
 *
 * @package	VirtueMart
 * @subpackage
 * @author Max Milbers, Eugen Stranz, Max Galt
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2014 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default.php 9292 2016-09-19 08:07:15Z Milbo $
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

/* Let's see if we found the product */
if (empty($this->product)) {
	echo vmText::_('COM_VIRTUEMART_PRODUCT_NOT_FOUND');
	echo '<br /><br />  ' . $this->continue_link_html;
	return;
}

echo shopFunctionsF::renderVmSubLayout('askrecomjs',array('product'=>$this->product));

if (vRequest::getInt('print',false)) { ?>
	<body onload="javascript:print();">
<?php } 

/* Telion #6.6 add. Добавление микроразметки og на страницу товара */
$doc = JFactory::getDocument();
$head = '<meta property="og:title" content="'.$this->product->product_name.'" />'."\r\n";
$head .= '<meta property="og:description" content="'.$this->product->metadesc.'" />'."\r\n";
$head .= '<meta property="og:image" content="'.JURI::base().$this->product->images[0]->file_url.'" />'."\r\n";
$head .= '<meta property="og:type" content="article" />'."\r\n";
$head .= '<meta property="og:site_name" content="www.mirtelcom.ru"/>'."\r\n";
$head .= '<meta property="og:url" content="'.JFactory::getURI().'" />'."\r\n";
$doc->addCustomTag($head);

/* Telion #6.17 add. Добавить "Возможные аналоги" к meta-description */
$db = JFactory::getDBO();
$db->setQuery('SELECT wxa43_virtuemart_product_customfields.customfield_value FROM wxa43_virtuemart_product_customfields WHERE wxa43_virtuemart_product_customfields.virtuemart_product_id = '.$this->product->virtuemart_product_id.' and wxa43_virtuemart_product_customfields.virtuemart_custom_id = 4');
$result = $db->loadObjectList('customfield_value');
$analogs = $db->loadColumn(0); // аналоги
if (isset($analogs[0])) {
	$telText = $doc->getMetaData('description').". Возможные аналоги: ".$analogs[0];
	$telText = str_replace("..", ".", $telText);
	$doc->setMetaData('description', $telText); 
}


?>

<div class="product-container productdetails-view productdetails b1c-good" itemscope itemtype="http://schema.org/Product">

      <?php
        // Product Navigation
        if (VmConfig::get('product_navigation', 1)) {
        ?>
            <div class="product-neighbours">
            <?php

	/* Telion #6.20.1 change */
            if (!empty($this->product->neighbours ['previous'][0])) {
	            $prev_link = JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $this->product->neighbours ['previous'][0] ['virtuemart_product_id'] . '&virtuemart_category_id=' . $this->product->virtuemart_category_id, FALSE);
        	    echo JHtml::_('link', $prev_link, '<i class="fa fa-arrow-left" aria-hidden="true"></i> '.'<p class="tlnx">'.$this->product->neighbours ['previous'][0]
                	['product_name'].'</p>', array('rel'=>'prev', 'class' => 'btn btn-default btn-sm btn-move-left pull-left','data-dynamic-update' => '0'));
            }
            if (!empty($this->product->neighbours ['next'][0])) {
	            $next_link = JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $this->product->neighbours ['next'][0] ['virtuemart_product_id'] . '&virtuemart_category_id=' . $this->product->virtuemart_category_id, FALSE);
        	    echo JHtml::_('link', $next_link, '<p class="tlnx">'.$this->product->neighbours ['next'][0] ['product_name'].'</p>'.' <i class="fa fa-arrow-right" aria-hidden="true"></i>', array('rel'=>'next','class' => 'btn btn-default btn-sm btn-move-right pull-right','data-dynamic-update' => '0'));
            }
	/* base
            if (!empty($this->product->neighbours ['previous'][0])) {
            $prev_link = JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $this->product->neighbours ['previous'][0] ['virtuemart_product_id'] . '&virtuemart_category_id=' . $this->product->virtuemart_category_id, FALSE);
            echo JHtml::_('link', $prev_link, '<i class="fa fa-arrow-left" aria-hidden="true"></i> '.$this->product->neighbours ['previous'][0]
                ['product_name'], array('rel'=>'prev', 'class' => 'btn btn-default btn-sm btn-move-left pull-left','data-dynamic-update' => '0'));
            }
            if (!empty($this->product->neighbours ['next'][0])) {
            $next_link = JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $this->product->neighbours ['next'][0] ['virtuemart_product_id'] . '&virtuemart_category_id=' . $this->product->virtuemart_category_id, FALSE);
            echo JHtml::_('link', $next_link, $this->product->neighbours ['next'][0] ['product_name'].' <i class="fa fa-arrow-right" aria-hidden="true"></i>', array('rel'=>'next','class' => 'btn btn-default btn-sm btn-move-right pull-right','data-dynamic-update' => '0'));
            }   */

            ?>
            <div class="clearfix"></div>
            </div>
        <?php } // Product Navigation END
        ?>
  
  
   <div class="productdetails-wrap">
        <?php // Product Title   ?>
        <h1 itemprop="name" class="b1c-name"><?php echo $this->product->product_name ?></h1>
        <?php // Product Title END   ?>
        
        <div class="product-icon-wrap">
            <?php // Back To Category Button
            if ($this->product->virtuemart_category_id) {
                $catURL =  JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$this->product->virtuemart_category_id, FALSE);
                $categoryName = vmText::_($this->product->category_name) ;
            } else {
                $catURL =  JRoute::_('index.php?option=com_virtuemart');
                $categoryName = vmText::_('COM_VIRTUEMART_SHOP_HOME') ;
            }
            ?> 
            <?php
            $backCategory = false; // показать кнопку Вернуться в категорию
            if($backCategory){ ?> 
                <div class="back-to-category">
                    <a href="<?php echo $catURL ?>" class="btn btn-default btn-sm btn-move-left" title="<?php echo $categoryName ?>"><?php echo '<i class="fa fa-reply" aria-hidden="true"></i> '.vmText::sprintf('COM_VIRTUEMART_CATEGORY_BACK_TO',$categoryName) ?></a>
                </div>
            <?php } ?>
            <?php // afterDisplayTitle Event
            echo $this->product->event->afterDisplayTitle ?>
            <?php
            // Product Edit Link
            echo $this->edit_link;
            // Product Edit Link END
            ?>
            <?php
            // PDF - Print - Email Icon
            if (VmConfig::get('show_emailfriend') || VmConfig::get('show_printicon') || VmConfig::get('pdf_icon')) {
            ?>
                <div class="icons btn-group">
                <?php

                $link = 'index.php?tmpl=component&option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $this->product->virtuemart_product_id;

                //echo $this->linkIcon($link . '&format=pdf', 'COM_VIRTUEMART_PDF', 'pdf_button', 'pdf_icon', false);
                if (VmConfig::get('pdf_icon')) echo JHTML::_('link', $link . '&format=pdf', '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>', array('class' => 'btn btn-default btn-sm'));
                //echo $this->linkIcon($link . '&print=1', 'COM_VIRTUEMART_PRINT', 'printButton', 'show_printicon');
                //echo $this->linkIcon($link . '&print=1', 'COM_VIRTUEMART_PRINT', 'printButton', 'show_printicon',false,true,false,'class="printModal"');
                if (VmConfig::get('show_printicon')) echo JHTML::_('link', $link . '&print=1', '<i class="fa fa-print" aria-hidden="true"></i>', array('class' => 'btn btn-default btn-sm recommened-to-friend'));
                $MailLink = 'index.php?option=com_virtuemart&view=productdetails&task=recommend&virtuemart_product_id=' . $this->product->virtuemart_product_id . '&virtuemart_category_id=' . $this->product->virtuemart_category_id . '&tmpl=component';
                //echo $this->linkIcon($MailLink, 'COM_VIRTUEMART_EMAIL', 'emailButton', 'show_emailfriend', false,true,false,'class="recommened-to-friend"');
                if (VmConfig::get('show_emailfriend')) echo JHTML::_('link', $MailLink, '<i class="fa fa-envelope-o" aria-hidden="true"></i>', array('class' => 'recommened-to-friend btn btn-default btn-sm'));
                ?>
                </div>
                <div class="clearfix"></div>
            <?php } // PDF - Print - Email Icon END
            ?>
        </div>
        <?php
            echo shopFunctionsF::renderVmSubLayout('customfields',array('product'=>$this->product,'position'=>'ontop'));
        ?>

        <div class="vm-product-container row">
            <div class="product-media-container col-lg-8 col-md-7 col-sm-12 col-xs-12">
               <?php echo shopFunctionsF::renderVmSubLayout('vmlabel',array('product'=>$this->product)); ?>
                <?php
                echo $this->loadTemplate('images');
                ?>
                <meta itemprop="image" content="<?php echo JURI::base().$this->product->images[0]->file_url?>"/>
            </div>

            <div class="product-details-container col-lg-4 col-md-5 col-sm-12 col-xs-12">
                <div class="spacer-buy-area" itemprop="offers" itemscope itemtype="http://schema.org/Offer">






 
                <?php 
                // товар дня
                $doc = JFactory::getDocument();
                $doc->addScript('/modules/mod_vm_product_day/assets/product-countdown.js');
                echo shopFunctionsF::renderVmSubLayout('productdayProduct',array('product'=>$this->product)); 
                ?>
               
                <?php
                // TODO in Multi-Vendor not needed at the moment and just would lead to confusion
                /* $link = JRoute::_('index2.php?option=com_virtuemart&view=virtuemart&task=vendorinfo&virtuemart_vendor_id='.$this->product->virtuemart_vendor_id);
                  $text = vmText::_('COM_VIRTUEMART_VENDOR_FORM_INFO_LBL');
                  echo '<span class="bold">'. vmText::_('COM_VIRTUEMART_PRODUCT_DETAILS_VENDOR_LBL'). '</span>'; ?><a class="modal" href="<?php echo $link ?>"><?php echo $text ?></a><br />
                 */
                ?>

                <?php
                echo shopFunctionsF::renderVmSubLayout('rating',array('showRating'=>$this->showRating,'product'=>$this->product));

		/* Telion #15.4 remove */
		/*  if (is_array($this->productDisplayShipments)) {
                      foreach ($this->productDisplayShipments as $productDisplayShipment) {
                      echo $productDisplayShipment;
                      }
                } */

		/*
                if (is_array($this->productDisplayPayments)) {
                    foreach ($this->productDisplayPayments as $productDisplayPayment) {
                    //echo $productDisplayPayment;
                    }
                }    */

                //In case you are not happy using everywhere the same price display fromat, just create your own layout
                //in override /html/fields and use as first parameter the name of your file

		/* Telion #6.7.1 change. Две цены на товар */
		$tel_unit = $this->product->product_unit;
		$new_string = ($tel_unit=='M') ? '₽/<span>м</span>' : '₽';	
		$is_change_price = 0;
		if(($this->product->prices['override']=='0') && ($this->user->id) && ($this->product->prices['product_price'] != $this->product->prices['salesPrice'])) {
			$is_change_price = 1;
			$retail_price = $this->product->prices['product_price'].' '.$new_string;
		} else {
			if (($this->product->prices['override'] != '0') && ($this->user->id)) {
				$this->product->prices['priceBeforeTax'] = $this->product->prices['product_price'];
			}
		}
		$your_price = str_replace('₽', $new_string, shopFunctionsF::renderVmSubLayout('prices',array('product'=>$this->product,'currency'=>$this->currency)));

		/* Telion #8.4 add. Цена кабеля на отрез */
		$count_prices = count($this->product->allPrices);
		$step_order = explode('|', $this->product->product_params)[2];
		$step_order_value = explode('"', $step_order)[1];
		$is_few_prices = 0;
		$is_have_step_count = 0;
		$is_product_unit_meter = 0;

		// Больше одной цены у товара, и товар измеряется в метрах
		if ( ($count_prices > 1) && ($tel_unit=='M') ) {
			$is_few_prices = 1;
			$is_product_unit_meter = 1;
			$opt_price_array = $this->product->allPrices[1];
			$rozn_price_array = $this->product->allPrices[0];
			$opt_price  = round(($opt_price_array["override"] == 1)  ? $opt_price_array["product_override_price"]  : $opt_price_array["product_price"]);
			$rozn_price = round(($rozn_price_array["override"] == 1) ? $rozn_price_array["product_override_price"] : $rozn_price_array["product_price"]);
			if ($is_change_price == 1) {
				foreach($this->product->allPrices[0]["Marge"] as $value) {
					$percent = $value[1];
				}
				$opt_price = ceil($opt_price - $opt_price * $percent / 100);
				$rozn_price = ceil($rozn_price - $rozn_price * $percent / 100);
			}

			$text_size_buht = '';
			foreach ($this->product->allPrices as $few_curr_price) {
				if ($few_curr_price['price_quantity_start'] == 0) continue;
				$text_size_buht .= $few_curr_price['price_quantity_start'] . ' м';
				if ($few_curr_price['price_quantity_end'] == 0) {
					$text_size_buht .= ' и более';
				} else {
					$text_size_buht .= ', ';
				}
			}

		$your_price = str_replace('class="product-price"', 'class="product-price" style="margin-bottom: 0;"', $your_price);
			

		// Одна цена на товар. Товар в метрах и Нет шага заказа товара
		} elseif ( ($tel_unit=='M') && ($step_order_value == '') ) {
			$is_product_unit_meter = 1;
			$tel_price = $this->product->prices['salesPrice'];
			$tel_itog = ceil($tel_price + $tel_price * 0.2);
		// Одна цена на товар. Товар в метрах и Есть шаг заказа товара
		} elseif ( ($tel_unit=='M') && ($step_order_value != '') ) {
			$is_have_step_count = 1;
			$is_product_unit_meter = 1;
		} ?> 

		<?php /* Telion */
		if ( $is_change_price == 1 ) { ?>
			<div style="font-size: 14px; color: #888;"><?= $retail_price ?> - розничная цена</div>
		<?php } ?>

		<?php /* Telion */
		if ( $is_few_prices && $is_product_unit_meter ) { ?>
			<div class="flexvalue" style="color: #888; padding: 0; margin-top: 0; text-align: left; font-size: 14px; font-weight: 400;"><?= $opt_price ?> ₽/м - при заказе кратно бухте</div>
		<?php } elseif (!$is_have_step_count && $is_product_unit_meter) { ?>
		    <div class="flexcontainer" style="height: 50px">
			<div class="flexvalue" style="margin-top: 4px;"><?= $tel_itog ?> руб/м - на отрез</div>
		    </div>
		<?php } elseif ($is_have_step_count && $is_product_unit_meter) { ?>
		    <div class="flexcontainer" style="height: 50px">
			<div class="flexheader">Продается кратно</div>
			<div class="flexvalue" style="margin-top: 1px;"><?= $step_order_value ?> м</div>
		    </div>
		<?php } ?> 

		<?php
		/* Telion */
		echo $your_price;

                echo "<meta itemprop='price' content='".$this->product->prices['salesPrice']."'>";
                echo "<meta itemprop='priceCurrency' content='".$this->currency->_vendorCurrency_code_3."'>";
                ?> <div class="clearfix"></div>
                           
                <?php
		/* Telion remove. Отключаю рейтинг */
                /* JPluginHelper::importPlugin('system', 'vmrating');
                $dispatcher = JDispatcher::getInstance();
                $ratingParams = [
                    array(
                        'id' => $this->product->virtuemart_product_id,
                        'average_rating' => true,
                        'count_votes' => true,
                        'count_votes_text' => true,
                        'active_voting' => true,
                        'only_reg' => false,
                        'micro_data' => true
                    )
                ];
                $results = $dispatcher->trigger('showRating', $ratingParams); */
                ?>

                <?php
                echo shopFunctionsF::renderVmSubLayout('addtocart',array('product'=>$this->product)); ?>

		
		<?php if ( $is_few_prices && $is_product_unit_meter ) { ?>
			<div style="border-bottom: 1px solid #eee; font-size: 12px; color: #777; margin-bottom: 14px; padding-bottom: 12px;">
			    <table style="width: -webkit-fill-available; max-width: 250px;">
			      <tr><td>При покупке кабеля на отрез</td><td style="width: 30px; text-align: center;">-</td><td style="text-align: right;"><?= $rozn_price ?> ₽/м</td></tr>
			      <tr><td><?= $text_size_buht ?></td><td style="width: 30px; text-align: center;">-</td><td style="text-align: right;"><?= $opt_price ?> ₽/м</td></tr>
			    </table>
			</div>
		<?php }  ?>

                   
                <?php
                if ( VmConfig::get ('display_stock', 1)){  ?> 
	                <div class="product-stock">
        	            <?php echo shopFunctionsF::renderVmSubLayout('stockhandle',array('product'=>$this->product)); ?>    
                	</div>   
	                <?php 
		
/*			if ($this->user->id == 442) {
	        	            echo shopFunctionsF::renderVmSubLayout('stockhandle',array('product'=>$this->product));
				echo "<pre>";
				print_r(array('product'=>$this->product));
				echo "</pre>";
			};
*/
		} ?>

		<?php

		/* Telion #8.1 add. Дата актуальности остатков */
		$cur_date = date('d-m-Y'); // текущая дата
		$yest_date = date('d.m.Y', strtotime('-1 day', strtotime($cur_date))); // вчерашняя дата
		// Делю дату yesterday
		$yd_d = substr($yest_date, 0, 2); // yesterday_day
		$yd_m = substr($yest_date, 3, 2); // yesterday_month
		$yd_y = substr($yest_date, 6, 4); // yesterday_year
		// Если дата попала на выходные, то сбрасываю ее на пятницу
		$d_week = date("w", mktime(0, 0, 0, $yd_m, $yd_d, $yd_y)); // week day - yesterday
		if ($d_week == 6) {$yest_date = date('d.m.Y', strtotime('-1 day', strtotime($yest_date)));} /* в воскресенье */
		if ($d_week == 0) {$yest_date = date('d.m.Y', strtotime('-2 day', strtotime($yest_date)));} /* в понедельник */
		// Делю дату yesterday
		$yd_d = substr($yest_date, 0, 2); // yesterday_day
		$yd_m = substr($yest_date, 3, 2); // yesterday_month
		$yd_y = substr($yest_date, 6, 4); // yesterday_year
		// Новогодние праздники (дни включительно) // Если вчера было от 01 до 10 числа 01 месяца, то пишу 31.12.20хх
		$d_ly = date("Y") - 1; // прошлый год
		if ($yd_m == '01' and $yd_d >= '01' and $yd_d <= '08'){
			$yest_date = '31.12.'.$d_ly;
		}
		// Другие праздники (дни включительно)
		while (($yd_m == '02' and $yd_d == '23') or ($yd_m == '03' and $yd_d == '08') or ($yd_m == '05' and ($yd_d == '01' or $yd_d == '02' or $yd_d == '09')) or ($yd_m == '06' and $yd_d == '12') or ($yd_m == '11' and $yd_d == '04')) {
			$yest_date = date('d.m.Y', strtotime('-1 day', strtotime($yd_d.'-'.$yd_m.'-'.$yd_y)));
			// Делю дату yesterday
			$yd_d = substr($yest_date, 0, 2); // yesterday_day
			$yd_m = substr($yest_date, 3, 2); // yesterday_month
			$yd_y = substr($yest_date, 6, 4); // yesterday_year
		}
		// Если дата попала на выходные, то сбрасываю ее на пятницу
		$d_week = date("w", mktime(0, 0, 0, $yd_m, $yd_d, $yd_y)); // week day - yesterday
		if ($d_week == 6) {$yest_date = date('d.m.Y', strtotime('-1 day', strtotime($yest_date)));} /* в воскресенье */
		if ($d_week == 0) {$yest_date = date('d.m.Y', strtotime('-2 day', strtotime($yest_date)));} /* в понедельник */
		?> 
		<div class="yesterday">Актуально на <?php echo JText::sprintf($yest_date); ?>г.</div>


		<!-- Telion #6.24.1 add. Удаленный склад, остатки -->
		<?php
		$db->setQuery('SELECT virtuemart_custom_id, customfield_value FROM wxa43_virtuemart_product_customfields WHERE virtuemart_product_id = '.$this->product->virtuemart_product_id);
		$result = $db->loadAssocList('virtuemart_custom_id');
/*		if (isset($result[59])) {
			$vendorCount = $result[59]['customfield_value'];
		}*/
		if (isset($vendorCount) and $vendorCount > 0) {
			$count = number_format($count, 0, '', ' ');
			echo '<div class="nostock text-success" style="color: #534F49; font-size: 13px; margin-bottom: 14px;">Удаленный склад: '.$vendorCount.' '.JText::sprintf('COM_VIRTUEMART_UNIT_SYMBOL_'.$this->product->product_unit).' (7&#8209;9&nbsp;дней)</div>';
		}
		?>

              
                <?php        
                // Ask a question about this product
                if (VmConfig::get('ask_question', 0) == 1) {
                    $askquestion_url = JRoute::_('index.php?option=com_virtuemart&view=productdetails&task=askquestion&virtuemart_product_id=' . $this->product->virtuemart_product_id . '&virtuemart_category_id=' . $this->product->virtuemart_category_id . '&tmpl=component', FALSE);
                    ?>
                    <div class="ask-a-question">
                        <a class="product-question btn btn-default btn-sm" href="<?php echo $askquestion_url ?>" rel="nofollow" ><i class="fa fa-question" aria-hidden="true"></i><?php echo vmText::_('COM_VIRTUEMART_PRODUCT_ENQUIRY_LBL') ?></a>
                    </div>
                <?php
                }
                ?>

                <?php
                // Manufacturer of the Product
                if (VmConfig::get('show_manufacturers', 1) && !empty($this->product->virtuemart_manufacturer_id)) {
                    echo $this->loadTemplate('manufacturer');
                }
                ?> 
                <?php // Telion #6.10 change. Скрыть пустой артикул и добавить div-обертку */
                // Показ артикула
		if (empty($this->product->product_sku)): else:
                	$showArticle = true;
	                if($showArticle){ ?>
	                    <div class="product-article">
	                        <div class="product-article-name"><?php echo JText::_('COM_VIRTUEMART_PRODUCT_SKU').': '.$this->product->product_sku; ?></div> 
	                    </div>   
	                <?php
	                }
		endif; 
                ?> 
                <?php /* base
                // Показ артикула
                $showArticle = true;
                if($showArticle){ ?>
                    <div class="product-article">
			<?php echo JText::_('COM_VIRTUEMART_PRODUCT_SKU').': '.$this->product->product_sku; ?>

                    </div>   
                <?php
                } */
                ?> 

		<?php /* Telion add вытаскиваю id товара для работы скрипта сравнения цен с ценами поставщиков */ 
		$idVisible = '"display: none;"';
		if ($this->user->id == 442 or $this->user->id == 408) {
			$idVisible = '"display: block; color: cornflowerblue; font-weight: 700;"';
		}
		?>
		<div style=<?php echo $idVisible;?> class="forme"><?php echo "ID: ".$this->product->virtuemart_product_id?></div>

                <?php
                // Показ краткого описания

		/* Telion edit. Отображение краткого описания на странице товара */
                $showDesc = true;
		/* base
                $showDesc = false; */

                if (!empty($this->product->product_s_desc) and $showDesc) {
                ?>
                    <div class="product-short-description">
                    <?php
                    /** @todo Test if content plugins modify the product description */
                    echo nl2br($this->product->product_s_desc);
                    ?>
                    </div>
                <?php
                } // Product Short Description END
                ?>	                 
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div> 

	<!-- Telion #8.7 add. Modal windows -->
	<div class="modal-main">
	  <label class="btn1" for="modal-1"><div class="div1"><img class="icon1" src="/images/icon/icon1.svg" alt=""></div><div class="div2"> Доставка </div></label>
	  <div class="modal1">
	    <input class="modal-open1" id="modal-1" type="checkbox" hidden>
	    <div class="modal-wrap1" aria-hidden="true" role="dialog">
	      <label class="modal-overlay1" for="modal-1"></label>
	      <div class="modal-dialog1">
	        <div class="modal-header1">
	          <h2> Доставка </h2>
	          <label class="btn-close1" for="modal-1" aria-hidden="true">×</label>
	        </div>
	        <div class="modal-body1">
	          <p class="text-bold1">Мы доставляем заказы по всей России.</p>
	          <p class="text-main1">Стоимость доставки любого заказа курьером по Тюмени – 500 руб., а при заказе от 5000 руб. – бесплатно! Доставка осуществляется с понедельника по пятницу, с 9:30 до 18:00. Возможноcть и стоимость доставки заказов в пригороде Тюмени обговаривается отдельно.</p>
	          <p class="text-main1">Доставка в другие города России осуществляется транспортными компаниями за счет покупателя (оплата доставки обычно происходит при получении в офисе ТК).</p>
	          <p class="text-main1">Подробности об условиях доставки и список поддерживаемых транспортных компаний можно узнать в разделе <a href="/delivery">"Доставка и оплата"</a> или по телефону  +7(3452)541&#8209;541.</p>
	        </div>
	      </div>
	    </div> 
	  </div>
	  <label class="btn1" for="modal-2"><div class="div1"><img class="icon2" src="/images/icon/icon4.svg" alt=""></div><div class="div2"> Оплата </div></label>
	  <div class="modal1">
	    <input class="modal-open1" id="modal-2" type="checkbox" hidden>
	    <div class="modal-wrap1" aria-hidden="true" role="dialog">
	      <label class="modal-overlay1" for="modal-2"></label>
	      <div class="modal-dialog1">
	        <div class="modal-header1">
	          <h2> Оплата </h2>
	          <label class="btn-close1" for="modal-2" aria-hidden="true">×</label>
	        </div>
	        <div class="modal-body1">
	          <p class="text-main2">Оплата заказа возможна следующими способами:</p>
	          <p class="text-bold2">Наличными деньгами</p>
	          <p class="text-dop2">Оплата наличными при доставке курьером или при получении заказа в нашем офисе.</p>
	          <p class="text-bold2">Банковской картой</p>
	          <p class="text-dop2">В офисе нашей компании или при получении заказа от курьера также возможна оплата банковскими картами.</p>
	          <p class="text-bold2">Безналичным расчетом</p>
	          <p class="text-dop2">Безналичная оплата на счет компании от физических и юридических лиц.</p>
	          <p class="text-main2">Подробнее об условиях оплаты можно узнать в разделе <a href="/delivery">"Доставка и оплата"</a> или по телефону  +7(3452)541&#8209;541.</p>
	        </div>
	      </div>
	    </div> 
	  </div>
	  <label class="btn1" for="modal-3"><div class="div1"><img class="icon3" src="/images/icon/icon2.svg" alt=""></div><div class="div2"> Наши услуги </div></label>
	  <div class="modal1">
	    <input class="modal-open1" id="modal-3" type="checkbox" hidden>
	    <div class="modal-wrap1" aria-hidden="true" role="dialog">
	      <label class="modal-overlay1" for="modal-3"></label>
	      <div class="modal-dialog1">
	        <div class="modal-header1">
	          <h2> Наши услуги </h2>
	          <label class="btn-close1" for="modal-3" aria-hidden="true">×</label>
	        </div>
	        <div class="modal-body1">
	          <p class="text-bold1">Мы предоставляем следующие виды услуг:</p>
	          <p style="font-weight: bold;padding-bottom: 0px;padding-left: 20px;">Монтаж СКС</p>
	          <!--<p class="text-dop2"><< Пояснение >> Подробнее <- ссылка</p>-->
	          <p style="font-weight: bold;padding-bottom: 0px;padding-left: 20px;">Монтаж оптоволоконных сетей</p>
	          <!--<p class="text-dop2"><< Пояснение >> Подробнее <- ссылка</p>-->
	          <p style="font-weight: bold;padding-bottom: 0px;padding-left: 20px;">Измерения качества сетей</p>
	          <!--<p class="text-dop2"><< Пояснение >> Подробнее <- ссылка</p>-->
	          <p style="font-weight: bold;padding-bottom: 0px;padding-left: 20px;">Монтаж кондиционеров</p>
	          <!--<p class="text-dop2"><< Пояснение >></p>-->
	          <p style="font-weight: bold;padding-bottom: 0px;padding-left: 20px;">Монтаж систем видеонаблюдения</p>
	          <!--<p class="text-dop2"><< Пояснение >></p>-->
	          <p style="margin: 0;text-align: justify;padding-top: 10px;">Получить смету монтажных работ вы можете по телефону +7(3452)541&#8209;541 или написав нам на почту info@541541.ru</p>
	        </div>
	      </div>
	    </div> 
	  </div>
	</div>

    <?php //конец блока с информацией о товаре ?>
    
    <?php //Подсчет количества отзывов о товаре ?>
    <?php
    $comments = JPATH_SITE . '/components/com_jcomments/jcomments.php';
    if (file_exists($comments)) {
    require_once($comments);
    $options = array();
    $options['object_id'] = $this->product->virtuemart_product_id;
    $options['object_group'] = 'com_virtuemart';
    $options['published'] = 1;
    $count = JCommentsModel::getCommentsCount($options);
    }
    ?>
    
    <div class="product-desc-wrap">
        <?php //Nav tabs ?>
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs" role="tablist" id="product-tabs">
                    <li class="tab-home active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?php echo vmText::_('COM_VIRTUEMART_PRODUCT_DESC_TITLE'); ?></a></li>
                    <?php if (!empty($this->product->customfieldsSorted['video'])) { ?>
                    <li><a href="#video" aria-controls="video" role="tab" data-toggle="tab"><?php echo vmText::_('COM_VIRTUEMART_TAB_VIDEO'); ?></a></li>
                    <?php } ?>
                    <li class="tab-reviews"><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab"><?php echo vmText::_('COM_VIRTUEMART_TAB_REVIEWS'); ?> (<?php echo $count; ?>)</a></li>
                </ul>
            </div>
        </div>


       
        <?php //Tab panels ?>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="home">	
                <?php
                // event onContentBeforeDisplay
                echo $this->product->event->beforeDisplayContent; ?>

                <?php
                // Product Description
                if (!empty($this->product->product_desc)) {
                    ?>
                    <div class="product-description" itemprop="description">
                    <?php echo $this->product->product_desc; ?>
                    </div>
                <?php
                } // Product Description END

                echo shopFunctionsF::renderVmSubLayout('customfields',array('product'=>$this->product,'position'=>'normal'));

                // Product Packaging
                $product_packaging = '';
                if ($this->product->product_box) {
                ?>
                    <div class="product-box">
                    <?php
                        echo vmText::_('COM_VIRTUEMART_PRODUCT_UNITS_IN_BOX') .$this->product->product_box;
                    ?>
                    </div>
                <?php } // Product Packaging END ?>	

                <?php echo $this->loadTemplate('reviews'); ?>
            </div>

            <?php if (!empty($this->product->customfieldsSorted['video'])) { ?>
            <div role="tabpanel" class="tab-pane fade" id="video">
                <?php echo shopFunctionsF::renderVmSubLayout('customfields',array('product'=>$this->product,'position'=>'video')); ?>
            </div>
            <?php } ?>  
            
            <div role="tabpanel" class="tab-pane fade" id="reviews">
                <?php // onContentAfterDisplay event
                echo $this->product->event->afterDisplayContent; 

                $comments = JPATH_ROOT . '/components/com_jcomments/jcomments.php';
                if (file_exists($comments)) {
                    require_once($comments);
                    echo JComments::showComments($this->product->virtuemart_product_id, 'com_virtuemart', $this->product->product_name);
                }
                ?>
            </div> 
        </div>

        <?php 
        echo shopFunctionsF::renderVmSubLayout('customfields',array('product'=>$this->product,'position'=>'onbot'));
        ?>
    </div> 
    <?php // конец блока с описанием товара ?>
    
    
    <?php
    if (!empty($this->product->customfieldsSorted['related_products'])) {
        echo '<h3 class="related-products-title">'.JTEXT::_('COM_VIRTUEMART_RELATED_PRODUCTS').'</h3>';
        echo shopFunctionsF::renderVmSubLayout('customfields',array('product'=>$this->product,'position'=>'related_products','class'=> 'product-related-products','customTitle' => true ));
    }

    if (!empty($this->product->customfieldsSorted['related_categories'])) {
        echo '<h3 class="related-categories-title">'.JTEXT::_('COM_VIRTUEMART_RELATED_CATEGORIES').'</h3>';
        echo shopFunctionsF::renderVmSubLayout('customfields',array('product'=>$this->product,'position'=>'related_categories','class'=> 'product-related-categories'));
    }
    ?>
    
<?php // onContentAfterDisplay event
echo $this->product->event->afterDisplayContent;

// Show child categories
if ($this->cat_productdetails)  {
	echo $this->loadTemplate('showcategory');
}

$j = 'jQuery(document).ready(function($) {
	$("form.js-recalculate").each(function(){
		if ($(this).find(".product-fields").length && !$(this).find(".no-vm-bind").length) {
			var id= $(this).find(\'input[name="virtuemart_product_id[]"]\').val();
			Virtuemart.setproducttype($(this),id);

		}
	});
});';
//vmJsApi::addJScript('recalcReady',$j);

if(VmConfig::get ('jdynupdate', TRUE)){

	/** GALT
	 * Notice for Template Developers!
	 * Templates must set a Virtuemart.container variable as it takes part in
	 * dynamic content update.
	 * This variable points to a topmost element that holds other content.
	 */
	$j = "Virtuemart.container = jQuery('.productdetails-view');
Virtuemart.containerSelector = '.productdetails-view';
//Virtuemart.recalculate = true;	//Activate this line to recalculate your product after ajax
";
    
	vmJsApi::addJScript('ajaxContent',$j);

	$j = "jQuery(document).ready(function($) {
	Virtuemart.stopVmLoading();
	var msg = '';
	$('a[data-dynamic-update=\"1\"]').off('click', Virtuemart.startVmLoading).on('click', {msg:msg}, Virtuemart.startVmLoading);
	$('[data-dynamic-update=\"1\"]').off('change', Virtuemart.startVmLoading).on('change', {msg:msg}, Virtuemart.startVmLoading);  
});";

	vmJsApi::addJScript('vmPreloader',$j);
}

echo vmJsApi::writeJS();

if ($this->product->prices['salesPrice'] > 0) {
  echo shopFunctionsF::renderVmSubLayout('snippets',array('product'=>$this->product, 'currency'=>$this->currency, 'showRating'=>$this->showRating));
}

?>
</div>