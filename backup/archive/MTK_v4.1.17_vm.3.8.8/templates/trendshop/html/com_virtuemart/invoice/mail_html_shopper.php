<?php
/**
 *
 * Define here the order number, pass and order total !
 * @author Spyros Petrakis
 * @link http://www.virtuemarttemplates.eu
 * @copyright Copyright (c) 2015 Spyros Petrakis. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 *
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

	/* Telion #1.11.1 add. cmsMTK reservDate. Получение даты резерва по определенному заказу (virtuemart_order_id) */
	$db = JFactory::getDBO();
	$db->setQuery('SELECT reserv_date FROM wxa43_virtuemart_order_histories WHERE virtuemart_order_id='.$this->orderDetails['details']['BT']->virtuemart_order_id.' ORDER BY virtuemart_order_history_id ASC LIMIT 1');
	$reservDate = $db->loadColumn()[0];
?>

<table width="600" align="center" cellpadding="10" cellspacing="0" border="0" bgcolor="#fff" style="border-collapse: collapse;font-family: Arial, Helvetica, sans-serif;">
	<tr><td style="padding: 0;">

	<div style="border-radius: 5px; border: solid 1px firebrick; width: max-content; margin: 0 auto; text-align: center; max-width: 540px;">
		<div style = "padding: 15px 25px;">
			<?php
			$status = $this->orderDetails['details']['BT']->order_status;
			if (($status == "U")) { ?>
<!--				<p style="margin: 0 0 10px;">Спасибо за покупку в магазине <a href="https://www.mirtelcom.ru/" target="_blank" rel=" noopener noreferrer" style="color: #0087c7; text-decoration: none;">Мир Телекоммуникаций</a>.</p> -->
				<p style="margin: 0 0 10px;">Ваш заказ успешно оформлен.</p>
				<p style="margin: 0;">В ближайшее время мы свяжемся с вами.</p>
			<?php } elseif ($status == "C") { ?>
				<p style="margin: 0">Ваш заказ 
					<?php if ($reservDate != "0000-00-00") { 
						echo " зарезервирован до ".date("d.m.Y", strtotime($reservDate))."г.";
					} else { 
						echo " передан на сборку.";
					} ?></p>
			<?php } elseif ($status == "P") { ?>
				<p style="margin: 0;">Ваш заказ собран и готов к выдаче.</p>
			<?php } elseif ($status == "X") { ?>
				<p style="margin: 0;">Ваш заказ отменен.</p>
			<?php } ?>
		</div>
		<?php 
		$nb = count($this->orderDetails['history']);
		$currentComment = $this->orderDetails['history'][$nb-1];		
		if($currentComment->customer_notified && !(empty($currentComment->comments))) { ?>
			<div style = "padding: 2px 15px 15px; border-top: solid 1px firebrick;">
				<p style="text-align: justify; font-size: 14px; margin: 0; padding-top: 13px; max-width: 470px; word-break: break-all;">
					<span style="color: firebrick; font-weight: 700;">Комментарий менеджера: </span>
					<?php echo nl2br($currentComment->comments); ?> </p> 
			</div>
		<?php } ?>
	</div></td></tr>
</table>
