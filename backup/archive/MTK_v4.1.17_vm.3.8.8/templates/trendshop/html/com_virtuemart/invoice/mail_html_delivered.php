<?php
/**
 *
 * Layout for the order email
 * shows the chosen adresses of the shopper
 * taken from the stored order
 *
 * @package	VirtueMart
 * @subpackage Order
 * @author Max Milbers,   Valerie Isaksen
 *
 * @link https://virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 *
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
?>

			<tr>
			  <td style="padding-top: 0;">
			    <div style="margin: auto;">
			      <p style="font-size: 16px; text-align: center; font-weight: bold; padding: 0 20px;">
			        Здравствуйте!</p>
			      <p style="font-size: 14px; margin: 0; padding: 0 20px; text-align: justify;">
			        Благодарим за покупку в нашем магазине.</p>
			      <p style="font-size: 14px; margin: 0; padding: 0 20px; text-align: justify;">
			        Номер вашего заказа <b><?php echo $this->orderDetails['details']['BT']->order_number; ?></b>.</p>
			      <p style="font-size: 14px; margin: 0; padding: 0 20px; text-align: justify;">
			        Мы дорожим оказанным доверием.</p>
			    </div>
			  </td>         	
			</tr>
			<tr>
			  <td>
			    <p style="font-size: 14px; margin: 0; padding: 0 20px; text-align: justify;">
			      Понравился наш сервис? Будем признательны за обратную связь в виде отзыва на
			      <a href="https://tyumen.flamp.ru/addreview/1830116630019132">2Гис</a>.
			      Опубликуйте отзыв, укажите номер заказа и получите скидку 3% на следующую покупку.</p>
			  </td>
			</tr>
			<tr>
			  <td>
			    <p style="font-size: 14px; margin: 0; padding: 0 20px; text-align: justify;">С наилучшими пожеланиями команда <a href="https://mirtelcom.ru">Мир Телекоммуникаций</a> - Тюмень.</p><br>
			  </td>
			</tr>