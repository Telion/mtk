<?php
/**
 *
 * Define here the Header for order mail
 * @author Spyros Petrakis
 * @link http://www.virtuemarttemplates.eu
 * @copyright Copyright (c) 2015 Spyros Petrakis. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 *
 */
// Check to ensure this file is included in Joomla!
defined ('_JEXEC') or die('Restricted access');
?>

<table width="600" align="center" cellpadding="10" cellspacing="0" border="0" bgcolor="#fff" style="margin: 11px auto 14px; text-align: right;">

	<?php if ($this->vendor->vendor_letter_header > 0) { ?>
	<tr>
		<?php if ($this->vendor->vendor_letter_header_image > 0) { ?>
		<td class="vmdoc-header-image" style="border: 0px solid #CCCCCC;" width="290" align="center">
			<a href="<?php echo JURI::root(); ?>" target="_blank">
			<img src="<?php  echo JURI::root() . $this->vendor->images[0]->file_url ?>" border="0" style="display: block;" alt="<?php echo $this->vendor->vendor_store_name; ?>" />
			</a>
		</td>

		<td width="290" class="vmdoc-header-vendor" style="border: 0px solid #CCCCCC;">
		<?php } else { // no image ?>
		<td width="580" class="vmdoc-header-vendor" style="border: 0px solid #CCCCCC;">
		<?php } ?>
			<div id="vmdoc-header" class="vmdoc-header">
			<?php 
				echo '
					<div style="padding: 0 15px; font-size: 12.5px;">
						<p style="margin: 5px 0;">г. Тюмень, ул. Максима Горького 90</p>
						<p style="color: #0077cc; margin: 5px 0;"><span class="js-phone-number_mr_css_attr highlight-phone_mr_css_attr" title="Позвонить через Веб-Агент"><a href="tel:83452541541" target="_blank" rel=" noopener noreferrer" style="color: #0087c7; text-decoration: none;"><span class="js-phone-number">+7 (3452) 541-541</span></a></span></p>
						<p style="color: #0077cc; margin: 5px 0;"><a href="mailto:info@541541.ru" target="_blank" rel=" noopener noreferrer" style="color: #0087c7; text-decoration: none;"><span>info@541541.ru</span></a></p>
					</div>
				';
				/* base
				echo VirtuemartViewInvoice::replaceVendorFields($this->vendor->vendor_letter_header_html, $this->vendor);*/
			?>
			</div>
		</td>
	</tr>
	<?php } // END if header ?>
</table>