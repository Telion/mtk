<?php
/**
 *
 * Define here the Vendor header for order mail
 * @author Spyros Petrakis
 * @link http://www.virtuemarttemplates.eu
 * @copyright Copyright (c) 2015 Spyros Petrakis. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 *
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
?>
<table align="center" width="580" border="0" cellpadding="10" cellspacing="0" class="html-email" style="border-collapse: collapse; font-family: Arial, Helvetica, sans-serif; font-size: 12.7px;">
<tr>
<td>
		<?php 
		$nb = count($this->orderDetails['history']);
		$currentComment = $this->orderDetails['history'][$nb-1];		
		if($currentComment->customer_notified && !(empty($currentComment->comments))) { ?>
			<div style = "padding: 2px 15px 15px; border-top: solid 1px firebrick;">
				<p style="text-align: justify; font-size: 14px; margin: 0; padding-top: 13px; max-width: 470px;">
					<span style="color: firebrick; font-weight: 700;">Комментарий&nbsp;менеджера: </span>
					<?php echo nl2br($currentComment->comments); ?> </p> 
			</div>
		<?php } ?>
</td>
</tr>
</table>