<?php
/**
*
* Layout for the shopping cart, look in mailshopper for more details
*
* @package	VirtueMart
* @subpackage Cart
* @author Max Milbers
*
* @link https://virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
*
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
/* TODO Chnage the footer place in helper or assets ???*/
if (empty($this->vendor)) {
		$vendorModel = VmModel::getModel('vendor');
		$this->vendor = $vendorModel->getVendor();
}

//$link = shopFunctionsF::getRootRoutedUrl('index.php?option=com_virtuemart');
//$link = JURI::root().'index.php?option=com_virtuemart';
$link = JURI::root();

?>

<table width="600" align="center" cellpadding="10" cellspacing="0" border="0" bgcolor="#fff" style="border-collapse: collapse;font-family: Arial, Helvetica, sans-serif;">
	<tr><td style="padding-bottom: 0;">
		<p style="text-align:right; font-size: 13px;">Есть вопросы? Позвоните нам по телефону 
			<span class="js-phone-number_mr_css_attr highlight-phone_mr_css_attr" title="Позвонить через Веб-Агент" style="color: #0077cc;"><a href="tel:83452541541" target="_blank" rel=" noopener noreferrer" style="color: #0087c7; text-decoration: none;"><span class="js-phone-number">+7 (3452) 541-541</span></a></span>
		</p>
	<hr>
	</td></tr>
</table>

<table width="500" align="center" cellpadding="10" cellspacing="0" border="0" bgcolor="#fff" style="border-collapse: collapse;font-family: Arial, Helvetica, sans-serif;">
	<tr style="font-size: 12.5px;">
		<td><a href="https://www.mirtelcom.ru/scs-components" target="_blank" style="color: #0087c7; text-decoration: none; text-decoration: none;">Сетевые компоненты</a></td>
		<td><a href="https://www.mirtelcom.ru/optical-components" target="_blank" style="color: #0087c7; text-decoration: none;">Оптические компоненты</a></td>
		<td><a href="https://www.mirtelcom.ru/equipments" target="_blank" style="color: #0087c7; text-decoration: none;">Оборудование</a></td>
	</tr>
</table>

<table width="500" align="center" cellpadding="10" cellspacing="0" border="0" bgcolor="#fff" style="border-collapse: collapse;font-family: Arial, Helvetica, sans-serif; margin-bottom: 1px;">
	<tr style="font-size: 12.5px;">
		<td><a href="https://www.mirtelcom.ru/electrical-product" target="_blank" style="color: #0087c7; text-decoration: none;">Электрика</a></td>
		<td><a href="https://www.mirtelcom.ru/cable-support-system" target="_blank" style="color: #0087c7; text-decoration: none;">Кабеленесущие системы</a></td>
		<td><a href="https://www.mirtelcom.ru/skud" target="_blank" style="color: #0087c7; text-decoration: none;">СКУД</a></td>
		<td><a href="https://www.mirtelcom.ru/sale" target="_blank" style="color: #0087c7; text-decoration: none;">Распродажа</a></td>
	</tr>
</table>

<?php if ($this->vendor->vendor_letter_footer == 1) { ?>
<?php if ($this->vendor->vendor_letter_footer_line == 1) { ?><hr <hr style="border-top: 1px solid #CCCCCC !important;" /><?php } ?>
<div id="vmdoc-footer" class="vmdoc-footer" style="font-size: <?php echo $this->vendor->vendor_letter_footer_font_size; ?>pt;">
<?php //echo $this->replaceVendorFields($this->vendor->vendor_letter_footer_html, $this->vendor); ?>
</div>

<?php } // END if footer ?>
