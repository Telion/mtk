<?php
$product = $viewData['product'];
// Availability
$stockhandle = VmConfig::get('stockhandle', 'none');
$product_available_date = substr($product->product_available_date,0,10);
$current_date = date("Y-m-d");

// label stock
// Telion #7.2 change. Наличие в Тюмени: 15 шт
$free_stock = $product->product_in_stock - $product->product_ordered;
//if (($product->product_in_stock - $product->product_ordered) < 1) {
if (($product->product_in_stock < 1) and ($product->product_ordered <= 0)) {
	echo '<div class="nostock text-warning">' . JText::sprintf('COM_VIRTUEMART_PRODUCT_NOSTOCK') . '</div>'; 
} else {
//	if ($product->product_ordered <= 0) {
//		echo '<div class="nostock text-success">' . JText::sprintf('Наличие в Тюмени: ' . $free_stock . ' ' . JText::sprintf('COM_VIRTUEMART_UNIT_SYMBOL_' . $product->product_unit)) . '</div>';
//	} else {
		echo '<div class="nostock text-success" 
			title="
Свободно: '.$free_stock. ' ' . JText::sprintf('COM_VIRTUEMART_UNIT_SYMBOL_'.$product->product_unit).'
Всего: '.$product->product_in_stock. ' ' . JText::sprintf('COM_VIRTUEMART_UNIT_SYMBOL_'.$product->product_unit).'">' 
			. JText::sprintf('Наличие в Тюмени: '
			. $free_stock . ' / '
			. $product->product_in_stock . ' ' . JText::sprintf('COM_VIRTUEMART_UNIT_SYMBOL_'.$product->product_unit))
			. '</div>';
//	} 
	/* base
    <div class="nostock text-success"><?php echo JText::sprintf('COM_VIRTUEMART_PRODUCT_STOCK'); ?></div> */
}
?>