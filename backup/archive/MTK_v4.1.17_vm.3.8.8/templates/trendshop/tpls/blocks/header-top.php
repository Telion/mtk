<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<!-- Telion #3.1.1 add -->
<?php $user = JFactory::getUser();?>

<?php if ($this->countModules('header-top-1 or header-top-2')) : ?>
	<div class="header-top navbar navbar-default">
		<div class="container">
		    <div class="row">
                <?php if ($this->countModules('header-top-1')) : ?>
		            <div class="header-top-1 col-lg-6 col-md-6 col-sm-6 col-xs-6">

			<!-- Telion #3.1.2 remove -->
			<?php /* $user = JFactory::getUser();
			if ($user->id){ ?>
			    <div class="hello-user"><?php echo JText::_('COM_VIRTUEMART_HELLO_USER').'<a href="'.JRoute::_('index.php?option=com_virtuemart&view=user&layout=edit', false).'">'.$user->name.'</a>'; ?>!</div>
			<?php } else { */ ?>

                            <jdoc:include type="modules" name="<?php $this->_p('header-top-1') ?>" style="raw"/>

	              <!-- Telion #3.1.3 remove -->
                        <?php
                       // }
                        ?>  

                    </div>

	                <!-- Telion #3.1.3 remove -->
		        <?php // endif ?>

		        <?php if ($this->countModules('header-top-2')) : ?>
		            <div class="header-top-2 col-lg-6 col-md-6 col-sm-6 col-xs-6">

		            <!-- Telion #3.1.3 add. �������� ������ "��� �������" �� ��� ������������ -->
		            <?php if ($user->id){ ?>
		              <div class="telion-hello-user">
		                <?php echo '<a href="'.JRoute::_('index.php?option=com_virtuemart&view=user&layout=edit', false).'" 
		                  class="telion-fa-hello-user">'.$user->name.'</a>'; ?></div>
		            <?php } else {?>  
		              <div class="telion-hello-user"><a href="/my-account" class="fa-hello-user">
		                <?php echo vmText::sprintf('COM_VIRTUEMART_SHOPPER_NOT_LOGIN');?> </a></div>
		            <?php } endif ?>

		                <jdoc:include type="modules" name="<?php $this->_p('header-top-2') ?>" style="raw"/>
		            </div>
		        <?php endif ?>
		    </div>
		</div>
	</div>
<?php endif ?>
