<?php
/**
*
* Modify user form view
*
* @package	VirtueMart
* @subpackage User
* @author Oscar van Eijk
* @link https://virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: edit.php 9523 2017-05-04 10:23:55Z Milbo $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

// Implement Joomla's form validation
vmJsApi::vmValidator();
vmJsApi::css('vmpanels'); // VM_THEMEURL
?>

<?php

/* Telion #15.25.1 add */
$user = JFactory::getUser();

$url = vmURI::getCurrentUrlBy('request');
$cancelUrl = JRoute::_($url.'&task=cancel');
?>

<h1><?php echo $this->page_title ?></h1>

<!-- Telion #15.25.2 add -->
<div class="telion-empty"></div>

<?php /* Telion #1.8.2.1 POINTS */
if (in_array(7, $this->userDetails->shopper_groups)) {
	$db = JFactory::getDBO();
	$db->setQuery('
		SELECT
		    sum(wxa43_virtuemart_order_items_points.increase_points) as pointsStat1
		FROM 
		    wxa43_virtuemart_orders,
		    wxa43_virtuemart_order_items,
		    wxa43_virtuemart_order_items_points
		WHERE
		    wxa43_virtuemart_orders.virtuemart_user_id = '.$this->userDetails->virtuemart_user_id.' AND
		    wxa43_virtuemart_order_items.virtuemart_order_id = wxa43_virtuemart_orders.virtuemart_order_id AND
		    wxa43_virtuemart_order_items_points.virtuemart_order_item_id = wxa43_virtuemart_order_items.virtuemart_order_item_id AND
		    wxa43_virtuemart_order_items_points.points_status = 1
		UNION
		ALL SELECT
		    sum(wxa43_virtuemart_order_items_points.increase_points) as pointsStat0
		FROM 
		    wxa43_virtuemart_orders,
		    wxa43_virtuemart_order_items,
		    wxa43_virtuemart_order_items_points
		WHERE
		    wxa43_virtuemart_orders.virtuemart_user_id = '.$this->userDetails->virtuemart_user_id.' AND
		    wxa43_virtuemart_order_items.virtuemart_order_id = wxa43_virtuemart_orders.virtuemart_order_id AND
		    wxa43_virtuemart_order_items_points.virtuemart_order_item_id = wxa43_virtuemart_order_items.virtuemart_order_item_id AND
		    wxa43_virtuemart_order_items_points.points_status = 0
		UNION
		ALL SELECT
		    sum(wxa43_virtuemart_decrease_points.decrease_points) as pointsStat3
		FROM 
		    wxa43_virtuemart_decrease_points
		WHERE
		    wxa43_virtuemart_decrease_points.virtuemart_user_id = '.$this->userDetails->virtuemart_user_id.'
		');
	$points = $db->loadColumn();

	$points_active = ($points[0] == "") ? 0 : $points[0];
	$points_unactive = ($points[1] == "") ? 0 : $points[1];
	$points_decrease = ($points[2] == "") ? 0 : $points[2];
	$points_available = $points_active - $points_decrease;
?>
<div class="telion-form" style="display: block;">
<div class="points">
	      <div class="tln-lk-points-header"><?php echo vmText::_('COM_VIRTUEMART_LOGIN_TLN_YOUR_POINTS') ?></div>
	      <div class="tln-lk-points">
		      <span style="color: #333; font-weight: 700;"><?php echo vmText::_('COM_VIRTUEMART_LOGIN_TLN_ACTIVE_POINTS').$points_available; ?></span> /  
		      <span style="color: #999; font-weight: 400;"><?php echo vmText::_('COM_VIRTUEMART_LOGIN_TLN_UNACTIVE_POINTS').$points_unactive; ?></span> / 
		      <span style="color: #f3adad; font-weight: 400;"><?php echo vmText::_('COM_VIRTUEMART_LOGIN_TLN_UNSET_POINTS').$points_decrease; ?></span>
	      </div>
</div>
<?php } else { ?>
<div class="telion-form">
<?php } ?>
<div class="tln-points-edit">

	<?php echo shopFunctionsF::getLoginForm(false,false); ?>

<!-- Telion #15.25.3 add -->
<div class="telion-form-registration">
<div class="<?php if($user->id != 0){ echo 'telion-form-reg-inside-1'; } else { echo 'telion-form-reg-inside-2'; } ?> ">

<?php if($this->userDetails->virtuemart_user_id==0) {
	echo '<h2>'.vmText::_('COM_VIRTUEMART_YOUR_ACCOUNT_REG').'</h2>';
}
?>


	<!-- Telion #1.9 add. MAILING. New position: "telion-mailing" -->
	<?php  if($this->userDetails->virtuemart_user_id != 0) {
	$db = JFactory::getDBO();
	$db->setQuery('
		SELECT wxa43_acym_user_has_list.status
		FROM wxa43_acym_user, wxa43_acym_user_has_list 
		WHERE wxa43_acym_user_has_list.user_id = wxa43_acym_user.id and wxa43_acym_user_has_list.list_id = 1 and wxa43_acym_user.cms_id = '.$this->userDetails->virtuemart_user_id.'
		');
	$mailingStatus = $db->loadColumn();
	?>
		<div class='telion-mailing' id="mailingMain">
			<jdoc:include type="modules" name="tln-mailing" />
			<div id="tln-mailing-lbl"><span><?php echo vmText::_('COM_TLN_MAILING'); ?></span></div>

			<div id="tln-mailing-chck"><input type="checkbox" id="checkMailing" name="check-mailing" onclick = "
				var mailingChecker = document.querySelector('#checkMailing').checked; 
				if(mailingChecker) {
					document.querySelector('#hiddenMessage').innerHTML = 'Вы успешно подписались';
					document.querySelector('#hiddenMessage').setAttribute('style', 'color: #3fcc3f;');
					document.querySelector('.subbutton').click();
				} else {
					document.querySelector('#hiddenMessage').innerHTML = 'Вы успешно отписались';
					document.querySelector('#hiddenMessage').setAttribute('style', 'color: #f3b3c3');
					document.querySelector('.unsubbutton').click();
				}
				" <?php echo $mailingStatus[0] ? "checked" : "0"; ?>></div>
			<div id="hiddenMessage"></div>
		</div>
	<?php  } ?>



<!-- Telion #15.25.4 add -->
<div class="telion_reg">

<form method="post" id="adminForm" name="userForm" action="<?php echo $url ?>" class="form-validate">
<?php if($this->userDetails->user_is_vendor){ ?>
    <div class="buttonBar-right">
	<button class="button" type="submit" onclick="javascript:return myValidator(userForm, true);" ><?php echo $this->button_lbl ?></button>
	&nbsp;
<button class="button" type="reset" onclick="window.location.href='<?php echo $cancelUrl ?>'" ><?php echo vmText::_('COM_VIRTUEMART_CANCEL'); ?></button></div>
    <?php } ?>                                    
<?php // Loading Templates in Tabs
if($this->userDetails->virtuemart_user_id!=0) {
    $tabarray = array();

    $tabarray['shopper'] = 'COM_VIRTUEMART_SHOPPER_FORM_LBL';

	if(!empty($this->manage_link)) {
		echo $this->manage_link;
	}

	if(!empty($this->add_product_link)) {
		echo $this->add_product_link;
	}

	if($this->userDetails->user_is_vendor){

		$tabarray['vendor'] = 'COM_VIRTUEMART_VENDOR';
	}

    //$tabarray['user'] = 'COM_VIRTUEMART_USER_FORM_TAB_GENERALINFO';
    if (!empty($this->shipto)) {
	    $tabarray['shipto'] = 'COM_VIRTUEMART_USER_FORM_ADD_SHIPTO_LBL';
    }
    if (($_ordcnt = count($this->orderlist)) > 0) {
	    $tabarray['orderlist'] = 'COM_VIRTUEMART_YOUR_ORDERS';
    }

    shopFunctionsF::buildTabs ( $this, $tabarray);

 } else {
    echo $this->loadTemplate ( 'shopper' );
	echo $this->captcha;
	// captcha addition
	/*if(VmConfig::get ('reg_captcha')){
		JHTML::_('behavior.framework');
		JPluginHelper::importPlugin('captcha');
		$dispatcher = JDispatcher::getInstance(); $dispatcher->trigger('onInit','dynamic_recaptcha_1');
		?>
		<div id="dynamic_recaptcha_1"></div>
		<?php
	}*/
 }

// end of captcha addition
?>

<!-- Telion #1.8.2.3 POINTS -->
</div>

<input type="hidden" name="option" value="com_virtuemart" />
<input type="hidden" name="controller" value="user" />
<?php echo JHtml::_( 'form.token' ); ?>
</form>

<!-- Telion #15.25.5 add -->
</div>
</div>
</div>
</div>

