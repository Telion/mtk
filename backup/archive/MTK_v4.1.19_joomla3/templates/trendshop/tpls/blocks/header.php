<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// get params
$sitename  = $this->params->get('sitename');
$slogan    = $this->params->get('slogan', '');
$logotype  = $this->params->get('logotype', 'text');
$logoimage = $logotype == 'image' ? $this->params->get('logoimage', T3Path::getUrl('images/logo.png', '', true)) : '';
$logoimgsm = ($logotype == 'image' && $this->params->get('enable_logoimage_sm', 0)) ? $this->params->get('logoimage_sm', T3Path::getUrl('images/logo-sm.png', '', true)) : false;

if (!$sitename) {
	$sitename = JFactory::getConfig()->get('sitename');
}

?>

<header id="t3-header" class="t3-header">
<?php //include 'color-switcher.php'; ?>
<?php include('header-top.php'); ?>
        
	<?php /* Telion #3.15 add. red Message */  ?>
	<?php /* <div class="title-message">
	    <a href="https://www.mirtelcom.ru/contacts"><span class="title-message-alarm">
		<?php echo vmText::sprintf('COM_VIRTUEMART_TELION_ALARM_MESSAGE_HREF');?></span></a>
	</div> 
	*/ ?>

	<?php /* Telion #3.10 add. red Message ?> */ ?>
	<?php /*<div class="title-message">
	    <a href="https://www.mirtelcom.ru/contacts#message"><span class="title-message-alarm">
		<?php echo vmText::sprintf('COM_VIRTUEMART_TELION_ALARM_MESSAGE_HREF_NEW_YEAR');?></span></a>
	</div> 
	*/ ?>

	<?php /* Telion #3.10 add. red Message */ /*?>
	<div class="title-message">
	    <a href="https://www.mirtelcom.ru/contacts#message"><span class="title-message-alarm">
		<?php echo vmText::sprintf('COM_VIRTUEMART_TELION_ALARM_MESSAGE_HREF_MAY');?></span></a>
	</div> <?php */ ?>


	<div class="container">
	    <div class="row header-inner">

            <?php if ($this->countModules('phone')) : ?>  

                <?php /* Telion #15.19.1 change */?>
		<div class="phone col-lg-3 col-md-3 col-sm-3 col-xs-12 <?php $this->_c('phone') ?>">
		<?php /* base
                <div class="phone col-lg-3 col-md-3 col-sm-4 col-xs-12 <?php $this->_c('phone') ?>"> */ ?>

                        <jdoc:include type="modules" name="<?php $this->_p('phone') ?>" />
                </div>
            <?php endif ?>

            <?php /* Telion #15.19.2 change */?>
            <div class="logo col-lg-4 col-md-4 col-sm-4 col-xs-12">
	    <?php /* base
            <div class="logo col-lg-6 col-md-6 col-sm-4 col-xs-12"> */ ?>
            
	            <div class="logo-<?php echo $logotype, ($logoimgsm ? ' logo-control' : '') ?>">
	                <a href="<?php echo JUri::base() ?>" title="<?php echo strip_tags($sitename) ?>">
	                    <?php if($logotype == 'image'): ?>
	                        <img class="logo-img" src="<?php echo JUri::base(true) . '/' . $logoimage ?>" alt="<?php echo strip_tags($sitename) ?>" />
	                    <?php endif ?>
	                </a>
	            </div>
	        </div>
	        
	        <?php /* Telion #15.19.3 change */ ?>
	        <div class="search-and-cart col-lg-5 col-md-5 col-sm-5 col-xs-8">
		<?php /* base
	        <div class="search-and-cart col-lg-3 col-md-3 col-sm-4 col-xs-8"> */ ?>


		<?php /* Telion #15.19 remove ?>
		if ($this->countModules('search')) : ?>  
                <div class="block-search <?php $this->_c('search') ?>">
                <span class="dropdown">
                    <a data-toggle="dropdown">
                        <i class="fa fa-search search-icon" aria-hidden="true"></i>
                    </a> */ ?>

	        <?php /* Telion #15.19.4 add */?>
                <div class="search-dropdown-menu">

                    <div class="dropdown-menu dropdown-menu-right">
                        <jdoc:include type="modules" name="<?php $this->_p('search') ?>" />
                    </div>  
                    </span>     
                </div>

		<?php /* Telion #15.19.5 remove */?>
		<?php // endif ?>
                
                <?php if ($this->countModules('cart')) : ?>  
                <div class="cart <?php $this->_c('cart') ?>">
                        <jdoc:include type="modules" name="<?php $this->_p('cart') ?>" />
                </div>
                <?php endif ?>
	        </div>
	    
	    </div>
	</div>
</header>