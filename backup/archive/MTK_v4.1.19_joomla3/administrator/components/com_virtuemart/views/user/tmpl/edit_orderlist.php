<?php
/**
*
* User details, Orderlist
*
* @package	VirtueMart
* @subpackage User
* @author Oscar van Eijk
* @link https://virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: edit_orderlist.php 9821 2018-04-16 18:04:39Z Milbo $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

/* Telion #1.8.8.1 POINTS */
if (isset($this->orderlist[0])) {
	if (in_array(7, explode(',', $this->orderlist[0]->user_shoppergroups))) {
		$db = JFactory::getDBO();
		$db->setQuery('
			SELECT 
				wxa43_virtuemart_orders.virtuemart_order_id, 
				wxa43_virtuemart_order_items_points.points_status, 
				sum(wxa43_virtuemart_order_items_points.increase_points) as pointsStat 
			FROM 
				wxa43_virtuemart_orders, 
				wxa43_virtuemart_order_items, 
				wxa43_virtuemart_order_items_points 
			WHERE 
				wxa43_virtuemart_order_items.virtuemart_order_id = wxa43_virtuemart_orders.virtuemart_order_id AND 
				wxa43_virtuemart_order_items_points.virtuemart_order_item_id = wxa43_virtuemart_order_items.virtuemart_order_item_id 
			GROUP BY wxa43_virtuemart_orders.virtuemart_order_id
			');
		$arr_points = $db->loadObjectList('virtuemart_order_id');
/*		$summ_points = 0;
		foreach ($this->orderlist as $i => $row) {
			$summ_points += round($row->order_subtotal * 0.02);
		}*/
	}
}

?>

<div id="editcell">
	<table class="adminlist table" cellspacing="0" cellpadding="0">
	<thead>
	<tr>

		<th>
			<?php echo vmText::_('COM_VIRTUEMART_ORDER_LIST_NUMBER'); ?>
		</th>
		<th>
			<?php echo vmText::_('COM_VIRTUEMART_PRINT_VIEW'); ?>
		</th>
		<th>
			<?php echo vmText::_('COM_VIRTUEMART_ORDER_CDATE'); ?>
		</th>
		<th>
			<?php echo vmText::_('COM_VIRTUEMART_ORDER_LIST_MDATE'); ?>
		</th>
		<th>
			<?php echo vmText::_('COM_VIRTUEMART_STATUS'); ?>
		</th>
		<th>
			<?php echo vmText::_('COM_VIRTUEMART_TOTAL'); ?>
		</th>

		<?php /* Telion #1.8.8.2 POINTS */
		if (isset($this->orderlist[0])) {
			if (in_array(7, explode(',', $this->orderlist[0]->user_shoppergroups))) {
			?>
				<th>
					<?php echo vmText::_('COM_VIRTUEMART_TLN_POINTS'); ?>
				</th>
			<?php }
		} ?>

	</thead>
	<?php
		$k = 0;
		$n = 1;
		foreach ($this->orderlist as $i => $row) {
			$editlink = JROUTE::_('index.php?option=com_virtuemart&view=orders&task=edit&virtuemart_order_id=' . $row->virtuemart_order_id);
			?>
			<tr class="row<?php echo $k ; ?>">
				 
				<td align="left">
					<a href="<?php echo $editlink; ?>"><?php echo $row->order_number; ?></a>
				</td>
				<td align="center">
					<?php // echo $print_link; ?>
				</td>
				<td align="left">
					<?php echo vmJsApi::date($row->created_on,'LC2',true); ?>
				</td>
				<td align="left">
					<?php echo vmJsApi::date($row->modified_on,'LC2',true); ?>
				</td>
				<td align="left">
					<?php echo shopFunctionsF::getOrderStatusName($row->order_status); ?>
				</td>
				<td align="left">
					<?php echo $this->currency->priceDisplay($row->order_total); ?>
				</td>

				<?php /* Telion #1.8.8.3 POINTS */
				if (isset($this->orderlist[0])) {
					if (in_array(7, explode(',', $this->orderlist[0]->user_shoppergroups))) {
					?>
						<td>
							<?php 
								if (isset($arr_points[$row->virtuemart_order_id])) 
								{
									if ($arr_points[$row->virtuemart_order_id]->points_status == 0)
										echo '<span style="color: #999">'.$arr_points[$row->virtuemart_order_id]->pointsStat.vmText::_('COM_VIRTUEMART_TLN_UNACTIVE_POINTS').'</span>';
									else if ($arr_points[$row->virtuemart_order_id]->points_status == 1)
										echo '<span style="color: #333; font-weight: 700;">'.$arr_points[$row->virtuemart_order_id]->pointsStat.vmText::_('COM_VIRTUEMART_TLN_ACTIVE_POINTS').'</span>';
									else
										echo '<span style="color: #fbb; text-decoration: line-through;">'.$arr_points[$row->virtuemart_order_id]->pointsStat.vmText::_('COM_VIRTUEMART_TLN_UNSET_POINTS').'</span>';
								}
							?>
						</td>
					<?php }
				} ?>

			</tr>
	<?php
			$k = 1 - $k;
		}
	?>
	</table>
</div>
