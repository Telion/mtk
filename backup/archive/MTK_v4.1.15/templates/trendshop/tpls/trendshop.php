<?php
/** 
 *------------------------------------------------------------------------------
 * @package       T3 Framework for Joomla!
 *------------------------------------------------------------------------------
 * @copyright     Copyright (C) 2004-2013 JoomlArt.com. All Rights Reserved.
 * @license       GNU General Public License version 2 or later; see LICENSE.txt
 * @authors       JoomlArt, JoomlaBamboo, (contribute to this project at github 
 *                & Google group to become co-author)
 * @Google group: https://groups.google.com/forum/#!forum/t3fw
 * @Link:         http://t3-framework.org 
 *------------------------------------------------------------------------------
 */


defined('_JEXEC') or die;
?>

<!DOCTYPE html>

<!-- Telion 3.11.2 change -->
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>"
	  class='<jdoc:include type="pageclass" />' prefix="og: http://ogp.me/ns#">
<!-- base
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>"
	  class='<jdoc:include type="pageclass" />'> -->

<head>

	<!-- Telion 3.11.3 add -->
	<?php 
	$uri = JFactory::getURI();
	echo '<meta property="og:image" content="'.$uri.'/images/logo-viber.png" />'
	?>

	<jdoc:include type="head" />
	<?php $this->loadBlock('head') ?>

	<!-- Telion #3.4 add. Disabled MooTools -->
	<?php 
	/*unset($this->_scripts[$this->baseurl.'/media/system/js/mootools-core.js'], 
	$this->_scripts[$this->baseurl.'/media/system/js/mootools-more.js'],
	$this->_scripts[$this->baseurl.'/media/system/js/core.js'],
	$this->_scripts[$this->baseurl.'/media/system/js/caption.js']);*/
	?>

</head>

<body>

<div class="t3-wrapper"> <!-- Need this wrapper for off-canvas menu. Remove if you don't use of-canvas -->

  <?php
    $manufacturerId = JRequest::getVar('virtuemart_manufacturer_id');
    ?>    
      
  <?php $this->loadBlock('header') ?>

	<script>
	/* Telion #3.3 add. Update css version on user pc. Replace custom.css to custom.css?208 */
	var arr = document.getElementsByTagName("link");
	for (var x = 0; x < arr.length; x++){
		var str = arr[x].href;
		if (str.includes('custom.css')){
			arr[x].href += '?460';
		}
	}
	</script>

  <?php $this->loadBlock('mainnav') ?>
  
  <?php if(empty($manufacturerId)): ?>
  <?php $this->loadBlock('breadcrumbs') ?>

  <?php $this->loadBlock('slider') ?>

  <?php $this->loadBlock('home-1') ?>
  
  <?php $this->loadBlock('top-banner') ?>
  
  <?php $this->loadBlock('tab-module') ?>
  
  <?php $this->loadBlock('middle-banner') ?>
  
  <?php $this->loadBlock('home-2') ?>
  <?php endif; ?>
  

  <?php $this->loadBlock('mainbody') ?>

	<!-- Telion #3.2 add -->
	<script>
	if(document.getElementsByClassName('telion-main-page-h1').length == true){
		/*document.getElementsByClassName("category-view")[0].style.display = "none"; 
		document.getElementsByTagName("h3")[0].style.display = "none";*/

		/* Telion #3.3 add */
		var telHideSkud = document.getElementsByClassName('t3-megamenu')[0];
		telHideSkud.getElementsByTagName('li')[90].style.display = "none";
	}
	</script>
  
  <?php if(empty($manufacturerId)): ?>
  <?php $this->loadBlock('home-3') ?>
  
  <?php $this->loadBlock('bottom-banner') ?>

  <?php $this->loadBlock('home-4') ?>
  <?php endif; ?>

  <?php $this->loadBlock('footer') ?>

</div>

</body>

</html>