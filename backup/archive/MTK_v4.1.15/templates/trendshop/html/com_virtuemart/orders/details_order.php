<?php
/**
 *
 * Order detail view
 *
 * @package	VirtueMart
 * @subpackage Orders
 * @author Oscar van Eijk, Valerie Isaksen
 * @link https://virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: details_order.php 9931 2018-09-18 15:57:50Z Milbo $
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

?>

<!-- Telion #10.1.1 change -->
<table class="telion-details-order-table" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
	<td class="orders-key-left"><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PO_NUMBER') ?></td>
	<td class="orders-key-right" align="left">
	    <?php echo $this->orderdetails['details']['BT']->order_number.'<span style="color:#ccc5b9">'
		.vmText::_('COM_VIRTUEMART_ORDER_OT')
		.vmJsApi::date($this->orderdetails['details']['BT']->created_on, 'LC4', true).'</span>'; ?>
<?php /* base
<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
	<td   class="orders-key"><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PO_NUMBER') ?></td>
	<td class="orders-key" align="left">
	    <?php echo $this->orderdetails['details']['BT']->order_number; */ ?>

        </td>
    </tr>

    <!-- Telion #10.1.3 remove -->
    <?php /* <tr>
	<td class=""><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PO_DATE') ?></td>
	<td align="left"><?php echo vmJsApi::date($this->orderdetails['details']['BT']->created_on, 'LC4', true); ?></td>
    </tr> */ ?>

    <tr>
        <td class=""><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PO_STATUS') ?></td>

	<!-- Telion #10.1.4 change -->
	<td align="left"><?php echo $this->orderstatuses[$this->orderdetails['details']['BT']->order_status]
		.'<span style="color:#ccc5b9"> ('
		.vmJsApi::date($this->orderdetails['details']['BT']->modified_on, 'LC4', true).')</span>'; ?>
	<?php /* base
	<td align="left"><?php echo $this->orderstatuses[$this->orderdetails['details']['BT']->order_status]; ?> */ ?>

	</td>
    </tr>

    <!-- Telion #10.1.5 remove -->
    <?php /* <tr>
	<td class=""><?php echo vmText::_('COM_VIRTUEMART_LAST_UPDATED') ?></td>
	<td align="left"><?php echo vmJsApi::date($this->orderdetails['details']['BT']->modified_on, 'LC4', true); ?></td>
    </tr> */ ?>

    <tr>
        <td class=""><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_SHIPMENT_LBL') ?></td>
        <td align="left"><?php
			echo $this->shipment_name;
			?></td>
    </tr>
    <tr>
        <td class=""><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PAYMENT_LBL') ?></td>
        <td align="left"><?php echo $this->payment_name; ?>
        </td>
    </tr>

<!-- Telion #10.1.6 add. �������� ������� �� ��� ������� -->
</table>
<table class="telion-details-order-table" width="100%" cellspacing="0" cellpadding="0" border="0">


    <!-- Telion #10.1.7 remove. ������� ���� ���� -->
    <?php /* <tr>
        <td><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_CUSTOMER_NOTE') ?></td>
        <td valign="top" align="left" width="50%"><?php echo $this->orderdetails['details']['BT']->customer_note; ?></td>
    </tr>
     <tr>
	<td class="orders-key"><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_TOTAL') ?></td>
	<td class="orders-key" align="left">
		<?php echo $this->currency->priceDisplay($this->orderdetails['details']['BT']->order_total, $this->user_currency_id); ?>
		</td>
    </tr> 
     <tr>
	<td colspan="2"> &nbsp;</td>
    </tr> */ ?>

    <tr>
	<!-- Telion #10.1.9 change -->
	<td class="orders-key-left" valign="top"><strong><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_SHIP_TO_LBL') ?>
		</strong></td>
	    <?php
	    foreach ($this->userfields['fields'] as $field) {

		if (!empty($field['value']) and $field['name'] != 'tos') {

		    echo '<tr><td class="key">' . $field['title'] . '</td>'
		    . '<td>' . $field['value'] . '</td></tr>';
		}
	    }
	    ?>
	<td class="telion-none" valign="top"><strong>
	    <?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_SHIP_TO_LBL') ?></strong><br/>
	    <?php
	    foreach ($this->shipmentfields['fields'] as $field) {
		if (!empty($field['value'])) {
		    echo '<tr><td class="key">' . $field['title'] . '</td>'
		    . '<td>' . $field['value'] . '</td></tr>';
		}
	    }
	    ?>
	</td>
	<?php /* base
	    <td valign="top"><strong>
	    <?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_BILL_TO_LBL') ?></strong> <br/>
	    <table border="0"><?php
	    foreach ($this->userfields['fields'] as $field) {
		if (!empty($field['value'])) {
		    echo '<tr><td class="key">' . $field['title'] . '</td>'
		    . '<td>' . $field['value'] . '</td></tr>';
		}
	    }
	    ?></table>
	</td>
	<td valign="top" ><strong>
	    <?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_SHIP_TO_LBL') ?></strong><br/>
	    <table border="0"><?php
	    foreach ($this->shipmentfields['fields'] as $field) {
		if (!empty($field['value'])) {
		    echo '<tr><td class="key">' . $field['title'] . '</td>'
		    . '<td>' . $field['value'] . '</td></tr>';
		}
	    }
	    ?></table>
	</td> */ ?>

    </tr>

    <tr>
        <td><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_CUSTOMER_NOTE') ?></td>
        <td valign="top" align="left"><?php echo $this->orderdetails['details']['BT']->customer_note; ?></td>
    </tr>
</table>

<!-- Telion #10.1.10 add. ��� ��������� ������ -->
<div class="telion-details-order-div-items">
    <hr class="telion-details-order-hr">
    <table class="telion-details-order-table" width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td class="orders-key-left" valign="top">
		<?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_TOTAL_SKIDKA') ?></td>
            <td valign="top" align="left">
		<?php echo $this->currency->priceDisplay($this->orderdetails['details']['BT']->order_billDiscountAmount, $this->user_currency_id); ?></td>
        </tr>
        <tr>
            <td><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_SHIPPING') ?></td>
            <td valign="top" align="left">
		<?php echo $this->currency->priceDisplay($this->orderdetails['details']['BT']->order_shipment+ $this->orderdetails['details']['BT']->order_shipment_tax, $this->user_currency_id); ?>
		</td>
        </tr>
        <tr>
            <td><span style="font-weight:bold;"><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_TOTAL') ?></span></td>
            <td valign="top" align="left"><span style="font-weight:bold;">
		<?php echo $this->currency->priceDisplay($this->orderdetails['details']['BT']->order_total, $this->user_currency_id); ?></span></td>
        </tr>
    </table>
</div>
