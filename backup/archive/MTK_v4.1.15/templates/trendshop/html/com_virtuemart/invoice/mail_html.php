<?php
/**
 *
 * Main wrapper for the order email
 * @author Spyros Petrakis
 * @link http://www.virtuemarttemplates.eu
 * @copyright Copyright (c) 2015 Spyros Petrakis. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 *
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title></title>
<style type="text/css">
.ReadMsgBody {width: 100%;}
body {}
.ExternalClass table {border-collapse:separate;}
a, a:link, a:visited {text-decoration: none; color: #00788a}
a:hover {text-decoration: underline;}
h2,h2 a,h2 a:visited,h3,h3 a,h3 a:visited,h4,h5,h6,.t_cht {color:#000 !important}
.ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td {line-height: 100%}
.ExternalClass {width: 100%;}
.ExternalClass * {line-height: 100%}
span.yshortcuts { color:#000; background-color:none; border:none;}
span.yshortcuts:hover,
span.yshortcuts:active,
span.yshortcuts:focus {color:#000; background-color:none; border:none;}
</style>
</head>
<body style="margin: 0; padding: 0;">
<table width="100%" cellpadding="10" cellspacing="0" border="0" bgcolor="#CCCCCC" align="center">
<tr>
	<!-- Telion 11.4.1 add. Полоска с градиентом в шапке письма покупателю -->
	<th style="background: linear-gradient(135deg, #6078ea -20%,#17ead9 120%);">&nbsp;</th>
</tr>
<tr>
<td>

<!-- Telion 11.4.2 change -->
<table width="600" align="center" cellpadding="10" cellspacing="0" border="0" bgcolor="#fff" 
	style="border-collapse: collapse; font-family: Arial, Helvetica, sans-serif; 
	font-size: 12px; margin: 0 auto 10px; border: 2px solid #777;">
<?php /* base
<table width="600" align="center" cellpadding="10" cellspacing="0" border="0" bgcolor="#FFFFFF" 
	style="border-collapse: collapse; font-family: Arial, Helvetica, sans-serif; 
	font-size: 12px; margin: 0 auto;"> */ ?>

<!-- Telion #11.4.3 remove -->
<?php /*
// Shop desc for shopper and vendor
if ($this->recipient == 'shopper') { */ ?>

<tr>
<td>
<?php echo $this->loadTemplate('header'); ?>
</td>
</tr>

<!-- Telion #11.4.4 remove -->
<?php /* } */ ?>

<!-- Telion #11.5.1 add. Если статус заказа "Выдан", то отправляем особенное сообщение -->
<?php 
$status = $this->orderstatuses[$this->orderDetails['details']['BT']->order_status];
/* Письмо со статусом заказа "Доставлен" */
if ($status == "Выдан") {
?>

<tr>
  <td style="padding-top: 0;">
    <div style="margin: auto;">
      <p style="font-size: 16px; text-align: center; font-weight: bold; padding: 0 20px;">
        Здравствуйте!</p>
      <p style="font-size: 14px; margin: 0; padding: 0 20px; text-align: justify;">
        Благодарим за покупку в нашем магазине.</p>
      <p style="font-size: 14px; margin: 0; padding: 0 20px; text-align: justify;">
        Номер вашего заказа <b><?php echo $this->orderDetails['details']['BT']->order_number; ?></b>.</p>
      <p style="font-size: 14px; margin: 0; padding: 0 20px; text-align: justify;">
        Мы дорожим оказанным доверием.</p>
    </div>
  </td>
</tr>
<tr>
  <td>
    <p style="font-size: 14px; margin: 0; padding: 0 20px; text-align: justify;">
      Понравился наш сервис? Будем признательны за обратную связь в виде отзыва на
      <a href="https://tyumen.flamp.ru/addreview/1830116630019132">2Гис</a>.
      Опубликуйте отзыв, укажите номер заказа и получите скидку 3% на следующую покупку.</p>
  </td>
</tr>
<tr>
  <td>
    <p style="font-size: 14px; margin: 0; padding: 0 20px; text-align: justify;">
      С наилучшими пожеланиями команда <a href="https://mirtelcom.ru">
      Мир Телекоммуникаций</a> - Тюмень.</p><br>
  </td>
</tr>

<?php
}
/* Письма с другими статусами заказа */
else {
?>

<tr>
<td>
<?php
// Message for shopper or vendor
echo $this->loadTemplate($this->recipient);
?>
</td>
</tr>
<tr>
<td>
<?php
// render shipto billto adresses
echo $this->loadTemplate('shopperaddresses');
?>
</td>
</tr>
<tr>
<td>
<?php
// render price list
echo $this->loadTemplate('pricelist');
?>
</td>
</tr>
<tr>
<td>
<?php
// end of mail
echo $this->loadTemplate('footer');
?>
</td>
</tr>
<?php } ?>
</table>
</td>
</tr>
</table>

<style type="text/css">
.ReadMsgBody {width: 100%;}
body {}
.ExternalClass table {border-collapse:separate;}
a, a:link, a:visited {text-decoration: none; color: #00788a}
a:hover {text-decoration: underline;}
h2,h2 a,h2 a:visited,h3,h3 a,h3 a:visited,h4,h5,h6,.t_cht {color:#000 !important}
.ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td {line-height: 100%}
.ExternalClass {width: 100%;}
.ExternalClass * {line-height: 100%}
span.yshortcuts { color:#000; background-color:none; border:none;}
span.yshortcuts:hover,
span.yshortcuts:active,
span.yshortcuts:focus {color:#000; background-color:none; border:none;}
</style>
</body>
</html>