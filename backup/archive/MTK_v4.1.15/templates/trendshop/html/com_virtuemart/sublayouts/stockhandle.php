<?php
$product = $viewData['product'];
// Availability
$stockhandle = VmConfig::get('stockhandle', 'none');
$product_available_date = substr($product->product_available_date,0,10);
$current_date = date("Y-m-d");

/* Telion #7.1 add */
$edit_result = $product->product_in_stock - $product->product_ordered;
$cur_date = date('d-m-Y'); // ������� ����
$yest_date = date('d.m.Y', strtotime('-1 day', strtotime($cur_date))); // ��������� ����
// ���� ���� yesterday
$yd_d = substr($yest_date,0,2); // yesterday_day
$yd_m = substr($yest_date,3,2); // yesterday_month
$yd_y = substr($yest_date,6,4); // yesterday_year
// ���� ���� ������ �� ��������, �� ��������� �� �� �������
$d_week = date("w", mktime(0,0,0,$yd_m,$yd_d,$yd_y)); // week day - yesterday
if ($d_week==6) {$yest_date = date('d.m.Y', strtotime('-1 day', strtotime($yest_date)));} /* � ����������� */
if ($d_week==0) {$yest_date = date('d.m.Y', strtotime('-2 day', strtotime($yest_date)));} /* � ����������� */
// ���� ���� yesterday
$yd_d = substr($yest_date,0,2); // yesterday_day
$yd_m = substr($yest_date,3,2); // yesterday_month
$yd_y = substr($yest_date,6,4); // yesterday_year
// ���������� ��������� (��� ������������) // ���� ����� ���� �� 01 �� 10 ����� 01 ������, �� ���� 31.12.20��
$d_ly = date("Y") - 1; // ������� ���
if ($yd_m == '01' and $yd_d >= '01' and $yd_d <= '08'){
	$yest_date = '31.12.'.$d_ly;
}
// ������ ��������� (��� ������������)
while (($yd_m == '02' and $yd_d == '23') or ($yd_m == '03' and $yd_d == '08') or ($yd_m == '05' and ($yd_d == '01' or $yd_d == '02' or $yd_d == '09')) or ($yd_m == '06' and $yd_d == '12') or ($yd_m == '11' and $yd_d == '04')) {
	$yest_date = date('d.m.Y', strtotime('-1 day', strtotime($yd_d.'-'.$yd_m.'-'.$yd_y)));
	// ���� ���� yesterday
	$yd_d = substr($yest_date,0,2); // yesterday_day
	$yd_m = substr($yest_date,3,2); // yesterday_month
	$yd_y = substr($yest_date,6,4); // yesterday_year
}
// ���� ���� ������ �� ��������, �� ��������� �� �� �������
$d_week = date("w", mktime(0,0,0,$yd_m,$yd_d,$yd_y)); // week day - yesterday
if ($d_week==6) {$yest_date = date('d.m.Y', strtotime('-1 day', strtotime($yest_date)));} /* � ����������� */
if ($d_week==0) {$yest_date = date('d.m.Y', strtotime('-2 day', strtotime($yest_date)));} /* � ����������� */

// label stock
if (($product->product_in_stock - $product->product_ordered) < 1) { ?>
    <div class="nostock text-warning"><?php echo JText::sprintf('COM_VIRTUEMART_PRODUCT_NOSTOCK'); ?></div>
<?php } else { ?>

    <!-- Telion #7.2 change. ������� � ������: 15 �� -->
    <div class="nostock text-success"><?php echo JText::sprintf('������� � ������: '.$edit_result.' '
	.JText::sprintf('COM_VIRTUEMART_UNIT_SYMBOL_'.$product->product_unit)); ?></div>
    <?php /* base
    <div class="nostock text-success"><?php echo JText::sprintf('COM_VIRTUEMART_PRODUCT_STOCK'); ?></div> */ ?>

<?php
}
?>