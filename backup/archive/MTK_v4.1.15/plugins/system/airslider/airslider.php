<?php
/**
 /*------------------------------------------------------------------------
 # Airslider 1.0
 # ------------------------------------------------------------------------
 # (C) 2016 Все права защищены.
 # Лицензия http://www.gnu.org/licenses/gpl-3.0.html GNU/GPLv3
 # Автор: Vladimir Pronin
 # Сайт: http://virtuemart.su
 -------------------------------------------------------------------------*/

// No direct access

defined('_JEXEC') or die('Restricted access..');


jimport('joomla.plugin.plugin');

class plgSystemAirslider extends JPlugin {
	public function onBeforeRender() {
        if (JFactory::getApplication()->isAdmin()) {
            return;
        }
		$doc = JFactory::getDocument();
        $view = JRequest::getVar('view');
		$option = JRequest::getVar('option');
        
        // get parameters
        $arrow = $this->params->get('arrow') ? "true" : "false"; // стрелки
        $dots = $this->params->get('dots') ? "true" : "false"; // точки
        $height = $this->params->get('height', ''); // высота

	/* Telion 15.8 add */
	$height = '245px';
            
		if ($view == 'category' && $option == 'com_virtuemart') {
            // Подключение стилей
            $doc->addStyleSheet(JURI::base().'/plugins/system/airslider/media/airslider.css');
            
            $style = '.vm-trumb-slider,.vm-trumb-slider .slick-slide{height:'.$height.'}.vm-trumb-slider .slick-slide img{max-height:'.$height.'}'."\n";
            $doc->addStyleDeclaration($style);
            
            // Подключение скриптов
            $doc->addScript(JURI::base().'/plugins/system/airslider/media/airslider.min.js');
            
            $js = "jQuery(document).ready(function(){jQuery('.vm-trumb-slider').slick({dots:".$dots.",arrows:".$arrow.",lazyLoad: 'ondemand',});});";
            $doc->addScriptDeclaration($js);
		}
	}	
}
?>