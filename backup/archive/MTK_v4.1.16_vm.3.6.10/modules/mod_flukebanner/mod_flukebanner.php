<?php

defined('_JEXEC') or die;

// Подключаем стили и скрипты js
$doc = JFactory::getDocument();
$doc->addStyleSheet('/modules/mod_flukebanner/css/style.css');
//$doc->addScript('/modules/mod_flukebanner/js/show.js');

// Подключаем helper.php
require_once __DIR__ . '/helper.php';

// Загрузка параметров модуля из объекта $params
$num_users = $params->get('num_users', 3);
$moduleclass_sfx = $params->get('moduleclass_sfx');

// Загрузка данных в объект из helper.php
//$users = ModFlukeBanner::getUsers($params);

// Подключение шаблона для вывода данных в HTML
require JModuleHelper::getLayoutPath('mod_flukebanner', 'default');

?>
