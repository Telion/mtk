<?php

defined('_JEXEC') or die;

?>

<?php
$base = JURI::base();
echo '<a class="flukebanner" href="'.$base.'menu-service/dimensions">';
?>

<div class="flukebanner-main">
	<p class="flukebanner-header">Тестирование вашей сети</p>

	<?php

	/* Разные картинки при обновлении страницы */
	$session = JFactory::getSession();

	if (!$session->get('banner_num')){
		$session->set('banner_num', '1');
	}
	$banner_num = $session->get('banner_num') + 1;
	if ($banner_num == 6) $banner_num = 1;

	$session->set('banner_num', $banner_num);

	if (($banner_num == 1) or ($banner_num == 2)){
		$border = "";
	} else {
		$border = "no";
	}
	echo '<div class="flukebanner-img-inside-'.$border.'border"><img class="flukebanner-img" src="'.$base.'images/banners/flukebanner/banner'.$banner_num.'.jpg"></div>';
	?>

	<p class="flukebanner-footer">Тестирование оптического волокна и витой пары</p>
	<p class="flukebanner-footer">Официальный отчет</p>
	<p class="flukebanner-footer">Поверенный кабельный анализатор Fluke</p>

</div>
</a>