<?php
/**
 *
 * Layout for the order email
 * shows the chosen adresses of the shopper
 * taken from the stored order
 *
 * @package	VirtueMart
 * @subpackage Order
 * @author Max Milbers,   Valerie Isaksen
 *
 * @link https://virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 *
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
?>
<table align="center" width="580" class="html-email" cellspacing="0" cellpadding="10" border="0" style="border-collapse: collapse; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
  <tr>

    <!-- Telion #11.9.1 change -->
    <th width="580" bgcolor="#EEEEEE" style="border: 1px solid #CCCCCC;">
    <?php /* base
    <th width="290" bgcolor="#EEEEEE" style="border: 1px solid #CCCCCC;"> */ ?>

      <?php echo vmText::_('COM_VIRTUEMART_USER_FORM_BILLTO_LBL'); ?>
    </th>

    <!-- Telion #11.9.2 remove -->
    <?php /* base
    <th width="290" bgcolor="#EEEEEE" style="border: 1px solid #CCCCCC;">
    <?php echo vmText::_('COM_VIRTUEMART_USER_FORM_SHIPTO_LBL'); ?>
    </th> */ ?>

  </tr>
  <tr>

    <!-- Telion #11.9.3 change -->
    <td valign="top" width="580" style="border: 1px solid #CCCCCC;">
    <?php /* base
    <td valign="top" width="290" style="border: 1px solid #CCCCCC;"> */ ?>

      <table style="border-collapse: collapse; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
        <?php
        foreach ($this->userfields['fields'] as $field) {

          /* Telion 11.9.4 change */
          if (!empty($field['value']) and $field['name'] != 'tos') {
          /* base
          if (!empty($field['value'])) { */

          ?>
            <tr>

              <!-- Telion #11.9.5 change -->
              <td width="580">
              <?php /* base
              <td width="290"> */ ?>

                <span class="values vm2<?php echo '-' . $field['name'] ?>" ><?php echo $field['value'] ?></span>
              </td>
            </tr>
            <?php 
          }
        } ?>
      </table>
    </td>
  </tr>

  <!-- Telion #11.9.6 add -->
  <?php if(!empty($this->orderDetails['details']['BT']->customer_note)){ ?>
    <tr>
      <td width="580" colspan="3" style="border: 1px solid #CCCCCC;">
        <?php echo vmText::sprintf('COM_VIRTUEMART_MAIL_SHOPPER_QUESTION',nl2br($this->orderDetails['details']['BT']->customer_note)) ?>
      </td>
    </tr>
  <?php } ?>

<!-- Telion #11.9.7 remove -->
<?php /* <td valign="top" width="290" style="border: 1px solid #CCCCCC;">
<table style="border-collapse: collapse; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
<?php
foreach ($this->shipmentfields['fields'] as $field) {
if (!empty($field['value'])) {
?>
<tr>
<td width="290">
<span class="values vm2<?php echo '-' . $field['name'] ?>" ><?php echo $field['value'] ?></span>
</td>
</tr>
<?php
  }
}
?>
</table>
</td>
</tr> */ ?>

</table>