<?php
/**
*
* User details, Orderlist
*
* @package	VirtueMart
* @subpackage User
* @author Oscar van Eijk
* @link https://virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: edit_orderlist.php 9413 2017-01-04 17:20:58Z Milbo $
*/

/* TELION #1.8.3.1 POINTS */
if (in_array(7, $this->userDetails->shopper_groups)) {
	$db = JFactory::getDBO();
	$db->setQuery('
		SELECT 
		    wxa43_virtuemart_order_items.virtuemart_order_id,
		    sum(wxa43_virtuemart_order_items_points.increase_points) as points
		FROM 
		    wxa43_virtuemart_order_items,
		    wxa43_virtuemart_order_items_points,
        	    wxa43_virtuemart_orders
		WHERE
		    wxa43_virtuemart_order_items.virtuemart_order_item_id = wxa43_virtuemart_order_items_points.virtuemart_order_item_id AND
	            wxa43_virtuemart_orders.virtuemart_order_id = wxa43_virtuemart_order_items.virtuemart_order_id AND
        	    wxa43_virtuemart_orders.virtuemart_user_id = '.$this->orderlist[0]->virtuemart_user_id.' 
		GROUP BY wxa43_virtuemart_order_items.virtuemart_order_id');
	$arr_result = $db->loadAssocList('virtuemart_order_id');

	$arr_points = array();
	foreach ($arr_result as $key => $value) {
		$arr_points[$key] = $value["points"];
	}
}

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access'); ?>

<div id="editcell">
	<table class="adminlist">
	<thead>
	<tr>

		<!-- Telion #15.26.1 add. ������� ���� ������ -->
		<th class="list-order-number">
			<?php echo vmText::_('COM_VIRTUEMART_ORDER_LIST_ORDER_NUMBER'); ?>
		</th>
		<th class="list-cdate">
			<?php echo vmText::_('COM_VIRTUEMART_ORDER_LIST_CDATE'); ?>
		</th>
		<th class="list-mdate">
			<?php echo vmText::_('COM_VIRTUEMART_ORDER_LIST_MDATE'); ?>
		</th>
		<th class="list-status">
			<?php echo vmText::_('COM_VIRTUEMART_ORDER_LIST_STATUS'); ?>
		</th>
		<th class="list-total">
			<?php echo vmText::_('COM_VIRTUEMART_ORDER_LIST_TOTAL'); ?>
		</th>

		<!-- Telion #1.8.3.2 POINTS -->
		<?php if (in_array(7, $this->userDetails->shopper_groups)) { ?>
			<th class="list-points"> 
				<?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_TLN_POINTS'); ?>
			</th>
		<?php } ?>

	</tr>
	</thead>
	<?php
		$k = 0;
		foreach ($this->orderlist as $i => $row) {
			$editlink = JRoute::_('index.php?option=com_virtuemart&view=orders&layout=details&order_number=' . $row->order_number);
			?>
			<tr class="<?php echo "row$k"; ?>">

				<!-- Telion #15.26.2 add. ������� ���� ������ -->
				<td class="list-order-number" align="left">
					<a href="<?php echo $editlink; ?>" rel="nofollow"><?php echo $row->order_number; ?></a>
				</td>
				<td class="list-cdate" align="left">
					<?php echo JHtml::_('date', $row->created_on); ?>
				</td>
				<td class="list-mdate" align="left">
					<?php echo JHtml::_('date', $row->modified_on); ?>
				</td>
				<td class="list-status" align="left">
					<?php echo ShopFunctionsF::getOrderStatusName($row->order_status); ?>
				</td>
				<td class="list-total" align="left">
					<?php echo $this->currency->priceDisplay($row->order_total); ?>
				</td>

				<!-- Telion #1.8.3.3 POINTS -->
				<?php if (in_array(7, $this->userDetails->shopper_groups)) { ?>
					<td class="list-points" align="left"> 
						<?php 
						switch ($row->order_status) {
							case "D":
							case "S":
							case "F": {$status = vmText::_('COM_VIRTUEMART_ORDER_PRINT_TLN_POINTS_STATUS_ACTIVE'); $status_color = '#333; font-weight: 700;'; break;}
							case "R":
							case "X": {$status = vmText::_('COM_VIRTUEMART_ORDER_PRINT_TLN_POINTS_STATUS_UNSET'); $status_color = '#fbb; text-decoration: line-through;'; break;}
							case "P":
							case "C":
							case "U": {$status = vmText::_('COM_VIRTUEMART_ORDER_PRINT_TLN_POINTS_STATUS_UNACTIVE'); $status_color = '#999;'; break;}
						}
						if (isset($arr_points[$row->virtuemart_order_id])) echo '<span style="color: '.$status_color.'">'.$arr_points[$row->virtuemart_order_id].$status.'</span>';
						else echo "-";
						?>
					</td>
				<?php } ?>

			</tr>
	<?php
			$k = 1 - $k;
		}
	?>
	</table>
</div>
