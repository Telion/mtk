<?php
defined('_JEXEC') or die('Restricted access');

	/* Mirtelcom megrate to Joomla 4 */		
	if (!empty(JFactory::getApplication()->input->getVar('vmsearch'))) {
	/* base
	if(!empty(JRequest::getVar('vmsearch'))){ */

    // параметры модуля
    $module = JModuleHelper::getModule('mod_vm_yandex_search');
    $param = json_decode($module->params);
    $apikey = $param->apikey;
    $searchid = $param->searchid;
    $count_limit = $param->count_limit;

    // отключение сортировки и фильтрации
    $this->orderByList['orderby'] = '';
    $this->orderByList['manufacturer'] = '';
    
    // очистка поискового запроса
    function clean($value) {
        $value = trim($value);
        $value = stripslashes($value);
        $value = strip_tags($value);
        $value = htmlspecialchars($value);
        $value = preg_replace("/ {2,}/"," ",$value);
        $value = urlencode($value);
        $value = str_replace("+","%20",$value);
        return $value;
    }

	/* Mirtelcom megrate to Joomla 4 */		
	if (version_compare(JVERSION, '3','lt')) {
		$text = clean(JRequest::getVar('vmsearch'));
	} else {
		$input = \Joomla\CMS\Factory::getApplication()->input;
		$text = clean($input->getVar('vmsearch'));
	}
   
    // запрос к яндексу для получения общего количества найденных товаров
    $result = @file_get_contents('https://catalogapi.site.yandex.net/v1.0?apikey='.$apikey.'&text='.$text.'&searchid='.$searchid.'&per_page=1'); 
    $result = json_decode($result);
    $total = $result->docsTotal; // общее количество найденных товаров  // 31

    if($total) {
        $ids = array();

	/* Mirtelcom megrate to Joomla 4 */		
	if (version_compare(JVERSION, '3','lt')) {
        	$limit_product = (JRequest::getVar('limit')) ? JRequest::getVar('limit') : $count_limit; // товаров на странице
	        $this->vmPagination = new VmPagination($total, JRequest::getVar('limitstart'), $limit_product , $this->perRow);
	} else {
		$input = \Joomla\CMS\Factory::getApplication()->input;
        	$limit_product = ($input->getVar('limit')) ? $input->getVar('limit') : $count_limit; // товаров на странице
	        $this->vmPagination = new VmPagination($total, $input->getVar('limitstart'), $limit_product , $this->perRow);
	}

        $current_page = $this->vmPagination->get('pages.current') - 1;
        $result2 = @file_get_contents('https://catalogapi.site.yandex.net/v1.0?apikey='.$apikey.'&text='.$text.'&searchid='.$searchid.'&page='.$current_page.'&per_page='.$limit_product.'');
        $result2 = json_decode($result2);  

        if($result2->documents) { // 41
            foreach($result2->documents as $prod) {
                $ids[] = $prod->id; // id товаров для показа    
            }
        }   

        $product_title = 'товаров';
 
        if(($total > 1 and $total <= 4) or ($total > 20 and ($total%10 > 1 and $total%10 <= 4))){
           $product_title = 'товара';  
        }
        
        echo '<h2 class="vmsearch-title">Результаты поиска</h2> <p>По запросу <strong>'.urldecode($text).'</strong> найденo <strong>'.$total.'</strong> '.$product_title.'.</p>';
    } else {
        $ids = '';
        echo '<h2 class="vmsearch-title">Результаты поиска</h2> <p>По запросу <strong>'.urldecode($text).'</strong> ничего не нашлось.</p> <p>Попробуйте сократить запрос или задать его по-другому.</p>';
    }
    
    // добавление поискового запроса в форму поиска
    $search_text = urldecode($text);
    echo "<script>jQuery(document).ready(function(){jQuery('input[name=\"vmsearch\"]').val('".$search_text."')});</script>";
} 
?> 
 
