<?php
/**
 * sublayout products
 *
 * @package	VirtueMart
 * @author Max Milbers
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2014 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL2, see LICENSE.php
 * @version $Id: cart.php 7682 2014-02-26 17:07:20Z Milbo $
 */

defined('_JEXEC') or die('Restricted access');

$products_per_row = empty($viewData['products_per_row'])? 1:$viewData['products_per_row'] ;
$currency = $viewData['currency'];
$showRating = $viewData['showRating'];
echo shopFunctionsF::renderVmSubLayout('askrecomjs');

$ItemidStr = '';
$Itemid = shopFunctionsF::getLastVisitedItemId();
if(!empty($Itemid)){
	$ItemidStr = '&Itemid='.$Itemid;
}

$dynamic = false;
if (vRequest::getInt('dynamic',false)) {
	$dynamic = true;
}

switch($products_per_row){
    case 1:
        $product_cellwidth = 'col-lg-12 col-md-12 col-sm-12 col-xs-12';
        break;
    case 2:
        $product_cellwidth = 'col-lg-6 col-md-6 col-sm-6 col-xs-12';
        break;
    case 3:
        $product_cellwidth = 'col-lg-4 col-md-4 col-sm-6 col-xs-12';
        break;
    case 4:
        $product_cellwidth = 'col-lg-3 col-md-4 col-sm-6 col-xs-12';
        break;
    case 5:
        $product_cellwidth = 'col-lg-3 col-md-4 col-sm-6 col-xs-12';
        break;
    case 6:
        $product_cellwidth = 'col-lg-2 col-md-3 col-sm-4 col-xs-12';
        break;
    default:
        $product_cellwidth = 'col-lg-4 col-md-4 col-sm-6 col-xs-12';
}


foreach ($viewData['products'] as $type => $products ) {

    if( (!empty($type) and count($products)>0) or (count($viewData['products'])>1 and count($products)>0)){
        $productTitle = vmText::_('COM_VIRTUEMART_'.strtoupper($type).'_PRODUCT'); ?>
        <div class="<?php echo $type ?>-view">
        <h4><?php echo $productTitle ?></h4>
        <?php // Start the Output
    }

	$BrowseTotalProducts = count($products);

    ?>
    <div class="row product-wrap grid" itemtype="http://schema.org/ItemList" itemscope>
        <?php
	$products_in_array = [];

// Mirtelcom. Сортировка цен.
for ($i = 0; $i < count($products) ; $i++) {
		$prevPrice = isset($products[$i]->allPrices[0]['salesPrice']) ? $products[$i]->allPrices[0]['salesPrice'] : floor($products[$i]->allPrices[0]['product_price']);
		for ($k = 1; $k < count($products[$i]->allPrices); $k ++) {
			$tempPrice = isset($products[$i]->allPrices[$k]['salesPrice']) ? $products[$i]->allPrices[$k]['salesPrice'] : floor($products[$i]->allPrices[$k]['product_price']);
			if ($tempPrice < $prevPrice) {
				$prevPrice = $tempPrice;
			}
		}
	for ($j = $i + 1; $j < count($products); $j++) {
		if (isset($products[$j]->allPrices[0]['salesPrice'])) {
			$currPrice = isset($products[$j]->allPrices[0]['salesPrice']) ? $products[$j]->allPrices[0]['salesPrice'] : floor($products[$j]->allPrices[0]['product_price']);
			for ($k = 1; $k < count($products[$j]->allPrices); $k ++) {
				$tempPrice = isset($products[$j]->allPrices[$k]['salesPrice']) ? $products[$j]->allPrices[$k]['salesPrice'] : floor($products[$j]->allPrices[$k]['product_price']);
				if ($tempPrice < $currPrice) {
					$currPrice = $tempPrice;
				}
			}
		}
		if ($prevPrice > $currPrice) {
			$temp = $products[$i];
			$products[$i] = $products[$j];
			$products[$j] = $temp;
			$prevPrice = $currPrice;
		}
	}
}

        foreach ( $products as $product ) {

		// Mirtelcom add. без этой проверки, при выборе фильтров, задваиваются товары, у которых есть несколько цен.
		if (in_array($product, $products_in_array)) {
			continue;
		} else {
			$products_in_array[] = $product;
		}

            if(!is_object($product) or empty($product->link)) {
                vmdebug('$product is not object or link empty',$product);
                continue;
            } ?>
            <div class="product-block <?php echo $product_cellwidth; ?> b1c-good" itemtype="http://schema.org/Product" itemprop="itemListElement" itemscope="" >
                <div class="spacer product-container card">
                  
                   <?php echo shopFunctionsF::renderVmSubLayout('vmlabel',array('product'=>$product)); ?>
                    <div class="product-image">  
                       <?php echo shopFunctionsF::renderVmSubLayout('productday',array('product'=>$product)); ?>              

		<?php /* Mirtelcom #5.13 edit. Fixed the conflict between plugins Airslider and Custom Filter */ ?>
			<div class="vm-trumb-slider id<?php echo $product->virtuemart_product_id; ?>" style="height:245px;">
			   <div>

                        <?php 
			/* Mirtelcom */
			$db = JFactory::getDBO();
			$query = 'SELECT id FROM wxa43_menu WHERE published = 1 AND (
				link = "index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $product->virtuemart_category_id . '&virtuemart_manufacturer_id=0" OR 
				link = "index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $product->virtuemart_category_id . '&virtuemart_manufacturer_id=0&clearCart=0" OR 
				link = "index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $product->virtuemart_category_id . '")';
			$db->setQuery($query);
			$ItemidStr = $db->loadAssoc()['id'];
			$ItemidStrFull = '&Itemid=' . $ItemidStr;
			$product_link = str_replace('&Itemid=101', '', $product->link) . $ItemidStrFull;
			?>
			      <a title="<?php echo $product->product_name; ?>" rel="nofollow ugc" label href="<?php echo $product_link; ?>">
			<?php /* base
			      <a title="<?php echo $product->product_name; ?>" rel="nofollow ugc" label href="<?php echo $product->link . $ItemidStr; ?>"> */ ?>


			   	    <?php echo $product->images[0]->displayMediaThumb('', false); ?>
			       </a>
			   </div> 
			   <?php
			   for ($i = 1; $i < count($product->images); $i++){ ?>
			        <div>
			            <a title="<?php echo $product->product_name ?>" rel="nofollow ugc" href="<?php echo $product->link.$ItemidStr; ?>">
				            <img data-lazy="<?php echo '/'.$product->images[$i]->getFileUrlThumb(); ?>">
			            </a>
			        </div>
			    <?php    
			    } 
			    ?>
			</div>
			 
			<?php if (count($product->images) > 1): ?>		
			    <script>
			        jQuery('.vm-trumb-slider.id' + <?php echo $product->virtuemart_product_id; ?>).slick({dots:true,arrows:true,lazyLoad: 'ondemand',}).slick('refresh');
			    </script>
			<?php endif; ?>
			<?php /* base
                        <div class="vm-trumb-slider" style="height:245px;">
                           <div>
                            <a title="<?php echo $product->product_name ?>" href="<?php echo $product->link.$ItemidStr; ?>">
                               <?php echo $product->images[0]->displayMediaThumb('class="img-rounded"', false); ?>
                               </a>
			   </div>

                           <?php
                            $number = 4;
                            if ($number > count($product->images)){
                                $number = count($product->images);
                            }
                            for ($i = 1; $i < $number; $i++){ ?>
                                <div>
                                    <a title="<?php echo $product->product_name ?>" href="<?php echo $product->link.$ItemidStr; ?>">
                                    <img class="img-rounded" data-lazy="<?php echo "/".$product->images[$i]->getFileUrlThumb(); ?>">
                                    </a>
                                </div>
                            <?php    
                            } 
                            ?>  
                        </div>
			*/ ?>

			<?php /* Mirtelcom #15.24.2 add */ ?>
			<?php //include JPATH_PLUGINS.'/system/vmquickview/tmpl/vmquickview-button.php'; ?>

                    </div>

<?php
		    /* Mirtelcom #17.24 change
		    if(isset($product->images[0])) { ?>
                        <meta itemprop="image" content="<?php echo JURI::base().$product->images[0]->file_url?>"/>
		    <?php } /* base ?>
                    <meta itemprop="image" content="<?php echo JURI::base().$product->images[0]->file_url?>"/>
		    <?php */ 
?>

                    <div class="product-info">
                        <div class="product-name b1c-name" itemprop="name">

                        <?php 
			/* Mirtelcom */
			$tmpUrl = JRoute::_( 'index.php?option=com_virtuemart&view=productdetails' 
				. '&virtuemart_product_id=' . $product->virtuemart_product_id
				. '&virtuemart_category_id=' . $product->virtuemart_category_id 
				. '&Itemid=' . $ItemidStr );
			echo JHTML::link($tmpUrl, $product->product_name, ' itemprop="url"'); 
			/* base 
			echo JHtml::link ($product->link . $ItemidStr, $product->product_name,' itemprop="url"'); */
			?>

                        </div>
                        
                        <div class="clearfix"></div>
                        
                        <?php
                        JPluginHelper::importPlugin('system', 'vmrating');

                        $ratingParams = [
                            array(
                                'id' => $product->virtuemart_product_id,
                                'average_rating' => false,
                                'count_votes' => true,
                                'count_votes_text' => false,
                                'active_voting' => false,
                                'only_reg' => false,
                                'micro_data' => false
                            )
                        ];

			/* Mirtelcom megrate to Joomla 4 */
			if (version_compare(JVERSION, '3','lt')) {
				$results = JDispatcher::getInstance()->trigger('showRating', $ratingParams); 
			} else {
				$results = JFactory::getApplication()->triggerEvent('showRating', $ratingParams); 
			}
			/* base
                        $results = $dispatcher->trigger('showRating', $ratingParams); */

                        ?>
                        <div class="product-stock-wrap">
                            <?php if ( VmConfig::get ('display_stock', 1)): ?>

<?php 
			    /* Mirtelcom #15.24.3 add. Моё? */
			    if (empty($product->product_sku)): else: ?>
			    	<div class="product-article-grey">
				    <?php echo JText::_('COM_VIRTUEMART_PRODUCT_SKU').': '.$product->product_sku; ?>
			    	</div>
                            <?php endif; ?>


                            <div class="product-stock">    
                                <?php echo shopFunctionsF::renderVmSubLayout('stockhandle',array('product'=>$product)); ?>
                            </div>
                            <?php endif; ?>

			    <?php /* Mirtelcom remove. Убрал отзывы
			    <div class="product-review">
                                <span>
                                    <?php
                                    $comments = JPATH_SITE . '/components/com_jcomments/jcomments.php';
                                    require_once($comments);
                                    $options = array();
                                    $options['object_id'] = $product->virtuemart_product_id;
                                    $options['object_group'] = 'com_virtuemart';
                                    $options['published'] = 1;
                                    $count = JCommentsModel::getCommentsCount($options);
                                    echo '<a href="'.$product->link.$ItemidStr.'#review-tab"><i class="fa fa-comment-o" aria-hidden="true"></i> '.$count.'</a>';
                                    ?>
                                </span>
                            </div>
			    */ ?>

                        <?php 
                        /* Mirtelcom */
			$user = JFactory::getUser(); // Получаем данные текущего пользователя Joomla
			$model = new VirtueMartModelUser();
			$model->setId($user->id); // Устанавливаем ID пользователя
			$vm_user = $model->getUser(); // Получаем данные пользователя
			$user_groups = $vm_user->shopper_groups; // Массив: Группы покупателей текущего пользователя.
			$label = "";
			$style = "";
			$suffix = ($product->product_unit == "M") ? "₽/м" : "₽";
			$isUserInUserGroup = ((in_array(3, $user_groups)) or (in_array(6, $user_groups)) or (in_array(8, $user_groups)) or (in_array(9, $user_groups)));
			$isFewPrices = (count($product->allPrices) > 1) ;
			$percent = 0;
			if (isset($product->allPrices[0]["Marge"])) {
				if (is_array($product->allPrices[0]["Marge"])) {
					foreach($product->allPrices[0]["Marge"] as $value) {
						$percent = $value[1];
					}
				}
			}
			if ( $isFewPrices ) {
				$basePrice = $product->allPrices[0];
				$minPrice = round(($basePrice["override"] == 1) ? $basePrice["product_override_price"] : $basePrice["product_price"]);
				$maxPrice = $minPrice;

				for ($i = 0; $i < count($product->allPrices); $i++) {
					$currPrices = $product->allPrices[$i];
					$currPrice = round(($currPrices["override"] == 1) ? $currPrices["product_override_price"] : $currPrices["product_price"]);
					$minPrice = ($currPrice < $minPrice) ? $currPrice : $minPrice;
					$maxPrice = ($currPrice > $maxPrice) ? $currPrice : $maxPrice;
				}
				$price_otrez = $maxPrice;
				$price_buhta = $minPrice;

				if ($isUserInUserGroup) {
					$price_otrez = ceil($price_otrez - $price_otrez * $percent / 100);
					$price_buhta = ceil($price_buhta - $price_buhta * $percent / 100);
				}
				$price_one = $price_buhta;
				$label = "При покупке бухты: " . $price_buhta . " руб/м" . "\r\n" . "При покупке на отрез: " . $price_otrez . " руб/м";
				$style = 'style="margin-bottom: 22px; height: 22px;"';
			} else if ( !$isFewPrices ) {
				if ( $product->allPrices[0]["override"] == 1 ) {
					$price_one = $product->allPrices[0]["product_override_price"];
				} else {
					$price_one = $product->allPrices[0]["product_price"];
					$price_one = ceil($price_one - $price_one * $percent / 100);
				}                                                                                            
			}
			/* end */

			?>



                        </div>
                        <?php if (!empty($product->product_s_desc)): ?>
                        <div class="product_s_desc" itemprop="description">
                            <?php echo nl2br($product->product_s_desc); ?>
                        </div>
                        <?php endif; ?>         
                    </div>

                    <div class="product-details" itemtype="http://schema.org/Offer" itemprop="offers" itemscope>

			<?php /* Mirtelcom */ ?>
			<div class="product-price" title="<?= $label ?>" <?= $style; ?> >

				<?php if ($product->prices['override']) { ?>
					<div class="price-crossed">
						<div class="PricebasePriceWithTax vm-display vm-price-value">
							<span class="PricebasePriceWithTax"><?= $product->prices['product_price'] . " " . $suffix ?></span>
						</div>
					</div>
				<?php } ?>

				<div class="PricesalesPrice vm-display vm-price-value">
					<span class="pricesales-price-mirtelcom"><?= $price_one . " " . $suffix ?></span>
				</div>
			</div>
			<?php if ($isFewPrices) { ?>
			<div class="category-naotrez">    
                              <div class="nostock text-success" style="color: #777;">Цена на отрез: <?= $price_otrez ?> ₽/м</div>              
	                </div>
			<?php } ?>

			<?php		
			/* base    
			echo shopFunctionsF::renderVmSubLayout('prices',array('product'=>$product,'currency'=>$currency));             */

                        echo "<meta itemprop='price' content='".$product->prices['salesPrice']."'>";
                        echo "<meta itemprop='priceCurrency' content='".$currency->_vendorCurrency_code_3."'>";
                        ?>
                        
                        <div class="product-cart"> 
                           <?php echo shopFunctionsF::renderVmSubLayout('addtocart',array('product'=>$product, 'position' => array('ontop', 'addtocart'))); ?>
                        </div>
                        
<?php 
			/* Mirtelcom #15.24.5 remove
                        // Показ артикула
                        $showArticle = true;
                        if($showArticle){ ?>
                            <div class="product-article">
                                <?php echo JText::_('COM_VIRTUEMART_PRODUCT_SKU').': '.$product->product_sku; ?>
                            </div>
                        <?php } */ 
?>
                       
                    </div>
                <?php if(vRequest::getInt('dynamic')){
                    echo vmJsApi::writeJS();
                } ?>
                </div>
            </div>
        <?php
        } ?>
    </div>
    <div class="clearfix"></div>
    <?php
    if(!empty($type)and count($products)>0){ ?>
        </div>
    <?php
    }
  }
