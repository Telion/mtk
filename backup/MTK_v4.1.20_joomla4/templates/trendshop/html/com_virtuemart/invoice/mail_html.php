<?php
/**
 *
 * Main wrapper for the order email
 * @author Spyros Petrakis
 * @link http://www.virtuemarttemplates.eu
 * @copyright Copyright (c) 2015 Spyros Petrakis. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 *
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title></title>
<link href="https://fonts.googleapis.com/css?family=PT+Sans&subset=latin-ext" rel="stylesheet">
<style type="text/css">
.ReadMsgBody {width: 100%;}
body {}
.ExternalClass table {border-collapse:separate;}
a, a:link, a:visited {text-decoration: none; color: #00788a}
a:hover {text-decoration: underline;}
h2,h2 a,h2 a:visited,h3,h3 a,h3 a:visited,h4,h5,h6,.t_cht {color:#000 !important}
.ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td {line-height: 100%}
.ExternalClass {width: 100%;}
.ExternalClass * {line-height: 100%}
span.yshortcuts { color:#000; background-color:none; border:none;}
span.yshortcuts:hover,
span.yshortcuts:active,
span.yshortcuts:focus {color:#000; background-color:none; border:none;}
.html-email_mailru_css_attribute_postfix {font-family: 'Source Sans Pro', sans-serif;}
</style>
</head>
<body style="margin: 0; padding: 0;">
<table width="100%" cellpadding="10" cellspacing="0" border="0" bgcolor="#CCCCCC" align="center">
	<tr>
		<?php /* Mirtelcom 11.4.1 add. Полоска с градиентом в шапке письма покупателю */ ?>
		<th style="background: linear-gradient(135deg, #6078ea -20%,#17ead9 120%);">&nbsp;</th>
	</tr>
	<tr>
	<td>

		<?php /* Mirtelcom 11.4.2 change */ ?>
		<table width="600" align="center" cellpadding="10" cellspacing="0" border="0" bgcolor="#fff" style="border: 2px solid #777; border-collapse: collapse;font-family: Arial, Helvetica, sans-serif; margin: 0 auto 10px;">
		<?php /* base
		<table width="600" align="center" cellpadding="10" cellspacing="0" border="0" bgcolor="#FFFFFF" style="border-collapse: collapse; font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0 auto;"> */ ?>

			<?php 
			/* Mirtelcom #11.4.3 remove
			// Shop desc for shopper and vendor
			if ($this->recipient == 'shopper') { */ 
?>

			<tr>
			<td>
				<?php echo $this->loadTemplate('header'); ?>
			</td>
			</tr>

<?php 
			/* Mirtelcom #11.4.4 remove	
			} */ 
?>

<?php 
			/* Mirtelcom #11.5.1 add. Если статус заказа "Выдан", то отправляем особенное сообщение */
			$status = $this->orderstatuses[$this->orderDetails['details']['BT']->order_status];
			if (($this->orderDetails['details']['BT']->order_status == "F") 
				or ($this->orderDetails['details']['BT']->order_status == "D") 
				or ($this->orderDetails['details']['BT']->order_status == "S")) {
?>

			<tr>
				<td>
					<?php
					echo $this->loadTemplate('delivered');
					?>
				</td>
			</tr>

			<?php
			} else { /* Письма с другими статусами заказа */
			?>

			<tr>
				<td>
					<?php
					// Message for shopper or vendor
					echo $this->loadTemplate('shopper');
					//echo $this->loadTemplate($this->recipient);
					?>
				</td>
			</tr>

			<tr>
				<td>
					<?php
					// render shipto billto adresses
					echo $this->loadTemplate('shopperaddresses');
					?>
				</td>
			</tr>

			<tr>
				<td>
					<?php
					// render price list
					echo $this->loadTemplate('pricelist');
					?>
				</td>
			</tr>

			<tr>
				<td style="padding: 0 0 7px;">
					<?php
					// end of mail
					echo $this->loadTemplate('footer');
					?>
				</td>
			</tr>
			<?php } ?>
		</table>
	</td>
	</tr>
</table>

<style type="text/css">
.ReadMsgBody {width: 100%;}
body {}
.ExternalClass table {border-collapse:separate;}
a, a:link, a:visited {text-decoration: none; color: #00788a}
a:hover {text-decoration: underline;}
h2,h2 a,h2 a:visited,h3,h3 a,h3 a:visited,h4,h5,h6,.t_cht {color:#000 !important}
.ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td {line-height: 100%}
.ExternalClass {width: 100%;}
.ExternalClass * {line-height: 100%}
span.yshortcuts { color:#000; background-color:none; border:none;}
span.yshortcuts:hover,
span.yshortcuts:active,
span.yshortcuts:focus {color:#000; background-color:none; border:none;}
</style>
</body>
</html>