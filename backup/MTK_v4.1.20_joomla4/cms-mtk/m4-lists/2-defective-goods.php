<?php 
ini_set('display_errors', 1);
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
visitsLog(__FILE__);

//printr($_POST);

// Получение POST данных
if($_SERVER["REQUEST_METHOD"]=="POST"){
	$_POST["productId"] = (empty($_POST["productId"])) ? 0 : $_POST["productId"];

	// нажата кнопка удалить (удаление записи)
	if (isset($_POST['delete_entry'])) {
		$db->setQuery('SELECT * FROM tln_active_equipments WHERE id="'.$_POST["delete_entry"].'"');
		$db->execute();
		$result = $db->loadAssoc();

		$query = 'INSERT INTO tln_active_equipments_history (create_date, product_id, document, supplier, product_name, serial, description, is_deffective, operation)
			VALUES ("'.$result["date"].'", '.$result["product_id"].', "'.$result["document"].'", "'.$result["supplier"].'", "'.$result["product_name"].'", "'.$result["serial"].'", "'.$result["description"].'", "'.$result["is_deffective"].'", "delete")';
		$db->setQuery($query);                                  
		$db->execute();

		$query = 'DELETE FROM tln_active_equipments WHERE id = '.$_POST['delete_entry'];
		$db->setQuery($query);                                  
		$db->execute();
	}

	// нажата кнопка Обновить (редактирование записи)
	if (isset($_POST['action']) && ($_POST['action'] == "Обновить")){
		$query = 'INSERT INTO tln_active_equipments_history (create_date, product_id, document, supplier, product_name, serial, description, is_deffective, operation)
			VALUES ("'.$_POST["date"].'", '.$_POST["productId"].', "'.$_POST["documentNumber"].'", "'.$_POST["supplier"].'", "'.$_POST["productName"].'", "'.$_POST["serialNumber"].'", "'.$_POST["description"].'", "1", "change")';
		$db->setQuery($query);                                  
		$db->execute();

		$query = 'UPDATE tln_active_equipments SET 
				date = "'.$_POST["date"].'",
				product_id = '.$_POST["productId"].',
				document = "'.$_POST["documentNumber"].'",
				supplier = "'.$_POST["supplier"].'",
				product_name = "'.$_POST["productName"].'",
				serial = "'.$_POST["serialNumber"].'",
				description = "'.$_POST["description"].'",
				comment = "'.$_POST["comment"].'"
			WHERE id = '.$_POST["id"];
		$db->setQuery($query);                                  
		$db->execute();
	}

	// нажата кнопка добавить (добавление записи)
	if (isset($_POST['action']) && ($_POST['action'] == "Добавить")){
		$db->setQuery('SELECT * FROM tln_active_equipments WHERE serial="'.$_POST["serialNumber"].'" and description="'.$_POST["description"].'" and is_deffective = 1');
		$copyEntry = $db->loadObjectList('id');
		if (empty($copyEntry)) {
			$query = 'INSERT INTO tln_active_equipments (date, product_id, document, supplier, product_name, serial, description, is_deffective, comment)
				VALUES ("'.$_POST["date"].'", '.$_POST["productId"].', "'.$_POST["documentNumber"].'", "'.$_POST["supplier"].'", "'.$_POST["productName"].'", "'.$_POST["serialNumber"].'", "'.$_POST["description"].'", 1, "'.$_POST["comment"].'")';
			$db->setQuery($query);                                  
			$db->execute();
		}
	} 
}

$query_string = 'SELECT * FROM tln_active_equipments WHERE is_deffective = 1 ORDER BY id DESC';
$db->setQuery($query_string);
$arr_products = $db->loadObjectList('id');

$title = "Брак";

?>

<html>
<head>
	<title>cmsMTK. <?=$title?></title>
	<style>
		form#form_insert_id {padding: 15px 15px; background: lightsalmon; width: fit-content; border-radius: 5px;}
		form table {margin: 0;}
		form th {color: inherit; background: inherit; padding: 0;}
		form td {padding: 7px 5px 0 14px; font-size: 15px;}
		form td input {padding-left: 10px; width: 224px;}
		.input-form button {padding: 5px 10px; margin: 0 6px;}
		.buttons {padding: 24px 0 10px; margin: 0 auto; width: fit-content;}
		.activeRow, .activeRow:hover {background:lightsalmon !important; outline:0;}
		input[type="number"]::-webkit-outer-spin-button, input[type="number"]::-webkit-inner-spin-button {-webkit-appearance: none; margin: 0;}
		input[type="number"] {-moz-appearance: textfield;}
		input[type="number"]:hover, input[type="number"]:focus {-moz-appearance: number-input;}
		input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button {-webkit-appearance: none; margin: 0;}

	</style>
<?php
	require_once CONST_MENU;
?>

	<div style="width: fit-content; margin: 0 auto;">
	<div style="font-size: 24px; font-weight: 700; padding: 14px 0; padding-left: 70px; margin-top: 10px;"><?=$title?>: </div>

	<form id="form_insert_id" method="post" class='input-form' name='form_insert' id="form_insert_id" style="position: relative;">
		<table>
		<tr>
			<th>ID</th>
			<th>Дата</th>
			<th>Документ</th>
			<th>ID товара</th>
			<th>Название товара</th>
			<th>Серийный номер</th>
			<th>Поставщик</th>
		</tr>
		<tr>			
			<td><input type="text" name="id" autocomplete="off" form="form_insert_id" style="width: 60px; background: #ccc;" readonly>
			<td><input type="date" name="date" autocomplete="off" form="form_insert_id" value="<?=date("Y-m-d", time())?>" style="width: 140px;">
                      	<td><input type="text" name="documentNumber" autocomplete="off" form="form_insert_id">
                      	<td><input type="number" name="productId" autocomplete="off" form="form_insert_id" style="width: 60px;">
			<td><input type="text" name="productName" autocomplete="off" form="form_insert_id" style="width: 304px;">
			<td><input type="text" name="serialNumber" autocomplete="off" form="form_insert_id">
			<td><input type="text" name="supplier" autocomplete="off" form="form_insert_id">
		</tr>
		<tr>
			<td colspan=5><textarea type="text" name="description" autocomplete="off" form="form_insert_id" required style="font-size: 14px; padding: 10px; height: 82px; width: -webkit-fill-available;" placeholder="Описание неисправности (обязательно к заполнению)"></textarea>
			<td colspan=2><textarea type="text" name="comment" autocomplete="off" form="form_insert_id" style="font-size: 14px; padding: 10px; height: 82px; width: -webkit-fill-available;" placeholder="Комментарий"></textarea>
		</tr>
		</table>
		<div class="buttons">
			<input type="submit" id="button_insert" name="action" value="Добавить" style="padding: 5px 10px; margin: 0 6px;">
			<input type="submit" id="button_update" name="action" value="Обновить" style="padding: 5px 10px; margin: 0 6px;" disabled>
			<input type="button" id="button_clean" name="tclean" value="Очистить" onclick="tcleaner();" style="padding: 5px 10px; margin: 0 6px;" disabled>
		</div>
	</form>

<div class="new-product">
<table id="mySuperTBL" class="zebra-table" style="max-width: 1400px;">
	<tr><th>ID</th><th>Дата</th><th>Документ</th><th>ID товара</th><th>Название товара</th><th>Серийный номер</th><th>Поставщик</th><th>Описание неисправности</th><th>Комментарий</th><th></th></tr>
	<?php
	foreach($arr_products as $key => $product) {
		echo "<tr>
			<td>".$product->id."</td>
			<td style='min-width: 70px;'>".Date("d.m.Y", strtotime($product->date))."</td>
			<td>".$product->document."</td>
			<td>".$product->product_id."</td>
			<td style='max-width: 300px;'>".$product->product_name."</td>
			<td>".$product->serial."</td>
			<td>".$product->supplier."</td>
			<td style='max-width: 400px; text-align: justify;'>".$product->description."</td>
			<td style='max-width: 200px; text-align: justify; word-break: break-word;'>".$product->comment."</td>
			<td>
				<form method='post' name='form_delete' id='form_delete_id".$key."' class='form-delete' onsubmit='return request_form();' style='margin: 2px;'>
					<input type='hidden' name='delete_entry' value='".$key."' form='form_delete_id".$key."'>
					<button type='submit' form='form_delete_id".$key."'>x</button></form>
			</td>
		</tr>";
	}
	?>
</table>
</div>

	<script>
		onload = function () {
			document.getElementById('mySuperTBL').onclick = mySuperFunc;
			document.form_insert.date.oninput = editInput;
			document.form_insert.documentNumber.oninput = editInput;
			document.form_insert.productId.oninput = editInput;
			document.form_insert.supplier.oninput = editInput;
			document.form_insert.productName.oninput = editInput;
			document.form_insert.serialNumber.oninput = editInput;
		}

		function request_form() {
			return confirm("Подтвердите действие");
		}

		function tchanger() {
			alert("Обновить запись?");
		}

		function tcleaner() {
			document.form_insert.id.value = "";
			document.form_insert.date.value = "<?=date("Y-m-d", time())?>";
			document.form_insert.documentNumber.value = "";
			document.form_insert.productId.value = "";
			document.form_insert.supplier.value = "";
			document.form_insert.productName.value = "";
			document.form_insert.serialNumber.value = "";
			document.form_insert.description.value = "";
			document.form_insert.comment.value = "";
			// Деактивировать кнопки
			document.form_insert.tclean.disabled = true;
			document.getElementById('button_update').disabled = true;
			document.getElementById('button_insert').disabled = false;
		}

		function data_to_form() {
			document.form_insert.productId.value = input[0].textContent;
		}

		function getParentTag(node,tag) { //Найти ближайшего родителя по tagName. Здесь мы движемся вверх, пока не встретим родителя, у которого тег = нашему заданному tag
			if (node) {return (node.tagName == tag) ? node : getParentTag(node.parentElement,tag);}
			return null;
		}

		function mySuperFunc(ev){
			// Заносим данные из таблицы в форму
			var e = ev || window.event, tag = (e.target).closest('tr') || e.srcElement;
			input = (e.target).closest('tr').getElementsByTagName('td');

			// Вставить данные из таблицы в форму
			document.form_insert.id.value = input[0].textContent;
			var partsOfDate = input[1].textContent.split('.')
			document.form_insert.date.value = partsOfDate[2] + '-' + partsOfDate[1] + '-' + partsOfDate[0];
			document.form_insert.documentNumber.value = input[2].textContent;
			document.form_insert.productId.value = input[3].textContent;
			document.form_insert.productName.value = input[4].textContent;
			document.form_insert.serialNumber.value = input[5].textContent;
			document.form_insert.supplier.value = input[6].textContent;
			document.form_insert.description.value = input[7].textContent;
			document.form_insert.comment.value = input[8].textContent;

			// Активировать кнопки
			document.form_insert.tclean.disabled = false;
			document.getElementById('button_update').disabled = false;
			document.getElementById('button_insert').disabled = true;

			// Выделение строки
			var row = getParentTag(e.target,'TR');
			if (!row) {return;}
				var tbl = this,  idx = tbl.getAttribute('activeRowIndex');
				if (idx) {
					tbl.rows[idx].classList.remove('activeRow');
				}
				row.classList.add('activeRow');
				tbl.setAttribute('activeRowIndex', row.rowIndex);
			}
			

		function editInput() {
			// Активировать кнопки
			document.form_insert.tclean.disabled = false;
		}


	</script>


