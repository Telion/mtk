<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);

ini_set('display_errors', 1);

$fileSuffix = getFileSuffix(__FILE__);;
$classSuffix = getClassSuffix($fileSuffix);

$viewName = requireClass('view', $fileSuffix, $classSuffix);
$modelName = requireClass('model', $fileSuffix, $classSuffix);
$controllerName = requireClass('controller', $fileSuffix, $classSuffix);

$view = new $viewName();
$model = new $modelName($db);
$controller = new $controllerName($model, $view);

$controller->action();

?>