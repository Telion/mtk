<?php

class Controller_ActiveEquipments {
	private $model;
	private $view;
	private $find = 0;
	private $data;

	public function __construct($model, $view) {
		$this->model = $model;
		$this->view = $view;
	}

	public function action() {
		$this->getData();
		$this->view->generate($this->data);
	}

	public function qUpdateButtonPressed() {
		if (isset($_POST['action']) && ($_POST['action'] == "Обновить")) {
			$result = $this->model->getEntryById($_POST["id"]);
			$this->model->saveEntryToHistory($result, 'update');
            echo 'debug';
			$this->model->updateEntry();
		}
	}

	public function qDeleteButtonPressed() {
		if (isset($_POST['delete_entry'])) {
			$result = $this->model->getEntryById($_POST["delete_entry"]);
			$this->model->saveEntryToHistory($result, 'delete');
			$this->model->deleteEntry($_POST["delete_entry"]);
		}
	}

	public function qInsertButtonPressed() {
		if (isset($_POST['action']) && ($_POST['action'] == "Добавить")){
			$sameEntry = $this->model->getEntryWithSameSerial();
			if (empty($sameEntry)) {
				$this->model->insertEntry();
			} else {
				echo '
					<script>
						alert("Запись не добавлена\r\nТовар с серийным номером '.$_POST["serial"].' есть в базе");
					</script>
				';
			}
		} 		
	}

	public function qSearchButtonPressed() {
		if (isset($_POST['action']) && ($_POST['action'] == "Искать")) {		
			$this->find = 1;
			if ($_POST['serial'] != '') {
				$dataBySerial = $this->searchByColumn($_POST["serial"], "serial");
				$this->data = $dataBySerial;
			}
			if ($_POST['productName'] != '') {
				$dataByProductName = $this->searchByColumn($_POST["productName"], "product_name");
				$this->data = $dataByProductName;
			}
			if (($_POST['serial'] != '') and ($_POST['productName'] != '')) {
				$this->data = [];
				foreach ($dataBySerial as $valueBySerial) {
					foreach ($dataByProductName as $valueByProductName) {
						if ($valueBySerial->id == $valueByProductName->id) {
							$this->data[] = $valueBySerial;
						}
					}
				}
			}
		}
	}

	private function searchByColumn($value, $column) {
		$listAll = $this->model->getAll($column);
		$productList = $this->getRelatedEntries($value, $listAll);
		$condition = $this->generateConditionForSearch($column, $productList);
		if (!$condition) return null;
		return $this->model->getEntriesByCondition($condition);
	}

	private function getRelatedEntries($value, $listToSearch) {
		$productsListFromValue = [];
		if (in_array($value, $listToSearch)) { // Найдена точная копия серийника
			$productsListFromValue[] = $value; 
		} else {
			$value = mb_strtolower($value);
			for ($i = 0; $i < count($listToSearch); $i++) {
				$productsList[]['source'] = mb_strtolower($listToSearch[$i]);
			}
			$productsListFromValue = $this->search_replace($value, $productsList, 40);
		}
		return $productsListFromValue;
	}

	private function search_replace($searchString, $searchList, $similar_perc){  // 56 - не строго // 76 - строго
		$resultList = [];
		foreach($searchList as $value) {
			if (strpos($value['source'], $searchString) === false) {
				$value['similar'] = similar_text($searchString, $value['source'], $value['similar_perc']);
				$soundex1_4_simbol = substr(soundex(translit($searchString)), 3, 1);
				$soundex2_4_simbol = substr(soundex(translit($value['source'])), 3, 1);
				$value['soundex_4_symbol_diff'] = abs($soundex1_4_simbol - $soundex2_4_simbol);
				$value['result_levenshtein'] = levenshtein($searchString, $value['source'], 100, 100, 100);
				if (
				      (($searchString != $value['source']) & ($value['similar_perc'] > $similar_perc)) || 
				      (($searchString != $value['source']) & ($value['similar_perc'] > 52) & ($value['similar'] > 34) & ($value['soundex_4_symbol_diff'] < 1))     //perc не меньше 48
				   ) {               
					$value['soundex_res'] = 0;
					$resultList[] = $value;
				} else {
					$value['soundex_res'] = 1;
				}
		        $value['result_soundex'] = $value['soundex_res'];
			} else {
				$resultList[] = $value;
			}
		}

		$variables = [];
		foreach ($resultList as $value) {
			$variables[] = $value['source'];
		}

		return $variables;
	}

	private function generateConditionForSearch($column, $products) {
		$condition = '';
		for ($i = 0; $i < count($products); $i++) {
			if ($i == 0) {
				$condition = ' ' . $column . ' = "' . $products[$i] . '"';
			} else {
				$condition .= ' or ' . $column . ' = "' . $products[$i] . '"';
			}			
		}
		return $condition;
	}

	public function getData() {
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$_POST["productId"] = (empty($_POST["productId"])) ? 0 : $_POST["productId"];
            $this->qUpdateButtonPressed();
			$this->qDeleteButtonPressed();
			$this->qInsertButtonPressed();
			$this->qSearchButtonPressed();
		}

		if (!$this->find) {
			$this->data = $this->model->getAllEquipments();
		}
	}


}

?>