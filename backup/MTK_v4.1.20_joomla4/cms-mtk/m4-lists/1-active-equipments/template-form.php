<form id="form_insert_id" method="post" class='input-form' name='form_insert' id="form_insert_id" style="position: relative;">
	
	<table>
		<tr>
			<th>ID</th>
			<th>Дата прихода</th>
			<th>Документ</th>
			<th>Название товара</th>
			<th>Серийный номер*</th>
			<th>Поставщик</th>
		</tr>
		<tr>			
			<td><input type="text" name="id" autocomplete="off" form="form_insert_id" style="width: 60px; background: #ccc;" readonly>
			<td><input type="date" name="date" autocomplete="off" form="form_insert_id" value="<?=date("Y-m-d", time())?>" style="width: 140px;">
			<td><input type="text" name="documentNumber" autocomplete="off" form="form_insert_id" style="width: 90px;">
			<td><input type="text" name="productName" autocomplete="off" form="form_insert_id" style="width: 496px;">
			<td><input type="text" name="serial" autocomplete="off" form="form_insert_id" required>
			<td><input type="text" name="supplier" autocomplete="off" form="form_insert_id" style="width: 150px;">
		</tr>
		<tr>
			<td colspan=7><textarea type="text" id="description_field" name="description" autocomplete="off" form="form_insert_id" style="font-size: 14px; padding: 10px; height: 82px; width: -webkit-fill-available;" placeholder="Комментарий"></textarea>
		</tr>
	</table>

	<div class="form-buttons">
		<table>
			<tr>
				<td width="33%">&nbsp;</td>
				<td class="align-center">
					<input type="submit" id="button_insert" name="action" value="Добавить">
					<input type="submit" id="button_update" name="action" value="Обновить" disabled>
					<input type="button" id="button_clean" name="tclean" value="Очистить" onclick="tcleaner();" disabled>	
				</td>
				<td width="33%" class="align-right">
					<input type="submit" id="button_search" name="action" value="Искать" formnovalidate>
					<input type="submit" id="button_search_clear" name="action" value="Показать все" formnovalidate>
				</td>
			</tr>
		</table>
	</div>

</form>