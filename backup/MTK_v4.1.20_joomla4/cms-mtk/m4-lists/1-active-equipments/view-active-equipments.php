<?php

class View_ActiveEquipments {

	public function __construct() {
	}

	public function showTemplate($data) {
		require_once 'template.php';
	}

	public function showForm() {
		require_once "template-form.php";
	}

	public function showResultTable($data) {
		require_once "template-table.php";
	}

	public function generate($data) {
		$this->showTemplate($data);
	}

}

?>