	
	<div class="new-product">

		<?php
		$equipments = $data;
		if (isset($equipments)) {
		?>

			<table id="mySuperTBL" class="zebra-table" style="max-width: 1264px;">

				<tr>
					<th>ID</th>
					<th>Дата<br>прихода</th>
					<th>Документ</th>
					<th>Название товара</th>
					<th>Серийный<br>номер</th>
					<th>Поставщик</th>
					<th>Комментарий</th>
					<th></th>
				</tr>

				<?php
				foreach($equipments as $key => $equipment) {
				?>
					<tr>
						<td><?= $equipment->id ?></td>
						<td style="min-width: 70px;"><?= Date("d.m.Y", strtotime($equipment->date)) ?></td>
						<td><?= $equipment->document ?></td>
						<td><?= $equipment->product_name ?></td>
						<td><?= $equipment->serial ?></td>
						<td><?= $equipment->supplier ?></td>
						<td><?= $equipment->description ?></td>
						<td>
							<form method="post" name="form_delete" id="form_delete_id_<?= $key ?>" class="form-delete" onsubmit="return request_form();" style="margin: 2px;">
								<input type="hidden" name="delete_entry" value="<?= $key ?>" form="form_delete_id_<?= $key ?>">
								<button type="submit" form="form_delete_id_<?= $key ?>">x</button></form>
						</td>
					</tr>
				<?php
				}
				?>

			</table>

		<?php
		} else {
		?>
		<div style="text-align: center; padding: 20px; font-size: large;">
			Данные не найдены
		</div><hr>
		<?php
		}
		?>

	</div>