
		onload = function () {
			document.getElementById('mySuperTBL').onclick = mySuperFunc;
			document.form_insert.date.oninput = editInput;
			document.form_insert.documentNumber.oninput = editInput;
			document.form_insert.supplier.oninput = editInput;
			document.form_insert.productName.oninput = editInput;
			document.form_insert.serial.oninput = editInput;
			document.form_insert.description.oninput = editInput;
		}

		function request_form() {
			return confirm("Подтвердите действие");
		}

		function tchanger() {
			alert("Обновить запись?");
		}

		function tcleaner() {
			var today = new Date();
			var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
			document.form_insert.id.value = "";
			document.form_insert.date.value = date;
			document.form_insert.documentNumber.value = "";
			document.form_insert.supplier.value = "";
			document.form_insert.productName.value = "";
			document.form_insert.serial.value = "";
			document.form_insert.description.value = "";
			// Деактивировать кнопки
			document.form_insert.tclean.disabled = true;
			document.getElementById('button_update').disabled = true;
		}

		function data_to_form() {
			document.form_insert.productId.value = input[0].textContent;
		}

		function getParentTag(node,tag) { //Найти ближайшего родителя по tagName. Здесь мы движемся вверх, пока не встретим родителя, у которого тег = нашему заданному tag
			if (node) {return (node.tagName == tag) ? node : getParentTag(node.parentElement,tag);}
			return null;
		}

		function mySuperFunc(ev){
			// Заносим данные из таблицы в форму
			var e = ev || window.event, tag = (e.target).closest('tr') || e.srcElement;
			input = (e.target).closest('tr').getElementsByTagName('td');

			// Вставить данные из таблицы в форму
			document.form_insert.id.value = input[0].textContent;
			var partsOfDate = input[1].textContent.split('.')
			document.form_insert.date.value = partsOfDate[2] + '-' + partsOfDate[1] + '-' + partsOfDate[0];
			document.form_insert.documentNumber.value = input[2].textContent;
			document.form_insert.productName.value = input[3].textContent;
			document.form_insert.serial.value = input[4].textContent;
			document.form_insert.supplier.value = input[5].textContent;
			document.form_insert.description.value = input[6].textContent;

			// Активировать кнопки
			document.form_insert.tclean.disabled = false;
			document.getElementById('button_update').disabled = false;

			// Выделение строки
			var row = getParentTag(e.target,'TR');
			if (!row) {return;}
				var tbl = this,  idx = tbl.getAttribute('activeRowIndex');
				if (idx) {
					tbl.rows[idx].classList.remove('activeRow');
				}
				row.classList.add('activeRow');
				tbl.setAttribute('activeRowIndex', row.rowIndex);
			}

		function editInput() {
			// Активировать кнопки
			document.form_insert.tclean.disabled = false;
		}