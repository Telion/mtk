<?php

class Model_ActiveEquipments {
	private $db;

	public function __construct($db) {
		$this->db = $db;
	}

	public function getAllEquipments() {
		$query = 'SELECT * FROM tln_active_equipments WHERE is_deffective = 0 ORDER BY id DESC';
		$this->db->setQuery($query);
		return $this->db->loadObjectList('id');
	}

	public function getEntryById($id) {
		$query = 'SELECT * FROM tln_active_equipments WHERE id = "' . $id . '"';
		$this->db->setQuery($query);
	        $this->db->execute();
        return $this->db->loadAssoc();
	}

	public function getAll($column) {
		$query = 'SELECT ' . $column . ' FROM tln_active_equipments WHERE is_deffective = 0 ORDER BY id DESC';
		$this->db->setQuery($query);
		return $this->db->loadColumn();
	}

	public function getEntriesByCondition($condition) {
		$query = 'SELECT * FROM tln_active_equipments WHERE '.$condition;
		$this->db->setQuery($query);
		$this->db->execute();
		return $this->db->loadObjectList('id');
	}	

	public function saveEntryToHistory($entry, $typeOperation) {
		if (isset($entry)) {
			$query = 'INSERT INTO tln_active_equipments_history 
						(create_date, product_id, document, supplier, product_name, serial, description, is_deffective, operation, change_date)
					VALUES 
						("'.$entry["date"].'", '.$entry["product_id"].', "'.$entry["document"].'", "'.$entry["supplier"].'", "'.$entry["product_name"].'", "'.trim($entry["serial"]).'", "'.$entry["description"].'", "'.$entry["is_deffective"].'", "' . $typeOperation . '", "' . Date("Y-m-d", time()) . '")';
		        $this->db->setQuery($query);
        		echo '<span style="display: none;">' . $query . '</span>';
			$this->db->execute();
		}

	}

	public function deleteEntry($id) {
		$query = 'DELETE FROM tln_active_equipments WHERE id = ' . $id;
		$this->db->setQuery($query);                                  
		$this->db->execute();
	}

	public function updateEntry() {
		if (isset($_POST)) {
			$query = 'UPDATE tln_active_equipments SET 
					date = "'.$_POST["date"].'",
					product_id = '.$_POST["productId"].',
					document = "'.$_POST["documentNumber"].'",
					supplier = "'.$_POST["supplier"].'",
					product_name = "'.$_POST["productName"].'",
					serial = "'.trim($_POST["serial"]).'",
					description = "'.$_POST["description"].'"
				WHERE id = '.$_POST["id"];
			$this->db->setQuery($query);
			$this->db->execute();
		}
	}

	public function getEntryWithSameSerial() {
		$query = 'SELECT * FROM tln_active_equipments WHERE serial="' . $_POST["serial"] . '" and is_deffective = 0';
		$this->db->setQuery($query);

		return $this->db->loadObjectList('id');
	}

	public function insertEntry() {
		if (isset($_POST)) {
			$query = 'INSERT INTO tln_active_equipments (date, product_id, document, supplier, product_name, serial, description)
					VALUES (
						"' . $_POST["date"] . '", 
						' . $_POST["productId"] . ', 
						"' . $_POST["documentNumber"] . '", 
						"' . $_POST["supplier"] . '", 
						"' . $_POST["productName"] . '", 
						"' . trim($_POST["serial"]) . '", 
						"' . $_POST["description"] . '")';
			$this->db->setQuery($query);                                  
			$this->db->execute();
		}
	}

}

?>