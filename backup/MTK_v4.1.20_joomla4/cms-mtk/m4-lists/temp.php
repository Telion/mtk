<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);
?>

<html>
<head>
	<title>cmsMTK. Список товаров в категории</title>

<?php
	require_once CONST_MENU;
?>
<div style="width: fit-content; margin: 0 auto;">
<div style="font-size: 20px; font-weight: 700; padding: 14px; padding-left: 110px;"><span style="color:grey">Товары из категории "Патч-корды"</span></div>

<?php

// Параметры
$categoryId = 97;
$percent = 30;


// Формирую список
$db->setQuery('
	SELECT 
		wxa43_virtuemart_products_ru_ru.virtuemart_product_id, 
		wxa43_virtuemart_products_ru_ru.product_name, 
        	wxa43_virtuemart_product_prices.product_price,
	        wxa43_virtuemart_product_prices.override,
        	wxa43_virtuemart_product_prices.product_override_price
	FROM wxa43_virtuemart_products_ru_ru, wxa43_virtuemart_categories_ru_ru, wxa43_virtuemart_product_categories, wxa43_virtuemart_product_prices, wxa43_virtuemart_products 
	WHERE
   		wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND
		wxa43_virtuemart_categories_ru_ru.virtuemart_category_id = '.$categoryId.' AND 
		wxa43_virtuemart_categories_ru_ru.virtuemart_category_id = wxa43_virtuemart_product_categories.virtuemart_category_id AND 
		wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_product_categories.virtuemart_product_id AND 
		wxa43_virtuemart_product_prices.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id
	ORDER BY product_price ASC;
');
$arr_products = $db->loadObjectList();

?>

	<table class='zebra-table suppliers'>
	<tr><th>Код товара</th><th>Название товара</th><th>Цена</th><th>Новая цена</th><th>Переопределение</th><th>Переопределенная цена</th><th>Новая переопределенная цена</th></tr>

<?php
foreach ($arr_products as $product) {
	$pole1 = '<td style="padding-right: 20px; text-align: right;">'.$product->virtuemart_product_id.'</td>';
	$pole2 = '<td>' . '<a class="suppliers" href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$product->virtuemart_product_id.'" target="_blank">'.$product->product_name.'</a>' . '</td>';
	$pole3 = '<td style="text-align: right;">' . round($product->product_price).' руб' . '</td>';
	$pole4 = '<td style="padding-right: 20px; text-align: right;">'.$product->override.'</td>';
	$pole5 = '<td style="text-align: right;">' . round($product->product_override_price).' руб' . '</td>';

	$coefficient = ($percent / 100 + 1);
	$new_product_price = $product->product_price * $coefficient;
	$new_override_price = $product->product_override_price * $coefficient; 
	$pole6 = '<td style="text-align: right;">' . round($new_product_price).' руб' . '</td>';
	$pole7 = '<td style="text-align: right;">' . round($new_override_price).' руб' . '</td>';


	echo "\t" . '<tr>' . $pole1 . $pole2 . $pole3 . $pole6 . $pole4 . $pole5 . $pole7 . '</tr>' . "\r\n";


$query = "UPDATE wxa43_virtuemart_product_prices SET product_price = '".round($new_product_price)."', product_override_price = '".round($new_override_price)."' WHERE wxa43_virtuemart_product_prices.virtuemart_product_id = " . $product->virtuemart_product_id;
/*$db->setQuery($query);                                  
$db->execute();*/

//printr($query);



}
?>
	</table>
</div>