<?php
$_SERVER['DOCUMENT_ROOT'] = '/home/mirtelcom/mirtelcom.ru/docs';
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);

    // Кому отправлять уведомления
    $recipient = TELEGRAM_ALIA;

	// Проверка наличия акционных товаров с истекающим сроком акции
	require_once DIR_ROOT . '/m2-reports/1-sales-handler.php';

    // Проверка наличия заказов с истекшей датой резерва и не завершенным статусом
    require_once DIR_ROOT . '/m2-reports/9-orders-reserv-handler.php';

    // Проверка наличия товаров с отрицательным остатком или резервом, или с остатком меньше запланированного
	require_once DIR_ROOT . '/m2-reports/8-tracking-product-stock-handler.php';

    // Проверка даты последней рассылки.
    require_once DIR_ROOT . '/m6-mailing-generation/mailing-dispatcher.php';

	// Запуск скрипта сверки остатков на складах Поставщиков и цен с нашими остатками и ценами
	require_once DIR_ROOT . '/m3-api-reports/2-vendors/update-prices-and-stocks.php';
?>