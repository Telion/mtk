<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);

/* Получение POST данных */
$change_filters = 1;
if($_SERVER["REQUEST_METHOD"]=="POST"){
	$select_category = $_POST['change_filters'];
	$change_filters = $_POST['change_filters'];
}

$sort = 'summ';
$direction = 'DESC';


if ($_SERVER["REQUEST_METHOD"] == "GET") {
    $sort = verifyArrayArgumentNULL($_GET, 'sort');
    $direction = verifyArrayArgumentNULL($_GET, 'direction');
}

$select_category = verifyArrayArgumentNULL($_POST, 'select_category');
$date_ot = verifyArrayArgumentNULL($_POST, 'date_ot');
$date_do = verifyArrayArgumentNULL($_POST, 'date_do');
$count = verifyArrayArgumentNULL($_POST, 'count');

// Если выбрана дата начала периода
$insert_date_ot = "";
if ($date_ot != "") $insert_date_ot = 'AND wxa43_virtuemart_orders.created_on > "'.$date_ot.'"';

// Если выбрана дата начала периода
$insert_date_do = "";
if ($date_do != "") $insert_date_do = 'AND wxa43_virtuemart_orders.created_on < "'.$date_do.'"';

// Если выбрана категория
$insert_category = "";
if ($select_category > 0) $insert_category = 'AND wxa43_virtuemart_categories.virtuemart_category_id = '.$select_category;

// Получаю список товаров из базы
$db = JFactory::getDBO();

// Список категорий
$db->setQuery('
SELECT 
    tln.virtuemart_category_id,
    tln.category_name, 
    tln.my_count
FROM (
    
	SELECT 
	    wxa43_virtuemart_categories_ru_ru.virtuemart_category_id,
	    wxa43_virtuemart_categories_ru_ru.category_name, 
	    COUNT(wxa43_virtuemart_categories_ru_ru.category_name) as my_count
	FROM
	    wxa43_virtuemart_products,
	    wxa43_virtuemart_product_categories,
	    wxa43_virtuemart_categories,
	    wxa43_virtuemart_categories_ru_ru,
	    wxa43_virtuemart_products_ru_ru,
	    (SELECT * FROM wxa43_virtuemart_product_prices GROUP BY wxa43_virtuemart_product_prices.virtuemart_product_id) as prices
	WHERE
	    wxa43_virtuemart_products.virtuemart_product_id NOT IN (
		    SELECT
	    	    wxa43_virtuemart_order_items.virtuemart_product_id
    	    FROM 
            	wxa43_virtuemart_order_items,
	    	    wxa43_virtuemart_orders,
     	   	    wxa43_virtuemart_products
	            WHERE
	                wxa43_virtuemart_order_items.virtuemart_order_id = wxa43_virtuemart_orders.virtuemart_order_id
		            AND wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_order_items.virtuemart_product_id
       	        	    '.$insert_date_ot.'
           	    	    '.$insert_date_do.'
    		GROUP BY wxa43_virtuemart_order_items.virtuemart_product_id )
		AND wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_product_categories.virtuemart_product_id
		AND wxa43_virtuemart_product_categories.virtuemart_category_id = wxa43_virtuemart_categories.virtuemart_category_id
		AND wxa43_virtuemart_categories.virtuemart_category_id = wxa43_virtuemart_categories_ru_ru.virtuemart_category_id
		AND wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id
		AND wxa43_virtuemart_products.virtuemart_product_id = prices.virtuemart_product_id 
		AND wxa43_virtuemart_products.published = 1 
		AND wxa43_virtuemart_products.product_in_stock <> 0   
	GROUP BY wxa43_virtuemart_categories_ru_ru.category_name '.$direction.'  

	UNION

	SELECT 
	    wxa43_virtuemart_categories_ru_ru.virtuemart_category_id,
	    wxa43_virtuemart_categories_ru_ru.category_name,
	    0
	FROM `wxa43_virtuemart_categories_ru_ru`  

	) as tln   
GROUP BY `category_name`  
ORDER BY `tln`.`my_count`  DESC
');

$arr_categories = $db->loadObjectList();

// Количество непродаваемых
$db->setQuery('
SELECT 
    count(wxa43_virtuemart_products.virtuemart_product_id)
FROM
    wxa43_virtuemart_products,
    wxa43_virtuemart_product_categories,
    wxa43_virtuemart_categories,
    wxa43_virtuemart_categories_ru_ru,
    wxa43_virtuemart_products_ru_ru,
    (SELECT * FROM wxa43_virtuemart_product_prices GROUP BY wxa43_virtuemart_product_prices.virtuemart_product_id) as prices
WHERE
    wxa43_virtuemart_products.virtuemart_product_id NOT IN (
	    SELECT
  	        wxa43_virtuemart_order_items.virtuemart_product_id
	    FROM 
            wxa43_virtuemart_order_items,
	        wxa43_virtuemart_orders,
    	    wxa43_virtuemart_products
        WHERE
            wxa43_virtuemart_order_items.virtuemart_order_id = wxa43_virtuemart_orders.virtuemart_order_id AND
	        wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_order_items.virtuemart_product_id
	        '.$insert_date_ot.'
	        '.$insert_date_do.'
	    GROUP BY wxa43_virtuemart_order_items.virtuemart_product_id) 
	AND wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_product_categories.virtuemart_product_id
    AND wxa43_virtuemart_product_categories.virtuemart_category_id = wxa43_virtuemart_categories.virtuemart_category_id
    AND wxa43_virtuemart_categories.virtuemart_category_id = wxa43_virtuemart_categories_ru_ru.virtuemart_category_id
    AND wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id
    AND wxa43_virtuemart_products.virtuemart_product_id = prices.virtuemart_product_id
    AND wxa43_virtuemart_products.published = 1
    AND wxa43_virtuemart_products.product_in_stock <> 0
');
$full_count = $db->loadResult();

switch ($sort) {
	case '':
		$orderBy = 'wxa43_virtuemart_categories_ru_ru.category_name';
		break;
	case 'category':
		$orderBy = 'wxa43_virtuemart_categories_ru_ru.category_name';
		break;
	case 'stock':
		$orderBy = 'wxa43_virtuemart_products.product_in_stock';
		break;
	case 'summ':
		$orderBy = 'itog';
		break;		
}

// Итоговая таблица непроданных
if ($change_filters == 1) {
$db->setQuery('
	SELECT 
	    wxa43_virtuemart_categories_ru_ru.category_name, 
	    wxa43_virtuemart_products.virtuemart_product_id, 
	    wxa43_virtuemart_products_ru_ru.product_name, 
	    prices.product_price,
	    wxa43_virtuemart_products.product_in_stock,
	    (prices.product_price * wxa43_virtuemart_products.product_in_stock) as itog
	FROM
	    wxa43_virtuemart_products,
	    wxa43_virtuemart_product_categories,
	    wxa43_virtuemart_categories,
	    wxa43_virtuemart_categories_ru_ru,
	    wxa43_virtuemart_products_ru_ru,
	    (SELECT * FROM wxa43_virtuemart_product_prices GROUP BY wxa43_virtuemart_product_prices.virtuemart_product_id) as prices
	WHERE
	    wxa43_virtuemart_products.virtuemart_product_id NOT IN (
            SELECT
                wxa43_virtuemart_order_items.virtuemart_product_id
            FROM 
                wxa43_virtuemart_order_items,
                wxa43_virtuemart_orders,
                wxa43_virtuemart_products
            WHERE
                wxa43_virtuemart_order_items.virtuemart_order_id = wxa43_virtuemart_orders.virtuemart_order_id
                AND wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_order_items.virtuemart_product_id
                '.$insert_date_ot.'
                '.$insert_date_do.'
		    GROUP BY wxa43_virtuemart_order_items.virtuemart_product_id )
	    AND wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_product_categories.virtuemart_product_id
	    AND wxa43_virtuemart_product_categories.virtuemart_category_id = wxa43_virtuemart_categories.virtuemart_category_id
	    AND wxa43_virtuemart_categories.virtuemart_category_id = wxa43_virtuemart_categories_ru_ru.virtuemart_category_id
	    AND wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id
	    AND wxa43_virtuemart_products.virtuemart_product_id = prices.virtuemart_product_id
	    AND wxa43_virtuemart_products.published = 1
	    AND wxa43_virtuemart_products.product_in_stock <> 0
	    '.$insert_category.'
	ORDER BY '.$orderBy.' '.$direction.'
	');
	$arr_products = $db->loadObjectList();
}

// Таблица "статистика самых продаваемых товаров"
if ($change_filters == 2) {
	$db->setQuery('
	SELECT
        wxa43_virtuemart_categories_ru_ru.category_name, 
	    wxa43_virtuemart_order_items.virtuemart_product_id,
	    wxa43_virtuemart_products_ru_ru.product_name,
        prices.product_price,
        wxa43_virtuemart_products.product_in_stock,
        wxa43_virtuemart_products.published,
	    sum(product_quantity) as "count",
	    avg(product_final_price) as "price",
	    sum(product_subtotal_with_tax) as "summ", 
	    COUNT(wxa43_virtuemart_order_items.virtuemart_product_id) as "cnt_orders" 
	FROM
	    wxa43_virtuemart_orders,
        wxa43_virtuemart_order_items,
        wxa43_virtuemart_product_categories,
	    wxa43_virtuemart_categories,
	    wxa43_virtuemart_categories_ru_ru,
	    wxa43_virtuemart_products,
	    wxa43_virtuemart_products_ru_ru,
        (SELECT * FROM wxa43_virtuemart_product_prices GROUP BY wxa43_virtuemart_product_prices.virtuemart_product_id) as prices
	WHERE
	    wxa43_virtuemart_order_items.virtuemart_order_id = wxa43_virtuemart_orders.virtuemart_order_id
        AND wxa43_virtuemart_order_items.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id
        AND wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id
        AND wxa43_virtuemart_products.virtuemart_product_id = prices.virtuemart_product_id
        AND wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_product_categories.virtuemart_product_id
        AND wxa43_virtuemart_product_categories.virtuemart_category_id = wxa43_virtuemart_categories.virtuemart_category_id
        AND wxa43_virtuemart_categories.virtuemart_category_id = wxa43_virtuemart_categories_ru_ru.virtuemart_category_id
	    AND (wxa43_virtuemart_order_items.order_status = "F" OR wxa43_virtuemart_order_items.order_status = "S")
	    '.$insert_date_ot.'
	    '.$insert_date_do.'
	    '.$insert_category.'
	GROUP By wxa43_virtuemart_order_items.virtuemart_product_id ORDER BY cnt_orders DESC
	');
	$arr_products = $db->loadObjectList();
}

if ($direction == 'ASC') {
	$direction = 'DESC';
} else {
	$direction = 'ASC';
}


       
?>

<html>
<head>
	<title>cmsMTK. Статистика продаж</title>

<?php
	require_once CONST_MENU;
?>


<div class="form-filters">
	<form action='1-statistic.php' method='post' name='form_filters' id="form_change">
	  <fieldset>
<!--	    <legend>Фильтры:</legend>-->
		<div class="padding-bottom"><div class="form-label-header">Тип отчета</div>
			<div class="field">
				<input type='radio' name='change_filters' value="1"  autocomplete="off" form="form_change" <?php if ((isset($_POST['change_filters'])) or ($change_filters != 2)) {echo "checked";} ?> >
				<label>Продаж небыло</label>
			</div>
			<div class="field">
				<input type='radio' name='change_filters' value="2"  autocomplete="off" form="form_change" <?php if ((isset($_POST['change_filters'])) and ($change_filters == 2)) {echo "checked";} ?> />
				<label>Продажи были</label>
			</div>
		</div>

		<div class="padding-bottom"><div class="form-label-header">В категории</div>
			<div class="field">
				<select name='select_category' form="form_change">
					<option value='0'>Все категории (<?php echo $full_count; ?>)</option>
					<?php    
					foreach ($arr_categories as $category) {
						echo "<option value = '".$category->virtuemart_category_id."'";
						if ((isset($_POST['select_category'])) and ($_POST['select_category'] == $category->virtuemart_category_id)) {echo "selected";} 
						echo ">".$category->category_name." (".$category->my_count.")</option>";
					}
					?>
				</select>
			</div>
		</div>


		<div class="padding-bottom"><div class="form-label-header">Даты продаж</div>
			<div class="field">
				<label>От</label>
				<input type='date' name='date_ot' placeholder="по умолчанию за все время"  autocomplete="off" form="form_change" <?php if (isset($_POST['date_ot'])) {echo "value=".$_POST['date_ot'];} ?> >
			</div>
			<div class="field">
				<label>До</label>
				<input type='date' name='date_do' placeholder="по умолчанию сегодня"  autocomplete="off" form="form_change" <?php if (isset($_POST['date_do'])) {echo "value=".$_POST['date_do'];} ?> >
			</div>
		</div>


		<div class="padding-bottom"><div class="form-label-header">Количество проданных</div>
			<div class="field">
				<label style="color: red; text-decoration: line-through;">Продано не менее</label>
				<input type='text' name='count' placeholder="0"  autocomplete="off" form="form_change" <?php if (isset($_POST['count'])) {echo "value=".$_POST['count'];} ?> >
			</div>
		</div>

		<div class="button" form="form_change">
			<button type='submit'>Сформировать</button>
		</div>
	  </fieldset>
	</form>
</div>

<div style="width: fit-content; margin: 0 auto;">
<div style="font-size: 20px; font-weight: 700; padding: 14px; padding-left: 110px;"><span style="color: #444;">
<?php
	$text = '<span style="color: chocolate">ОТЧЕТ:</span> Товары, которые <span style="color: chocolate">не продавались</span>';
	if ($change_filters == 2) {$text = "ОТЧЕТ: Общая сумма продаж по каждому товару";}
	else if (($date_ot != "") and ($date_do != "")) {$text .= ' за период с <span style="color: chocolate">'. date("d.m.Y", strtotime($date_ot)).'</span>г по <span style="color: chocolate">'. date("d.m.Y", strtotime($date_do))."</span>г";}
	else if ($date_ot != "") {$text .= ' за период <span style="color: chocolate">с '. date("d.m.Y", strtotime($date_ot))."</span> по настоящий день";}
	else if ($date_do != "") {$text .= ' за период с начала ведения статистики <span style="color: chocolate"> до '. date("d.m.Y", strtotime($date_do))."</span>";}
	else $text .= ' <span style="color: chocolate">никогда</span>';
	echo $text;
	?>
</span></div>

	<table>
<?php
	if ($change_filters == 1) {
		echo '<tr>
				<th style="width: 200px;"><a href="1-statistic.php?sort=category&direction='.$direction.'">Категория</a></th>
				<th style="width: 34px;">ID</th>
				<th style="width: 559px;">Название товара</th>
				<th style="width: 68px;">Текущая<br>цена</th>
				<th style="width: 67px;"><a href="1-statistic.php?sort=stock&direction='.$direction.'">Текущий<br>остаток</a></th>
				<th style="width: 83px;"><a href="1-statistic.php?sort=summ&direction='.$direction.'">На сумму</a></th></tr>';
	} else if ($change_filters == 2) {
		echo '<tr>
			<th style="width: 34px;">ID</th>
			<th style="width: 559px;">Название товара</th>
			<th style="width: 85px;">Количество<br>проданных</th>
			<th style="width: 92px;">Cредняя цена</th>
			<th style="width: 94px;">Общая<br>сумма продаж</th>
			<th style="width: 85px;">Количество<br>заказов</th>
			<th style="width: 67px;">Текущий<br>остаток</th>
		</tr>';
	}
?>
</table>

<div class="table-rows">
<table class="zebra-table">
<?php

$pole1_save = 0;
foreach ($arr_products as $product) {

	if ($change_filters == 1) {
		$pole1 = '<td style="width: 200px;">' . $product->category_name . '</td>';
		$pole2 = '<td class="right" style="width: 30px;">' . $product->virtuemart_product_id . '</td>';
		$pole3 = '<td style="width: 560px;">' . '<a target="_blank" href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id=' . $product->virtuemart_product_id . '">' . $product->product_name . '</a>' . '</td>';
		$pole4 = '<td class="right" style="width: 64px;">' . round($product->product_price) . " руб" . '</td>';
		$pole5 = '<td class="right" style="width: 63px;">' . $product->product_in_stock . '</td>';
		$pole6 = '<td class="right" style="width: 79px;">' . round($product->itog) . " руб" . '</td>';

		if (($pole1 != $pole1_save) and (($sort == '') or ($sort == 'category'))) {
			echo '<tr><th class="th-header" colspan=6><div>'.$product->category_name.'</div></th></tr>';
		}

		echo "\t" . '<tr>' . $pole1 . $pole2 . $pole3 . $pole4 . $pole5 . $pole6 . '</tr>' . "\r\n";
		$pole1_save = $pole1;

	} else if ($change_filters == 2) {
		$pole1 = '<td class="right" style="width: 30px;">' . $product->virtuemart_product_id . '</td>';
		$pole2 = '<td style="width: 560px;">' . '<a target="_blank" href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id=' . $product->virtuemart_product_id . '">' . $product->product_name . '</a>' . '</td>';
		$pole3 = '<td class="right" style="width: 73px;">' . $product->count . '</td>';
		$pole4 = '<td class="right" style="width: 88px;">' . round($product->price, 2) . " руб.</td>";
		$pole5 = '<td class="right" style="width: 90px;">' . round($product->summ) . " руб.</td>";
		$pole6 = '<td class="right" style="width: 73px;">' . $product->cnt_orders . '</td>';
		$pole7 = '<td class="right" style="width: 55px;">' . $product->product_in_stock . '</td>'; 
		echo "\t" . '<tr>' . $pole1 . $pole2 . $pole3 . $pole4 . $pole5 . $pole6 . $pole7 . '</tr>' . "\r\n";
	}
}
?>
	</table>
</div></div>