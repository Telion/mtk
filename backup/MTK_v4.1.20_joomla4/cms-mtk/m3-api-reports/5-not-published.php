<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);

// Формирую список поставщиков
$db->setQuery('
	SELECT 
		wxa43_virtuemart_products_ru_ru.virtuemart_product_id,
		wxa43_virtuemart_categories_ru_ru.category_name,
	        wxa43_virtuemart_products_ru_ru.product_name
	FROM
		wxa43_virtuemart_products_ru_ru, wxa43_virtuemart_products, wxa43_virtuemart_categories_ru_ru, wxa43_virtuemart_product_categories
	WHERE
		wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND
		wxa43_virtuemart_products.published = 0 AND
		wxa43_virtuemart_products_ru_ru.virtuemart_product_id NOT IN (
			SELECT virtuemart_product_id FROM wxa43_virtuemart_order_items
			GROUP BY virtuemart_product_id) AND
		wxa43_virtuemart_categories_ru_ru.virtuemart_category_id = wxa43_virtuemart_product_categories.virtuemart_category_id AND
		wxa43_virtuemart_product_categories.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id  
	ORDER BY wxa43_virtuemart_categories_ru_ru.category_name ASC, wxa43_virtuemart_products_ru_ru.virtuemart_product_id
');
$arr_products = $db->loadObjectList();

?>

<html>
<head>
	<title>cmsMTK. Список неопубликованных товаров</title>

<?php
	require_once CONST_MENU;
?>
	<div style="width: fit-content; margin: 0 auto;">
	<div style="font-size: 20px; font-weight: 700; padding: 14px; padding-left: 110px;">Список неопубликованных товаров (без заказов): </div>

	<table class='zebra-table'>
	<tr><th>ID товара</th><th>Категория товара</th><th>Название товара</th></tr>
<?php
$prev = "first";
foreach ($arr_products as $product) {
	$pole1 = $product->virtuemart_product_id;
	$pole2 = $product->category_name;
	$pole3 = '<a class="related" href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$product->virtuemart_product_id.'" target="_blank">'.$product->product_name.'</a>';
	echo "<!-- prev=".$prev.", pole1=".$pole1."-->";
	if ($prev != $pole2) $class = ' style="border-top: #aaa solid 2px;"';
	else $class = '';
	$prev = $pole2;
	echo "\t".'<tr><td'.$class.'>'.$pole1.'</td><td'.$class.'>'.$pole2.'</td><td'.$class.'>'.$pole3.'</td></tr>'."\r\n";
}
?>
	</table>
</div>