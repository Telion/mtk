<?php

$rootPath = $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk';
$projectPath = $rootPath . '/m3-api-reports/7-unnecessary-urls';
require_once $rootPath . '/function.php';
require_once $rootPath . '/includes/simple_html_dom.php';
//require_once $projectPath . '/services/service.php';
visitsLog(__FILE__);


function getProductsFromSitemap()
{
    $xml = simplexml_load_file($_SERVER['DOCUMENT_ROOT'] . '/sitemap.xml');
    $result = array();
    foreach ($xml->url as $item) {
        $url = $item->loc->asXML();
        $url = str_replace("<loc>", "", $url);
        $url = str_replace("</loc>", "", $url);
        $result[] = $url;
    }
    return $result;
}


function getProductsFromYandex()
{
    $result = array();
    $fp = @fopen("yandex.csv", "r");
    if ($fp) {
        while (($buffer = fgets($fp, 4096)) !== false) {
            $result[] = trim($buffer);
        }
        if (!feof($fp)) {
            echo "Ошибка: fgets() неожиданно потерпел неудачу\n";
        }
        fclose($fp);
    }
    return $result;
}


function getSearchableProductsFromYandex($array, $query) {
    $searchableProducts = array();
    foreach ($array as $item) {
        $line = explode('","', $item);
        if (isset($line[2]) && $line[2] == $query) {
            $searchableProducts[] = trim(str_replace('"', '', $line[0]));
        }
    }
    return $searchableProducts;
}


function extractZipArchive($path, $folder)
{
    $zip = new ZipArchive;
    if ($zip->open($path) === TRUE) {
        $zip->extractTo($folder);
        $zip->close();
    }
}


// Загрузка файла из формы
//if (isset($_FILES['file']['name'])) {
//    $allowedTypes = array('application/x-gzip');
//    $uploadDir = ""; //Директория загрузки. Если она не существует, обработчик не сможет загрузить файлы и выдаст ошибку
//    for ($i = 0; $i < count($_FILES['file']['name']); $i++) { //Перебираем загруженные файлы
//        $uploadFile[$i] = $uploadDir . basename($_FILES['file']['name'][$i]);
//        $fileChecked[$i] = false;
//        echo $_FILES['file']['name'][$i] . " | " . $_FILES['file']['type'][$i] . " — ";
//        for ($j = 0; $j < count($allowedTypes); $j++) { //Проверяем на соответствие допустимым форматам
//            if ($_FILES['file']['type'][$i] == $allowedTypes[$j]) {
//                $fileChecked[$i] = true;
//                break;
//            }
//        }
//        if ($fileChecked[$i]) { //Если формат допустим, перемещаем файл по указанному адресу
//            if (move_uploaded_file($_FILES['file']['tmp_name'][$i], $uploadFile[$i])) {
//                echo "Успешно загружен <br>";
//                extractZipArchive($uploadFile[$i], "");
//            } else {
//                echo "Ошибка " . $_FILES['file']['error'][$i] . "<br>";
//            }
//        } else {
//            echo "Недопустимый формат <br>";
//        }
//    }
//} else {
//    echo "Вы не прислали файл!";
//}


?>
<html lang="ru">
<head>
    <title>CMS-MTK 3-7</title>
    <style>
        .file-drop {
            background:#fff;
            margin:auto;
            padding:70px 150px;
            border:2px solid #333;
        }
        .file-drop_dragover {
            border:2px dashed #333;
        }
        .file-drop__input {
            border:0;
        }
        .message-div {
            background:#fefefe;
            border:2px solid #333;
            color:#333;
            width:350px;
            height:150px;
            position:fixed;
            bottom:25px;
            right:25px;
            font-size:15px;
            padding:5px;
            z-index:99999;
            box-shadow: 0 0 10px rgba(0,0,0,0.5);
        }
        .message-div_hidden {
            right:-9999999999999999999px;
        }
        .header-up {
            background: #7FD0FC;
            color: #203954;
            padding: 3px 7px;
            cursor: pointer;
            border-radius: 4px;
        }
        .text-block {
            display: none;
            width: 98%;
            margin: 0 auto;
        }
        .low-text {
            color: grey;
            font-size: 18px;
        }
    </style>
    <?php
    require_once CONST_MENU;
    ?>

    <div style="margin: 20px auto; width: 1330px; background: aliceblue; border: solid 1px darkgrey; border-radius: 7px; padding: 3px 20px;">
        <p>Этот отчет показывает какие ссылки ведут себя не правильно в поиске.</p>
        <p>Для того чтобы отчет отображал актуальные данные, необходимо:</p>
        <ul>
            <li>В Яндекс.Вебмастере скачать актуальные данные о ссылках на сайте, для этого:
                <ul>
                    <li>Открыть раздел "Индексирование" -> "<a href="https://webmaster.yandex.ru/site/https:www.mirtelcom.ru:443/indexing/all-pages/" target="_blank">Все страницы</a>", скачать "Архив".</li>
                    <li>Извлечь csv файл из архива.</li>
                    <li>Переименовать его в yandex.csv.</li>
                    <li>Скопировать yandex.csv на хостинг, по пути: /docs/cms-mtk/m3-api-reports/7-unnecessary-urls/yandex.csv.</li>
                </ul>
            </li>
            <li>Запустить сравнение списков ссылок из Sitemap и из Вебмастера, для этого обновить текущую страницу.</li>
            <li>В отчете есть два раздела: "Нет в Поиске" и "Нет в SiteMap"
                <ul>
                    <li>"Нет в Поиске", содержит список ссылок, которые сформировались в SiteMap, но отсутсвуют в поисковой выдаче Yandex</li>
                    <li>"Нет в SiteMap", содержит список ссылок, которые есть в поисковой выдаче Yandex, но отсутствуют в файле SiteMap (плохие ссылки на товар)</li>
                </ul>
            </li>
            <li>Необходимо проанализировать эти списки.
                <ul>
                    <li>Ссылки из списка "Нет в Поиске", необходимо добавить в поисковую выдачу, для этого переходим в Вебмастер, открываем страницу "Индексирование" -> "Переобход страниц". Вставляем список файлов из верхней части отчета в текстовое поле, нажимаем "Отправить" (до 180 ссылок в день)</li>
                    <li>Ссылки из списка "Нет в SiteMap", Необходимо удалить из поисковой выдачи, для этого переходим в Вебмастер, открываем страницу "Инструменты" -> "Удаление страниц из поиска". Вставляем список файлов из верхней части отчета в текстовое поле, нажимаем "Удалить" (до 500 ссылок в день)</li>
                </ul>
            </li>
        </ul>
    </div>

    <!-- Форма для загрузки файла -->
    <!-- <form method='post' enctype="multipart/form-data" style="width: max-content; margin: 0 auto;">
        <input type="hidden" name="MAX_FILE_SIZE" value="5000000">
        <input type='file' name='file[]' class='file-drop' id='file-drop' multiple required><br>
        <input type='submit' value='Загрузить' >
    </form>
    <div class='message-div message-div_hidden' id='message-div'></div> -->

    <?php
    $productsFromYandex = getSearchableProductsFromYandex(getProductsFromYandex(), 'SEARCHABLE');
    $productsFromSitemap = getProductsFromSitemap();

    $netVYandex = array_diff($productsFromSitemap, $productsFromYandex);
    $netVSitemap = array_diff($productsFromYandex, $productsFromSitemap);

    echo '<h2 id="yandex-header" class="header-up" onclick="showYandex()">Нет в Поиске <span class="low-text">(всего: ' . count($netVYandex) . ' шт)</span></h2>';
    echo '<div id="net-v-yandex" class="text-block">';
    foreach ($netVYandex as $item) {
        echo $item . '<br>';
    }
    echo '</div>';

    echo '<h2 id="sitemap-header" class="header-up" onclick="showSitemap()">Нет в Sitemap <span class="low-text">(всего: ' . count($netVSitemap) . ' шт)</span></h2>';
    echo '<div id="net-v-sitemap" class="text-block">';
    foreach ($netVSitemap as $item) {
        echo $item . '<br>';
    }
    echo '</div>';
    ?>

    <script>
        function showYandex() {
            document.querySelector('#net-v-yandex').style.display = "block";
            document.querySelector('#yandex-header').onclick = hideYandex;
        }
        function hideYandex() {
            document.querySelector('#net-v-yandex').style.display = "none";
            document.querySelector('#yandex-header').onclick = showYandex;
        }
        function showSitemap() {
            document.querySelector('#net-v-sitemap').style.display = "block";
            document.querySelector('#sitemap-header').onclick = hideSitemap;
        }
        function hideSitemap() {
            document.querySelector('#net-v-sitemap').style.display = "none";
            document.querySelector('#sitemap-header').onclick = showSitemap;
        }
        document.querySelector("body").style.overflow = "scroll";
    </script>

</body>
</html>





