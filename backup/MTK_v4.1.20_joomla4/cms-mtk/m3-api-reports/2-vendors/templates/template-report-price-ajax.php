<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';

jimport('joomla.database.table');
$db = JFactory::getDBO();

$id = $_POST['product_id'];
$newPrice = $_POST['new_price'];
$oldPrice = $_POST['old_price'];

$query = 'UPDATE wxa43_virtuemart_product_prices SET product_price = ' . $newPrice . ', override = 0 WHERE virtuemart_product_id = ' . $id;
$db->setQuery($query);
$db->execute();

echo '<div class="change-price-header">Цена обновлена:</div><div class="change-price-value">' . $oldPrice . ' → ' . $newPrice . '</div>';

?>