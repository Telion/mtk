<div class="wrap-head">
    <table id="tab-head">
        <tr>
            <th style="width: 62px;">ID</th>
            <th style="width: 75px;">Артикул вендора</th>
            <th style="width: 526px;">Название МTK</th>
            <th style="width: 80px;">Опт цена вендора</th>
            <th style="width: 80px;">Розн цена вендора</th>
            <th style="width: 102px;">Цена на МТК</th>
            <th style="width: 123px;">Установить цену</th>
            <th style="width: 107px;">Кол-во</th>
        </tr>
    </table>
</div>


<div class="wrap-body">
    <table id="tab-body" class="zebra-table">
        <?php
        foreach ($products as $product) {
            $styleBackground = ($product['vendor_count'] == 'no' or $product['vendor_count'] === '0') ? ' background: #fde0e4;' : '';
            echo '<tr class="line" style="border: solid 2px #888;' . $styleBackground . '">';
            $this->printMtkId($product);
            $this->printVendorArticle($product);
            $this->printMtkName($product);
            if ($product['vendor_price'] != '') {
                $this->printVendorPriceApi($product);
                $this->printVendorPriceRetail($product);
                $this->printMtkPrice($product);
                $this->printInputNewPrice($product);
                $this->printVendorCountCurrent($product);
            } else {
                $this->printAboutMessage();
            }
            echo '</tr>';
        }
        ?>
    </table>
</div>