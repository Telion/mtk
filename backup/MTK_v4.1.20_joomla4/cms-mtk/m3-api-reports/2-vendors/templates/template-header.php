<html>

<head>
    <title>Report: Anker vs SDS</title>

    <link href="resources/report.css" rel="stylesheet" type="text/css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        function updatePrice(id, oldPrice) {
            newPrice = document.getElementById('new-price-' + id).value;
            path = "templates/template-report-price-ajax.php";
            $.ajax({
                type: "POST",
                url: path,
                data: {product_id: id, new_price: newPrice, old_price: oldPrice}
            }).done(function (result) {
                $("#mtk-price-" + id).html(result);
            });
        }
    </script>
    <?php require_once CONST_MENU; ?>

<body>
<div class="report-wrap">
    <div class="m2-sds-report">
        <table class="header-block">
            <tr>
                <td class="without-border">
                    <form class="search-form" method='POST' name='form_choice'>
                        <table>
                            <tr>
                                <td class="right-align"><label>Текущий поставщик</label></td>
                                <td class="left-align">
                                    <select class="filter-element" name='vendor' onchange="this.form.submit()">
                                        <option value="all" selected>Все товары</option>
                                        <?= $this->printVendorOptions() ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="right-align"><label>Режим сортировки</label></td>
                                <td class="left-align">
                                    <select class="filter-element" name='sortBy' onchange="this.form.submit()">
                                        <?= $this->printSortOptions() ?>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </form>
                </td>

                <td class="without-border">
                    <a href="update-prices-and-stocks.php?count=1" target="blank">
                        <div class="btn right-block">Запуск парсинга</div>
                    </a>
                </td>
            </tr>
        </table>
		