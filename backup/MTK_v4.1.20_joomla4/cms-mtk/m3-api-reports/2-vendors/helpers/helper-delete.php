<?php

// Список товаров с двумя поставщиками

ini_set('display_errors', 1);
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
visitsLog(__FILE__);

$db->setQuery('
			SELECT virtuemart_product_id, count(*) as countVendor FROM wxa43_virtuemart_product_customfields
			WHERE virtuemart_custom_id = 48  
			GROUP by virtuemart_product_id  
			HAVING countVendor > 1
			ORDER BY countVendor  DESC
	');
$result = $db->loadAssocList('virtuemart_product_id');

echo '<table>';
foreach ($result as $key => $row) {
    echo '<tr>';
    echo '<td style="width: 550px; text-align: left;"><a href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id=' . $key . '" target="blank">' . $key . '</a></td>';
    echo '<td>' . $row['countVendor'] . '</td>';
    echo '</tr>';
}
echo '</table>';

?>