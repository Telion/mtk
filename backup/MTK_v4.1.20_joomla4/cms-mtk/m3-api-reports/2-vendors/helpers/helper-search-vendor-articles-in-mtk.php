<?php
ini_set('display_errors', 1);

$rootPath = $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk';
$projectPath = $rootPath . '/m3-api-reports/2-vendors';
require_once $rootPath . '/function.php';
require_once $rootPath . '/includes/simple_html_dom.php';
require_once $projectPath . '/services/service.php';
require_once $projectPath . '/models/model-search-vendor-articles-in-mtk.php';
require_once $projectPath . '/controllers/controller-model.php';
require_once $projectPath . '/controllers/controller-mtk-product.php';
require_once $projectPath . '/controllers/controller-load-products-from-files.php';

visitsLog(__FILE__);

$service = new Service();
$currentVendorList = array('electromir'); // sds, lindex, netko, absolute, electromir
//$currentVendorList = array('sds', 'lindex', 'netko', 'absolute'); // sds, lindex, netko, absolute
$currentVendors = $service->getOptionsCurrentVendors($currentVendorList);
printr($currentVendors);
//printr($currentVendorsOptions);

$service->requireControllers($projectPath, $currentVendors);
$service->loadProductsFromFiles($currentVendors);

$model = new Model_ParseVendors($db);
$controller = new ControllerModel($model);
//$productListFromMtk = $controller->getAllProductsWithArticleOrIntnotes();
$productListFromMtk = $controller->getVendorProductsWithArticleOrIntnotesAll($currentVendors);

$countList = $service->getCountList(count($productListFromMtk));
$countIterations = $service->getCountIterations($productListFromMtk);
//$countIterations = 1;

$i = 0;
foreach ($productListFromMtk as $productMtk) {
    $product = new MtkProduct($model, $currentVendors, $productMtk, $service->getProductListFromVendors());
    $i++;

    echo '<a href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id=' . $productMtk['mtk_id'] . '">
		<div style="font-size: 20px; background: #2D2824; padding: 15px 25px; border-radius: 4px; color: #E3DE2D;">		
		MTK ID: ' . $productMtk['mtk_id'] . ', Артикул: ' . $productMtk['mtk_article'] . '</div></a><div style="margin: 0 50px;">';

    // Запуск контроллера поставщика и запись найденных данных в базу
    foreach ($currentVendors as $vendor) {
        $countList = $product->getProductFromVendor($countList, $vendor);
    }

    echo '<hr></div>';
    if ($i >= $countIterations) break;
}

echo '<div style="text-align: center; margin: 18px; font-size: 24px; background: #E3DE2D; padding: 30px; border-radius: 4px; color: #2D2824; font-weight: 700;">Скрипт завершил работу, обработано товаров: ' . $i . ' </div>';

$messageText = "Количество обработанного товара: " . $i . "\r\n";
foreach ($currentVendorList as $vendor) {
    $messageText .= ucfirst($currentVendors[$vendor]['vendor_name']) . ': ' . $countList[$vendor] . "\r\n";
}

?>