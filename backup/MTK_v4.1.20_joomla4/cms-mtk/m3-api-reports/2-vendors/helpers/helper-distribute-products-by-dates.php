<?php

ini_set('display_errors', 1);
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/m3-api-reports/2-vendors/services' . '/service.php';
require_once CONST_MENU;
visitsLog(__FILE__);

// НАСТРОЙКИ
$whoVendorChange = 'sds'; // sds, lindex, netko, absolute, electromir

$service = new Service();
$whoVendorChange = $service->getMtkVendorNameByVendorName($whoVendorChange);
$baseDay = Date("d", time() - 60 * 60 * 24 * 3);
$baseDay = ($baseDay <= 0) ? 1 : $baseDay;
//$baseDay = 1; //

printr("Текущий поставщик: " . $whoVendorChange);
printr("Начальный день: " . $baseDay);

$currentMonthYear = Date(".m.Y", time());
$currentDay = $baseDay;

$db->setQuery('
		SELECT date.virtuemart_product_id as mtk_id, date.customfield_value as date, vendor.customfield_value as vendor 
		FROM wxa43_virtuemart_product_customfields as date 
			left join wxa43_virtuemart_product_customfields as vendor on date.virtuemart_product_id = vendor.virtuemart_product_id and vendor.virtuemart_custom_id = 48 
		WHERE date.virtuemart_custom_id = 62 ORDER BY vendor ASC	
	');
$productList = $db->loadAssocList('mtk_id');

foreach ($productList as $id => $product) {
    if ($product['vendor'] == $whoVendorChange) {
        $currentDate = $currentDay . $currentMonthYear;
        $query = 'UPDATE wxa43_virtuemart_product_customfields SET customfield_value = "' . $currentDate . '" WHERE virtuemart_product_id = ' . $product['mtk_id'] . ' and virtuemart_custom_id = 62';
        printr($query);
        /*$db->setQuery($query);
        $db->execute();
        echo 'ok';*/
        $currentDay = ($currentDay == $baseDay + 2) ? $baseDay : $currentDay + 1;
    }
}

?>