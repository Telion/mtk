<?php

// Очистка полей gtin и mpn, в базе. // В этих полях хранились данные о остатках и ценах поставщиков, но теперь они хранятся в настраиваемых полях.

ini_set('display_errors', 1);
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
require_once CONST_MENU;
visitsLog(__FILE__);

if ((isset($_POST['clean'])) and ($_POST['clean'] == 'on')) {
    $query = 'UPDATE wxa43_virtuemart_products SET product_gtin="", product_mpn="" WHERE product_gtin != "" OR product_mpn !=""';
    $db->setQuery($query);
    $db->execute();
}

$db->setQuery('
		SELECT pr.virtuemart_product_id as mtk_id, ru.product_name as mtk_product_name, pr.product_gtin as mtk_gtin, pr.product_mpn as mtk_mpn 
		FROM wxa43_virtuemart_products AS pr 
			LEFT JOIN wxa43_virtuemart_products_ru_ru AS ru ON pr.virtuemart_product_id = ru.virtuemart_product_id
		WHERE pr.product_gtin != "" OR pr.product_mpn != ""
		ORDER BY pr.virtuemart_product_id
	');
$productList = $db->loadAssocList('mtk_id');
?>


<h1 style="width: fit-content; margin: 35px auto 0;">Очистка полей gtin и mpn в базе</h1>

<form method="POST" style="width: 65%; margin: 0 auto;">
    <input type="hidden" name="clean" value="on">
    <input type="submit"
           style="display: block; width: fit-content; margin-left: auto; padding: 9px 12px 10px; border-radius: 6px; color: white; background: #ab8a8a;"
           value="Очистить базу">
</form>

<table class="base">
    <tr>
        <th>mtk-id</th>
        <th>mtk-product-name</th>
        <th>mtk-gtin</th>
        <th>mtk-mpn</th>
    </tr>

    <?php
    if (count($productList) > 0) {
        foreach ($productList as $key => $product) {
            ?>

            <tr>
                <td><?= $key ?></td>
                <td>
                    <a href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id=<?= $key ?>"
                       target="blank"><?= $product['mtk_product_name'] ?></a></td>
                <td><?= $product['mtk_gtin'] ?></td>
                <td><?= $product['mtk_mpn'] ?></td>
            </tr>

            <?php
        }
    } else {
        ?>

        <tr>
            <td colspan="4" style="text-align: center;">Записей не найдено</td>
        </tr>

        <?php
    }
    ?>
</table>