<?php

class ControllerModel
{
    private $model;

    function __construct($model)
    {
        $this->model = $model;
    }

    public function getProductsByVendors($currentVendors)
    {
        $condition = '';
        $union = '';
        $i = 0;
        foreach ($currentVendors as $vendorName => $vendor) {
            $union = ($i > 0) ? ' or ' : ' ';
            $condition .= $union . 'vendors.customfield_value = "' . $vendor['mtk_vendor_name'] . '"';
            $i++;
        }

        $query = '
			SELECT
				products.virtuemart_product_id as mtk_id,
				ru.product_name as mtk_name,
				articles.customfield_value as mtk_article,
				products.product_unit as mtk_unit,
				IF(prices.override = 1, ROUND(prices.product_override_price), ROUND(prices.product_price)) AS mtk_price,
				vendors.customfield_value as vendor,
				count.customfield_value as vendor_count,
				price.customfield_value as vendor_price,
				retail_price.customfield_value as vendor_retail_price,
				last_parse_date.customfield_value as last_parse_date
			FROM
				wxa43_virtuemart_products AS products
				JOIN wxa43_virtuemart_products_ru_ru as ru ON products.virtuemart_product_id = ru.virtuemart_product_id
				JOIN wxa43_virtuemart_product_prices as prices ON products.virtuemart_product_id = prices.virtuemart_product_id
				JOIN wxa43_virtuemart_product_customfields as vendors ON products.virtuemart_product_id = vendors.virtuemart_product_id and vendors.virtuemart_custom_id = 48 and (' . $condition . ')
				JOIN wxa43_virtuemart_product_customfields as articles ON products.virtuemart_product_id = articles.virtuemart_product_id and articles.virtuemart_custom_id = 58
				LEFT JOIN wxa43_virtuemart_product_customfields as count ON products.virtuemart_product_id = count.virtuemart_product_id and count.virtuemart_custom_id = 59
				LEFT JOIN wxa43_virtuemart_product_customfields as price ON products.virtuemart_product_id = price.virtuemart_product_id and price.virtuemart_custom_id = 60
				LEFT JOIN wxa43_virtuemart_product_customfields as retail_price ON products.virtuemart_product_id = retail_price.virtuemart_product_id and retail_price.virtuemart_custom_id = 61
				LEFT JOIN wxa43_virtuemart_product_customfields as last_parse_date ON products.virtuemart_product_id = last_parse_date.virtuemart_product_id and last_parse_date.virtuemart_custom_id = 62
                #WHERE products.virtuemart_product_id = 1857
			ORDER BY mtk_id ASC;
		';
        return $this->model->getProducts($query);
    }

    public function getAllProductsWithArticleOrIntnotes()
    {
        $query = '
			SELECT
			    products.virtuemart_product_id as mtk_id,
			    CONCAT(products.product_sku, ", ", products.intnotes) as mtk_article,
			    ru.product_name as mtk_name,
			    products.product_unit as mtk_unit,
			    IF(prices.override = 1, ROUND(prices.product_override_price), ROUND(prices.product_price)) AS mtk_price
			FROM
			    wxa43_virtuemart_products AS products
				LEFT JOIN wxa43_virtuemart_products_ru_ru AS ru ON products.virtuemart_product_id = ru.virtuemart_product_id
				LEFT JOIN wxa43_virtuemart_product_prices AS prices ON products.virtuemart_product_id = prices.virtuemart_product_id				
			WHERE (products.product_sku != "" or products.intnotes != "")
		';
        return $this->model->getProducts($query);
    }

    public function getVendorProductsWithArticleOrIntnotes($currentVendors)
    {
        $condition = '';
        $union = '';
        $i = 0;
        foreach ($currentVendors as $vendorName => $vendor) {
            $union = ($i > 0) ? ' or ' : ' ';
            $condition .= $union . 'vendors.customfield_value = "' . $vendor['mtk_vendor_name'] . '"';
            $i++;
        }

        $query = '
            SELECT
                products.virtuemart_product_id AS mtk_id,
                CONCAT(products.product_sku, ", ", products.intnotes) AS mtk_article,
                ru.product_name AS mtk_name,
                products.product_unit AS mtk_unit,
                IF(prices.override = 1, ROUND(prices.product_override_price), ROUND(prices.product_price)) AS mtk_price
            FROM
                wxa43_virtuemart_products AS products
                LEFT JOIN wxa43_virtuemart_products_ru_ru AS ru ON products.virtuemart_product_id = ru.virtuemart_product_id
                LEFT JOIN wxa43_virtuemart_product_prices AS prices ON products.virtuemart_product_id = prices.virtuemart_product_id
                JOIN wxa43_virtuemart_product_customfields AS vendors ON products.virtuemart_product_id = vendors.virtuemart_product_id AND vendors.virtuemart_custom_id = 48 AND (' . $condition . ')
            WHERE 
            	(products.product_sku != "" OR products.intnotes != "") AND
                products.virtuemart_product_id NOT IN (
                    SELECT vendor_price.virtuemart_product_id
                    FROM wxa43_virtuemart_product_customfields AS vendor_price 
                    WHERE vendor_price.virtuemart_custom_id = 60
                    )
            AND products.virtuemart_product_id = 1857
		    ';
        printr($query);
        return $this->model->getProducts($query);
    }

    public function getVendorProductsWithArticleOrIntnotesAll($currentVendors)
    {
        $condition = '';
        $union = '';
        $i = 0;
        foreach ($currentVendors as $vendorName => $vendor) {
            $union = ($i > 0) ? ' or ' : ' ';
            $condition .= $union . 'vendors.customfield_value = "' . $vendor['mtk_vendor_name'] . '"';
            $i++;
        }

        $query = '
            SELECT
                products.virtuemart_product_id AS mtk_id,
                CONCAT(products.product_sku, ", ", products.intnotes) AS mtk_article,
                ru.product_name AS mtk_name,
                products.product_unit AS mtk_unit,
                IF(prices.override = 1, ROUND(prices.product_override_price), ROUND(prices.product_price)) AS mtk_price
            FROM
                wxa43_virtuemart_products AS products
                LEFT JOIN wxa43_virtuemart_products_ru_ru AS ru ON products.virtuemart_product_id = ru.virtuemart_product_id
                LEFT JOIN wxa43_virtuemart_product_prices AS prices ON products.virtuemart_product_id = prices.virtuemart_product_id
                JOIN wxa43_virtuemart_product_customfields AS vendors ON products.virtuemart_product_id = vendors.virtuemart_product_id AND vendors.virtuemart_custom_id = 48 AND (' . $condition . ')
            WHERE 
            	(products.product_sku != "" OR products.intnotes != "") AND
                products.virtuemart_product_id = 1857
		    ';
        return $this->model->getProducts($query);
    }

}

?>