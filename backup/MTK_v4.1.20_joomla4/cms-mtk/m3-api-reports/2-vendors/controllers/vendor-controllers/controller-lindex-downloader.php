<?php 

class Controller_LindexDownloader extends Controller_Vendors {

    public function getAllStringsFromFile($extractFolder) {
        $fd = fopen($extractFolder . '/xl/worksheets/sheet1.xml', 'r') or die("не удалось открыть файл");
        $allStrings = [];
        while(!feof($fd)) {
            $allStrings[] = htmlentities(fgets($fd));
        }
        fclose($fd);

        return $allStrings;
    }

	public function getProductListFromVendor($extractFolder, $vendor) {
		$rowList = $this->getRowListFromVendor($extractFolder, $vendor);
		$productList = [];
		if (is_array($rowList) || is_object($rowList)) {
			foreach ($rowList as $row) {
				$rowNumber = $row->getRowNumber();
				$params = $row->getParams();
				if (($rowNumber > 15)
						and isset($params['A'])
						and isset($params['C'])
						and isset($params['M'])
						and ($params['A']['type'] == 's')
						and ($params['C']['type'] == 's')
						and ($params['F']['type'] == 's')
					) {
					$key = $params['A']['text'];
					$productList[$key]['id'] = $rowNumber - 1;
					$productList[$key]['article'] = $params['A']['text'];
					$productList[$key]['name'] = $params['C']['text'];
					$productList[$key]['unit'] = $params['F']['text'];
					$productList[$key]['stock'] = (isset($params['G']['value'])) ? $params['G']['value'] : 0;
					$productList[$key]['price'] = $params['M']['value'];
				}
			}
		}
        
		return $productList;
	}

  public function isCorrectFile($tableRowList) {
    	$result = 1;
    	echo '<div style="display: inline-block; padding: 10px;"><table style="border: solid 1px #ddd; padding: 10px; border-radius: 6px; border-spacing: 0; background: palegreen;">
    		<tr><th colspan=2 style="text-decoration: underline;">LINDEX: Проверка excel файла:</th></tr>';
    	$result = ($this->checkColumn($tableRowList, 11, 'A', 'Артикул')) ? $result : 0;
    	$result = ($this->checkColumn($tableRowList, 11, 'C', 'Номенклатура')) ? $result : 0;
    	$result = ($this->checkColumn($tableRowList, 11, 'F', 'Упаковка')) ? $result : 0;
    	$result = ($this->checkColumn($tableRowList, 10, 'G', 'Склад Москва')) ? $result : 0;
    	$result = ($this->checkColumn($tableRowList, 10, 'H', 'Склад СПб')) ? $result : 0;
    	$result = ($this->checkColumn($tableRowList, 10, 'I', 'Склад Краснодар')) ? $result : 0;
    	//$result = ($this->checkColumn($tableRowList, 10, 'J', 'Склад Ростов')) ? $result : 0;
    	$result = ($this->checkColumn($tableRowList, 10, 'K', 'Склад Симферополь')) ? $result : 0;
    	$result = ($this->checkColumn($tableRowList, 10, 'M', 'Розница')) ? $result : 0;
    	echo '</table></div>';

    	return $result;
    }  


}

?>