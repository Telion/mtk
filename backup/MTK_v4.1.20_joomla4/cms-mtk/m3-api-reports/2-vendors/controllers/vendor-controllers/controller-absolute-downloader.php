<?php 

class Controller_AbsoluteDownloader extends Controller_Vendors {

    public function getAllStringsFromFile($extractFolder) {
        $fd = fopen($extractFolder . '/price.xls', 'r') or die("не удалось открыть файл");
        $allStrings = [];
        while(!feof($fd)) {
            $allStrings[] = htmlentities(fgets($fd));
        }

        if (count($allStrings) <= 1) {
        	$content = $allStrings[0];
        	$allStrings = explode("&lt;" . 'row ', $content);
        }
        fclose($fd);

        return $allStrings;
    }

	public function getProductListFromVendor($extractFolder, $vendor) {
		$rowList = $this->getRowListFromVendor($extractFolder, $vendor);
		$productList = [];
		if (is_array($rowList) || is_object($rowList)) {
			foreach ($rowList as $row) {
				$rowNumber = $row->getRowNumber();
				$params = $row->getParams();
				if (($rowNumber > 11)
						and isset($params['A'])
						and isset($params['B'])
						and isset($params['D'])
						and ($params['A']['type'] == 's')
						and ($params['B']['type'] == 's')
						and ($params['D']['type'] == 's')
					) {
					$key = $params['B']['text'];
					$productList[$key]['id'] = $rowNumber - 1;
					$productList[$key]['article'] = $params['B']['text'];
					$productList[$key]['name'] = $params['A']['text'];
					$productList[$key]['unit'] = (isset($params['E']['text'])) ? $params['E']['text'] : '-';
					$productList[$key]['stock'] = (isset($params['G']['text'])) ? $params['G']['text'] : '-';
					$productList[$key]['price'] = $params['D']['text'];
				}
			}
		}

		return $productList;
	}

	public function isCorrectFile($tableRowList) {
    	$result = 1;
    	echo '<div style="display: inline-block; padding: 10px;"><table style="border: solid 1px #ddd; padding: 10px; border-radius: 6px; border-spacing: 0; background: palegreen;">
    		<tr><th colspan=2 style="text-decoration: underline;">NETKO: Проверка excel файла:</th></tr>';
    	$result = ($this->checkColumn($tableRowList, 7, 'A', 'Наименование товара')) ? $result : 0;
    	$result = ($this->checkColumn($tableRowList, 7, 'B', 'Код товара')) ? $result : 0;
    	$result = ($this->checkColumn($tableRowList, 7, 'C', 'Артикул')) ? $result : 0;
    	$result = ($this->checkColumn($tableRowList, 7, 'D', 'РРЦ, руб')) ? $result : 0;
    	$result = ($this->checkColumn($tableRowList, 7, 'E', 'Единица измерения')) ? $result : 0;
    	echo '</table></div>';

    	return $result;
    }

}

?>