<?php 

class Controller_Vendors {
	protected $productMtk;
	protected $productVendor;
	protected $symbolExceptions = ['+', '"М"', '-'];
	protected $reelSizeTmplExcluding = ['100 мм', '200 мм'];
	protected $reelSizeTmpl = ['305 м', '305м', '200 м', '100 м', '100м', '50 м', '100 шт./уп.', '500 шт', '(3шт.)', 'катушка 305', 'коробка 305', 'катушка 500']; // какие размеры ищу в названии товара
	private $vendorOptions = array(
			'sds' => array('mtk_vendor_name' => 'Rexant, Proconnect', 'percent' => -5),
			'lindex' => array('mtk_vendor_name' => 'Линдекс', 'percent' => -5),
			'netko' => array('mtk_vendor_name' => 'Абсолют ТДС', 'percent' => -5)
	);

	public function getVendorOptions() {
		return $this->vendorOptions;
	}

	public function getProductMtk() {
		return $this->productMtk;
	}

	public function getValues() {
		echo '<table style="border-collapse: collapse;">
			<tr><td style="border: solid 1px #777; padding: 2px 10px 3px;">vendor</td><td style="border: solid 1px #777; padding: 2px 10px 3px;">' . $this->productVendor['vendor'] . '</td></tr>
			<tr><td style="border: solid 1px #777; padding: 2px 10px 3px;">article</td><td style="border: solid 1px #777; padding: 2px 10px 3px;">'   . $this->productVendor['article'] . '</td></tr>
			<tr><td style="border: solid 1px #777; padding: 2px 10px 3px;">countProducts</td><td style="border: solid 1px #777; padding: 2px 10px 3px;">'    . $this->productVendor['countProducts'] . '</td></tr>
			<tr><td style="border: solid 1px #777; padding: 2px 10px 3px;">price</td><td style="border: solid 1px #777; padding: 2px 10px 3px;">'  . $this->productVendor['price'] . '</td></tr>
			<tr><td style="border: solid 1px #777; padding: 2px 10px 3px;">retailPrice</td><td style="border: solid 1px #777; padding: 2px 10px 3px;">' . $this->productVendor['retailPrice'] . '</td></tr>
			<tr><td colspan=2>МТК</td></tr>
			<tr><td style="border: solid 1px #777; padding: 2px 10px 3px;">id</td><td style="border: solid 1px #777; padding: 2px 10px 3px;">' . $this->productMtk['id'] . '</td></tr>
			<tr><td style="border: solid 1px #777; padding: 2px 10px 3px;">mtk_article</td><td style="border: solid 1px #777; padding: 2px 10px 3px;">' . $this->productMtk['article'] . '</td></tr>
			<tr><td style="border: solid 1px #777; padding: 2px 10px 3px;">price</td><td style="border: solid 1px #777; padding: 2px 10px 3px;">' . $this->productMtk['price'] . '</td></tr>
			</table></td>
		';

		$result = [];
		$result['id'] = $this->productMtk['id'];
		$result['vendor'] = $this->productVendor['vendor_db'];
		$result['article'] = $this->productVendor['article'];
		$result['vendorCount'] = $this->productVendor['countProducts'];
		$result['vendorPrice'] = $this->productVendor['price'];
		$result['vendorRetailPrice'] = $this->productVendor['retailPrice'];

		return $result;
	}

	public function getVendorProductName() {
		if (isset($this->productVendor['name'])) {
			return $this->productVendor['name'];
		}
		return '';
	}

	public function parseIntnotes($intnotes) {
		$temp = str_replace(' ', ',', $intnotes);
		$temp = str_replace('&#9;', ',', $temp);
		$temp = str_replace("&#13;&#10;", ',', $temp);
		$temp = preg_replace('/[\,]{2,}/', ',', $temp);
		$temp = explode(',', $temp);
		$result = array_diff($temp, array(''));

		return $result;
	}

	public function saveProduct($productFromDb) {
		$this->productMtk['id'] = $productFromDb['mtk_id'];
		$this->productMtk['article'] = $productFromDb['mtk_article'];
		$this->productMtk['name'] = $productFromDb['mtk_name'];
		$this->productMtk['unit'] = $productFromDb['mtk_unit'];
		$this->productMtk['price'] = round($productFromDb['mtk_price'], 2);
		$this->productMtk['url'] = '<div style="width: max-content; overflow: hidden; white-space: nowrap; width: 520px; text-overflow: ellipsis;"><a target="_blank" href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id=' . $this->productMtk['id'] . '">' . $this->productMtk['name'] . '</a></div>';
	}

	public function searchInText($search, $excl = 1) { // Поиск размера бухты в названии СДС
		if (strpos($this->productVendor['name'], $search)) { 
//					echo 'Нашел кусок: ' . $search . ', в тексте: ' . $this->productVendor['name'] . ' -> ';
					$numberFromSearch = preg_replace('/[^0-9]/', '', $search);
			$this->productVendor['reelSize'] = ($excl == 1) ? $numberFromSearch + 0 : 0;

			return 1;
		}

		return 0;
	}	

	public function getProjectPath() {
		$rootPath = $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk';
		$projectPath = $rootPath . '/m3-api-reports/2-vendors';

		return $projectPath;
	}

	public function downloadNewPriceList($vendor) {
		$extractFolder = $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk/m3-api-reports/2-vendors/controllers/extract/' . $vendor['vendor_name'];
		if (!file_exists($extractFolder)) {
			mkdir($extractFolder);
		}
		$extractXlsxFolder = $extractFolder . '/' . $vendor['extract_xlsx_folder'];
		$xlsFile = $extractFolder . '/' . $vendor['file_list'][0];

		$this->getFileByUrl($vendor['vendor_url'], $xlsFile);
		foreach ($vendor['file_list'] as $file) {
			$this->extractZipArchive($extractFolder . '/' . $file, $extractFolder);
		}

		return $extractFolder;
	}

	public function extractZipArchive($path, $folder) {
        $zip = new ZipArchive;
        if ($zip->open($path) === TRUE) {
            $zip->extractTo($folder);
            $zip->close();
        }
    }

    public function getFileByUrl($from, $path) {
        file_put_contents($path, fopen($from, 'r'));
    }

    public function getProducts($extractFolder) {
        $xml = simplexml_load_file($extractFolder . '/xl/sharedStrings.xml');
        $sharedStrings = array();
        foreach ($xml->children() as $item) {
            $sharedStrings[] = (string)$item->t;
        }
        return $sharedStrings;
    }

    public function getRowListFromVendor($extractFolder, $vendorName) {
		$sharedStrings = $this->getProducts($extractFolder);
		$allStringsFromFile = $this->getAllStringsFromFile($extractFolder);
		$tableRowList = [];
		$currentTableRow = new Controller_TableRow();
		$setCurrentStringMethodName = 'setCurrentString' . ucfirst($vendorName);
		$parseRowMethodName = 'parseRow' . ucfirst($vendorName);
		foreach ($allStringsFromFile as $line) {
		    $currentTableRow->$setCurrentStringMethodName($line);
		    $currentTableRow->$parseRowMethodName($sharedStrings);
		    $currentRow = $currentTableRow->getRowNumber();
		    if (!empty($currentRow)) {
		        $tableRowList[$currentRow-1] = $currentTableRow;
		        $currentTableRow = new Controller_TableRow();
		    }
		}
		if ($this->isCorrectFile($tableRowList)) {
			return $tableRowList;
		}

		return 0;
   }

    public function checkColumn($tableRowList, $row, $letter, $value) {
    	if ($tableRowList[$row]->getCellText($letter) === $value) {
    		echo '<tr style="background: palegreen;"><td>' . $value . '</td><td style="text-align: center;">+</td></tr>';
    		$result = 1;
    	} else {
    		echo '<tr style="background: pink;"><td>' . $value . '</td><td style="text-align: center;">-</td></tr>';
    		$result = 0;
    	}

    	return $result;
    }

}

?>