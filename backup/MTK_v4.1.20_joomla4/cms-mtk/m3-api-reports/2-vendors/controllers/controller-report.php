<?php
require_once $projectPath . '/controllers/controller-vendors.php';

class Controller_Report extends Controller_Vendors
{

    public function getVendorArticle($product, $vendors)
    {
        $article = $product['mtk_article'];
        $vendorProductUrl = $vendors[$product['vendorShortName']]['vendor_url'];
        if ($vendorProductUrl != 'none') {
            if ($vendors[$product['vendorShortName']]['vendor_name'] === 'electromir') {
                if (strpos($article, ':')) {
                    $article = explode(':', $article)[0];
                }
                $url = 'https://rubilnik.ru/catalog/krepezhnye_izdeliya/aksessuary_dlya_trosa_tsepey_nitey/?digiSearch=true&term=' . $article . '&params=%7Csort%3DDEFAULT';
            } else {
                $url = $vendorProductUrl . $article;
            }
            return '
				<a class="vendor-article" target="_blank" href="' . $url . '">' .
                $product['mtk_article'] . '
				</a>';
        }
        return $article;
    }

    public function getDiffSign($value)
    {
        return ($value > 0) ? 'diff-plus' : 'diff-minus';
    }

    public function getNewPrice($product, $vendors)
    {
        $currentVendor = $this->getCurrentVendor($product, $vendors);
        if ($currentVendor != 0) {
            $newPrice = $this->calculateNewPrice($product['vendor_retail_price'], $currentVendor['percent']);
            return $newPrice;
        }
        return -1;
    }

    public function calculateNewPrice($price, $percent)
    {
        $vendorPrice = (double) $price;
        $price = (double) $price;
        $newPrice = floor($vendorPrice + ($vendorPrice * $percent / 100));
        $newPrice = ($newPrice == 0) ? 1 : $newPrice;
        $newPrice = ($newPrice == floor($price)) ? $newPrice++ : $newPrice;
        return $newPrice;
    }

    public function getCurrentVendor($product, $vendors)
    {
        foreach ($vendors as $vendor) {
            if ($vendor['mtk_vendor_name'] == $product['vendor']) {
                return $vendor;
            }
        }
        return 0;
    }

    public function getPriceDifference($mtkPrice, $newPrice)
    {
        if ($newPrice == '-') return '';
        $priceDifference = $newPrice - $mtkPrice;
        $priceDifference = ($priceDifference > 0) ? '+' . $priceDifference : $priceDifference;
        return $priceDifference;
    }

    public function getVendorByPost($vendorOptions)
    {
        return (isset($_POST['vendor'])) ? $_POST['vendor'] : $vendorOptions['sds']['mtk_vendor_name'];
    }

    public function getSortByPost()
    {
        return (isset($_POST['sortBy'])) ? $_POST['sortBy'] : 'diff';
    }

}

?>