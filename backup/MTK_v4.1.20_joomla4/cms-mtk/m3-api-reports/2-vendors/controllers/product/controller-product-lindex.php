<?php

class Controller_ProductLindex extends Controller_Vendors
{
    public $currentVendor;
    protected $productVendor;
    protected $intnotesList = '';

    public function __construct($vendor, $productFromDb)
    {
        $this->currentVendor = $vendor;
        $this->saveProductMtk($productFromDb);
        $this->intnotesList = $this->parseIntnotes($productFromDb['mtk_article']);
    }

    public function getProduct($productsFromVendor)
    {
        $this->productVendor['countArticles'] = 0;
        $this->productVendor['vendor'] = $this->currentVendor['vendor_name'];
        $this->productVendor['vendor_db'] = $this->currentVendor['mtk_vendor_name'];
        $this->productVendor['reelSize'] = 0;

        echo '<table style="background: ' . $this->currentVendor['color'] . ';"><tr><td>';
        foreach ($this->intnotesList as $value) {
            echo ucfirst($this->productVendor['vendor']) . ": <span style='color: cornflowerblue;'>" . $value . "</span> -> ";
            if (isset($productsFromVendor[$value])) {
                $this->productVendor['isProductFound'] = true;
                $this->productVendor['article'] = $value;
                $this->productVendor['verifyProduct'] = 1;
                $this->productVendor['countArticles']++;
                $this->productVendor['unit'] = $productsFromVendor[$value]['unit'];
                $this->productVendor['name'] = $productsFromVendor[$value]['name'];
                $this->productVendor['url'] = $this->productVendor['article'];
                $this->getReelSize();
                $this->productVendor['countProducts'] = $productsFromVendor[$value]['stock'];
                if ($this->productVendor['reelSize'] != 0 and ($this->productMtk['unit'] == 'P' or $this->productMtk['unit'] == 'M')) {
                    echo '<span style="color: red;">делю цену и умножаю количество на размер бухты</span>';
                    $this->productVendor['retailPrice'] = round($productsFromVendor[$value]['price'] / $this->productVendor['reelSize'], 2);
                    $this->productVendor['countProducts'] = $productsFromVendor[$value]['stock'] * $this->productVendor['reelSize'];
                } else {
                    echo '<span style="color: #bbb;">оставляю цену и количество как у поставщика</span>';
                    $this->productVendor['retailPrice'] = $productsFromVendor[$value]['price'];
                }
                $this->productVendor['percentPrice'] = round(($this->getProductMtk()['price'] - $this->productVendor['retailPrice']) / $this->productVendor['retailPrice'] * 100, 0);
                $this->productVendor['price'] = '-';
                echo ' <span style="color: darkgreen;">PASS</span>';
            } else {
                echo ' <span style="color: red;">FAIL</span>';
            }
            echo '</td></tr><tr><td>';
        }
        echo '</td></tr></table>';
    }

    public function getReelSize()
    { // Получить объем бухты
        foreach ($this->reelSizeTmpl as $value) {
            $result = $this->searchInText($value);
            if ($result) break;
        }
    }


}

?>