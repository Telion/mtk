<?php

class Controller_ProductElectromir extends Controller_Vendors
{
    public $currentVendor;
    protected $productVendor;
    protected $intnotesList = '';

    function __construct($vendorName, $productFromDb)
    {
        $this->currentVendor = $vendorName;
        $this->saveProductMtk($productFromDb);
        $this->intnotesList = $this->parseIntnotes($productFromDb['mtk_article']);
    }

    function getProduct($productsFromVendor = [])
    {
        echo '<table style="background: ' . $this->currentVendor['color'] . ';"><tr><td>';
        foreach ($this->intnotesList as $currentValue) {
            $this->productVendor['vendor'] = $this->currentVendor['vendor_name'];
            $this->productVendor['vendor_db'] = $this->currentVendor['mtk_vendor_name'];
            echo ucfirst($this->productVendor['vendor']) . ": <span style='color: cornflowerblue;'>" . $currentValue . "</span> -> ";
            $isProductFound = $this->parseVendorSite($currentValue);
            if ($isProductFound) {
                $this->productVendor['isProductFound'] = true;
                $this->productVendor['reelSize'] = 0;
                $this->productVendor['countArticles'] = 0;
                $this->productVendor['countProducts'] = '-';
                $this->productVendor['currentArticle'] = $currentValue;
                $this->productVendor['article'] = $currentValue;
                $this->productVendor['verifyProduct'] = 1;
                $this->productVendor['countArticles']++;
                echo ' <span style="color: darkgreen;">PASS</span>';
            } else {
                $modifiedCurrentValue = $this->getModifiedCurrentValue($currentValue);
                $isModifiedArticleProductFound = $this->parseVendorSite($modifiedCurrentValue);
                if ($isModifiedArticleProductFound) {
                    $this->productVendor['reelSize'] = 0;
                    $this->productVendor['countArticles'] = 0;
                    $this->productVendor['countProducts'] = '-';
                    $this->productVendor['currentArticle'] = $modifiedCurrentValue;
                    $this->productVendor['article'] = $modifiedCurrentValue;
                    $this->productVendor['verifyProduct'] = 1;
                    $this->productVendor['countArticles']++;
                    echo ' <span style="color: darkgreen;">PASS</span>';
                } else {
                    echo ' <span style="color: orange;">FAIL</span>';
                }
            }
            echo '</td></tr><tr><td>';
        }
        echo '</tr></table>';
    }

    function parseVendorSite($currentArticle)
    {
        if ($currentArticle !== 0) {
            $productsFromSearchPage = $this->getProductsFromSearchPage($currentArticle);
            if (!isset($productsFromSearchPage)) {
                return false;
            }

            $foundProduct = $this->getFoundProduct($productsFromSearchPage, $currentArticle);
            if ($foundProduct == 0) {
                return false;
            }
            $this->productVendor['isProductFound'] = true;
            $this->productVendor['name'] = $foundProduct['name'];
            $this->productVendor['price'] = $foundProduct['price'];
            $this->productVendor['retailPrice'] = '-';
            $this->productVendor['unit'] = $foundProduct['attributes']['обозначение единицы измерения'][0];
            $this->productVendor['countProducts'] = '-';
            return true;
        }
        return false;
    }

    function getProductsFromSearchPage($article)
    {
        if (strpos($article, ':')) {
            $article = explode(':', $article)[0];
        }
        $urlSearchPage = $this->currentVendor['vendor_url'] . $article . '&apiKey=8CPFJ688A9&shuffle=true&strategy=advanced,zero_queries&productsSize=100&regionId=global&forIs=false&showUnavailable=false&withContent=false&withSku=false';;
        printr($urlSearchPage);
        $curlResult = $this->getDataFromSiteByCurl($urlSearchPage);
        $curlResult = json_decode($curlResult, true);
        return $curlResult;
    }

    function getFoundProduct($productsFromSearchPage, $article)
    {
        if (strpos($article, ':')) {
            $article = explode(':', $article)[1];
        }
        $productList = $productsFromSearchPage['products'];
        foreach ($productList as $product) {
            $pos = strpos($product['attributes']['артикул'][0], $article);
            if ($pos !== false) {
                return $product;
            }
        }
        return 0;
    }

    function getModifiedCurrentValue($currentValue) {
        if (strlen($currentValue) == 5) {
            return '0' . $currentValue;
        }
        return 0;
    }
}

?>