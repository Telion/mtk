<?php

class Controller_ProductSds extends Controller_Vendors
{
    public $currentVendor;
    protected $productVendor;
    protected $intnotesList = '';

    public function __construct($vendorName, $productFromDb)
    {
        $this->currentVendor = $vendorName;
        $this->saveProductMtk($productFromDb);
        $this->intnotesList = $this->parseIntnotes($productFromDb['mtk_article']);
    }

    public function getProduct($productsFromVendor = [])
    {
        echo '<table style="background: ' . $this->currentVendor['color'] . ';"><tr><td>';
        foreach ($this->intnotesList as $value) {
            $this->productVendor['isProductFound'] = true;
            $this->productVendor['vendor'] = $this->currentVendor['vendor_name'];
            $this->productVendor['vendor_db'] = $this->currentVendor['mtk_vendor_name'];
            $this->productVendor['reelSize'] = 0;
            $this->productVendor['countArticles'] = 0;
            $this->productVendor['countProducts'] = 0;
            $this->productVendor['currentArticle'] = $value;
            $result = (!in_array($value, $this->symbolExceptions)) ? $this->getProductName() : 0;
            echo ucfirst($this->productVendor['vendor']) . ": <span style='color: cornflowerblue;'>" . $value . "</span> -> ";

            if ($result > 0) {
                $this->productVendor['article'] = $value;
                $this->productVendor['verifyProduct'] = 1;
                $this->productVendor['countArticles']++;
                $this->getRemain();
                $this->getPrices();
                $this->getRetailPriceSDS();
                echo ' <span style="color: darkgreen;">PASS</span>';
            } else {
                echo ' <span style="color: red;">FAIL</span>';
            }
            echo '</td></tr><tr><td>';
        }
        echo '</tr></table>';
    }

    public function getProductName()
    { // Получить название и ед.изм по текущему товару СДС
        if ($this->productVendor['currentArticle']) {
            $queryResult = $this->queryToSDS('product');
            if (isset($queryResult['count']) && ($queryResult['count'] > 0)) {
                $this->productVendor['unit'] = $queryResult['results'][0]['unit'];
                $this->productVendor['name'] = $queryResult['results'][0]['name'];
                $this->productVendor['url'] = '<a target="_blank" href="https://www.sds-group.ru/search.htm?search=' . $this->productVendor['currentArticle'] . '">' . $this->productVendor['currentArticle'] . '</a>';
                $this->getReelSize();

                return $queryResult;
            } else {
                echo 'getProductName(), пустой ответ от сервера -> ';
                return 0;
            }

        } else {
            echo 'пустой комментарий (intnotes) -> ';
            return -1;
        }
    }

    public function getReelSize()
    { // Получить объем бухты
        if (($this->productVendor['unit'] === 'бухта') or ($this->productVendor['unit'] === 'упак')) {

            // если в названии есть текст "100 мм" или "200 мм", то не продолжаем поиск
            foreach ($this->reelSizeTmplExcluding as $value) {
                $result = $this->searchInText($value, 0);
                if ($result) break;
            }
            // если нет, то ищем другие фразы
            if (!$result) {
                foreach ($this->reelSizeTmpl as $value) {
                    $result = $this->searchInText($value);
                    if ($result) break;
                }
            }
        }
    }

    public function getRemain()
    {  // Получить остатки по текущему товару СДС
        if ($this->productVendor['currentArticle']) {
            $queryResult = $this->queryToSDS('remain');
            if ($queryResult['count'] > 0) {
		if (isset($queryResult['results'][0]['attribute']['count'])) {
			$countFromSds = $queryResult['results'][0]['attribute']['count'];
		} else {
			$countFromSds = $this->getCountFromSds($queryResult['results'][0]['attribute']);
		}
                $count = ($this->productVendor['reelSize'] != 0) ? $countFromSds * $this->productVendor['reelSize'] : $countFromSds + 0;
                $this->productVendor['countProducts'] += $count;
                echo " / Получаю остаток: <span style='color: red;'>+" . $count . " = " . $this->productVendor['countProducts'] . " шт.</span>";
                return $queryResult;
            } else {
                echo ' getRemain(), пустой ответ от сервера -> ';
                return 0;
            }
        } else {
            echo ' пустой комментарий (intnotes) -> ';
            return -1;
        }
    }

    public function getCountFromSds($array) {
	$count = 0;
	if (count($array) > 0) {
	    foreach ($array as $value) {
		$count = ($value['count'] > $count) ? $value['count'] : $count;
	    }
	}
	return $count;
    }

    public function getPrices()
    {
        if ($this->productVendor['currentArticle']) {
            $queryResult = $this->queryToSDS('prices');
            if ($queryResult['count'] > 0) {
		$priceValue = $this->getPriceValueFromSds($queryResult['results'][0]['attribute']);
                if ($this->productVendor['reelSize'] != 0 and ($this->productMtk['unit'] == 'P' or $this->productMtk['unit'] == 'M')) $this->productVendor['price'] = round($priceValue / $this->productVendor['reelSize'], 2);
                else $this->productVendor['price'] = $priceValue;
                echo " / Получаю цену по API: <span style='color: blue;'>" . $this->productVendor['price'] . " руб.</span>";
                $this->productVendor['percentPrice'] = round(($this->productMtk['price'] - $this->productVendor['price']) / $this->productVendor['price'] * 100, 0);
                return $queryResult;
            } else {
                echo 'getPrices(), пустой ответ от сервера -> ';
                return 0;
            }
        } else {
            echo 'пустой комментарий (intnotes) -> ';
            return -1;
        }
    }

    public function getPriceValueFromSds($array) {
	if (count($array) > 0) {
	    foreach ($array as $value) {
		if ($value['ratename'] == "Субдилер") {
		    return $value['value'];
		}
	    }
	}
	return 0;
    }

    public function queryToSDS($method)
    { // запрос к API СДС и получение остатков товаров по артикулу

        $prefix = $this->getPrefixSDS($method);
        if ($prefix) {
            $url = "http://193.106.69.207:8760/rexant/hs/api/v1/" . $prefix . $this->productVendor['currentArticle'];

            $headers = [
                'Content-Type: application/json',
                'Accept: application/json',
                'Authorization: Token 350f169dc59fd14a606c9c6a87c1ed1d7a4b21e1'
            ];

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_REFERER, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_COOKIESESSION, true);
            curl_setopt($curl, CURLOPT_POST, false);
            curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0 (Windows; U; Windows NT 5.0; En; rv:1.8.0.2) Gecko/20070306 Firefox/1.0.0.4");
            $queryResult = json_decode(curl_exec($curl), true);
            curl_close($curl);
        } else {
            return 0;
        }

        return $queryResult['result'];
    }

    public function getPrefixSDS($method)
    { // Сгенерировать префикс для подключения к нужному методу API сдс
        if ($method === 'product') {
            $prefix = 'product?article=';
        } elseif ($method === 'remain') {
            $prefix = 'remain?productid__article=';
        } elseif ($method === 'prices') {
            $prefix = 'prices?productid__article=';
        } else {
            echo "Неизвеcтный метод API ->";
            return 0;
        }
        return $prefix;
    }

    public function getRetailPriceSDS()
    {
        echo ' / Получаю розничную цену СДС: ';
        $html = file_get_html('https://www.sds-group.ru/search.htm?search=' . $this->productVendor['currentArticle']);

        $retailSdsPrice = 0;
        $sdsIntnote = 'not found';
        if ($html->find('ul.search-page li', 1)) {
            // Перебираю все товары найденные на странице поиска в СДС
            foreach ($html->find('ul.search-page li') as $product) {
                $sdsArticle = $product->find('div', 4);
                $sdsArticle = str_replace(array("\r\n", "\r", "\n"), '', strip_tags($sdsArticle));
                if ($sdsArticle == $this->productVendor['currentArticle']) {
                    $sdsIntnote = $this->productVendor['currentArticle'];
                    $retailSdsPrice = $product->find('div[class*="price"]', 0)->find('div', 1);
                    $retailSdsPrice = preg_replace("/[^,.0-9]/", '', $retailSdsPrice);
                    $retailSdsPrice = str_replace(array("\r\n", "\r", "\n"), '', strip_tags($retailSdsPrice));
                    $retailSdsPrice = (($this->productMtk['unit'] == 'P' or $this->productMtk['unit'] == 'M') and ($this->productVendor['unit'] == 'бухта' or $this->productVendor['unit'] == 'упак') and $this->productVendor['reelSize'] != 0) ? round($retailSdsPrice / $this->productVendor['reelSize'], 2) : $retailSdsPrice;
                    $this->productVendor['retailPrice'] = $retailSdsPrice;
                }
            }
        } elseif ($html->find('.price', 1)) {
		$price = $html->find('.price', 1);
		$price = str_replace('&8381;', '', html_entity_decode($price)); 
		$this->productVendor['retailPrice'] = preg_replace("/[^,.0-9]/", '', $price);
	}
        echo "<span style='color: green;'>" . $this->productVendor['retailPrice'] . " руб.</span>";
        echo '<br><br>';
    }

}

?>