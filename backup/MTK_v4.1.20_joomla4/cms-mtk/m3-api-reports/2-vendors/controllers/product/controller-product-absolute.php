﻿<?php

class Controller_ProductAbsolute extends Controller_Vendors
{
    protected $baseProductPageUrl = 'https://absolut-tds.com';
    public $currentVendor;
    protected $productVendor;
    protected $intnotesList = '';

    function __construct($vendorName, $productFromDb)
    {
        $this->currentVendor = $vendorName;
        $this->saveProductMtk($productFromDb);
        $this->intnotesList = $this->parseIntnotes($productFromDb['mtk_article']);
    }

    function getProduct($productsFromVendor = [])
    {
        echo '<table style="background: ' . $this->currentVendor['color'] . ';"><tr><td>';
        foreach ($this->intnotesList as $value) {
            $this->productVendor['vendor'] = $this->currentVendor['vendor_name'];
            $this->productVendor['vendor_db'] = $this->currentVendor['mtk_vendor_name'];
            echo ucfirst($this->productVendor['vendor']) . ": <span style='color: cornflowerblue;'>" . $value . "</span> -> ";
            $isProductFound = $this->parseVendorSite($value);
            if ($isProductFound) {
                $this->productVendor['isProductFound'] = true;
                $this->getReelSize();

                if (isset($this->productVendor['reelSize']) and $this->productVendor['reelSize'] != 0 and ($this->productMtk['unit'] == 'P' or $this->productMtk['unit'] == 'M')) {
                    echo '<span style="color: red;">делю цену и умножаю количество на размер бухты</span>';
                    $this->productVendor['retailPrice'] = round($this->productVendor['price'] / $this->productVendor['reelSize'], 2);
                    $this->productVendor['price'] = $this->productVendor['retailPrice'];
                    $this->productVendor['countProducts'] = '-';
                } else {
                    echo '<span style="color: #bbb;">оставляю цену и количество как у поставщика</span>';
                    $this->productVendor['retailPrice'] = $this->productVendor['price'];
                }
                $this->productVendor['countArticles'] = 0;
                $this->productVendor['currentArticle'] = $value;
                $this->productVendor['article'] = $value;
                $this->productVendor['verifyProduct'] = 1;
                $this->productVendor['countArticles']++;
                echo ' <span style="color: darkgreen;">PASS</span>';
            } else {
                echo ' <span style="color: orange;">FAIL - не нашел в поске на сайте поставщика</span>';
                // todo: если не нашел товар на сайте поставщика, значит нужно сбросить остатки и цену
            }
            echo '</td></tr><tr><td>';
        }
        echo '</tr></table>';
    }

    function parseVendorSite($currentArticle)
    {
        $foundProductUrl = $this->getProductPageUrl($currentArticle);
        if (empty($foundProductUrl)) {
            return false;
        }
        $htmlProductPage = $this->getHtmlProductPage($foundProductUrl);
        $this->productVendor['name'] = $this->getProductName($htmlProductPage);
        $this->productVendor['price'] = $this->getProductPrice($htmlProductPage);
        $this->productVendor['retailPrice'] = '-';
        $this->productVendor['countProducts'] = '-';
        $this->productVendor['unit'] = $this->getProductUnit($htmlProductPage);
        return true;
    }

    function getProductPageUrl($article)
    {
        $urlSearchPage = $this->currentVendor['vendor_url'] . $article;
        $htmlSearchPage = file_get_html($urlSearchPage);
        if (isset($htmlSearchPage->find('.inner-listing div.absent a')[0]->href)) {
            return $htmlSearchPage->find('.inner-listing div.absent a')[0]->href;
        }
        return 0;
    }

    function getHtmlProductPage($foundProductUrl)
    {
        $urlProductPage = $this->baseProductPageUrl . $foundProductUrl;
        $str = file_get_contents($urlProductPage);
        $str = iconv('Windows-1251', 'UTF-8', $str);
        return str_get_html($str);
    }

    function getProductPrice($html)
    {
        return (double) $html->find('.price-box .price')[0]->innertext;
    }

    function getProductUnit($html)
    {
        $text = $html->find('.price-box .price')[0]->innertext;
        $unit = explode('/', $text)[1];
        return explode('.', $unit)[0];
    }

    function getProductCount($html)
    {
        $countProductsText = $html->find('.product-info-stock-sku .info-deta')[0]->innertext;
        return ($countProductsText == 'Есть на складе') ? 1 : 0;
    }

    function getProductName($html)
    {
        return $html->find('.product-item-name')[0]->innertext;
    }

}

?>