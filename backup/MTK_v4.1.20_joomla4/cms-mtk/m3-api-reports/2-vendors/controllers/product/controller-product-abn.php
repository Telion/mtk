<?php

class Controller_ProductAbn extends Controller_Vendors
{
    public $currentVendor;
    protected $productVendor;
    protected $intnotesList = '';

    function __construct($vendorName, $productFromDb)
    {
        $this->currentVendor = $vendorName;
        $this->saveProductMtk($productFromDb);
        $this->intnotesList = $this->parseIntnotes($productFromDb['mtk_article']);
    }

    function getProduct($productsFromVendor = [])
    {
        echo '<table style="background: ' . $this->currentVendor['color'] . ';"><tr><td>';
        foreach ($this->intnotesList as $currentValue) {
            $this->productVendor['vendor'] = $this->currentVendor['vendor_name'];
            $this->productVendor['vendor_db'] = $this->currentVendor['mtk_vendor_name'];
            echo ucfirst($this->productVendor['vendor']) . ": <span style='color: cornflowerblue;'>" . $currentValue . "</span> -> ";
            $isProductFound = $this->parseVendorSite($currentValue);
            if ($isProductFound) {
                $this->productVendor['isProductFound'] = true;
                $this->productVendor['reelSize'] = 0;
                $this->productVendor['countArticles'] = 0;
                $this->productVendor['countProducts'] = 0;
                $this->productVendor['currentArticle'] = $currentValue;
                $this->productVendor['article'] = $currentValue;
                $this->productVendor['verifyProduct'] = 1;
                $this->productVendor['countArticles']++;
                echo ' <span style="color: darkgreen;">PASS</span>';
            } else {
                $modifiedCurrentValue = $this->getModifiedCurrentValue($currentValue);
                $isModifiedArticleProductFound = $this->parseVendorSite($modifiedCurrentValue);
                if ($isModifiedArticleProductFound) {
                    $this->productVendor['reelSize'] = 0;
                    $this->productVendor['countArticles'] = 0;
                    $this->productVendor['countProducts'] = 0;
                    $this->productVendor['currentArticle'] = $modifiedCurrentValue;
                    $this->productVendor['article'] = $modifiedCurrentValue;
                    $this->productVendor['verifyProduct'] = 1;
                    $this->productVendor['countArticles']++;
                    echo ' <span style="color: darkgreen;">PASS</span>';
                } else {
                    echo ' <span style="color: orange;">FAIL</span>';
                }
            }
            echo '</td></tr><tr><td>';
        }
        echo '</tr></table>';
    }

    function parseVendorSite($currentArticle)
    {
        if ($currentArticle !== 0) {
            $productFromSearchPage = $this->getProductFromSearchPage($currentArticle);
            if ($productFromSearchPage !== 0) {
                $foundProduct = $this->getFoundProduct($productFromSearchPage, $currentArticle);
                if ($foundProduct != 0) {
                    return true;
                }
            }
        }
        return false;
    }

    function getProductFromSearchPage($article)
    {
        $urlSearchPage = $this->currentVendor['vendor_url'] . $article;
        $productFromWebSite = $this->getDataFromSiteBySimple($urlSearchPage);
        return $productFromWebSite;
    }

    public function getDataFromSiteBySimple($url)
    {
        $html = file_get_html($url);
        if ($html->find('.section-panel-item-list__item', 1)) {
            foreach ($html->find('.section-panel-item-list__item a') as $product) {
                if (stristr($product->href, '/catalog/')) {
                    return $product->href;
                }
            }
        }
        echo '<br><br>';
        return 0;
    }

    function getFoundProduct($productUrl, $article)
    {
        $urlProductPage = 'https://www.abn.ru' . $productUrl;
        $html = file_get_html($urlProductPage);

        if ($html->find('.product-article-header__title')) {
            $this->productVendor['isProductFound'] = true;
            $this->productVendor['article'] = $article;
            $this->productVendor['name'] = $html->find('.product-article-header__title span')[0]->plaintext;
            $this->productVendor['price'] = $html->find('.price')[0]->plaintext;
            $this->productVendor['retailPrice'] = '-';
            $stockString = $html->find('.product-article-table__item-count')[0]->plaintext;
            $this->productVendor['countProducts'] = explode(' ', $stockString)[0];
            $this->productVendor['unit'] = explode(' ', $stockString)[1];
        }
        echo '<br><br>';
        return 0;
    }

    function getModifiedCurrentValue($currentValue) {
        if (strlen($currentValue) == 5) {
            return '0' . $currentValue;
        }
        return 0;
    }
}

?>
