<?php

class Controller_Vendors
{
    protected $productMtk;
    protected $productVendor;
    protected $symbolExceptions = ['+', '"М"', '-'];
    protected $reelSizeTmplExcluding = ['100 мм', '200 мм'];
    protected $reelSizeTmpl = ['(бухта 200 м)', ' 50 м ', ' 100 м ', ' 100 м,', '100м', '200 м', '305 м', '305м',
        '100 шт./уп.', 'катушка 305', 'коробка 305', 'катушка 500',
        '2 шт', '3шт.', '4 шт ', '25шт', '10шт', '10 шт', '20шт', '5шт', '250шт', '500 шт']; // какие размеры ищу в названии товара

    function getProductMtk()
    {
        return $this->productMtk;
    }

    function getValues()
    {
        echo '<table style="border-collapse: collapse;">
			<tr><td style="border: solid 1px #777; padding: 2px 10px 3px;">vendor</td><td style="border: solid 1px #777; padding: 2px 10px 3px;">' . $this->productVendor['vendor'] . '</td></tr>
			<tr><td style="border: solid 1px #777; padding: 2px 10px 3px;">article</td><td style="border: solid 1px #777; padding: 2px 10px 3px;">' . $this->productVendor['article'] . '</td></tr>
			<tr><td style="border: solid 1px #777; padding: 2px 10px 3px;">countProducts</td><td style="border: solid 1px #777; padding: 2px 10px 3px;">' . $this->productVendor['countProducts'] . '</td></tr>
			<tr><td style="border: solid 1px #777; padding: 2px 10px 3px;">price</td><td style="border: solid 1px #777; padding: 2px 10px 3px;">' . $this->productVendor['price'] . '</td></tr>
			<tr><td style="border: solid 1px #777; padding: 2px 10px 3px;">retailPrice</td><td style="border: solid 1px #777; padding: 2px 10px 3px;">' . $this->productVendor['retailPrice'] . '</td></tr>
			<tr><td colspan=2>МТК</td></tr>
			<tr><td style="border: solid 1px #777; padding: 2px 10px 3px;">id</td><td style="border: solid 1px #777; padding: 2px 10px 3px;">' . $this->productMtk['id'] . '</td></tr>
			<tr><td style="border: solid 1px #777; padding: 2px 10px 3px;">mtk_article</td><td style="border: solid 1px #777; padding: 2px 10px 3px;">' . $this->productMtk['article'] . '</td></tr>
			<tr><td style="border: solid 1px #777; padding: 2px 10px 3px;">price</td><td style="border: solid 1px #777; padding: 2px 10px 3px;">' . $this->productMtk['price'] . '</td></tr>
			</table></td>
		';
        $result = [];
        $result['id'] = $this->productMtk['id'];
        $result['vendor'] = $this->productVendor['vendor_db'];
        $result['article'] = $this->productVendor['article'];
        $result['vendorCount'] = $this->productVendor['countProducts'];
        $result['vendorPrice'] = $this->productVendor['price'];
        $result['vendorRetailPrice'] = $this->productVendor['retailPrice'];
        $result['isProductFound'] = $this->productVendor['isProductFound'];
        return $result;
    }

    function getVendorProductName()
    {
        if (isset($this->productVendor['name'])) {
            return $this->productVendor['name'];
        }
        return '';
    }

    function parseIntnotes($intnotes)
    {
        $temp = str_replace(' ', ',', $intnotes);
        $temp = str_replace('&#9;', ',', $temp);
        $temp = str_replace("&#13;&#10;", ',', $temp);
        $temp = preg_replace('/[\,]{2,}/', ',', $temp);
        $temp = explode(',', $temp);
        $result = array_diff($temp, array(''));
        return $result;
    }

    function saveProductMtk($productFromDb)
    {
        $this->productMtk['id'] = $productFromDb['mtk_id'];
        $this->productMtk['article'] = $productFromDb['mtk_article'];
        $this->productMtk['name'] = $productFromDb['mtk_name'];
        $this->productMtk['unit'] = $productFromDb['mtk_unit'];
        $this->productMtk['price'] = round($productFromDb['mtk_price'], 2);
        $this->productMtk['url'] = '<div style="width: max-content; overflow: hidden; white-space: nowrap; width: 520px; text-overflow: ellipsis;"><a target="_blank" href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id=' . $this->productMtk['id'] . '">' . $this->productMtk['name'] . '</a></div>';
    }

    public function getReelSize()
    { // Получить объем бухты
        foreach ($this->reelSizeTmpl as $value) {
            $result = $this->searchInText($value);
            if ($result) break;
        }
    }

    function searchInText($search, $excl = 1) {
        if (strpos($this->productVendor['name'], $search)) {
            $numberFromSearch = preg_replace('/[^0-9]/', '', $search);
            $this->productVendor['reelSize'] = ($excl == 1) ? $numberFromSearch + 0 : 0;
            return 1;
        }
        return 0;
    }

    function getProjectPath()
    {
        $rootPath = $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk';
        $projectPath = $rootPath . '/m3-api-reports/2-vendors';
        return $projectPath;
    }

    function downloadNewPriceList($vendor)
    {
        $extractFolder = $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk/m3-api-reports/2-vendors/controllers/extract/' . $vendor['vendor_name'];
        if (!file_exists($extractFolder)) {
            mkdir($extractFolder);
        }
        $xlsFile = $extractFolder . '/' . $vendor['file_list'][0];
        $this->getFileByUrl($vendor['vendor_url'], $xlsFile);
        foreach ($vendor['file_list'] as $file) {
            $this->extractZipArchive($extractFolder . '/' . $file, $extractFolder);
        }
        return $extractFolder;
    }

    function extractZipArchive($path, $folder)
    {
        $zip = new ZipArchive;
        if ($zip->open($path) === TRUE) {
            $zip->extractTo($folder);
            $zip->close();
        }
    }

    function getFileByUrl($from, $path)
    {
        file_put_contents($path, fopen($from, 'r'));
    }

    function getProducts($extractFolder)
    {
        $xml = simplexml_load_file($extractFolder . '/xl/sharedStrings.xml');
        $sharedStrings = array();
        foreach ($xml->children() as $item) {
            $sharedStrings[] = (string)$item->t;
        }
        return $sharedStrings;
    }

    function getRowListFromVendor($extractFolder, $vendorName)
    {
        $sharedStrings = $this->getProducts($extractFolder);
        $allStringsFromFile = $this->getAllStringsFromFile($extractFolder);
        $tableRowList = [];
        $currentTableRow = new Controller_TableRow();
        $setCurrentStringMethodName = 'setCurrentString' . ucfirst($vendorName);
        $parseRowMethodName = 'parseRow' . ucfirst($vendorName);
        foreach ($allStringsFromFile as $line) {
            $currentTableRow->$setCurrentStringMethodName($line);
            $currentTableRow->$parseRowMethodName($sharedStrings);
            $currentRow = $currentTableRow->getRowNumber();
            if (!empty($currentRow)) {
                $tableRowList[$currentRow - 1] = $currentTableRow;
                $currentTableRow = new Controller_TableRow();
            }
        }
        if ($this->isCorrectFile($tableRowList)) {
            return $tableRowList;
        }
        return 0;
    }

    function checkColumn($tableRowList, $row, $letter, $value)
    {
        if ($tableRowList[$row]->getCellText($letter) === $value) {
            echo '<tr style="background: palegreen;"><td>' . $value . '</td><td style="text-align: center;">+</td></tr>';
            $result = 1;
        } else {
            echo '<tr style="background: pink;"><td>' . $value . '</td><td style="text-align: center;">-</td></tr>';
            $result = 0;
        }
        return $result;
    }

    function getDataFromSiteByCurl($url) {
        $headers = [
            'Content-Type: application/json',
            'Accept: application/json'
        ];
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_REFERER, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_COOKIESESSION, true);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36 OPR/86.0.4363.50");
        $curlResult = curl_exec($curl);
        curl_close($curl);
        return $curlResult;
    }

}

?>