<?php

class Controller_TableRowXlsx
{
    private $rowNumber = '';
    private $currentString = '';
    private $currentStringDecode = '';
    private $currentLetter = '';
    private $params = [];

    public function __construct() {

    }

    public function getRowNumber()
    {
        if (isset($this->rowNumber)) return $this->rowNumber;
        return '';
    }

    public function setRowNumber($value)
    {
        $this->rowNumber = $value;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function printLine($line)
    {
        echo '<span style="color: red;">' . $line . '</span><hr>';
    }

    public function getCurrentString()
    {
        return $this->currentString;
    }

    public function getCellText($letter)
    {
        return $this->params[$letter]['text'];
    }

    public function getStartPos($text, $needle)
    {
        $needle = htmlspecialchars($needle);
        return mb_stripos($text, $needle) + strlen($needle);
    }


    public function setCurrentString($string)
    {
        $this->currentString = $string;
        //$this->currentStringDecode = html_entity_decode($string);
        $this->currentStringDecode = htmlspecialchars($string);
    }


    public function parseRow($stringList)
    {
        if (mb_stripos($this->currentStringDecode, 'row r=')) {
            $temp = mb_stristr($this->currentString, '&quot; ht=', true);
            if (mb_stripos($this->currentString, '&quot; spans')) {
                $temp = mb_stristr($this->currentString, '&quot; spans', true);
            }
            $start = mb_stripos($temp, 'row r=&quot;') + 12;
            $this->rowNumber = mb_substr($temp, $start, 10);
        }

        if (mb_stripos($this->currentStringDecode, 'c r=')) {
            $temp = mb_stristr($this->currentString, htmlspecialchars('" s="'), true);
            $start = $this->getStartPos($temp, 'c r="');
            $columnLetter = mb_substr($temp, $start, 10);
            $columnLetter = preg_replace('/[[:digit:]]/', '', $columnLetter);
            $this->currentLetter = $columnLetter;
            $temp = mb_stristr($this->currentString, htmlspecialchars(' t="'), false);
            if (!empty($temp)) {
                $columnType = explode('&quot;', $temp)[1];
                $this->params[$this->currentLetter]['type'] = $columnType;
            }
        }

        if (mb_stripos($this->currentStringDecode, '/v')) {
            $temp = mb_stristr($this->currentString, htmlspecialchars('</v'), true);
            $columnValue = round(str_replace("&lt;v&gt;", "", $temp));
            $this->params[$this->currentLetter]['value'] = $columnValue;
            if ($this->params[$this->currentLetter]['type'] === 's') {
                $this->params[$this->currentLetter]['text'] = $stringList[$columnValue];
            }
        }
    }
}

?>