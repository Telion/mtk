<?php

class controller_ProductsFromVendor
{
    public $vendor;
    public $extractFolder;
    public $tableRowList = [];

    function __construct($vendor)
    {
        $this->vendor = $vendor;
    }

    function downloadPriceListFromVendor()
    {
        $this->extractFolder = $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk/m3-api-reports/2-vendors/controllers/extract/' . $this->vendor['vendor_name'];
        if (!file_exists($this->extractFolder)) {
            mkdir($this->extractFolder);
        }

        $extractedFile = $this->extractFolder . '/' . $this->vendor['file_list'][0];
        $this->getFileByUrl($this->vendor['vendor_url'], $extractedFile);

        if ($this->vendor['file_type'] == 'xlsx') {
            $extractXlsxFolder = $this->extractFolder;

            $i = 0;
            foreach ($this->vendor['file_list'] as $file) {
                $isLastElement = ($i == $this->getLastElement($this->vendor['file_list']));
                if ($isLastElement) {
                    $extractXlsxFolder = $this->extractFolder . '/' . $this->vendor['extract_xlsx_folder'];
                }
                $this->extractZipArchive($this->extractFolder . '/' . $file, $extractXlsxFolder);
                $i++;
            }
            $this->extractFolder = $extractXlsxFolder;
        }
    }

    function getLastElement($array)
    {
        return count($array) - 1;
    }

    function getFileByUrl($from, $path)
    {
        file_put_contents($path, fopen($from, 'r'));
    }

    function extractZipArchive($path, $folder)
    {
        $zip = new ZipArchive;
        if ($zip->open($path) === TRUE) {
            $zip->extractTo($folder);
            $zip->close();
        }
    }

    function getRowListFromVendorXlsx()
    {
        $sharedStrings = $this->getProducts();
        $allStringsFromFile = $this->getAllStringsFromFile();
        $tableRow = new Controller_TableRowXlsx();
        foreach ($allStringsFromFile as $line) {
            $tableRow->setCurrentString($line);
            $tableRow->parseRow($sharedStrings);
            $currentRow = $tableRow->getRowNumber();
            if (!empty($currentRow)) {
                $this->tableRowList[$currentRow - 1] = $tableRow;
                $tableRow = new Controller_TableRowXlsx();
            }
        }

        $isCorrectFileVendorMethod = 'isCorrectFile' . ucfirst($this->vendor['vendor_name']);
        $this->$isCorrectFileVendorMethod();
    }

    function getRowListFromVendorXls()
    {
        $allStringsFromFile = $this->getAllStringsFromFile();
    }

    function getProducts()
    {
        $xml = simplexml_load_file($this->extractFolder . '/xl/sharedStrings.xml');
        $sharedStrings = array();
        foreach ($xml->children() as $item) {
            $sharedStrings[] = (string)$item->t;
        }
        return $sharedStrings;
    }

    function getAllStringsFromFile()
    {
        printr($this->vendor);
        switch ($this->vendor['file_type']) {
            case 'xlsx':
                $path = $this->extractFolder . '/xl/worksheets/sheet1.xml';
                break;
            case 'xls':
                $path = $this->extractFolder . '/' . $this->vendor['file_list'][0];
                break;
        }

        $fd = fopen($path, 'r') or die("не удалось открыть файл");
        $allStrings = [];
        while (!feof($fd)) {
            $allStrings[] = htmlentities(fgets($fd));
        }

        fclose($fd);
        if (count($allStrings) == 1) {
            $allStrings = explode("&gt;", $allStrings[0]);
        }
        return $allStrings;
    }

    function checkColumn($row, $letter, $value)
    {
        if ($this->tableRowList[$row]->getCellText($letter) === $value) {
            echo '<tr style="background: palegreen;"><td>' . $value . '</td><td style="text-align: center;">+</td></tr>';
            $result = 1;
        } else {
            echo '<tr style="background: pink;"><td>' . $value . '</td><td style="text-align: center;">-</td></tr>';
            $result = 0;
        }

        return $result;
    }

    function getProductListFromSds()
    {
        printr('получаю список товаров из sds');
    }

    function getProductListFromLindex()
    {
        printr('получаю список товаров из линдекс');
        $this->downloadPriceListFromVendor();
        return $this->parsePriceListFromLindex();
    }

    function parsePriceListFromLindex()
    {
        $this->getRowListFromVendorXlsx();
        $productList = [];
        if (is_array($this->tableRowList) || is_object($this->tableRowList)) {
            foreach ($this->tableRowList as $row) {
                $rowNumber = $row->getRowNumber();
                $params = $row->getParams();
                if (($rowNumber >= 15)
                    and isset($params['A'])
                    and isset($params['C'])
                    and isset($params['M'])
                    and ($params['A']['type'] == 's')
                    and ($params['C']['type'] == 's')
                    and ($params['F']['type'] == 's')
                ) {
                    $key = $params['A']['text'];
                    $productList[$key]['id'] = $rowNumber - 1;
                    $productList[$key]['article'] = $params['A']['text'];
                    $productList[$key]['name'] = $params['C']['text'];
                    $productList[$key]['unit'] = $params['F']['text'];
                    $productList[$key]['stock'] = (isset($params['G']['value'])) ? $params['G']['value'] : 0;
                    $productList[$key]['price'] = $params['M']['value'];
                }
            }
        }

        return $productList;
    }

    function isCorrectFileLindex()
    {
        $result = 1;
        echo '<div style="display: inline-block; padding: 10px;"><table style="border: solid 1px #ddd; padding: 10px; border-radius: 6px; border-spacing: 0; background: palegreen;">
    		<tr><th colspan=2 style="text-decoration: underline;">LINDEX: Проверка excel файла:</th></tr>';
        $result = ($this->checkColumn(11, 'A', 'Артикул')) ? $result : 0;
        $result = ($this->checkColumn(11, 'C', 'Номенклатура')) ? $result : 0;
        $result = ($this->checkColumn(11, 'F', 'Упаковка')) ? $result : 0;
        $result = ($this->checkColumn(10, 'G', 'Склад Москва')) ? $result : 0;
        $result = ($this->checkColumn(10, 'H', 'Склад СПб')) ? $result : 0;
        $result = ($this->checkColumn(10, 'I', 'Склад Краснодар')) ? $result : 0;
        $result = ($this->checkColumn(10, 'K', 'Склад Симферополь')) ? $result : 0;
        $result = ($this->checkColumn(10, 'M', 'Розница')) ? $result : 0;
        echo '</table></div>';

        return $result;
    }

    function getProductListFromNetko()
    {
        printr('получаю список товаров из нетко');
        $this->downloadPriceListFromVendor();
        return $this->parsePriceListFromNetko();
    }

    function parsePriceListFromNetko()
    {
        $this->getRowListFromVendorXlsx();
        $productList = [];
        if (is_array($this->tableRowList) || is_object($this->tableRowList)) {
            foreach ($this->tableRowList as $row) {
                $rowNumber = $row->getRowNumber();
                $params = $row->getParams();
                if (($rowNumber > 11)
                    and isset($params['A'])
                    and isset($params['B'])
                    and isset($params['D'])
                    and ($params['A']['type'] == 's')
                    and ($params['B']['type'] == 's')
                    and ($params['D']['type'] == 's')
                ) {
                    $key = $params['B']['text'];
                    $productList[$key]['id'] = $rowNumber - 1;
                    $productList[$key]['article'] = $params['B']['text'];
                    $productList[$key]['name'] = $params['A']['text'];
                    $productList[$key]['unit'] = (isset($params['E']['text'])) ? $params['E']['text'] : '-';
                    $productList[$key]['stock'] = (isset($params['G']['text'])) ? $params['G']['text'] : '-';
                    $productList[$key]['price'] = $params['D']['text'];
                }
            }
        }

        return $productList;
    }

    function isCorrectFileNetko()
    {
        $result = 1;
        echo '<div style="display: inline-block; padding: 10px;"><table style="border: solid 1px #ddd; padding: 10px; border-radius: 6px; border-spacing: 0; background: palegreen;">
    		<tr><th colspan=2 style="text-decoration: underline;">NETKO: Проверка excel файла:</th></tr>';
        $result = ($this->checkColumn(8, 'A', 'Наименование товара')) ? $result : 0;
        $result = ($this->checkColumn(8, 'B', 'Код товара')) ? $result : 0;
        $result = ($this->checkColumn(8, 'C', 'Артикул')) ? $result : 0;
        $result = ($this->checkColumn(8, 'D', 'РРЦ, руб')) ? $result : 0;
        $result = ($this->checkColumn(8, 'E', 'Единица измерения')) ? $result : 0;
        echo '</table></div>';

        return $result;
    }

    function getProductListFromAbsolute()
    {
        printr('получаю список товаров из абсолюта');
        $this->downloadPriceListFromVendor();
        return $this->parsePriceListFromAbsolute();
    }

    function parsePriceListFromAbsolute()
    {
        $this->getRowListFromVendorXls();
        /*$productList = [];
        if (is_array($this->tableRowList) || is_object($this->tableRowList)) {
            foreach ($this->tableRowList as $row) {
                $rowNumber = $row->getRowNumber();
                $params = $row->getParams();
                if (($rowNumber > 11)
                        and isset($params['A'])
                        and isset($params['B'])
                        and isset($params['D'])
                        and ($params['A']['type'] == 's')
                        and ($params['B']['type'] == 's')
                        and ($params['D']['type'] == 's')
                    ) {
                    $key = $params['B']['text'];
                    $productList[$key]['id'] = $rowNumber - 1;
                    $productList[$key]['article'] = $params['B']['text'];
                    $productList[$key]['name'] = $params['A']['text'];
                    $productList[$key]['unit'] = (isset($params['E']['text'])) ? $params['E']['text'] : '-';
                    $productList[$key]['stock'] = (isset($params['G']['text'])) ? $params['G']['text'] : '-';
                    $productList[$key]['price'] = $params['D']['text'];
                }
            }
        }

        return $productList;*/
    }

    function isCorrectFileAbsolute()
    {
        $result = 1;
        /*echo '<div style="display: inline-block; padding: 10px;"><table style="border: solid 1px #ddd; padding: 10px; border-radius: 6px; border-spacing: 0; background: palegreen;">
            <tr><th colspan=2 style="text-decoration: underline;">NETKO: Проверка excel файла:</th></tr>';
        $result = ($this->checkColumn(8, 'A', 'Наименование товара')) ? $result : 0;
        $result = ($this->checkColumn(8, 'B', 'Код товара')) ? $result : 0;
        $result = ($this->checkColumn(8, 'C', 'Артикул')) ? $result : 0;
        $result = ($this->checkColumn(8, 'D', 'РРЦ, руб')) ? $result : 0;
        $result = ($this->checkColumn(8, 'E', 'Единица измерения')) ? $result : 0;
        echo '</table></div>';

        return $result;*/
    }
}

?>