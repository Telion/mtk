<?php

class MtkProduct
{
    private $model;
    private $product;
    private $productListFromVendors;
    private $vendors;

    function __construct($model, $vendors, $product, $productListFromVendors)
    {
        $this->model = $model;
        $this->vendors = $vendors;
        $this->product = $product;
        $this->productListFromVendors = $productListFromVendors;
    }

    function getProductFromVendor($countList, $vendor)
    {
        $vendorName = $vendor['vendor_name'];
        $controllerName = 'Controller_Product' . ucfirst($vendorName);
        $controllerVendorProduct = new $controllerName($vendor, $this->product);
        $sourceList = ($vendor['parsing_type'] === 'fromFile') ? $this->productListFromVendors[$vendorName] : [];

        $controllerVendorProduct->getProduct($sourceList);
        if ($controllerVendorProduct->getVendorProductName()) {
            $vendorNameFromDb = $this->model->getVendorNameFromDb($controllerVendorProduct->getProductMtk()['id']);
            if ($vendorNameFromDb != $vendor['mtk_vendor_name']) {
                echo $controllerVendorProduct->getProductMtk()['id'] . ': 
                    <span style="color: orange; font-size: 22px;">
                        Разные поставщики: , текущий: ' . $vendorNameFromDb . ', по артикулу: ' . $vendor['mtk_vendor_name'] . '
                    </span> ->';
            }
            $countList[$vendorName]++;
            $countList['all']++;
            $counter = $countList['all'] . ' из ' . $countList['mtkProducts'];
            //$controllerVendorProduct->getValues();
            $this->model->saveProduct($controllerVendorProduct->getValues(), $counter);
        }
        echo '</tr></table>';
        return $countList;
    }

    function getVendorName($vendor)
    {
        foreach ($this->vendors as $key => $value) {
            if ($value['mtk_vendor_name'] == $vendor) {
                return $key;
            }
        }
        return 0;
    }
}

?>