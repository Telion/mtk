Как завести нового поставщика:
   1. В админке МТК проставить артикулы поставщика в комментарии к товару (АБН GT-100MC)
   2. Выбрать один из товаров поставщика с нашего сайта, записать его id (34)
   3. services/service.php - Вписываю описание нового поставщика в конструктор (https://www.abn.ru/search/?search-text=)
   4. helpers/helper-search-vendor-articles-in-mtk.php - указать нового поставщика
   5. controllers/controller-model - метод getAllProductsWithArticleOrIntnotes - добавить условие с id нужного товара (and products.virtuemart_product_id = 34)
   6. controllers/product/controller-product-название.php - создать контроллер поставщика. (если xlsx, то такой же как линдекс)
   7. если парсим сайт, то переписать метод getProduct в контроллере из пункта выше. Метод должен собрать информацию о товаре.
      1. редактируем метод parseVendorSite
   8. если парсим из файла, то создать методы в контроллере controller-products-from-vendor.php
   9. controllers/controller-model - убрать обработку одного товара
   10. report.php - добавить поставщика в список отображаемых поставщиков

запусти
https://www.mirtelcom.ru/cms-mtk/m3-api-reports/2-vendors/helpers/helper-search-vendor-articles-in-mtk.php

    АБН -> 230
    НАГ -> 221
    Ситилинк, Мерлион -> 133
    Производственная группа -> 100
    Солярис -> 88
    Лансет -> 84
    Файбертул Технологии -> 75
    Хозсервис-Опт -> 73
    Тайле -> 62
    Ост-Ком -> 53
    Спутниковая компания -> 41
    Ирбис/Опт-Ком -> 36
    Торус -> 34
    EKF -> 32
    Электромир/АБН -> 31
    ИнсталлГрупп -> 28
    Фибо-Телеком -> 25
    Fortisflex -> 23
    АВ-Телеком -> 16
    Каскад -> 15


    Rexant, Proconnect -> 480
    Линдекс -> 114
    ТДФ Netko -> 113
    Абсолют ТДС -> 83
    Электромир -> 346
