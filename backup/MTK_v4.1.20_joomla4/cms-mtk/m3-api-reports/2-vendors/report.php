<?php
ini_set('display_errors', 1);

$rootPath = $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk';
$projectPath = $rootPath . '/m3-api-reports/2-vendors';
require_once $rootPath . '/function.php';
require_once $projectPath . '/services/service.php';
require_once $projectPath . '/models/model-report.php';
require_once $projectPath . '/controllers/controller-report.php';
require_once $projectPath . '/views/view-report.php';
visitsLog(__FILE__);

$service = new Service();
$currentVendorList = array('sds', 'lindex', 'netko', 'absolute', 'electromir');
$defaultVendor = 'sds';
$defaultSort = 'diff';
$currentVendorsOptions = $service->getOptionsCurrentVendors($currentVendorList);

$controller = new Controller_Report();
$model = new Model_Report($db);
$view = new View_Report($controller, $projectPath, $currentVendorsOptions);

$vendor = (isset($_POST['vendor'])) ? $_POST['vendor'] : $service->getVendorOptions()[$defaultVendor]['mtk_vendor_name'];
$sortBy = (isset($_POST['sortBy'])) ? $_POST['sortBy'] : $defaultSort;

$products = $model->getProducts($vendor, $sortBy, $currentVendorsOptions); // Линдекс; Rexant, Proconnect; // mtk_id, diff
$products = $service->addVendorShortName($products);

$view->generateTemplate($products);

?>

<script>
    vendorArticlesList = document.getElementsByClassName('vendor-count');
    for (i = 0; i < vendorArticlesList.length; i++) {
        currentArticle = vendorArticlesList[i].textContent;
        articleLength = currentArticle.length - 5;
        if (articleLength > 8) {
            vendorArticlesList[i].style.fontSize = '11px';
        }
    }

    vendorArticlesList = document.getElementsByClassName('vendor-article');
    for (i = 0; i < vendorArticlesList.length; i++) {
        currentArticle = vendorArticlesList[i].textContent;
        articleLength = currentArticle.length - 5;
        if (articleLength > 8) {
            vendorArticlesList[i].style.fontSize = '12px';
        }
    }
</script>