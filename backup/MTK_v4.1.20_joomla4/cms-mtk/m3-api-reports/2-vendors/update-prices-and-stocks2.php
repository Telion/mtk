<?php
ini_set('display_errors', 1);

$rootPath = $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk';
$projectPath = $rootPath . '/m3-api-reports/2-vendors';
require_once $rootPath . '/function.php';
require_once $rootPath . '/includes/simple_html_dom.php';
require_once $projectPath . '/services/service.php';
require_once $projectPath . '/models/model-update-prices-and-stocks.php';
require_once $projectPath . '/controllers/controller-model.php';
require_once $projectPath . '/controllers/controller-mtk-product.php';
require_once $projectPath . '/controllers/controller-load-products-from-files.php';
//require_once $projectPath . '/controllers/controller-tablerow-xlsx.php';
visitsLog(__FILE__);

$service = new Service();

$currentVendorList = array(
    'sds',
    'lindex',
    'netko',
    'absolute',
    'electromir'
);
$currentVendorList = array ('electromir');
$currentVendorsOptions = $service->getOptionsCurrentVendors($currentVendorList);

$service->requireControllers($projectPath, $currentVendorsOptions);
$service->loadProductsFromFiles($currentVendorsOptions); // он не должен делать это для СДС

$model = new Model_UpdatePricesAndStocks($db);
$controller = new ControllerModel($model);
$productListFromMtk = $controller->getProductsByVendors($currentVendorsOptions);

$countList = $service->getCountList(count($productListFromMtk));
$countIterations = $service->getCountIterations($productListFromMtk);

$i = 0;
// ПЕРЕБИРАЕМ ТОВАРЫ ИЗ МТК
foreach ($productListFromMtk as $productMtk) {
    if (array_key_exists('last_parse_date', $productMtk)) {
        if ($service->isTooEarly($productMtk)) {
            continue;
        }
    }

    $product = new MtkProduct($model, $currentVendorsOptions, $productMtk, $service->getProductListFromVendors());
    $i++;
    echo '<a href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id=' . $productMtk['mtk_id'] . '">
		<div style="font-size: 20px; background: #2D2824; padding: 15px 25px; border-radius: 4px; color: #E3DE2D;">		
		MTK ID: ' . $productMtk['mtk_id'] . ', Артикул: ' . $productMtk['mtk_article'] . '</div></a><div style="margin: 0 50px;">';

    if (isset($productMtk['vendor'])) {
        $vendorName = $product->getVendorName($productMtk['vendor']);
        $countList = $product->getProductFromVendor($countList, $currentVendorsOptions[$vendorName]);
    } else {
        foreach ($currentVendorsOptions as $vendor) {
            $countList = $product->getProductFromVendor($countList, $vendor);
        }
    }

    echo '<hr></div>';
    if ($i >= $countIterations) break;
}

echo '<div style="text-align: center; margin: 18px; font-size: 24px; background: #E3DE2D; padding: 30px; border-radius: 4px; color: #2D2824; font-weight: 700;">Скрипт завершил работу, обработано товаров: ' . $i . ' </div>';


$messageText = "Количество обработанного товара: " . $i . "\r\n";
foreach ($currentVendorList as $vendor) {
    $messageText .= ucfirst($currentVendorsOptions[$vendor]['vendor_name']) . ': ' . $countList[$vendor] . "\r\n";
}

?>