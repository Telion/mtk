<?php

$rootPath = $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk';
$projectPath = $rootPath . '/m3-api-reports/2-vendors';
require_once $rootPath . '/function.php';
require_once $rootPath . '/includes/simple_html_dom.php';
require_once $projectPath . '/services/service.php';
require_once $projectPath . '/models/model-update-prices-and-stocks.php';
require_once $projectPath . '/controllers/controller-model.php';
require_once $projectPath . '/controllers/controller-mtk-product.php';
require_once $projectPath . '/controllers/controller-load-products-from-files.php';
require_once $projectPath . '/controllers/controller-tablerow-xlsx.php';
visitsLog(__FILE__);



message_to_telegram('<b>Запущен</b> скрипт парсинга цен и остатков поставщиков', $recipient);
$service = new Service();

$currentVendorList = array(
        'sds',
        'lindex',
        'netko',
        'absolute',
        'electromir'
);
//$currentVendorList = array('absolute');

// Получаю опции выбранных поставщиков
$currentVendorsOptions = $service->getOptionsCurrentVendors($currentVendorList);

// Подключаю контроллеры, необходимые для парсинга выбранных поставщиков
$service->requireControllers($projectPath, $currentVendorsOptions);

// Получаю и сохраняю список товаров по поставщикам, у которых товары берутся из файлов.
$service->loadProductsFromFiles($currentVendorsOptions); // он не должен делать это для СДС

// Подключаем модель и контроллер
$model = new Model_UpdatePricesAndStocks($db);
$controller = new ControllerModel($model);

// Получаю список товаров, у которых указан поставщик и артикул поставщика, из базы данных МТК.
$productListFromMtk = $controller->getProductsByVendors($currentVendorsOptions);

// Создаю переменную для хранения количества обработанных товаров по каждому поставщику
$countList = $service->getCountList(count($productListFromMtk));
// Счетчик итераций
$countIterations = $service->getCountIterations($productListFromMtk);


$i = 0;
// ПЕРЕБИРАЕМ ТОВАРЫ ИЗ МТК
foreach ($productListFromMtk as $productMtk) {
    if (array_key_exists('last_parse_date', $productMtk)) {
        if ($service->isTooEarly($productMtk)) {
//                    continue; // todo: (включить пропуск товаров, котороые уже были спарсены сегодня) раскомментируй
                    echo '<hr>';
                }
    }

    $product = new MtkProduct($model, $currentVendorsOptions, $productMtk, $service->getProductListFromVendors());

    $i++;
    echo '<a href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id=' . $productMtk['mtk_id'] . '">
		<div style="font-size: 20px; background: #2D2824; padding: 15px 25px; border-radius: 4px; color: #E3DE2D;">		
		MTK ID: ' . $productMtk['mtk_id'] . ', Артикул: ' . $productMtk['mtk_article'] . '</div></a><div style="margin: 0 50px;">';

    if (isset($productMtk['vendor'])) {
        $vendorName = $product->getVendorName($productMtk['vendor']);
        $countList = $product->getProductFromVendor($countList, $currentVendorsOptions[$vendorName]);
    } else {
        foreach ($currentVendorsOptions as $vendor) {
            $countList = $product->getProductFromVendor($countList, $vendor);
        }
    }

    echo '<hr></div>';
    if ($i >= $countIterations) break;
}

echo '<div style="text-align: center; margin: 18px; font-size: 24px; background: #E3DE2D; padding: 30px; border-radius: 4px; color: #2D2824; font-weight: 700;">Скрипт завершил работу, обработано товаров: ' . $i . ' </div>';


$messageText = '<b>Парсинг поставщиков завершен</b>' . "\r\n\r\n" . "<b>ИТОГО</b> обработано товаров: <b>" . $i . '</b>' . "\r\n\r\n";
foreach ($currentVendorList as $vendor) {
    $messageText .= ucfirst($currentVendorsOptions[$vendor]['vendor_name']) . ': <b>' . $countList[$vendor] . '</b>' . "\r\n";
}
message_to_telegram($messageText, $recipient);

?>