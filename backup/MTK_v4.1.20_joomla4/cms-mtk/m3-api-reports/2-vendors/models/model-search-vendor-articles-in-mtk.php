<?php
require_once $projectPath . '/models/model.php';

class Model_ParseVendors extends Model
{

    public function getProducts($query)
    {
        $this->db->setQuery($query);
        $result = $this->db->loadAssocList('mtk_id');
        return $result;
    }

    public function saveProduct($product, $counter)
    {
        if (!$this->getEntryByIdFromCustomField($product, 48, 'vendor')) {
            $this->insertEntryToCustomField($product, 48, 'vendor');
        }

        if (!$this->getEntryByIdFromCustomField($product, 58, 'article')) {
            $this->insertEntryToCustomField($product, 58, 'article');
        } else {
            $this->updateEntryToCustomField($product, 58, 'article');
        }

        if (!$this->getEntryByIdFromCustomField($product, 59, 'vendorCount')) {
            $this->insertEntryToCustomField($product, 59, 'vendorCount');
        } else {
            $this->updateEntryToCustomField($product, 59, 'vendorCount');
        }

        if (!$this->getEntryByIdFromCustomField($product, 60, 'vendorPrice')) {
            $this->insertEntryToCustomField($product, 60, 'vendorPrice');
        } else {
            $this->updateEntryToCustomField($product, 60, 'vendorPrice');
        }

        if (!$this->getEntryByIdFromCustomField($product, 61, 'vendorRetailPrice')) {
            $this->insertEntryToCustomField($product, 61, 'vendorRetailPrice');
        } else {
            $this->updateEntryToCustomField($product, 61, 'vendorRetailPrice');
        }

        $product['lastParseDate'] = Date("d.m.Y", time());
        if (!$this->getEntryByIdFromCustomField($product, 62, 'lastParseDate')) {
            $this->insertEntryToCustomField($product, 62, 'lastParseDate');
        } else {
            $this->updateEntryToCustomField($product, 62, 'lastParseDate');
        }
    }

    public function getEntryByIdFromCustomField($product, $columnId, $columnName)
    {
        $conditionChangeValue = ($columnId == 48) ? ' and customfield_value = "' . $product[$columnName] . '"' : '';
        $query = '
				SELECT COUNT(*)
				FROM wxa43_virtuemart_product_customfields
				WHERE
				    virtuemart_product_id = "' . $product['id'] . '"
				    and virtuemart_custom_id = "' . $columnId . '"
				    ' . $conditionChangeValue;
        $this->db->setQuery($query);
        $result = $this->db->loadResult();
        return $result;
    }

    public function insertEntryToCustomField($product, $columnId, $columnName)
    {
        $query = 'INSERT INTO wxa43_virtuemart_product_customfields (virtuemart_product_id, virtuemart_custom_id, customfield_value, published) 
				VALUES ("' . $product['id'] . '", "' . $columnId . '", "' . $product[$columnName] . '", "1")'; //
        printr($query);
        /*$this->db->setQuery($query);
        $this->db->execute();*/
    }

    public function updateEntryToCustomField($product, $columnId, $columnName)
    {
        $query = 'UPDATE wxa43_virtuemart_product_customfields SET customfield_value = "' . $product[$columnName] . '" WHERE virtuemart_product_id = ' . $product['id'] . ' and virtuemart_custom_id = ' . $columnId;
        printr($query);
        $this->db->setQuery($query);
        $this->db->execute();
    }

    public function getVendorNameFromDb($productId)
    {
        $query = 'SELECT customfield_value FROM wxa43_virtuemart_product_customfields WHERE virtuemart_product_id = ' . $productId . ' and virtuemart_custom_id = 48';
        $this->db->setQuery($query);
        $result = $this->db->loadResult();
        return $result;
    }

}

?>