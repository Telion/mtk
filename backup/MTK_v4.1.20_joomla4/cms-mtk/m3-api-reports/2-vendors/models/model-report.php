<?php

require_once $projectPath . '/models/model.php';

class Model_Report extends Model
{

    public function getProducts($vendorName, $sortBy, $currentVendorsOptions)
    {
        if ($vendorName == 'all') return $this->getAllProducts($sortBy);
        return $result = $this->getProductsByVendor($vendorName, $sortBy, $currentVendorsOptions);
    }

    public function getAllProducts($sortBy)
    {
        $query = '
			SELECT tb.*, round((tb.mtk_price-tb.vendor_retail_price)/tb.mtk_price*100) as diff FROM (
				SELECT 
				    prod.virtuemart_product_id as mtk_id, 
				    ru.product_name as mtk_name, 
                    vendor_article.customfield_value as mtk_article,
				    IF (prices.override = 1, prices.product_override_price, prices.product_price) as mtk_price,
                    vendor.customfield_value as vendor,
                    vendor_count.customfield_value as vendor_count,
                    vendor_price.customfield_value as vendor_price,
                    vendor_retail_price.customfield_value as vendor_retail_price,
                    vendor_last_date.customfield_value as vendor_last_date
				FROM 
				    wxa43_virtuemart_products as prod
				    JOIN wxa43_virtuemart_products_ru_ru as ru ON prod.virtuemart_product_id = ru.virtuemart_product_id AND prod.published = 1
				    JOIN wxa43_virtuemart_product_prices as prices ON prod.virtuemart_product_id = prices.virtuemart_product_id
                    JOIN wxa43_virtuemart_product_customfields as vendor ON prod.virtuemart_product_id = vendor.virtuemart_product_id AND vendor.virtuemart_custom_id = 48
                    JOIN wxa43_virtuemart_product_customfields as vendor_article ON prod.virtuemart_product_id = vendor_article.virtuemart_product_id AND vendor_article.virtuemart_custom_id = 58
                    LEFT JOIN wxa43_virtuemart_product_customfields as vendor_count ON prod.virtuemart_product_id = vendor_count.virtuemart_product_id AND vendor_count.virtuemart_custom_id = 59
                    LEFT JOIN wxa43_virtuemart_product_customfields as vendor_last_date ON prod.virtuemart_product_id = vendor_last_date.virtuemart_product_id AND vendor_last_date.virtuemart_custom_id = 62
                    LEFT JOIN wxa43_virtuemart_product_customfields as vendor_price ON prod.virtuemart_product_id = vendor_price.virtuemart_product_id AND vendor_price.virtuemart_custom_id = 60
                    LEFT JOIN wxa43_virtuemart_product_customfields as vendor_retail_price ON prod.virtuemart_product_id = vendor_retail_price.virtuemart_product_id AND vendor_retail_price.virtuemart_custom_id = 61
                ) as tb
			ORDER BY ' . $sortBy . ' ASC
		';
        $this->db->setQuery($query);
        $result = $this->db->loadAssocList('mtk_id');
        return $result;
    }


    public function getProductsByVendor($vendorName, $sortBy, $currentVendorsOptions)
    {
        $currentVendorType = $this->getVendorTypeByMtkName($currentVendorsOptions, $vendorName);
        $vendorPriceType = ($currentVendorType == 'fromFile') ? 'vendor_retail_price' : 'vendor_price';
        $query = '
			SELECT tb.*, round((tb.mtk_price-tb.' . $vendorPriceType . ')/tb.mtk_price*100) as diff FROM (
				SELECT 
				    prod.virtuemart_product_id as mtk_id, 
				    ru.product_name as mtk_name, 
                    vendor_article.customfield_value as mtk_article,
				    IF (prices.override = 1, prices.product_override_price, prices.product_price) as mtk_price,
                    vendor.customfield_value as vendor,
                    vendor_count.customfield_value as vendor_count,
                    vendor_price.customfield_value as vendor_price,
                    vendor_retail_price.customfield_value as vendor_retail_price,
                    vendor_last_date.customfield_value as vendor_last_date
				FROM 
				    wxa43_virtuemart_products as prod
				    JOIN wxa43_virtuemart_products_ru_ru as ru ON prod.virtuemart_product_id = ru.virtuemart_product_id AND prod.published = 1
				    JOIN wxa43_virtuemart_product_prices as prices ON prod.virtuemart_product_id = prices.virtuemart_product_id
                    JOIN wxa43_virtuemart_product_customfields as vendor ON prod.virtuemart_product_id = vendor.virtuemart_product_id AND vendor.virtuemart_custom_id = 48 AND vendor.customfield_value = "' . $vendorName . '"
                    JOIN wxa43_virtuemart_product_customfields as vendor_article ON prod.virtuemart_product_id = vendor_article.virtuemart_product_id AND vendor_article.virtuemart_custom_id = 58
                    LEFT JOIN wxa43_virtuemart_product_customfields as vendor_count ON prod.virtuemart_product_id = vendor_count.virtuemart_product_id AND vendor_count.virtuemart_custom_id = 59
                    LEFT JOIN wxa43_virtuemart_product_customfields as vendor_last_date ON prod.virtuemart_product_id = vendor_last_date.virtuemart_product_id AND vendor_last_date.virtuemart_custom_id = 62
                    LEFT JOIN wxa43_virtuemart_product_customfields as vendor_price ON prod.virtuemart_product_id = vendor_price.virtuemart_product_id AND vendor_price.virtuemart_custom_id = 60
                    LEFT JOIN wxa43_virtuemart_product_customfields as vendor_retail_price ON prod.virtuemart_product_id = vendor_retail_price.virtuemart_product_id AND vendor_retail_price.virtuemart_custom_id = 61
                ) as tb
			ORDER BY ' . $sortBy . ' ASC
		';
        $this->db->setQuery($query);
        $result = $this->db->loadAssocList('mtk_id');
        return $result;
    }

    public function getVendorTypeByMtkName($vendorsOptions, $vendorName)
    {
        foreach ($vendorsOptions as $vendor) {
            if ($vendor['mtk_vendor_name'] == $vendorName) {
                return $vendor['parsing_type'];
            }
        }
        return 0;
    }

}

?>