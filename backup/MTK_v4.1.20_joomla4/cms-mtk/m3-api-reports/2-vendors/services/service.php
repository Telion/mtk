<?php

class Service
{
    public $actualVendors;
    public array $productListFromVendors = array();
    private array $vendorOptions = array();

    public function __construct()
    {
        $this->addToOptions('sds', 'Rexant, Proconnect', -5, 'lightcyan',
            'fromWebSite',
            'https://www.sds-group.ru/search.htm?search=');

        $this->addToOptions('lindex', 'Линдекс', -5, 'lightgoldenrodyellow',
            'fromFile',
            'https://lindex.ru/download/moscow/Full_Stock_Lindex.zip?hash=x9k7hYwtWQ',
            'open-xlsx',
            array('getfile.zip', 'Прайс-лист Lindex (XLSX).xlsx'));

        $this->addToOptions('netko', 'ТДФ Netko', -5, 'lightgreen',
            'fromFile',
            'https://www.tktdf.ru/upload/price_all_rub_rrcjh.xlsx',
            'open-xlsx',
            array('price_all_rub_rrcjh.xlsx'));

        $this->addToOptions('absolute', 'Абсолют ТДС', -5, 'lightgrey',
            'fromWebSite',
            'https://absolut-tds.com/search/?q=');

        $this->addToOptions('electromir', 'Электромир', -5, 'gainsboro',
            'fromWebSite',
            'https://autocomplete.diginetica.net/autocomplete?st=');

        $this->addToOptions('abn', 'АБН', -5, '#adf7ff',
            'fromWebSite',
            'https://www.abn.ru/search/?search-text=');
    }

    /**
     * @param $vendorName vendor_name Название поставщика, коротко и латиницей (sds; lindex; ...)
     * @param $mtkVendorName mtk_vendor_name Название пост-ка у нас на сайте (Rexant, Proconnect; Линдекс; ...)
     * @param $percent percent Процент рассчета новой цены от цены поставщика (-5)
     * @param $color color Цвет отчетов поставщика
     * @param $parsingType parsing_type Откуда парсим данные (fromWebSite, fromFile)
     * @param $vendorUrl vendor_url Ссылка на сайт поставщика (url-сайта или url-файла)
     * @param $extractXlsxFolder extract_xlsx_folder Папка, куда будет распаковываться xlsx файл
     * @param $fileList file_list Список разархивируемых файлов (например прайс линдекса скачивается в getfile.zip, после разархивации получается файл Прайс-Лист...xlsx, который также нужно разархивировать.)
     * @param $fileType file_type расширение файла, содержащего прайс
     * @return void
     */
    public function addToOptions($vendorName, $mtkVendorName, $percent, $color, $parsingType, $vendorUrl,
                                 $extractXlsxFolder = '', $fileList = [])
    {
        $this->vendorOptions[$vendorName]['vendor_name'] = $vendorName;
        $this->vendorOptions[$vendorName]['mtk_vendor_name'] = $mtkVendorName;
        $this->vendorOptions[$vendorName]['percent'] = $percent;
        $this->vendorOptions[$vendorName]['color'] = $color;
        $this->vendorOptions[$vendorName]['parsing_type'] = $parsingType;
        $this->vendorOptions[$vendorName]['vendor_url'] = $vendorUrl;
        $this->vendorOptions[$vendorName]['extract_xlsx_folder'] = $extractXlsxFolder;
        $this->vendorOptions[$vendorName]['file_list'] = $fileList;
        $this->vendorOptions[$vendorName]['file_type'] = (!empty($fileList)) ? $this->getFileType($fileList) : ''; //
    }

    public function getVendorOptions()
    {
        return $this->vendorOptions;
    }

    public function getMtkVendorNameByVendorName($vendor)
    {
        return $this->vendorOptions[$vendor]['mtk_vendor_name'];
    }

    public function getFileType($fileList)
    {
        $lastElement = count($fileList) - 1;
        $nameLastFile = $fileList[$lastElement];
        return explode('.', $nameLastFile)[1];
    }


    public function getProductListFromVendors(): array
    {
        return $this->productListFromVendors;
    }

    public function getOptionsCurrentVendors($vendorList): array
    {
        $currentVendorsList = array();
        foreach ($vendorList as $key => $currentVendorName) {
            $currentVendorsList[$currentVendorName] = $this->vendorOptions[$currentVendorName];
        }
        return $currentVendorsList;
    }

    public function requireControllers($projectPath, $vendorList)
    {
        $controllersPath = $projectPath . '/controllers';
        require_once $controllersPath . '/controller-vendors.php';

        foreach ($vendorList as $vendorName => $vendor) {
            $fileName = $controllersPath . '/product/controller-product-' . $vendorName . '.php';
            $this->requireController($fileName, $vendorName);
            if ($vendor['parsing_type'] == 'fromFile') {
                $fileName = $controllersPath . '/controller-tablerow-xlsx.php';
                $this->requireController($fileName, $vendorName);
            }
        }
    }

    public function requireController($fileName, $vendorName)
    {
        if (file_exists($fileName)) {
            $this->actualVendors[] = $vendorName;
            require_once $fileName;
        }
    }

    public function getCountList($countMtkProducts): array
    {
        $countList = array(
            'all' => 0,
            'mtkProducts' => $countMtkProducts
        );
        foreach ($this->actualVendors as $vendor) {
            $countList[$vendor] = 0;
        }
        return $countList;
    }

    public function isTooEarly($productMtk) {
        if ($this->isParseFromFile($productMtk)) {
            return 0;
        }
        if ($this->isTimeToUpdateDate($productMtk)) {
            return 1;
        }
        return 0;
    }

    public function isParseFromFile($productMtk)
    {
        $currentVendor = $this->getVendorByMtkName($productMtk['vendor']);
        if ($currentVendor['parsing_type'] == 'fromFile') {
            return 1;
        }
        return 0;
    }

    public function getVendorByMtkName($mtkName)
    {
        foreach ($this->vendorOptions as $vendor) {
            if ($vendor['mtk_vendor_name'] == $mtkName) {
                return $vendor;
            }
        }
    }

    public function isTimeToUpdateDate($productMtk)
    {
        $lastParseDate = $productMtk['last_parse_date'];
        $lastParseDateUnix = strtotime($lastParseDate) + 4 * 60 * 60;
        $nextParseDateUnix = $lastParseDateUnix + 3 * 24 * 60 * 60;
        $currentDateUnix = time();
        if ($currentDateUnix < $nextParseDateUnix) {
            return 1;
        }
        return 0;
    }

    public function getCountIterations($mtkProducts)
    {
        if (isset($_GET['count'])) {
            return $_GET['count'];
        }
        return count($mtkProducts);
    }

    public function loadProductsFromFiles($vendorList)
    {
        foreach ($vendorList as $vendorName => $vendor) {
            if ($vendor['parsing_type'] == 'fromFile') {
                $controllerGetProducts = new controller_LoadProductsFromFiles($vendor);
                $getProductListMethod = 'getProductListFrom' . ucfirst($vendorName);
                $productListFromVendor = $controllerGetProducts->$getProductListMethod();
                foreach ($productListFromVendor as $key => $product) {
                    $this->productListFromVendors[$vendorName][$key] = $product;
                }
            }
        }
    }

    public function addVendorShortName($products): array
    {
        $vendors = $this->vendorOptions;
        $newVendors = [];
        $newProducts = [];
        foreach ($vendors as $vendor) {
            $newVendors[$vendor['mtk_vendor_name']] = $vendor;
        }
        foreach ($products as $key => $product) {
	        if (isset($newVendors[$product['vendor']])) {
                $products[$key]['vendorShortName'] = $newVendors[$product['vendor']]['vendor_name'];
	        }
        }
        return $products;
    }
}

?>