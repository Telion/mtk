<?php
//require_once $projectPath . '/models/model.php';

class View_Report
{
    private $controller;
    private $templateFolder;
    private $vendors;

    public function __construct($controller, $projectPath, $vendors)
    {
        $this->controller = $controller;
        $this->templateFolder = $projectPath . '/templates';
        $this->vendors = $vendors;
    }

    public function generateTemplate($products)
    {
        $controller = $this->controller;
        require_once $this->templateFolder . '/template-header.php';
        require_once $this->templateFolder . '/template-report.php';
        require_once $this->templateFolder . '/template-footer.php';
    }

    public function printMtkId($product)
    {
        $textColor = '#709ec5';
        if (strtotime($product['vendor_last_date']) < (time() - 3 * 24 * 60 * 60)) {
            $textColor = 'lightcoral';
        }

        echo '
			<td style="width: 60px;">' .
            $product['mtk_id'] . ' <br> 
				<span style="font-size: 10px; color: ' . $textColor . '">' . $product['vendor_last_date'] . '</span>
			</td>
		';
    }

    public function printVendorArticle($product)
    {
        $vendorArticle = (isset($product['vendorShortName'])) ? $this->controller->getVendorArticle($product, $this->vendors) : '';
        echo '
			<td style="width: 80px;" class=" left">
				<div style="width: 80px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">' .
                    $vendorArticle . '
				</div>
			</td>
		';
    }

    public function printMtkName($product)
    {
        echo '<td style="width: 530px;" class="left ">
				<div class="product-name">
					<a target="_blank" href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id=' . $product['mtk_id'] . '">' .
            $product['mtk_name'] . '
					</a>
				</div>
			</td>';
    }

    public function printVendorPriceApi($product)
    {
        echo '
			<td style="width: 75px;" class="right">' .
            $product['vendor_price'] . '
			</td>';
    }

    public function printVendorPriceRetail($product)
    {
        echo '
			<td style="width: 75px;" class="right">' .
            $product['vendor_retail_price'] . '
			</td>';
    }

    public function printMtkPrice($product)
    {
        $diffClass = $this->controller->getDiffSign($product['diff']);
        echo '
			<td style="width: 98px; position: relative;" class="right" id="mtk-price-' . $product['mtk_id'] . '">' .
            round($product['mtk_price']) . ' 
				<div class="' . $diffClass . '" title="Если отрицательное значение' . "\r\n" . 'значит наша цена занижена">' . $product['diff'] . '%</div>
			</td>';
    }

    public function printInputNewPrice($product)
    {
        $newPrice = $this->controller->getNewPrice($product, $this->vendors);
        $priceDifference = $this->controller->getPriceDifference($product['mtk_price'], $newPrice);
        if ($newPrice == '-') {
            $button = '';
            $inputStatus = 'disabled';
        } else {
            $button = '<button class="mini-button" onclick="updatePrice(' . $product['mtk_id'] . ', ' . $product['mtk_price'] . ');">v</button>';
            $inputStatus = '';
        }

        echo '
			<td class="left">
				<div style="display: -webkit-inline-box; border: solid 0;">
					<input id="new-price-' . $product['mtk_id'] . '" name="newPrice" value="' . $newPrice . '" class="new-price" ' . $inputStatus . '>
					<div style="text-align: right; width: 72px;">
						<span class="diff">' . $priceDifference . '</span>' .
            $button . '
					</div>
				</div>
			</td>';
    }

    public function printVendorCountCurrent($product)
    {
        $countValue = $product['vendor_count'];
        if ($countValue == 'yes') {
            $countValue = 'в наличии';
        } else if ($countValue == 'no' or $countValue === '0') {
            $countValue = 'нет в наличии';
        } else if ($countValue == '-') {
            $countValue = 'нет информации';
        }
        echo '
			<td style="width: 85px; position: relative;" class="vendor-count right ">' .
             $countValue . '
			</td>';
    }

    public function printAboutMessage()
    {
        echo '
			<td colspan="5" style="color: orangered;">У поставщика не найден указанный артикул</td>
		';
    }

    public function printVendorOptions()
    {
        $currentVendor = $this->controller->getVendorByPost($this->vendors);
        foreach ($this->vendors as $key => $vendor) {
            $selected = ($currentVendor == $vendor['mtk_vendor_name']) ? 'selected' : '';
            echo '<option value="' . $vendor['mtk_vendor_name'] . '" ' . $selected . '>' . ucfirst($vendor['mtk_vendor_name']) . '</option>';
        }
    }

    public function printSortOptions()
    {
        $sortBy = $this->controller->getSortByPost();
        echo '
			<option value="mtk_id" ' . (($sortBy == 'mtk_id') ? 'selected' : '') . '>по id</option>
			<option value="diff" ' . (($sortBy == 'diff') ? 'selected' : '') . '>по %</option>';
    }

}

?>