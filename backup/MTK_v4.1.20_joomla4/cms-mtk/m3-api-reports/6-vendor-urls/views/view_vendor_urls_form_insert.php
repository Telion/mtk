<div id="formInsertUrls">
	<form method="post" name="insertUrls" id="insertUrls">
		<table>		
			<tr>
				<th>URL MTK</th>
				<td>
					<input type="url" form="insertUrls" id="insertMtkUrl" name="insertMtkUrl" required minlength="8">
				</td>				
				<th rowspan="2">
					<button type="submit" form="insertUrls" id="insertButton" name="insertButton" style="width: 100px;">
						Добавить
					</button>
				</th>
				<th rowspan="2">
					<button type="reset" name="reset" onclick="clearForm();" 
							style="background: #d4e9f4; color: #4e4e4e; border: solid 2px; padding: 15px 11px;">
						Очистить
					</button>
				</th>
			</tr>
			<tr>
				<th>URL Поставщика</th>				
				<td>
					<input type="url" form="insertUrls" id="insertVendorUrl" name="insertVendorUrl" required minlength="8">
				</td>
			</tr>
		</table>
		<input type="hidden" form="insertUrls" id="operationType" name="operationType" value="insert">
		<input type="hidden" form="insertUrls" id="id" name="id" value="">
	</form>
</div>