<div id="resultTable">
<table>
	<tr>
		<th class="td-id">ID</th>
		<th class="td-category">Категория товара</th>
		<th class="td-name">Название товара</th>
		<th class="td-intnote">Комментарий</th>
		<th class="td-vendor">Ссылка на вендора</th>
		<th class="td-ourprice">Наша цена</th>
		<th class="td-vendorprice">Цена вендора</th>
		<th class="td-about" colspan="2"></th>
	</tr>
</table>

<table id="dataTable" class="zebra-table">

	<?php
	foreach($data as $product) {    //  id, our_url, vendor_url, our_id, vendor_name, vendor_price, 
									//  product_name, published, intnotes, product_price, override, product_override_price
	?>

		<tr>
			<td id="id" class="td-id"><?=$product['id']?></td>

			<td class="td-category"><?=$product['category_name']?></td>

			<td class="td-name">
				<?php if ($product['our_id']) {
					$publishedColor = ($product['published'] == 1) ? '#3cb371' : '#db7093'; ?>
					<div style="width: 10px; height: 10px; background: <?= $publishedColor ?>; border-radius: 10px; display: inline-block;"></div>	
					<a href="<?=$product['our_url']?>" target="_blank">
						id: <?=$product['our_id']?><br>
						<?=$product['product_name']?>
					</a></td>
				<?php } else {
					echo '<span style="color: red;">' . $product['our_url'] . '</span>';
				} ?>

			<td class="td-intnote"><?=$product['intnotes']?></td>

			<td class="td-vendor">				
				<span style="color: mediumseagreen;"><?=$product['vendor_name']?></span>
				<br>
				<a href="<?=$product['vendor_url']?>" target="_blank"><?=$product['vendor_url']?></a>
			</td>

			<td class="td-ourprice" style="text-align: right; padding-right: 10px;"
					title="<?='Дата проверки: ' . Date("d.m.Y", strtotime($product['check_date']))?>">
				<?php $ourPrice = ($product['override'] == 1) ? $product['product_override_price'] : $product['product_price'];
				echo round($ourPrice); ?>
				<br>			
				<span style="color: mediumseagreen;"><?=$product['product_in_stock']?></span>
			</td>

			<td id="vendorPrice<?=$product['id']?>" class="td-vendorprice" style="text-align: right; padding-right: 10px;"
					title="<?='Дата проверки: ' . Date("d.m.Y", strtotime($product['check_date']))?>">
				<?=$product['vendor_price']?>
				<br>
				<span style="color: mediumseagreen;"><?=$product['vendor_stock']?></span>
			</td>
			
			<td class="td-minibutton"><button type="button" class="mini-button" onclick="getVendorPrice(<?=$product['id']?>, '<?=$product['vendor_name']?>', '<?=$product['vendor_url']?>');">&orarr;</button></td>
			
			<td class="td-minibutton">
				<form method='post' name='formDelete' id='formDeleteId"<?=$product['id']?>"' class='form-delete' onsubmit='return requestForm();' style='margin: 2px;'>
					<input type='hidden' name='delete' value='"<?=$product['id']?>"' form='formDeleteId"<?=$product['id']?>"'>
					<button type='submit' class="mini-button" form='formDeleteId"<?=$product['id']?>"'>x</button></form>
			</td>
			
			<td id="ourUrl" style="display: none;"><?=$product['our_url']?></td>
			
			<td id="vendorUrl" style="display: none;"><?=$product['vendor_url']?></td>

		</tr>
	<?php } ?>
</table>
</div>