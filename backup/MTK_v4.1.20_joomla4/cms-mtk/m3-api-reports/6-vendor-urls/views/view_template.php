<html>
<head>
	<title>cmsMTK. Список активного оборудования</title>
	<style>
		form#formInsertId {padding: 15px 15px; background: lightblue; width: fit-content; border-radius: 5px;}
		form table {margin: 0;}
		form th {color: inherit; background: inherit; padding: 0;}
		form td {padding: 7px 5px 0 14px; font-size: 15px;}
		form td input {padding-left: 10px;}
		.inputForm button {padding: 5px 10px; margin: 0 6px;}
		.buttons {padding: 24px 0 10px; margin: 0 auto; width: -webkit-fill-available;}
		.activeRow, .activeRow:hover {background:#B9D9EC !important; outline:0;}		
		input[type="number"]::-webkit-outer-spin-button, input[type="number"]::-webkit-inner-spin-button {-webkit-appearance: none; margin: 0;}
		input[type="number"] {-moz-appearance: textfield;}
		input[type="number"]:hover, input[type="number"]:focus {-moz-appearance: number-input;}
		input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button {-webkit-appearance: none; margin: 0;}
	</style>
	<script
  		src="https://code.jquery.com/jquery-3.6.0.js"
  		integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
  		crossorigin="anonymous"></script>
	<script src="6-vendor-urls/scripts.js"></script>
	<?php
	require_once CONST_MENU;

	include '6-vendor-urls/views/'.$contentView; 
	include '6-vendor-urls/views/tasks.php'; 
	?>

</body>
</html>