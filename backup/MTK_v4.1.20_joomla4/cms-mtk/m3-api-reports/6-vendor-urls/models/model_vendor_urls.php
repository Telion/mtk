<?php

class Model_Vendor_Urls extends Model
{
	public function getData() {	
		$this->db->setQuery('
			SELECT 
				vnd.id, vnd.our_url, vnd.vendor_url, vnd.our_id, vnd.vendor_name, vnd.vendor_price, vnd.vendor_stock, vnd.check_date,
                pr.published, pr.product_in_stock, pr.intnotes, ru.product_name, pri.product_price, pri.override, pri.product_override_price, pcatru.category_name
			FROM tln_vendor_urls as vnd
				LEFT JOIN wxa43_virtuemart_products as pr ON vnd.our_id = pr.virtuemart_product_id
				LEFT JOIN wxa43_virtuemart_products_ru_ru as ru ON vnd.our_id = ru.virtuemart_product_id
                LEFT JOIN wxa43_virtuemart_product_categories as pcat ON pcat.virtuemart_product_id = pr.virtuemart_product_id
                LEFT JOIN wxa43_virtuemart_categories_ru_ru as pcatru ON pcat.virtuemart_category_id = pcatru.virtuemart_category_id
    			LEFT JOIN wxa43_virtuemart_product_prices as pri ON vnd.our_id = pri.virtuemart_product_id;
			');
		$this->db->execute();
		$result = $this->db->loadAssocList();

		return $result;
	}

	public function getDataWithoutVendorName() {
		$this->db->setQuery('SELECT id, vendor_url, vendor_name FROM tln_vendor_urls WHERE vendor_name IS NULL');
		$this->db->execute();
		$result = $this->db->loadAssocList();

		return $result;
	}

	public function getInsert($mtkUrl, $vendorUrl) {
		$this->db->setQuery('SELECT COUNT(id) as count FROM tln_vendor_urls WHERE our_url="'.$mtkUrl.'" AND vendor_url="'.$vendorUrl.'"');
		$this->db->execute();
		$result = $this->db->loadResult();

		return $result;
	}

	public function getOurIdByAlias($alias) {
		$this->db->setQuery('SELECT virtuemart_product_id FROM wxa43_virtuemart_products_ru_ru WHERE slug = "'.$alias.'"');
		$this->db->execute();
		$result = $this->db->loadResult();

		return $result;
	}

	public function insertUrl($mtkProductId, $mtkUrl, $vendorUrl) {
		$query = 'INSERT INTO tln_vendor_urls (our_id, our_url, vendor_url) VALUES ("' . $mtkProductId . '", "' . $mtkUrl . '", "' . $vendorUrl . '")';
		$this->db->setQuery($query);
		$this->db->execute();
	}

	public function updateUrl($id, $mtkProductId, $mtkUrl, $vendorUrl) {
		$query = 'UPDATE tln_vendor_urls SET 
			our_id = "' . $mtkProductId . '" ,
			our_url = "' . $mtkUrl . '",
			vendor_url = "' . $vendorUrl . '"
			WHERE id = ' . $id;
		$this->db->setQuery($query);
		$this->db->execute();
	}

	public function updateVendorName($id, $vendorName) {
		$query = 'UPDATE tln_vendor_urls SET vendor_name="' . $vendorName . '" WHERE id=' . $id;
		$this->db->setQuery($query);
		$this->db->execute();
	}

	function deleteEntry($id) {
		$query = 'DELETE FROM tln_vendor_urls WHERE id=' . $id;
		$this->db->setQuery($query);
		$this->db->execute();
	}

}

?>