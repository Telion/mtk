onload = function () {
	document.getElementById('dataTable').onclick = mySuperFunc;
}

function mySuperFunc(ev){
	// Заносим данные из таблицы в форму
	var e = ev || window.event, tag = (e.target).closest('tr') || e.srcElement;
	input = (e.target).closest('tr').getElementsByTagName('td');

	// Вставить данные из таблицы в форму
	document.insertUrls.insertMtkUrl.value = input['ourUrl'].textContent;
	document.insertUrls.insertVendorUrl.value = input['vendorUrl'].textContent;

	// Подпись кнопки в форме
	document.insertUrls.insertButton.innerHTML = "Изменить";
	document.insertUrls.operationType.value = "update";
	document.insertUrls.id.value = input['id'].textContent;

	// Выделение строки
	var row = getParentTag(e.target,'TR');
	if (!row) {return;}
	var tbl = this,  idx = tbl.getAttribute('activeRowIndex');
	if (idx) {
		tbl.rows[idx].classList.remove('activeRow');
	}
	row.classList.add('activeRow');
	tbl.setAttribute('activeRowIndex', row.rowIndex);
}

//Найти ближайшего родителя по tagName. Здесь мы движемся вверх, пока не встретим родителя, у которого тег = нашему заданному tag
function getParentTag(node,tag) { 
	if (node) {return (node.tagName == tag) ? node : getParentTag(node.parentElement,tag);}
	return null;
}

// Запрос на подтверждение удаления записи
function requestForm() {
    return confirm("Подтвердите удаление записи");
}

function clearForm() {
	document.insertUrls.insertButton.innerHTML = "Добавить";
	document.insertUrls.operationType.value = "insert";
	document.insertUrls.id.value = "";
	document.getElementsByClassName('activeRow')[0].classList.remove('activeRow');
}

// AJAX POST запрос
function getVendorPrice(id, vendorName, vendorUrl) {
    $.ajax({
        type: "POST",
        url: "6-vendor-urls/services/service_ajax.php",
        data: {id:id, vendorName:vendorName, vendorUrl:vendorUrl}
    }).done(function( result )
        {
            $("#vendorPrice" + id).html( result );
        });
}

// Очистить форму, при клике вне документа
$(document).mouseup(function (e) {
    var container = $("#mainBlock");
    if (container.has(e.target).length === 0){
        clearForm();
        document.getElementById('insertMtkUrl').value = '';
        document.getElementById('insertVendorUrl').value = '';
    }
});