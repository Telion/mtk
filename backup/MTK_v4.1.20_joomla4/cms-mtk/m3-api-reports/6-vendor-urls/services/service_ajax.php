<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
require_once 'service_get_data.php';

jimport('joomla.database.table');
$db = JFactory::getDBO();

// Подключаю файл для парсинга этого поставщика
// Результат парсинга возвращаю обратно

$id = $_POST['id'];
$vendorName = $_POST['vendorName'];
$vendorUrl = $_POST['vendorUrl'];

$serviceName = str_replace('.', '', $vendorName);
$serviceName = str_replace('-', '', $serviceName);
$serviceName = 'ServiceGetData' . $serviceName;
$serviceFile = 'service_get_data_' . $vendorName . '.php';

if (file_exists($serviceFile)) {
	require_once $serviceFile;
	$service = new $serviceName($db, $id);

	$dataFromVendor = $service->getDataWithCurl($vendorUrl);
	$dataFromVendor = $service->translateData1251toUTF8($dataFromVendor);

	//	echo '<div style="position: absolute; top: 0; left: 0; background: lightpink; z-index: 1000;">' . $dataFromVendor . '</div>';

	$vendorPrice = $service->getPrice($dataFromVendor);
	$vendorStock = $service->getStock($dataFromVendor);

	echo '<span style="color: lightcoral;">' . $vendorPrice . '<br>' . $vendorStock . '</span>';

} else {
	echo '<div style="text-align: center; color: lightcoral;">не могу получить цену</div>';
}


?>