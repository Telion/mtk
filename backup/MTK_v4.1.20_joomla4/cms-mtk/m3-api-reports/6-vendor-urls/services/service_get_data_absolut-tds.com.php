<?php

class ServiceGetDataAbsolutTdsCom extends ServiceGetData {
	
	function getPrice($data) {
		$temp = explode('price blue">', $data)[1];
		$price = explode(' Р /шт.', $temp)[0];
		$this->saveDataToDB('vendor_price', $price);

		return $price;
	}

	function getStock($data) {
		$temp = explode('info-deta blue">', $data)[1];
		$stock = explode('</span>', $temp)[0];
		$stock = ($stock == 'Есть на складе') ? 'есть' : 'нет';
		$this->saveDataToDB('vendor_stock', $stock);

		return $stock;
	}

}

?>