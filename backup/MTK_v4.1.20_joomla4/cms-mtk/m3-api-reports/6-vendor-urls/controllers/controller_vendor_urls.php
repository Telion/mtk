<?php 

class Controller_Vendor_Urls extends Controller
{

	/*
		Контроллер создает объекты модели и вьюшки
	*/
	function __construct($db)
	{
		$this->model = new Model_Vendor_Urls($db);
		$this->view = new View();
	}
	
	/*
		Получение данных из модели и передача их во вьюшку
	*/
	function actionIndex()
	{
		$this->checkVendorName();

		$data = $this->model->getData();	
		$this->view->generate('view_vendor_urls.php', 'view_template.php', $data);
	}

	/*
		Проверяем имеется ли у записи Название поставщика
		Если нет, то пытаемся его узнать и записать.
	*/
	function checkVendorName() {
		$data = $this->model->getDataWithoutVendorName();
		foreach ($data as $entry) {
			$vendorName = $this->getVendorName($entry['vendor_url']);
			if ($vendorName) {
				$this->model->updateVendorName($entry['id'], $vendorName);
			}
		}
	}

	/*
		Добавление новой записи в список URL
	*/
	function checkInsertUrl($mtkUrl, $vendorUrl) {
		$isHave = $this->model->getInsert($mtkUrl, $vendorUrl);
		if (!$isHave) {			
			$mtkProductId = $this->getMtkProductId($mtkUrl);
			$this->model->insertUrl($mtkProductId, $mtkUrl, $vendorUrl);
		}
	}

	/*
		Изменение записи в списке URL
	*/
	function updateUrl($id, $mtkUrl, $vendorUrl) {
		$mtkProductId = $this->getMtkProductId($mtkUrl);
		$this->model->updateUrl($id, $mtkProductId, $mtkUrl, $vendorUrl);
	}	

	/*
		Поиск id товара в нашей базе, по алиасу
	*/
	function getMtkProductId($mtkUrl) {
		$urlParts = explode('/', $mtkUrl);
		$alias = $urlParts[count($urlParts)-1];
		$alias = str_replace('-detail', '', $alias);
		$our_id = $this->model->getOurIdByAlias($alias);

		return $our_id;
	}

	/*
		Достаем Название поставщика из URL
	*/
	function getVendorName($vendorUrl) {
		$urlParts = explode('/', $vendorUrl);

		if (isset($urlParts[2])) {
			return $urlParts[2];
		} else {
			return 0;
		}
	}

	/*
		Удаление записи
	*/
	function deleteEntry($id) {
		$this->model->deleteEntry($id);
	}

}

?>