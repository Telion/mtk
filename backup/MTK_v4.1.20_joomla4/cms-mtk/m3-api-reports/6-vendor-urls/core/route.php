<?php

class Route
{

	static function start($db)
	{		
		$controllerName = 'Vendor_Urls';
		$actionName = 'Index';
		$actionName = 'action'.$actionName;
		$action = $actionName;

		$modelName = 'Model_'.$controllerName;
		$modelFile = strtolower($modelName).'.php';
		$modelPath = "6-vendor-urls/models/".$modelFile;
		if(file_exists($modelPath)) {
			include $modelPath;
		}

		$controllerName = 'Controller_'.$controllerName;
		$controllerFile = strtolower($controllerName).'.php';
		$controllerPath = "6-vendor-urls/controllers/".$controllerFile;
		if(file_exists($controllerPath)) {
			include $controllerPath;
		}

		$controller = new $controllerName($db);

		if (isset($_POST['operationType']) and ($_POST['operationType'] == 'insert')) {
			$controller->checkInsertUrl($_POST['insertMtkUrl'], $_POST['insertVendorUrl']);
		}

		if (isset($_POST['operationType']) and ($_POST['operationType'] == 'update')) {
			$controller->updateUrl($_POST['id'], $_POST['insertMtkUrl'], $_POST['insertVendorUrl']);
		}

		if (!empty($_POST['delete'])) {
			$controller->deleteEntry($_POST['delete']);
		}

		if(method_exists($controller, $action)) {
			$controller->$action();
		}

	}
}

?>