<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);

$db->setQuery("SELECT * FROM tln_visits_log ORDER BY time DESC");
$arr = $db->loadObjectList();

$arr_page_list = [
	'supplier-choice.php' => '1. Дашбоард',
	'supplier-choice-report.php' => '1а. Отчет. Остатки по поставщику',
	'1-sales.php' => '2-1. Акционные товары',
	'2-user-points.php' => '2-2. Пользовательские баллы',
	'3-check-price-change.php' => '2-3. Изменения цен',
	'4-products-reserv.php' => '2-4. Товары с резервом',
	'5-suppliers-categories.php' => '2-5. Товары без поставщика. Категории',
	'5-suppliers-products.php' => '2-5а. Товары без поставщика. Товары',
	'6-related-categories.php' => '2-6. Без сопутствующих. Категории',
	'6-related-products.php' => '2-6а. Без сопутствующих. Товары',
	'6-related-products-insert.php' => '2-6b. Без сопутствующих. Insert',
	'7-related-categories-not8.php' => '2-7. Не 8 сопутствующих. Категории',
	'7-related-products-not8.php' => '2-7а. Не 8 сопутствующих. Товары',
	'7-related-products-update.php' => '2-7b. Не 8 сопутствующих. Update',
	'8-tracking-product-stock.php' => '2-8. Отслеживание остатков',
	'9-orders-reserv.php' => '2-9. Зарезервированные заказы',
	'10-product-search-by-id.php' => '2-10. Поиск товара по ID',
	'1-statistic.php' => '3-1. Статистика',
	'tabs-apiSDSvsMTK.php' => '3-2. АPI. SDS vs MTK',
	'3-report-anker.php' => '3-3. Сравнение цен Анкер и СДС',
	'4-report-airtek.php' => '3-4. Сравнение цен Аиртек и Русклимат',
	'5-not-published.php' => '3-5. Неопубликованные товары',
	'visit-history.php' => '4. Лог посещений',
	'programmer-tasks.php' => '4. Задачи',
	'mailing.php' => '5. Рассылка',
	'2-apiSDSvsMTK-handler.php' => 'CRON 1. МТК сравнение остатков и цен СДС и МТК',
	'9-orders-reserv-handler.php' => 'CRON 2. Проверка остатков'
];

?>

<html>
<head>
	<title>cmsMTK. История посещений</title>

<?php
	require_once CONST_MENU;
?>

<div style="width: fit-content; margin: 0 auto;">
<div style="font-size: 20px; font-weight: 700; padding: 14px; padding-left: 110px;">История посещений: </div>

	<table class='zebra-table'>
	<tr><th>Время посещения</th><th>Страница</th><th>Посетитель</th></tr>

<?php

foreach ($arr as $value) {
	$time = Date("d.m.Y H:i:s", strtotime($value->time));
	
	$page = (array_key_exists($value->file_name, $arr_page_list)) ? $arr_page_list[$value->file_name] : $value->file_name;
	
	if (strpos($value->user_agent, 'Windows NT 6.1')) {
		$user = "С";
	} else if (strpos($value->user_agent, 'Windows NT 10.0; Win64; x64')) {
		$user = "Алия";
	} else if (strpos($value->user_agent, 'Windows NT 10.0; WOW64')) {
		$user = "Шеф";
	} else if (!$value->user_agent) {
		$user = "Планировщик";
	}

	$time = getField($time);
	$page = getField($page);
	$user = getField($user);
	echo "<tr>".$time.$page.$user."</tr>";
}

function getField($value) {
	return "<td>".$value."</td>";
}

?>