	function getParentTag(node,tag) { //Найти ближайшего родителя по tagName. Здесь мы движемся вверх, пока не встретим родителя, у которого тег = нашему заданному tag
		if (node) {return (node.tagName == tag) ? node : getParentTag(node.parentElement,tag);}
		return null;
	}

	function copyToBuffer(mytext) {
 		var copytext = document.createElement('input')
		copytext.value = mytext
		document.body.appendChild(copytext)
		copytext.select()
		document.execCommand('copy')
		document.body.removeChild(copytext)
	}

	onload = function () {document.getElementById ('mySuperTBL').onclick = mySuperFunc;}

	function reverseDate(myDate){
		const parts = myDate.split('.');
		const today = new Date(parts[2], parts[1]-1, parts[0]); 
		const dateSrc = today.toLocaleString('ru-RU', { year: 'numeric', month: 'numeric', day: 'numeric' });
		dateDst = dateSrc.split(".").reverse().join("-");
		return dateDst;
	}

	function mySuperFunc(ev){
		// Заносим данные из таблицы в форму
		var e = ev || window.event, tag = (e.target).closest('tr') || e.srcElement;
		input = (e.target).closest('tr').getElementsByTagName('td');

		// Удалить блок с конвертом из полученных данных из ячейки "Название"
		var tname = (input[3].textContent).trim();
		tname = tname.replace('			✉', '');

		// Удалить блок с конвертом из полученных данных из ячейки "Название"
		var tgroup = (input[2].textContent).trim();
		tgroup = tgroup.replace('⇚', '');

		// Вставить данные из таблицы в форму
		document.form_add.form_task_id.value = input[0].textContent;
		document.form_add.form_task_date_add.value = reverseDate(input[1].textContent);
		document.form_add.form_task_group.value = tgroup;
		document.form_add.form_task_name.value = tname;
		document.form_add.form_task_descr.value = input[4].textContent;
		document.form_add.form_task_comment.value = input[5].textContent;
		document.form_add.form_task_priority.value = input[6].textContent;
		document.form_add.form_task_deadline.value = reverseDate(input[7].textContent);
		document.form_add.form_task_author.value = input[8].textContent;
		document.form_add.form_task_date_completion.value = reverseDate(input[10].textContent);//reverseDate(input[10].textContent);
/*		if (reverseDate(input[10].textContent) === "[object Date]") {
			document.form_add.form_task_date_completion.value = reverseDate(input[10].textContent);
		}*/
//		console.log(document.form_add.form_task_date_completion.value);

		// Подпись кнопки в форме
		document.form_add.tbutton.innerHTML = "Сохранить изменения";

		// Выделение строки
		var row = getParentTag(e.target,'TR');
		if (!row) {return;}
		var tbl = this,  idx = tbl.getAttribute('activeRowIndex');
		if (idx) {
			tbl.rows[idx].classList.remove('activeRow');
		}
		row.classList.add('activeRow');
		tbl.setAttribute('activeRowIndex', row.rowIndex);

		// Стрелка
		var arr = document.querySelectorAll('.arrow');
		arr.forEach(function(item, i, arr) {
			item.style.display = "none";
		});
		document.querySelectorAll('.arrow')[row.rowIndex-1].style.display = "block";

		// Цвет фона формы
		var formColor = 'lightblue';
		if (input[12].textContent == 1) { formColor = '#d3e0e4'; }
		document.getElementById('mainForm').style.background = formColor;
	}

	// Очистка формы от данных
	function tcleaner(){
		currDate = new Date();
		let year = currDate.getFullYear() + ""; // год;
		let day = currDate.getDate(); // текущий день
		day = day < 10 ? "0" + day : day;
		let month = currDate.getMonth() + 1; //текущий месяцж
		month = month < 10 ? "0" + month : month;

		document.form_add.form_task_id.value = "";
		document.form_add.form_task_group.value = "";
		document.form_add.form_task_name.value = "";
		document.form_add.form_task_descr.value = "";
		document.form_add.form_task_comment.value = "";
		document.form_add.form_task_priority.value = "";
		document.form_add.form_task_date_add.value = year + '-' + month + '-' + day;
		document.form_add.form_task_deadline.value = year + '-' + month + '-' + day;
		document.form_add.form_task_date_completion.value = "";
		document.form_add.form_task_author.value = "";

		document.form_add.tbutton.innerHTML = "Добавить задачу";
	}

	// Запрос на подтверждение удаления записи
	function request_form() {
	    return confirm("Подтвердите действие");
	}

	// Показать/Спрятать дополнительные поля формы
	function tviewer(){

	    var obj1 = document.getElementById('tdid'); 

            if (obj1.style.display != "block") {
		obj1.style.display = "block";
		var reg = 1;
	    } else {
		obj1.style.display = "none";
		var reg = 0;
	    }

	    var objs2 = document.querySelectorAll('.tr-tln'); 
       	    var objs3 = document.querySelectorAll('.tr-other'); 

		objs2.forEach( function(item, i, objs2) {
	            if (reg == 1) item.style.display = "table-row";
		    else item.style.display = "none";
		})

		objs3.forEach( function(item, i, objs3) {
	            if (reg == 0) item.style.display = "table-row";
		    else item.style.display = "none";
		})
	}

