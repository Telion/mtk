<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
visitsLog(__FILE__);

$form_task_id = 0;
$form_task_group = 0;
$form_task_name = 0;
$form_task_descr = 0;
$form_task_comment = 0;
$form_task_priority = 0;
$form_task_date_add = 0;
$form_task_deadline = 0;
$form_task_date_completion = 0;
$form_task_author = 0;
$complete_query_id = 0;
$delete_query = 0;
$form_task_count = 0;


/* Получение POST данных */
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $form_task_id = verifyArrayArgumentString($_POST, 'form_task_id');
    $form_task_group = verifyArrayArgumentString($_POST, 'form_task_group');
    $form_task_name = verifyArrayArgumentString($_POST, 'form_task_name');
    $form_task_descr = strip_tags(verifyArrayArgumentString($_POST, 'form_task_descr'));
    $form_task_comment = strip_tags(verifyArrayArgumentString($_POST, 'form_task_comment'));
    $form_task_priority = verifyArrayArgumentString($_POST, 'form_task_priority');
    $form_task_date_add = verifyArrayArgumentString($_POST, 'form_task_date_add');
    $form_task_deadline = verifyArrayArgumentString($_POST, 'form_task_deadline');
    $form_task_date_completion = verifyArrayArgumentString($_POST, 'form_task_date_completion');
    $form_task_author = verifyArrayArgumentString($_POST, 'form_task_author');
    $complete_query_id = verifyArrayArgumentString($_POST, 'complete_query_id');
    $delete_query = verifyArrayArgumentString($_POST, 'delete_query');
    $form_task_count = verifyArrayArgumentString($_POST, 'form_task_count');

    if ($form_task_id != "") {
        $db = JFactory::getDBO();
        $query = 'UPDATE tln_programmer_tasks SET task_group="' . $db->escape($form_task_group) . '", task_name="' . $db->escape($form_task_name) . '", task_description="' . $db->escape($form_task_descr) . '", task_comment="' . $db->escape($form_task_comment) . '", task_priority="' . $form_task_priority . '", task_date_added="' . $form_task_date_add . '", task_date_deadline="' . $form_task_deadline . '", task_date_completion="' . $form_task_date_completion . '", task_author="' . $form_task_author . '" WHERE task_id=' . $form_task_id;
        echo $query;
        $db->setQuery($query);
        $db->execute();
    } else {
        if (((trim($form_task_name) != '') or (trim($form_task_descr) != '')) and ((count($arr_queries)) == $form_task_count)) {
            $db = JFactory::getDBO();
            $db->setQuery('INSERT INTO tln_programmer_tasks(task_group, task_name, task_description, task_priority, task_date_added, task_date_deadline, task_date_completion, task_comment, task_author) VALUES("' . $db->escape($form_task_group) . '", "' . $db->escape($form_task_name) . '", "' . $db->escape($form_task_descr) . '", "' . $form_task_priority . '", "' . $form_task_date_add . '", "' . $form_task_deadline . '", "' . $form_task_date_completion . '", "' . $db->escape($form_task_comment) . '", "' . $db->escape($form_task_author) . '")');
            $db->execute();
        }
    }

    /* Выполнение задачи, если нажата кнопка "v" */
    if ($complete_query_id) {
        if ($arr_queries[$complete_query_id]->task_status == 1) $complete_value = 0;
        else $complete_value = 1;

        $db = JFactory::getDBO();
        $db->setQuery('UPDATE tln_programmer_tasks SET task_status="' . $complete_value . '", task_date_completion="' . date("Y-m-d", time()) . '" WHERE task_id=' . $complete_query_id);
        $db->execute();
    }

    /* Удаление записи из базы, если нажата кнопка "x" */
    if ($delete_query) {
        $db = JFactory::getDBO();
        $db->setQuery('DELETE FROM tln_programmer_tasks WHERE task_id=' . $delete_query);
        $db->execute();
    }
}

/* Добавление данных в базу, если заполнены url'ы */
$db->setQuery('SELECT * FROM tln_programmer_tasks');
$arr_queries = $db->loadObjectList('task_id');

$db->setQuery('SELECT *, DATEDIFF(task_date_deadline, CURRENT_DATE())+1 as minus FROM tln_programmer_tasks WHERE task_status = 0 and task_date_added != task_date_deadline and DATEDIFF(task_date_deadline, CURRENT_DATE())+1 <= 0 ORDER BY minus DESC');
$arr_q = $db->loadObjectList();
$arr_union = $arr_q;

$db->setQuery('SELECT *, DATEDIFF(task_date_deadline, CURRENT_DATE())+1 as minus FROM tln_programmer_tasks WHERE task_status = 0 and task_date_added != task_date_deadline and DATEDIFF(task_date_deadline, CURRENT_DATE())+1 > 0 ORDER BY minus ASC, task_priority DESC');
$arr_q = $db->loadObjectList();
$arr_union = array_merge($arr_union, $arr_q);

$db->setQuery('SELECT *, 0 as minus FROM tln_programmer_tasks WHERE task_status = 0 and task_date_added = task_date_deadline ORDER BY task_priority DESC');
$arr_q = $db->loadObjectList();
$arr_union = array_merge($arr_union, $arr_q);

$db->setQuery('SELECT *, 0 as minus FROM tln_programmer_tasks WHERE task_status = 1 ORDER BY task_date_completion DESC');
$arr_q = $db->loadObjectList();
$arr_union = array_merge($arr_union, $arr_q);

?>

<html>
<head>
    <title>cmsMTK. Задачи программисту</title>
    <script src="programmer-tasks.js"></script>
    <link href="main.css" rel="stylesheet" type="text/css"/>

    <?php
    require_once CONST_MENU;
    ?>
    <div class="mainPage">
        <div class="container">

            <div class="table_add_new fixed" id="mainForm">
                <form action='programmer-tasks.php' method='post' name='form_add' id="form_insert">

                    <!-- ID ДАТА -->
                    <div class="line-1">
                        <div class="field1">
                            <?php
                            if ($form_task_id != "") $text = "ID: " . $form_task_id;
                            else $text = "ID: -";
                            ?>
                            <div class="lbl"><input type='text' name='form_task_id' id='form_task_id'
                                                    placeholder="<?php echo "$text"; ?>" autocomplete="off"
                                                    form="form_insert" readonly/></div>
                        </div>
                        <div class="field2">
                            <div class="lbl"><label>Дата добавления</label></div>
                            <div class="lbl"><input type='date' name='form_task_date_add' id='form_task_date_add'
                                                    value="<?php echo date("Y-m-d", time()); ?>" autocomplete="off"
                                                    form="form_insert"/></div>
                        </div>
                    </div>

                    <!-- ГРУППА НАЗВАНИЕ -->
                    <div class="line-1">
                        <div class="field2">
                            <input type='text' name='form_task_group' id='form_task_group' placeholder="Группа"
                                   autocomplete="off" form="form_insert"/>
                        </div>
                        <div class="field2">
                            <input type='text' name='form_task_name' id='form_task_name' placeholder="Название"
                                   autocomplete="off" form="form_insert"/>
                        </div>
                    </div>

                    <!-- ОПИСАНИЕ -->
                    <div class="line-1">
                        <textarea name='form_task_descr' id='form_task_descr' form="form_insert"> </textarea>
                    </div>

                    <!-- АВТОР ДАТА ВЫПОЛНЕНИЯ -->
                    <div class="line-1" id="lblDate">
                        <div class="field2">
                            <input class="border-red" type='text' name='form_task_author' id='form_task_author'
                                   placeholder="Кто придумал?" autocomplete="off" form="form_insert"/>
                        </div>
                        <div class="field2" style="display: none;">
                            <div class="lbl"><label class="label-red">Дата выполнения</label></div>
                            <div class="lbl"><input type='date' name='form_task_date_completion'
                                                    id='form_task_date_completion' value="0" autocomplete="off"
                                                    form="form_insert" class="border-red"/></div>
                        </div>
                    </div>

                    <!-- КОММЕНТ -->
                    <div class="line-1" id="lblComment">
                        <textarea class="border-red" name='form_task_comment' id='form_task_comment'
                                  form="form_insert"> </textarea>
                    </div>

                    <!-- ПРИОРИТЕТ ДАТА -->
                    <div class="line-1">
                        <div class="field2">
                            <?php
                            if ($form_task_priority != "") $text = "Приоритет: " . $form_task_priority;
                            else $text = "Приоритет: 0";
                            ?>
                            <div class="lbl"><input type='text' name='form_task_priority' id='form_task_priority'
                                                    placeholder="<?php echo $text; ?>" autocomplete="off"
                                                    form="form_insert"/></div>
                        </div>
                        <div class="field2">
                            <div class="lbl"><label>Дата дедлайна</label></div>
                            <div class="lbl"><input type='date' name='form_task_deadline' id='form_task_deadline'
                                                    value="<?php echo date("Y-m-d", time()); ?>" autocomplete="off"
                                                    form="form_insert"/></div>
                        </div>
                    </div>

                    <hr>
                    <input type="hidden" name="form_task_count" value="<?php echo(count($arr_union) + 1); ?>"
                           form="form_insert">
                    <div class="button">
                        <button type='submit' name="tbutton" width="1000px">Добавить задачу</button>
                        <input type="button" name="tclean" value="Очистить форму" onclick="tcleaner();">
                    </div>
                </form>
            </div>


            <div class="table_add_report rubbery">
                <table id="mySuperTBL" class="table zebra-table">

                    <thead>
                    <tr>
                        <!--<th><span id="tdid">id</span></th>-->
                        <th>
                            <div>Дата создания
                        </th>
                        <th>Группа</th>
                        <th>Название <span onclick="copyToBuffer('~');">~~~</span> <span onclick="copyToBuffer('°');">°°°</span>
                            <span onclick="copyToBuffer('±');">±±±</span> <span onclick="copyToBuffer('²');">²²²</span>
                            <span onclick="copyToBuffer('⌀');">⌀⌀⌀</span> <span onclick="copyToBuffer('℃');">℃℃℃</span>
                            <span onclick="copyToBuffer('×');">×××</span></th>
                        <th class="hide">Постановка задачи</th>
                        <th class="hide">Комментарий</th>
                        <th><a href="visit-history.php">
                                <div>Приоритет</div>
                            </a></th>
                        <th>
                            <div>Deadline
                        </th>
                        <th>
                            <div>Готово</div>
                        </th>
                        <th>
                            <div>Дата выполнения
                        </th>
                        <th>
                            <div>Уда<span value="tviewt" id="tview" onclick="tviewer();">л</span>ить</div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($arr_union as $value) {

                        $tr_class = $mini_btn_class = '';
                        if ($value->task_status == 1) {
                            $tr_class = ' tr-unactive';
                            $mini_btn_class = 'mini-button-unactive';
                        }

                        if ($value->task_author === 'Tln') $tr_class_who = 'tr-tln';
                        else $tr_class_who = 'tr-other';

                        echo "<tr class='trline " . $tr_class_who . $tr_class . "' id='dedicatedLine'>";

                        ?>
                        <!-- ID -->
                        <td style="border-left: solid 1px #c7c7c7; color: #888; display: none;"
                            class="right"><?php echo $value->task_id; ?></td>

                        <!-- Дата создания -->
                        <td style="border-left: solid 1px #c7c7c7; color: #888;"><?php echo edit_date($value->task_date_added); ?></td>

                        <!-- Группа задачи -->
                        <td style="position: relative;" id="tgroup"><?php
                            echo '<div class="arrow" style="position: absolute; left: -152px; top: -16px; font-size: 50px; color: lightblue; display: none;">&#8666;</div>';
                            echo $value->task_group; ?>
                        </td>

                        <!-- Название задачи -->
                        <td id="tname">
                            <span><?php echo $value->task_name; ?></span>
                            <div class="have-comment"
                                 onclick='showhide();' <?php echo (trim($value->task_comment) == "") ? 'style="display: none;"' : ''; ?>>
                                &#9993;
                            </div>
                        </td>

                        <!-- Постановка задачи -->
                        <td id="tdesc" class="hide">
                            <div><?php echo $value->task_description; ?></div>
                        </td>

                        <!-- Постановка задачи -->
                        <td id="tcomment" class="hide">
                            <div><?php echo $value->task_comment; ?></div>
                        </td>

                        <!-- Приоритет -->
                        <td id="tpriority" class="center"><?php echo $value->task_priority; ?></td>

                        <!-- Контрольный срок -->
                        <?php
                        $term_days = (strtotime($value->task_date_deadline) - strtotime($value->task_date_added)) / (60 * 60 * 24);
                        $deadline_style = '';
                        if (($term_days > 0) and ($value->task_status == 0)) $deadline_style = " style='border: solid 1px black; border-radius: 6px; padding: 1px 9px;font-weight: 700; background: lightblue;'";
                        else $deadline_style = " style='color: #888;'";

                        $delay = (time() - strtotime($value->task_date_deadline)) / (60 * 60 * 24);
                        if (($term_days > 0) and ($delay > 0) and ($value->task_status == 0)) $deadline_style = " style='border: solid 1px black; border-radius: 6px; padding: 1px 9px;font-weight: 700; color: black; background: lightpink;'";

                        ?>

                        <td class="center">
                            <span <?php echo $deadline_style; ?> > <?php echo edit_date($value->task_date_deadline); ?> </span>
                        </td>

                        <!-- Автор задачи -->
                        <td class="hide"><?php echo $value->task_author; ?></td>

                        <!-- Выполнить задачу -->
                        <td style="padding: 0;" class="center">
                            <form action='programmer-tasks.php' method='post' name='form_complete'
                                  id='form_complete<?php echo $value->task_id; ?>'>
                                <input type="hidden" name="complete_query_id" value="<?php echo $value->task_id; ?>"
                                       form="form_complete<?php echo $value->task_id; ?>">
                                <button class='mini-button <?php echo $mini_btn_class; ?>' type='submit'
                                        form="form_complete<?php echo $value->task_id; ?>">v
                                </button>
                            </form>
                        </td>

                        <!-- Дата выполнения -->
                        <td class="center"><?php echo edit_date($value->task_date_completion); ?></td>

                        <!-- Удалить задачу -->
                        <td style="padding: 0; border-right: solid 1px #c7c7c7;" class="center">
                            <form action='programmer-tasks.php' method='post' name='form_del'
                                  id='form_delete<?php echo $value->task_id; ?>' onsubmit='return request_form();'>
                                <input type="hidden" name="delete_query" value="<?php echo $value->task_id; ?>"
                                       form="form_delete<?php echo $value->task_id; ?>">
                                <button class='mini-button <?php echo $mini_btn_class; ?>' type='submit'
                                        form="form_delete<?php echo $value->task_id; ?>">x
                                </button>
                            </form>
                        </td>

                        <!-- status -->
                        <td class="hide"><?php echo $value->task_status; ?></td>

                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>