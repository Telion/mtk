<?php
ini_set('display_errors', 1);
//$path = $_SERVER['DOCUMENT_ROOT'] . '/widget/widjet.js';
$path = 'https://www.mirtelcom.ru/widget/widjet.js';
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Пример работы виджета ПВЗ</title>
    <script type="text/javascript" src="<?= $path ?>" id="ISDEKscript" charset="utf-8"></script>
    <style>
        #infowidjet {height: 450px; border: 2px solid coral;}
    </style>
</head>
<body>
<script type="text/javascript">
    var widjet = new ISDEKWidjet({
        country: 'Россия',
        defaultCity: 'auto',
        cityFrom: 'Тюмень',
//        choose: false, // не будем отображать кнопку выбора пункта выдачи
//        link: 'forpvz'
        link: 'infowidjet' // привяжем виджет к указанной ссылке
    });
</script>
<div id='infowidjet'>Здесь после загрузки появится виджет</div>


</body>
</html>
