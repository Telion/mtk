<?php

$rootPath = $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk';
require_once $rootPath . '/function.php';
require_once $rootPath . '/includes/simple_html_dom.php';

// Файл для остановки скрипта (просто убей, для остановки)
$current_date = date("d.m.Y") . "  //  " . date("H:i:s");
$fdie = fopen('die.txt', 'w') or die("не удалось создать файл");
fwrite($fdie, $current_date . "\r\nудали, для остановки скрипта");
fclose($fdie);

$filter = array('Интернет-магазин', 'Virtuemart');

$baseUrl = 'https://www.joomfox.org/index.php?option=com_tags&view=tag&id[1]=178&id[2]=65&return_any_or_all=2&start=';
$baseTemplateUrl = 'https://www.joomfox.org';
$allPages = array();
for ($i = 0; $i < 410; $i += 10) {
    $catalogUrl = $baseUrl . $i;
    printr($catalogUrl);
    $catalogHtml = file_get_html($catalogUrl);
    foreach ($catalogHtml->find('.page-header a') as $catalogElement) {
        $pageUrl = $baseTemplateUrl . $catalogElement->href;
        printr($pageUrl);
        $nameList = explode('/', $pageUrl);
        $fullName = $nameList[count($nameList) - 1];
        $name = explode('.', $fullName)[0];
        $pageHtml = file_get_html($pageUrl);
        $dateCreate = $pageHtml->find('.t_pad')[0]->innertext;
        $dateLastUpdate = $pageHtml->find('.t_pad')[1]->innertext;
        $attributes = array();
        foreach ($pageHtml->find('.t_pad a') as $pageElement) {
            $element = trim($pageElement->plaintext);
            if ($element != 1 && $element != '') {
            } else {
                $element = trim($pageElement->title);
            }
            $attributes[] = $element;

        }
        $allPages[] = new Page($name, $pageUrl, $dateCreate, $dateLastUpdate, $attributes);
        break;
    }
    break;
}

printr($allPages);

echo '<style> th, td {border: solid 1px grey; padding: 4px 7px;} </style>';
echo '<table style="border-collapse: collapse;"><tr><th>Название (URL)</th><th>Дата создания</th><th>Дата обновления</th><th>Аттрибуты</th></tr>';
foreach ($allPages as $page) {
    echo '<tr>';
    echo '<td>' . '<a href="' . $page->url . '"> ' . $page->name . ' </a>' . '</td>';
    echo '<td>' . $page->dateCreate . '</td>';
    echo '<td>' . $page->dateLastUpdate . '</td>';
    foreach ($page->attributes as $attribute) {
        echo '<td>' . $attribute . '</td>';
    }
    echo '</tr>';
}

class Page
{
    public $name;
    public $url;
    public $dateCreate;
    public $dateLastUpdate;
    public $attributes;

    public function __construct($name, $url, $dateCreate, $dateLastUpdate, $attributes)
    {
        $this->name = $name;
        $this->url = $url;
        $this->dateCreate = $dateCreate;
        $this->dateLastUpdate = $dateLastUpdate;
        $this->attributes = $attributes;
    }
}
