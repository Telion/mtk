<?php
require_once 'connectionDB.php';

/* Получение POST данных */
if($_SERVER["REQUEST_METHOD"]=="POST"){
	$delete_query = $_POST['delete_query'];
}

/* Для сброса POST параметров, чтобы не отправлялись повторно по F5 */
if (isset($_POST['delete_query'])) {
	header("Location: z_main_9_mtk_sds.php");
}

/* Удаление записи из базы, если нажата кнопка "x" */
if ($delete_query){
	$db_delete = JFactory::getDBO();
	$db_delete->setQuery('DELETE FROM tln_compare_sds WHERE id='.$delete_query);
	$db_delete->execute();
}

$db->setQuery('
	SELECT 
	    tln_compare_sds.*,
	    wxa43_virtuemart_product_prices.product_price,
            wxa43_virtuemart_products_ru_ru.product_name
	FROM 
	    tln_compare_sds, 
	    wxa43_virtuemart_product_prices,
            wxa43_virtuemart_products_ru_ru,
	    wxa43_virtuemart_products
	WHERE
	    tln_compare_sds.our_id = wxa43_virtuemart_product_prices.virtuemart_product_id
            AND wxa43_virtuemart_products_ru_ru.virtuemart_product_id = tln_compare_sds.our_id
            AND wxa43_virtuemart_products.virtuemart_product_id = tln_compare_sds.our_id
            AND wxa43_virtuemart_products.published = 1
');
$arr_products = $db->loadObjectList();
?>


<html><head><title>cmsMTK. Сравнение цен MTK с сайтом SDS</title>
<link rel="shortcut icon" href="favicon.ico">
<link href="main.css" rel="stylesheet" type="text/css" />
<style>
	table {border: 1px solid #494540; border-collapse: collapse;} 
	th {color: #fff; font-weight: 700; font-size: 13px; padding: 10px; width: 80px; border: solid 1px #494540; background: #494540;} 
	td {border-left: solid 0; border-right: solid 0; font-size: 14px; color: #666; text-align: right; border: solid 1px #494540; padding: 8px; padding-right: 20px;} 
	.telion_post {margin-bottom: 300px;}
	.no-in-sds {background: mistyrose;}
	.price-down-0 {background: #fff5f2;}
	.price-down-5 {background: #fff9db;}
	.price-up-5 {background: #f2ffed;}
</style>
</head>
<body>

<?php
 $menu_item = 0;
 require 'menu.php';
?>

<div class="main-block-airtek">
<div class="info-table">

<table cellpadding=0 cellspacing=0><tr><th style="width: 50px;">№ п/п</th><th>Товар MTK</th><th>Артикул товара, СДС</th><th>Цена, МТК</th><th>Цена, СДС</th><th>Разница цен, руб</th><th>Разница цен, %</th><th style="width: 100px">Дата последней проверки</th><th>Удалить запись</th></tr>

<?php

// Добавляю поле разницы цен
for ($j=0;$j<count($arr_products);$j++){
	if ($arr_products[$j]->sds_price == 0){
		$arr_products[$j]->diff_price = 99999;
		$arr_products[$j]->diff_price_perc = 99999;
	} else {
		$arr_products[$j]->diff_price = $arr_products[$j]->product_price - $arr_products[$j]->sds_price;
		$arr_products[$j]->diff_price_perc = ($arr_products[$j]->product_price - $arr_products[$j]->sds_price) / $arr_products[$j]->sds_price * 100;
	}
}

// Сортирую массив по полю разницы цен
for ($a=0; $a<count($arr_products); $a++){
	for ($b=$a+1; $b<count($arr_products); $b++){
		if ($arr_products[$a]->diff_price_perc > $arr_products[$b]->diff_price_perc){
			$temp = $arr_products[$a];
			$arr_products[$a] = $arr_products[$b];
			$arr_products[$b] = $temp;
		}
	}
}

// Вывод данных в таблицу
$i = 0;
foreach ($arr_products as $product){
	$class = '';

	// № п/п
	$i++;

	// ID товара, МТК
	$our_id = '<a href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$product->our_id.'" target="_blank"><div style="color: #0081ff;">ID: '.$product->our_id.' // '.$product->product_name.'</div></a>';
	// Артикул товара, СДС
	$sds_intnote = '<a href="'.'https://www.sds-group.ru/search.htm?search='.$product->sds_intnotes.'" target="_blank"><div style="color: red;">'.$product->sds_intnotes.'</div></a>';

	// Цена, МТК
	$our_price = '<a href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$product->our_id.'" target="_blank"><div style="color: #0081ff;">'.round($product->product_price, 2).' р</div></a>';

	// Цена, СДС
	$sds_price = round($product->sds_price, 2);
	$sds_price_for_table = '<a href="'.'https://www.sds-group.ru/search.htm?search='.$product->sds_intnotes.'" target="_blank"><div style="color: red;">'.$sds_price.' р</div></a>';

	// Разница цен, руб
	$diff_price = round($product->diff_price, 2);
	if($diff_price == 99999){
		$diff_price = "нет в СДС";
	} else {
		$diff_price .= " р";
	}

	// Разница цен, %
	if($sds_price == 0){
		$diff_price_perc = 0;
		$class = ' class="no-in-sds"';
	} else {
		$diff_price_perc = round($product->diff_price_perc, 2).'%';
		if($diff_price_perc < 0){
			$class = ' class="price-down-0"';
		} else if ($diff_price_perc < 5){
			$class = ' class="price-down-5"';
		} else {
			$class = ' class="price-up-5"';
		}
	}

	// last_check
	$d = new DateTime();
	$d->setTimestamp($product->last_check);
	$last_check = $d->format('d.m.Y');
	$last_check_class = '';
	if (((time() - $product->last_check)/24/60/60) > 5){
		$last_check_class = ' class="back-orange"';
	}

	echo '<tr'.$class.'><td>'.$i.'</td><td style="width: 600px; text-align: left; padding: 6; padding-left: 12px;">'.$our_id.'</td><td style="text-align: left; padding: 0; padding-left: 12px;">'.$sds_intnote.'</td><td style="padding: 0; padding-right: 20px;">'.$our_price.'</td><td style="padding: 0; padding-right: 20px;">'.$sds_price_for_table.'</td><td>'.$diff_price.'</td><td>'.$diff_price_perc.'</td>
<td'.$last_check_class.'>'.$last_check.'</td>';
?>
		<td style="padding: 0;">
		<form class="frm-delete" action='z_main_9_mtk_sds.php' method='post' name='form_del' id='form_delete<?php echo $product->id; ?>'>
			<input type="hidden" name="delete_query" value="<?php echo $product->id; ?>" form="form_delete<?php echo $product->id; ?>">
			<button class="btn-delete" type='submit' form="form_delete<?php echo $product->id; ?>">X</button>
		</form>
		</td>
	</tr>

<?php
}
?>

</table>
</div>
</div>
</body>
</html>