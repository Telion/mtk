<?php
require_once 'connectionDB.php';
require_once 'function.php';
include_once('includes/simple_html_dom.php');

// Файл для остановки скрипта (просто убей, для остановки)
$current_date = date("d.m.Y")."  //  ".date("H:i:s");
$fdie = fopen('die.txt', 'w') or die("не удалось создать файл");
fwrite($fdie, $current_date."\r\nудали, для остановки скрипта");
fclose($fdie);
                                                                          
?>

<html>
<head>
<title>Сравнение цен МТК и СДС</title>
<link rel="shortcut icon" href="favicon.ico">
</head>
<body>

<?php

// Товары, на которые цена СДС указана за 100 шт (например бухта 100м)

$arr_products_100pc = [
	"01-6104-3",
	"01-6106-3",
	"01-2431-6",
	"01-2424",
	"01-5101",
	"01-2436-1",
        "01-2251",
	"01-2003",
	"01-2201",
	"01-2654",
	"01-5001",
	"01-2401",
	"01-2001",
	"01-5001-6",
	"01-6305",
	"01-6106-6",
	"01-2641"
];

$arr_products_200pc = [
	"01-4015"
];

$arr_products_305pc = [
	"01-0047",
	"01-2651"
];


$db->setQuery('
	SELECT 
	    wxa43_virtuemart_products.virtuemart_product_id,
	    wxa43_virtuemart_products.intnotes,
	    wxa43_virtuemart_product_prices.product_price
	FROM 
	    wxa43_virtuemart_products, 
	    wxa43_virtuemart_product_prices
	WHERE 
	    wxa43_virtuemart_product_prices.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id
	    and wxa43_virtuemart_products.intnotes != ""
	    and wxa43_virtuemart_products.published = 1
	ORDER BY `wxa43_virtuemart_products`.`virtuemart_product_id` ASC
	');
$arr_compares = $db->loadObjectList(); // id, comment, our_url, our_last_price, compare_url, compare_last_price, last_check

$db->setQuery('SELECT sds_intnotes FROM tln_compare_sds');
$arr_history_result = $db->loadObjectList();
$arr_history = [];
foreach ($arr_history_result as $hist){
	$arr_history[] = $hist->sds_intnotes;
}
$db_insert = JFactory::getDBO();

$how_many = rand(100, 150);
//$how_many = 7;

echo "Выбираю из базы все товары с комментариями, выбрать из них только товары, соответствующие маске";
echo "<hr>";

echo "Количество товаров с артикулом: ".count($arr_compares);
echo "<hr>";

$save_current_intnotes_file = "z_main_9_price_checker_SDS_logic_last_intnotes.cfg";
$last_intnotes = htmlentities(file_get_contents($save_current_intnotes_file));

$negative_diff = 0;
// Перебираю товары МТК
for ($i = 0; $i < $how_many; $i++) {
	
	$mtk_id = $arr_compares[$last_intnotes]->virtuemart_product_id;
	$mtk_article = $arr_compares[$last_intnotes]->intnotes;
	$mtk_price = $arr_compares[$last_intnotes]->product_price;

	echo "<br>ID: ".$mtk_id.", Комментарий на МТК: ".$mtk_article."<br>";

	preg_match_all('/(\d{2}-\d{4}-\d{1,2}|\d{2}-\d{4})/', $mtk_article, $arr_intnotes_1);
	$arr_intnotes_2 = array_unique($arr_intnotes_1);
	$arr_intnotes = array_unique($arr_intnotes_2[0]);

	if (empty($arr_intnotes)){
		echo "В комментарии не найдены артикулы рексанта<br><br>";
		$how_many++;

	}

	// Если в одном товаре МТК записано несколько артикулов Rexant, то проверяем цену на все.
	foreach ($arr_intnotes as $current_intnote){
		echo "zz";
		$html = file_get_html('https://www.sds-group.ru/search.htm?search='.$current_intnote);

		// 1. На странице может быть несколько товаров, нужно сравнивать артикулы товаров на странице с артикулом из базы
		// 2. Для товара с правильным артикулом получить цену

		// ЕСТЬ ли товар с таким АРТИКУЛОМ в поиске СДС
		$sds_price = 0;
		$sds_intnote = 'not found';
		if ($html->find('ul.search-page li',1)) {
	
			// Перебираю все товары найденные на странице поиска в СДС
			foreach ($html->find('ul.search-page li') as $product){

				//echo "-1-<br>".$product."<br>-1-<br><br>";
			
				$sds_article = $product->find('div',4);
				$sds_article = str_replace(array("\r\n", "\r", "\n"), '',  strip_tags($sds_article)); // Удаление переноса строки

				// Если артикулы совпадают
				//$string_price = ", артикулы отличаются.";

				if ($sds_article == $current_intnote){

					$sds_intnote = $current_intnote;
					$sds_price = $product->find('div[class*="price"]', 0)->find('div',1);
					echo "-2-<br>".$sds_price."<br>-2-<br><br>";
					$sds_price = preg_replace("/[^,.0-9]/", '', $sds_price);
					$sds_price = str_replace(array("\r\n", "\r", "\n"), '',  strip_tags($sds_price)); // Удаление переноса строки
					if (in_array($sds_intnote, $arr_products_100pc)){
						$sds_price /= 100;
					} else if (in_array($sds_intnote, $arr_products_200pc)){
						$sds_price /= 200;
					} else if (in_array($sds_intnote, $arr_products_305pc)){
						$sds_price /= 305;
					}

					echo "-2a-<br>".$sds_price."<br>-2a-<br><br>";
					//$string_price = ", артикулы совпали, цена на СДС: ".$sds_price;
				}
				//echo "SDS артикул: ".$sds_article.$string_price."<hr>";
			}

			$url_mtk = '<a href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$mtk_id.'" target="_blank">ID в МТК:'.$mtk_id.'</a>';
			echo '<span style="color: red;">№ п/п: '.$last_intnotes.'</span> // <span style="color: green;">'.$url_mtk.'</span> // <a href="'.'https://www.sds-group.ru/search.htm?search='.$current_intnote.'"><span style="color: blue;">Артикул Rexant: '.$current_intnote.'</span></a> // <span style="color: orange;">Цена МТК: '.$mtk_price.'</span> // <span style="color: violet;">Цена СДС: '.$sds_price.'</span>';

		} else {
			echo 'ТОВАРА С АРТИКУЛОМ <a href="'.'https://www.sds-group.ru/search.htm?search='.$current_intnote.'"><span style="color: blue;">"'.$current_intnote.'"</span></a> НЕТ В СДС';
		}

		if ($sds_price > $mtk_price) $negative_diff = 1;

		// Добавить/обновить данные в таблице с результатами проверки
		// Узнать есть ли в таблице проверки цен сдс, запись с текущим артикулом.
		echo "<br>-3-".$sds_price."-3-<br><br>";

		if (in_array($current_intnote, $arr_history)){
			echo " - ЕСТЬ в массиве<br>";
			$db_insert->setQuery('UPDATE tln_compare_sds SET sds_price='.$sds_price.', last_check='.time().' WHERE our_id='.$mtk_id.' and sds_intnotes="'.$current_intnote.'"'); // 
		} else {
			echo " - НЕТ в массиве<br>";
			$db_insert->setQuery('INSERT INTO tln_compare_sds(our_id, sds_intnotes, sds_price, last_check) VALUES("'.$mtk_id.'", "'.$current_intnote.'", "'.$sds_price.'", "'.time().'")');
		}       	
		$db_insert->execute();
	}
	
	// Переход к следующему товару
	if ($last_intnotes == count($arr_compares)-1) {
		$last_intnotes = 0;
	} else {
		$last_intnotes++;
	}	

echo "<hr>";
}

// Добавление записи по текущему товару к HTML отчету
$fdhtml = fopen($save_current_intnotes_file, 'w') or die("не удалось создать файл");
fwrite($fdhtml, $last_intnotes);
fclose($fdhtml);
?>



<?php
// Проверка акционных товаров, отправка сообщения в ТЕЛЕГРАММ АЛИЕ 

$db = JFactory::getDBO();
$db->setQuery('SELECT wxa43_virtuemart_products_ru_ru.product_name, wxa43_virtuemart_product_prices.product_price, wxa43_virtuemart_product_prices.product_price_publish_down FROM wxa43_virtuemart_product_prices, wxa43_virtuemart_products_ru_ru WHERE product_price_publish_down > 0 and wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_product_prices.virtuemart_product_id ORDER BY `wxa43_virtuemart_product_prices`.`product_price_publish_down` ASC');
$arr_products = $db->loadObjectList();

$message_text = "";
$send_message = 0;
foreach ($arr_products as $product) {

	$days = floor((strtotime($product->product_price_publish_down) - time()) / (60*60*24));
	if ($days < 4) {
		$message_text .= $product->product_name." (".floor($product->product_price)." руб) - ".$days." дн. до окончания\r\n\r\n";
		$send_message = 1;
	}
}

if ($send_message == 1) {
	echo "Акционные товары:<br>".$message_text;
}









// ОТСЛЕЖИВАЮ ТОВАРЫ С НОВЫМИ/ИЗМЕНЕННЫМИ ЦЕНАМИ
	// Изменения цен // g1_checkPriceChange.php // Количество новых товаров (цена еще не в базе сравнения)
	$db->setQuery('
		SELECT
			COUNT(wxa43_virtuemart_products.virtuemart_product_id)
		FROM 
			wxa43_virtuemart_products
		WHERE 
			published = 1 AND
			wxa43_virtuemart_products.virtuemart_product_id NOT IN (
				SELECT virtuemart_product_id 
				FROM wxa43_virtuemart_product_customfields 
				WHERE virtuemart_custom_id = 54
			)
	');
	$count_products_with_new_price = $db->loadResult();

	// Изменения цен // g1_checkPriceChange.php // Количество товаров с измененной ценой
	$db->setQuery('
		SELECT
			COUNT(wxa43_virtuemart_product_prices.virtuemart_product_id)
		FROM 
			wxa43_virtuemart_product_customfields, 
			wxa43_virtuemart_product_prices
		WHERE 
			wxa43_virtuemart_product_prices.virtuemart_product_id = wxa43_virtuemart_product_customfields.virtuemart_product_id AND
			wxa43_virtuemart_product_customfields.virtuemart_custom_id = 54 AND
			wxa43_virtuemart_product_prices.product_override_price != wxa43_virtuemart_product_customfields.customfield_value AND
			wxa43_virtuemart_product_prices.product_price != wxa43_virtuemart_product_customfields.customfield_value
	');
	$count_products_with_change_price = $db->loadResult();
?>