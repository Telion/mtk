<script>
	window.onload = function () {
		var b = document.getElementById("form_alarm_message");
		b.onkeyup = function showMessage() {
			if (document.getElementById("form_alarm_message").value == '') {
				document.getElementById("form_check_alarm_message").checked = false;
			} else {
				document.getElementById("form_check_alarm_message").checked = true;
			}
		};		
	}

	<?php if (($_SERVER["REQUEST_METHOD"] == "POST") && ($_POST['generationActive']) == 1) { ?>
		var link = document.createElement('a');
		link.href = '<?= '/cms-mtk' .'/m6-mailing-generation/'.$file ?>';
		link.download = 'template.zip';
		link.click();
		window.close();
	<?php } ?>

	function repeater(stroka, regularka, result) {
		for (var i=0; i<5; i++){
			stroka = stroka.replace(regularka, result);
		}
	}


	function counter(type, id) {

		if (type == 'new') {
			var classNameChk = 'chk-input-new';
			var classNameLbl = 'new-products';
			var col0 = 'col-0-0';
			var col1 = 'col-0-1';
			var baseColor = '#3B80A2';
			var stringId = 'list_id_new_form';			
			if (document.querySelector('#line-new-' + id).style.background == 'slategrey') {
				document.querySelector('#line-new-' + id).style.background = "rgb(214, 236, 243)";
			} else {
				document.querySelector('#line-new-' + id).style.background = "slategrey";
			}
		}

		if (type == 'sale') {
			var classNameChk = 'chk-input-sale';
			var classNameLbl = 'sales';
			var col0 = 'col-1-0';
			var col1 = 'col-1-1';
			var baseColor = '#FF7B36';
			if (document.querySelector('#line-sale-' + id).style.background == 'burlywood') {
				document.querySelector('#line-sale-' + id).style.background = "rgb(255, 235, 212)";
			} else {
				document.querySelector('#line-sale-' + id).style.background = "burlywood";
			}
		}

		if (type == 'top') {
			var classNameChk = 'chk-input-top';
			var classNameLbl = 'top-products';
			var col0 = 'col-2-0';
			var col1 = 'col-2-1';
			var baseColor = '#0D990D';
			var stringId = 'list_id_top_form';
			if (document.querySelector('#line-top-' + id).style.background == 'seagreen') {
				document.querySelector('#line-top-' + id).style.background = "rgb(200, 247, 200)";
			} else {
				document.querySelector('#line-top-' + id).style.background = "seagreen";
			}
		}

		var count = document.getElementsByClassName(classNameChk);
		var countChecked = 0;
		for (var i = 0; i < count.length; i++) {
			if (count[i].checked == true) {countChecked++;}
		}

		arrString = '';
		if (stringId) {
			var fromString = document.getElementById(stringId).value;

			fromString = fromString.replace(/,/g, ', ');
			fromString = fromString.replace(/  /g, ' ');
			fromString = fromString.replace(/ /g, ', ');
			fromString = fromString.replace(/[^.,\d]+/g,"").replace( /\./g, ", ");
			fromString = fromString.replace(/,,/g, ', ');
			fromString = fromString.replace(/,,/g, ', ');
			fromString = fromString.replace(/,,/g, ', ');
			fromString = fromString.replace(/, ,/g, ', ');
			fromString = fromString.replace(/, ,/g, ', ');
			fromString = fromString.replace(',.', ', ');

			document.getElementById(stringId).value = fromString;

			var arrString = fromString.split(',');
		
			arrString = arrString.filter(function (item, pos) {
				if (item == null) return
		                return arrString.indexOf(item) == pos;
	        	});
		
			var arrString = arrString.filter(function (el) {
				return el != "";
			});
		
			countChecked += arrString.length;
		}

		document.getElementById(col0).innerHTML = countChecked;
		document.getElementById(col1).innerHTML = count.length + arrString.length;

		var lblNew = document.getElementsByClassName(classNameLbl);
		var color = baseColor;
		if (countChecked%4 != 0) color = '#BAC4C7';
		for (var i = 0; i < lblNew.length; i++) {
			lblNew[i].setAttribute('Style', 'color: ' + color + ';');
		}
	}


</script>