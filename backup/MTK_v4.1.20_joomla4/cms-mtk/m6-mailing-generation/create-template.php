<?php

// ----------------------------------- Получение POST данных. Форма 2 ----------------------------------- //


if (($_SERVER["REQUEST_METHOD"] == "POST") && ($_POST['generationActive'])) {
    $start_date = verifyArrayArgument($_POST, 'start_date');
	$end_date = verifyArrayArgument($_POST, 'end_date');
	$check_alarm_message = verifyArrayArgument($_POST, 'check_alarm_message');
	$alarm_message = verifyArrayArgument($_POST, 'alarm_message');

	$check_sales = verifyArrayArgument($_POST, 'check_sales');
	$select_sale = verifyArrayArgument($_POST, 'select_sale');

	$check_new_products = verifyArrayArgument($_POST, 'check_new_products');
	$select_new_product = verifyArrayArgument($_POST, 'select_new_product');

	$check_top_products = verifyArrayArgument($_POST, 'check_top_products');
	$select_top_product = verifyArrayArgument($_POST, 'select_top_product');

	$list_new_product_add = verifyArrayArgument($_POST, 'list_id_new');
	$list_top_product_add = verifyArrayArgument($_POST, 'list_id_top');

    $list_new_product_add_print = '';
    if ($list_new_product_add) {
        $list_new_product_add = str_replace(' ', '', $list_new_product_add);
        $list_new_product_add_print = str_replace(',', ', ', $list_new_product_add);
        $select_new_product = getProductsFromList($db, $select_new_product, $list_new_product_add);
    }

    $list_top_product_add_print = '';
	if ($list_top_product_add) {
		$list_top_product_add = str_replace(' ', '', $list_top_product_add);
		$list_top_product_add_print = str_replace(',', ', ', $list_top_product_add);
		$select_top_product = getProductsFromList($db, $select_top_product, $list_top_product_add);
	}

	$file_template = fopen('template/template.html','w');

	clear_img_folder(); // Очистка папки image от предыдущих изображений

	$greyLine = '
		<table class="row acym__wysid__row__element" bgcolor="#343538" style="background-color: rgb(52, 53, 56); z-index: 100; padding: 0px;" cellpadding="0">
			<tbody bgcolor="" style="background-color: inherit;">
			<tr>
			<th class="small-12 medium-12 large-12 columns acym__wysid__row__element__th" valign="top" style="height: auto;">
			<table class="acym__wysid__column" border="0" cellpadding="0" cellspacing="0" style="min-height: 0px; display: table; height: auto;">
				<tbody class="ui-sortable" style="min-height: 0px; display: table-row-group;">
				<tr class="acym__wysid__column__element ui-draggable" style="position: relative; top: inherit; left: inherit; right: inherit; bottom: inherit; height: auto;">
				<td class="large-12 acym__wysid__column__element__td" style="outline-width: 0px;">
				<div class="acym__wysid__tinymce--image mce-content-body" style="position: relative;" spellcheck="false" id="mce_38" contenteditable="false">
				<p style="font-family: Helvetica; font-size: 12px; font-weight: normal; font-style: normal; color: #7b7769; word-break: break-word;" data-mce-style="font-family: Helvetica; font-size: 12px; font-weight: normal; font-style: normal; color: #7b7769; word-break: break-word;">
				<img class="acym__wysid__media__inserted" style="max-width: 100%; height: auto; box-sizing: border-box; padding: 0px 5px; display: block; margin-left: auto; margin-right: auto; float: right; width: 580px;" src="images/middle.jpg" alt="middle.jpg" data-mce-src="https://www.mirtelcom.ru/media/com_acym/templates/images/middle.jpg" data-mce-style="max-width: 100%; height: auto; box-sizing: border-box; padding: 0 5px; display: block; margin-left: auto; margin-right: auto;" height="28" width="580">
			</p></div></td></tr></tbody></table>
		</th></tr></tbody></table>
		';
	$greyLine = '';
	
	$blueLine = '
		<table class="row acym__wysid__row__element" bgcolor="#3eade2" style="background-color: rgb(62, 173, 226); z-index: 100; padding: 0px; position: relative; top: 0px; left: 0px;" cellpadding="0">
			<tbody bgcolor="" style="background-color: inherit;">
			<tr>
			<th class="small-12 medium-12 large-12 columns acym__wysid__row__element__th" valign="top" style="height: auto;">
			<table class="acym__wysid__column" border="0" cellpadding="0" cellspacing="0" style="min-height: 0px; display: table; height: auto;">
				<tbody class="ui-sortable" style="min-height: 0px; display: table-row-group;">
				<tr class="acym__wysid__column__element ui-draggable" style="position: relative; top: inherit; left: inherit; right: inherit; bottom: inherit; height: auto;">
				<td class="large-12 acym__wysid__column__element__td" style="outline-width: 0px; height: 10px;">
				<span class="acy-editor__space" style="display: block; padding: 0px; margin: 0px; height: 100%;">
			</span></td></tr></tbody></table>
		</th></tr></tbody></table>
		';

	$blueLineProductsBlock = '
		<table class="row acym__wysid__row__element" bgcolor="#3eade2" style="background-color: rgb(62, 173, 226); z-index: 100; padding: 0px; position: relative; top: 0px; left: 0px; margin-top: 15px;" cellpadding="0">
			<tbody bgcolor="" style="background-color: inherit;">
			<tr>
			<th class="small-12 medium-12 large-12 columns acym__wysid__row__element__th" valign="top" style="height: auto;">
			<table class="acym__wysid__column" border="0" cellpadding="0" cellspacing="0" style="min-height: 0px; display: table; height: auto;">
				<tbody class="ui-sortable" style="min-height: 0px; display: table-row-group;">
				<tr class="acym__wysid__column__element ui-draggable" style="position: relative; top: inherit; left: inherit; right: inherit; bottom: inherit; height: auto;">
				<td class="large-12 acym__wysid__column__element__td" style="outline-width: 0px; height: 10px;">
				<span class="acy-editor__space" style="display: block; padding: 0px; margin: 0px; height: 100%;">
			</span></td></tr></tbody></table>
		</th></tr></tbody></table>
		';
	$blueLineProductsBlock = '';


	// Строка 1. До Даты
	$stroka = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta name="subject" content="Новости телекоммуникаций от '.Date("d.m.Y", time()).'" />
			<meta name="settings" content="{&quot;p&quot;:{&quot;font-family&quot;:&quot;Helvetica&quot;,&quot;font-size&quot;:&quot;12px&quot;,&quot;color&quot;:&quot;#7b7769&quot;,&quot;overridden&quot;:{&quot;font-size&quot;:true}},&quot;#acym__wysid__background-colorpicker&quot;:{&quot;background-color&quot;:&quot;#ebebeb&quot;},&quot;h1&quot;:{&quot;font-family&quot;:&quot;Helvetica&quot;,&quot;font-size&quot;:&quot;20px&quot;,&quot;font-weight&quot;:&quot;bold&quot;,&quot;color&quot;:&quot;#1267ff&quot;},&quot;h2&quot;:{&quot;font-family&quot;:&quot;Helvetica&quot;,&quot;font-size&quot;:&quot;14px&quot;,&quot;color&quot;:&quot;#1267ff&quot;,&quot;font-weight&quot;:&quot;bold&quot;},&quot;h3&quot;:{&quot;font-family&quot;:&quot;Helvetica&quot;,&quot;font-size&quot;:&quot;20px&quot;,&quot;color&quot;:&quot;#343538&quot;},&quot;h4&quot;:{&quot;font-family&quot;:&quot;Helvetica&quot;,&quot;font-size&quot;:&quot;14px&quot;,&quot;color&quot;:&quot;#f1f1f1&quot;},&quot;default&quot;:{&quot;font-family&quot;:&quot;Helvetica&quot;},&quot;a&quot;:{&quot;font-family&quot;:&quot;Helvetica&quot;},&quot;span.acym_link&quot;:{&quot;font-family&quot;:&quot;Helvetica&quot;},&quot;li&quot;:{&quot;font-family&quot;:&quot;Helvetica&quot;},&quot;h5&quot;:{&quot;font-family&quot;:&quot;Helvetica&quot;},&quot;h6&quot;:{&quot;font-family&quot;:&quot;Helvetica&quot;}}" />
			<title>Рассылка_'.Date("Ymd", time()).'</title>
			<style> 
				h4 a {color: #fff !important;}
			</style>
		</head>
		<body><div id="acym__wysid__template" class="cell">
		<table class="body"><tbody><tr><td align="center" class="center acym__wysid__template__content" valign="top" style="background-color: rgb(235, 235, 235); padding: 30px 0px 120px;"><center>
		<table align="center" border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="acym__wysid__row ui-droppable ui-sortable" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); min-height: 0px; display: table-cell;">
	';

	// ЛОГОТИП
	$stroka .= '
		<table class="row acym__wysid__row__element" bgcolor="transparent" style="background-color: transparent; z-index: 100; padding: 15px 0px;" cellpadding="0"><tbody bgcolor="" style="background-color: inherit;"><tr><th class="small-12 medium-12 large-12 columns acym__wysid__row__element__th" valign="top" style="height: auto;">
			<table class="acym__wysid__column" border="0" cellpadding="0" cellspacing="0" style="min-height: 0px; display: table; height: auto;"><tbody class="ui-sortable" style="min-height: 0px; display: table-row-group;"><tr class="acym__wysid__column__element ui-draggable" style="position: relative; top: inherit; left: inherit; right: inherit; bottom: inherit; height: auto;"><td class="large-12 acym__wysid__column__element__td" style="outline-width: 0px;"><div class="acym__wysid__tinymce--image mce-content-body" style="position: relative;" spellcheck="false" id="mce_101" contenteditable="false"><p style="text-align: center; font-family: Helvetica; font-size: 12px; font-weight: normal; font-style: normal; color: rgb(123, 119, 105); word-break: break-word;" data-mce-style="text-align: center"><img class="acym__wysid__media__inserted" style="max-width: 100%; height: 54px; box-sizing: border-box; padding: 0px 5px; display: inline-block; margin-left: auto; margin-right: auto; width: 340px;" src="images/logo-mtk.png" alt="" title="" data-mce-src="images/logo-mtk.png" data-mce-style="max-width: 100%; height: 54px; box-sizing: border-box; padding: 0px 5px; display: inline-block; margin-left: auto; margin-right: auto;" height="54" width="340"></p></div></td></tr></tbody></table></th></tr></tbody></table>
		';

	// Контакты
	$stroka .= '
		<table class="row acym__wysid__row__element" bgcolor="#343538" style="background-color: rgb(255, 255, 255); z-index: 100; padding: 0px 20px 0px 0px;" cellpadding="0" id="acym__wysid__row__element6543439">
			<style data-vertical-padding="acym__wysid__row__element6543439"> 
				@media screen and (max-width: 480px){ 
					#acym__wysid__row__element6543439 .acym__wysid__row__element__th__vertical__padding-0 {padding-bottom: 0px !important}
					#acym__wysid__row__element6543439 .acym__wysid__row__element__th__vertical__padding-1 {padding-top: 0px !important}
				} 
				a.tln-tzs {color: rgb(52, 53, 56) !important;}
			</style>
			<tbody bgcolor="" style="background-color: inherit;">
			<tr>

		<th class="small-12 medium-6 large-6 columns acym__wysid__row__element__th acym__wysid__row__element__th__vertical__padding-0" valign="top" style="height: auto;">
			<table class="acym__wysid__column" border="0" cellpadding="0" cellspacing="0" style="min-height: 0px; display: table; height: auto;">
			<tbody class="ui-sortable" style="min-height: 0px; display: table-row-group;">
			<tr class="acym__wysid__column__element ui-draggable" style="position: relative; top: inherit; left: inherit; right: inherit; bottom: inherit; height: auto;">
			<td class="large-12 acym__wysid__column__element__td" style="outline-width: 0px;">
			<div class="acym__wysid__tinymce--image mce-content-body" style="position: relative;" spellcheck="false" id="mce_118" contenteditable="false">
			<p style="font-family: Helvetica; font-size: 12px; font-weight: normal; font-style: normal; color: #7b7769; word-break: break-word;" data-mce-style="font-family: Helvetica; font-size: 12px; font-weight: normal; font-style: normal; color: #7b7769; word-break: break-word;">
			<img class="acym__wysid__media__inserted" style="max-width: 100%; height: auto; box-sizing: border-box; padding: 0px 5px; display: block; margin-left: auto; margin-right: auto; float: left; width: 270px; display: none;" src="images/bottom.jpg" alt="bottom.jpg" data-mce-src="https://www.mirtelcom.ru/images/bottom.jpg" data-mce-style="max-width: 100%; height: auto; box-sizing: border-box; padding: 0 5px; display: block; margin-left: auto; margin-right: auto;" height="27" width="270">
			</p></div></td></tr></tbody></table>
		</th>

		<th class="small-12 medium-6 large-6 columns acym__wysid__row__element__th acym__wysid__row__element__th__vertical__padding-1" valign="top" style="height: auto;">
			<table class="acym__wysid__column" border="0" cellpadding="0" cellspacing="0" style="min-height: 0px; display: table; height: auto;">
			<tbody class="ui-sortable" style="min-height: 0px; display: table-row-group;">
			<tr class="acym__wysid__column__element ui-draggable" style="position: relative; top: inherit; left: inherit; right: inherit; bottom: inherit; height: auto;">
			<td class="large-12 acym__wysid__column__element__td" style="outline-width: 0px;">
			<div class="acym__wysid__tinymce--text mce-content-body" style="position: relative;" spellcheck="false" id="mce_99" contenteditable="false">
			<h4 data-mce-style="font-family: Helvetica; font-size: 12px; font-weight: normal; font-style: normal; color: #7b7769; text-align: right;" style="font-family: Helvetica; font-size: 14px; font-weight: normal; font-style: normal; color: rgb(241, 241, 241); text-align: right;">
			<span style="font-size: 12px; color: rgb(52, 53, 56) !important" data-mce-style="font-size: 12px;">
			<a href="www.541541.ru" class="tln-tzs" data-mce-style="color: green;">www.541541.ru</a>
			 | +7(3452) 541-541
			</span></h4></div></td></tr></tbody></table>
		</th>

		</tr></tbody></table>';

	$stroka .= $greyLine;




	// ВАЖНОЕ СООБЩЕНИЕ
	if ($check_alarm_message) {
		$stroka .= '
			<table class="row acym__wysid__row__element" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); z-index: 100;" cellpadding="0">
			<tbody bgcolor="" style="background-color: inherit;">
			<tr>
			<th class="small-12 medium-12 large-12 columns acym__wysid__row__element__th" valign="top" style="height: auto;">
			<table class="acym__wysid__column" border="0" cellpadding="0" cellspacing="0" style="min-height: 0px; display: table; height: auto;">
				<tbody class="ui-sortable" style="min-height: 0px; display: table-row-group;">
				<tr class="acym__wysid__column__element ui-draggable" style="position: relative; top: inherit; left: inherit; right: inherit; bottom: inherit; height: auto;">
				<td class="large-12 acym__wysid__column__element__td" style="outline-width: 0px;">
				<div class="acym__wysid__tinymce--text mce-content-body" id="mce_54" style="position: relative; margin: 10px;" spellcheck="false" contenteditable="false">
				<h1 class="acym__wysid__tinymce--title--placeholder acym__wysid__tinymce--text--placeholder" style="font-family: Helvetica; font-size: 18px; font-weight: bold; font-style: normal; color: #1267ff; margin: 10px 0;" data-mce-style="font-family: Helvetica; font-size: 20px; font-weight: bold; font-style: normal; color: #1267ff;">
				<span style="color: rgb(123, 119, 105);" data-mce-style="color: #7b7769;">
				<span style="color: #c75461; font-size: 18px;" data-mce-style="color: #ff0000;">
		';
		$stroka .= 'Важное сообщение:';
		$stroka .= '</span></span></h1></div></td></tr>';

		$stroka .= '
			<tr class="acym__wysid__column__element ui-draggable" style="position: relative; top: inherit; left: inherit; right: inherit; bottom: inherit; height: auto;">
			<td class="large-12 acym__wysid__column__element__td" style="outline-width: 0px;">
			<div class="acym__wysid__tinymce--text mce-content-body" id="mce_55" style="position: relative; padding: 0 15px;" spellcheck="false" contenteditable="false">';
		$stroka .= $alarm_message;
		$stroka .= '</div></td></tr></tbody></table></th></tr></tbody></table>';

		$stroka .= $greyLine;
	}

	// НОВИНКИ
	if ((count($select_new_product)) and ($check_new_products)) {
		$stroka = get_block($db, $stroka, $greyLine, $blueLineProductsBlock, "Новинки", $select_new_product, 1); // 1-новинки, 2-акции, 3-топ продаж
	}


	// АКЦИИ
	if ((count($select_sale)) and ($check_sales)) {
		$stroka = get_block($db, $stroka, $greyLine, $blueLineProductsBlock, "Действующие акции", $select_sale, 2); // 1-новинки, 2-акции, 3-топ продаж
	}


	// ТОП ПРОДАЖ
	if ((count($select_top_product)) and ($check_top_products)) {
		$stroka = get_block($db, $stroka, $greyLine, $blueLineProductsBlock, "Топ продаж", $select_top_product, 3); // 1-новинки, 2-акции, 3-топ продаж
	}

	// КНОПКА ОТПИСКИ
	$stroka .= '
		<table class="row acym__wysid__row__element" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); z-index: 100;" cellpadding="0"><tbody bgcolor="" style="background-color: inherit;"><tr><th class="small-12 medium-12 large-12 columns acym__wysid__row__element__th" valign="top" style="height: auto;">
		<table class="acym__wysid__column" border="0" cellpadding="0" cellspacing="0" style="min-height: 0px; display: table; height: auto;"><tbody class="ui-sortable" style="min-height: 0px; display: table-row-group;"><tr class="acym__wysid__column__element ui-draggable" style="position: relative; top: inherit; left: inherit; right: inherit; bottom: inherit; height: auto;"><td class="large-12 acym__wysid__column__element__td" style="outline-width: 0px;"><div style="text-align: right; box-sizing: inherit;"><!--[if mso]><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word"style="width: 321; height:34;v-text-anchor:middle;" href="{unsubscribe}{/unsubscribe}" arcsize="10%" strokecolor="rgb(52, 53, 56)" strokeweight="2px" fillcolor="rgb(62, 173, 226)"><w:anchorlock/><center style="color :rgb(255, 255, 255); font-family:Arial; font-size:12px;">Не хотите получать нашу рассылку? Отписаться.</center></v:roundrect><![endif]--><!--[if !mso]> --><a class="acym__wysid__column__element__button acym__wysid__content-no-settings-style acym__wysid__column__element__button--focus" style="background-color: rgb(62, 173, 226); color: rgb(255, 255, 255); padding: 9px 21px; max-width: 100%; overflow: unset; border: 2px solid rgb(52, 53, 56); text-overflow: ellipsis; text-align: center; text-decoration: none; word-break: break-all; display: inline-block; box-shadow: none; font-family: Arial; font-size: 12px; cursor: pointer; line-height: 1; border-radius: 5px; font-weight: inherit; font-style: inherit;" href="{unsubscribe}{/unsubscribe}" target="_blank">Не хотите получать нашу рассылку? Отписаться.</a><!-- <![endif]--></div></td></tr></tbody></table></th></tr></tbody></table>';

	$stroka .= '</div></body></html>';

	fputs($file_template, $stroka);
	fclose($file_template);

	// Создаю и скачиваю архив
	$file = 'template.zip';
	createZip('template/', $file);

} // КОНЕЦ. Получение отправленных данных из формы.

?>