<?php

$repository = new Repository($db);

Class Repository {
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function getProductsWithLargeStockList()
    {
        $this->db->setQuery('
            SELECT 
                products.virtuemart_product_id as id, 
                products.published, 
                products.product_in_stock as stock, 
                products.product_sales as sales,
                products_ru_ru.product_name as name,
                IF (prices.override = 1, ROUND(prices.product_override_price), ROUND(prices.product_price)) as price
            FROM 
                wxa43_virtuemart_products as products 
                LEFT JOIN wxa43_virtuemart_products_ru_ru as products_ru_ru ON products.virtuemart_product_id = products_ru_ru.virtuemart_product_id
                LEFT JOIN (SELECT * FROM wxa43_virtuemart_product_prices GROUP BY wxa43_virtuemart_product_prices.virtuemart_product_id) as prices ON products.virtuemart_product_id = prices.virtuemart_product_id
            ORDER BY products.product_in_stock DESC;
    ');
        return $this->db->loadObjectList();
    }

    public function getCountDaysAfterLastMailing()
    {
        $query = '
            SELECT TIMESTAMPDIFF(DAY, CURRENT_TIMESTAMP, sending_date) AS DateDiff
            FROM wxa43_acym_campaign 
            ORDER BY sending_date DESC 
            LIMIT 1;
        ';
        $this->db->setQuery($query);
        return $this->db->loadResult();
    }

    public function getArrayNewProducts($start_date, $end_date) {
        $this->db->setQuery('
            SELECT 
                products.virtuemart_product_id, 
                products_ru_ru.product_name, 
                products.product_in_stock,
                prices.product_price,
                prices.override,
                prices.product_override_price,
                products.created_on,
                medias.file_url
            FROM 
                wxa43_virtuemart_products as products, 
                wxa43_virtuemart_products_ru_ru as products_ru_ru, 
                (SELECT * FROM wxa43_virtuemart_product_prices GROUP BY wxa43_virtuemart_product_prices.virtuemart_product_id) as prices,
                wxa43_virtuemart_product_medias as product_medias,
                wxa43_virtuemart_medias as medias
            WHERE 
                product_medias.ordering = 1
                AND products.virtuemart_product_id = products_ru_ru.virtuemart_product_id 
                AND products.virtuemart_product_id = product_medias.virtuemart_product_id
                AND product_medias.virtuemart_media_id = medias.virtuemart_media_id
                AND products.published = 1
                AND products.virtuemart_product_id = prices.virtuemart_product_id
                AND products.product_in_stock > 0
                AND products.created_on >= "' . $start_date . '"
                AND products.created_on <= "' . $end_date . '"
            ORDER BY products.virtuemart_product_id ASC
            LIMIT 20
        ');
        return $this->db->loadObjectList();
    }

    public function getArraySales()
    {
        $this->db->setQuery('
            SELECT 
                prices.virtuemart_product_id, 
                products_ru_ru.product_name, 
                products.product_in_stock,
                ROUND(prices.product_price) as product_price, 
                prices.override, 
                ROUND(prices.product_override_price) as product_override_price, 
                prices.product_price_publish_down,
                medias.file_url
            FROM 
                (SELECT * FROM wxa43_virtuemart_product_prices GROUP BY wxa43_virtuemart_product_prices.virtuemart_product_id) as prices, 
                wxa43_virtuemart_products as products,
                wxa43_virtuemart_products_ru_ru as products_ru_ru,
                wxa43_virtuemart_product_medias as product_medias,
                wxa43_virtuemart_medias as medias
            WHERE 
                product_medias.ordering = 1
                AND prices.product_price_publish_down > 0
                AND products.virtuemart_product_id = products_ru_ru.virtuemart_product_id
                AND prices.virtuemart_product_id = product_medias.virtuemart_product_id
                AND product_medias.virtuemart_media_id = medias.virtuemart_media_id
                AND products_ru_ru.virtuemart_product_id = prices.virtuemart_product_id 
                AND products.product_in_stock > 0  
            ORDER BY prices.product_price_publish_down ASC;
            ');
        return $this->db->loadObjectList();
    }

    public function getArrayTopProductsSumm($start_date, $end_date) {
        $this->db->setQuery('
            SELECT
                categories_ru_ru.category_name, 
                order_items.virtuemart_product_id,
                products_ru_ru.product_name,
                prices.product_price,
                prices.override,
                prices.product_override_price,
                products.product_in_stock,
                sum(product_quantity) as count,
                sum(product_subtotal_with_tax) as summ, 
                COUNT(order_items.virtuemart_product_id) as cnt_orders,
                tln_medias.ordering,
                tln_medias.file_url
            FROM
                wxa43_virtuemart_orders as orders,
                wxa43_virtuemart_order_items as order_items,
                wxa43_virtuemart_product_categories as product_categories,
                wxa43_virtuemart_categories as categories,
                wxa43_virtuemart_categories_ru_ru as categories_ru_ru,
                wxa43_virtuemart_products as products,
                wxa43_virtuemart_products_ru_ru as products_ru_ru,
                (SELECT * FROM wxa43_virtuemart_product_prices GROUP BY wxa43_virtuemart_product_prices.virtuemart_product_id) as prices,
                (
                    SELECT virtuemart_product_id, ordering, file_url
                    FROM wxa43_virtuemart_product_medias, wxa43_virtuemart_medias 
                    WHERE wxa43_virtuemart_product_medias.virtuemart_media_id = wxa43_virtuemart_medias.virtuemart_media_id and ordering = 1
                ) as tln_medias
            WHERE
                order_items.virtuemart_order_id = orders.virtuemart_order_id
                AND order_items.virtuemart_product_id = products.virtuemart_product_id
                AND products_ru_ru.virtuemart_product_id = products.virtuemart_product_id
                AND products.virtuemart_product_id = tln_medias.virtuemart_product_id
                AND products.virtuemart_product_id = prices.virtuemart_product_id
                AND products.virtuemart_product_id = product_categories.virtuemart_product_id
                AND product_categories.virtuemart_category_id = categories.virtuemart_category_id
                AND categories.virtuemart_category_id = categories_ru_ru.virtuemart_category_id
                AND orders.created_on >= "' . $start_date . '"
                AND orders.created_on <= "' . $end_date . '"
                AND (orders.order_status = "S" or orders.order_status = "F")
                AND products.product_in_stock > 0  
            GROUP BY order_items.virtuemart_product_id 
            ORDER BY summ DESC
            LIMIT 10
        ');
        return $this->db->loadObjectList();
    }
}

?>