<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);

define('ROOT_FOLDER', "m6-mailing-generation/"); // если нужно убрать, то укажи пустое значение - ""
require_once "functions.php";

$end_date = date("Y-m-d", time() + 24 * 60 * 60);
$start_date = date("Y-m-d", time() - 60 * 60 * 24 * 30);
$cntCheckedNew = $cntCheckedSale = $cntCheckedTop = 0;

// ----------------------------------- Получение POST данных. Форма 1 ----------------------------------- //

/* Получение POST данных */
if($_SERVER["REQUEST_METHOD"] == "POST") {
	if (!empty($_POST['start_date'])) {
		$start_date = $_POST['start_date'];
	}
	if (!empty($_POST['end_date'])) {
		$end_date = $_POST['end_date'];
	}
}

require_once "create-template.php";
require_once "repository.php";
?>

<html>
<head>
<title>cmsMTK. E-Mail рассылка</title>
<link rel="stylesheet" type="text/css" href="styles.css">

<?php 
	require_once CONST_MENU;
	require_once "forms.php";
	require_once "scripts.php";
    require_once "statTable.php";
?>
</body>
</html>

