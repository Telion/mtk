<?php

// ------------------------------ ФУНКЦИИ ------------------------------ //


// ФУНКЦИЯ. Создать архив
function createZip($source, $destination){
	unlink($destination);

	if (!extension_loaded('zip') || !file_exists($source)) {
		return false;
	}
 
	$zip = new ZipArchive();
	if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
		return false;
	}
 
	$source = str_replace('\\', '/', realpath($source));
 
	if (is_dir($source) === true) {
		$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
 
		foreach ($files as $file) {
			$file = str_replace('\\', '/', $file);
 
			// Ignore "." and ".." folders
			if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
				continue;
 
			$file = realpath($file);
			$file = str_replace('\\', '/', $file);
				 
			if (is_dir($file) === true) {
				$zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
			} else if (is_file($file) === true) {
				$zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
			}
		}
	} else if (is_file($source) === true) {
		$zip->addFromString(basename($source), file_get_contents($source));
	}
	return $zip->close();
}



// ФУНКЦИЯ. Формирую ИНФО БЛОК
function get_block($db, $stroka, $greyLine, $blueLine, $header, $array, $type){
	$stroka .= '
	<table class="row acym__wysid__row__element" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); z-index: 100; padding-left: 20px; padding-right: 20px; position: relative; top: 0px; left: 0px; padding-bottom: 10px;" cellpadding="0"><tbody bgcolor="" style="background-color: inherit;"><tr><th class="small-12 medium-12 large-12 columns acym__wysid__row__element__th" valign="top" style="height: auto;">
		<table class="acym__wysid__column" border="0" cellpadding="0" cellspacing="0" style="min-height: 0px; display: table; height: auto;"><tbody class="ui-sortable" style="min-height: 0px; display: table-row-group;"><tr class="acym__wysid__column__element ui-draggable" style="position: relative; top: inherit; left: inherit; right: inherit; bottom: inherit; height: auto;"><td class="large-12 acym__wysid__column__element__td" style="outline-width: 0px; height: 20px;"><span class="acy-editor__space acy-editor__space--focus" style="display: block; padding: 0px; margin: 0px; height: 100%;"></span></td></tr><tr class="acym__wysid__column__element ui-draggable" style="right: inherit; bottom: inherit; height: auto; width: 100px; z-index: 100; position: relative; top: inherit; left: inherit;"><td class="large-12 acym__wysid__column__element__td" style="outline-width: 0px;"><div class="acym__wysid__tinymce--text mce-content-body" style="position: relative;" spellcheck="false" id="mce_32" contenteditable="false"><h2 data-mce-style="font-family: Helvetica; font-size: 14px; font-weight: bold; font-style: normal; color: #1267ff;" style="font-family: Helvetica; font-size: 14px; font-weight: bold; font-style: normal; color: rgb(18, 103, 255);" class="acym__wysid__tinymce--text--placeholder"><span data-mce-style="color: #3eade2; font-size: 18px;" style="color: #39718D; font-size: 18px;">
	';
	$stroka .= $header;
	$stroka .= '</span></h2></div></td></tr></tbody></table></th></tr></tbody></table>
	<table class="row acym__wysid__row__element" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); z-index: 100; position: relative; top: 0px; left: 0px; padding-top: 10px;" cellpadding="0" id="acym__wysid__row__element5177812"><tbody bgcolor="" style="background-color: inherit;"><tr>
	';

	$i = 0;

	foreach($array as $product) {
		$arr_product = explode(' /// ', $product);
		$productId = $arr_product[0];
		$productName = $arr_product[1];
		$itemid = getItemid($db, $productId);
		//$itemid = '492';

		$stroka .= '<th class="small-12 medium-3 large-3 columns acym__wysid__row__element__th acym__wysid__row__element__th__vertical__padding-0" valign="top" style="height: auto; padding: 0 20px;">
			<table class="acym__wysid__column" border="0" cellpadding="0" cellspacing="0" style="min-height: 0px; display: table; height: auto;"><tbody class="ui-sortable" style="min-height: 0px; display: table-row-group;">';

		// КАРТИНКА
		$stroka .= '
			<tr class="acym__wysid__column__element ui-draggable" style="position: relative; inset: inherit; height: auto; display: table-row;">
			<td class="large-12 acym__wysid__column__element__td" style="outline-width: 0px;">
			<div class="acym__wysid__tinymce--image mce-content-body" style="position: relative;" spellcheck="false" id="mce_59" contenteditable="false">
			<p style="text-align: center; font-family: Helvetica; font-size: 12px; font-weight: normal; font-style: normal; color: rgb(123, 119, 105); word-break: break-word;" data-mce-style="text-align: center">';

		$stroka .= '<a href="https://www.mirtelcom.ru/';
		$url = 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $productId . '&Itemid=' . $itemid;
		$stroka .= $url.'" style="color: #696969; font-size: 1em; font-weight: 700;">';

		$stroka .= '<img class="acym__wysid__media__inserted" style="max-width: 100%; height: auto; box-sizing: border-box; padding: 0px 5px; display: inline-block; margin-left: auto; margin-right: auto; width: 140px;" src="';
		$file_img_name = explode('/', $arr_product[5])[4];
		$file_img = '../../images/stories/virtuemart/product/'.$file_img_name;
		$new_file_img = 'template/images/'.$productId.'.jpg';
		$file_img_path = 'images/'.$productId.'.jpg';
		$stroka .= $file_img_path;
		$stroka .= '" alt="" title="" data-mce-src="';
		$stroka .= $file_img_path;
		if (copy($file_img, $new_file_img)) {
			echo "";
		} else {
			echo "";
		}   
		$stroka .= '" data-mce-style="max-width: 100%; height: auto; box-sizing: border-box; padding: 0 5px; display: inline-block; margin-left: auto; margin-right: auto;" height="140" width="140"></a>';
		$stroka .= '</p></div></td></tr>';

		// КАРТИНКА -------------------------------------------------------------------------


		// НАЗВАНИЕ
		$stroka .= '
			<tr class="acym__wysid__column__element ui-draggable" style="position: relative; top: inherit; left: inherit; right: inherit; bottom: inherit; height: auto; height: 72px;">
			<td class="large-12 acym__wysid__column__element__td" style="outline-width: 0px;">
			<div class="acym__wysid__tinymce--text mce-content-body" style="position: relative; overflow: hidden; height: 3.7em; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical;" spellcheck="false" id="mce_33" contenteditable="false">
			<p class="acym__wysid__tinymce--text--placeholder" style="font-family: Helvetica; font-size: 12px; font-weight: normal; font-style: normal; color: rgb(123, 119, 105); word-break: break-word; text-align: center;" data-mce-style="font-family: Helvetica; font-size: 12px; font-weight: normal; font-style: normal; color: #7b7769; word-break: break-word; text-align: center;">
			<span style="font-size: 12px;" data-mce-style="font-size: 12px;"><strong>';

		$stroka .= '<a href="https://www.mirtelcom.ru/';
		$url = 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $productId . '&Itemid=' . $itemid;
		$stroka .= $url.'">'; 
		$stroka .= '<span class="tln-product-name" style="color: #696969 !important; font-size: 1em; font-weight: 700;">'.$productName
.'</span></a>';
		$stroka .= '</strong></span></p></div></td></tr>';
		// НАЗВАНИЕ --------------------------------------------------------------------------


		// ЦЕНА
		if ($arr_product[3] == 0) { // Одна цена
			$stroka .= '
			<tr class="acym__wysid__column__element ui-draggable" style="position: relative; inset: inherit; height: auto; display: table-row;">
			<td class="large-12 acym__wysid__column__element__td" style="outline-width: 0px;">
			<div class="acym__wysid__tinymce--text mce-content-body" style="position: relative;" spellcheck="false" id="mce_34" contenteditable="false">
			<p class="acym__wysid__tinymce--text--placeholder" style="font-family: Helvetica; font-size: 12px; font-weight: normal; font-style: normal; color: rgb(123, 119, 105); word-break: break-word; text-align: center; line-height: 200%;" data-mce-style="font-family: Helvetica; font-size: 12px; font-weight: normal; font-style: normal; color: #7b7769; word-break: break-word; text-align: center;">
			<span style="line-height: 200%; color: #c75461; font-family: Helvetica; font-size: 14px; font-weight: 700 !important;" data-mce-style="line-height: 200%; color: #c75461; font-family: Helvetica; font-size: 15px; font-weight: 700 !important;">';

			$price = round($arr_product[2])." руб.";
			$stroka .= $price; 

		} else { // Две цены
			$stroka .= '
			<tr class="acym__wysid__column__element_mr_css_attr ui-draggable_mr_css_attr" style="vertical-align: top;text-align: left;right: inherit;bottom: inherit;height: auto;">
			<td class="large-12_mr_css_attr acym__wysid__column__element__td_mr_css_attr" style="word-wrap: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;vertical-align: top;text-align: left;color: #0a0a0a;font-family: Helvetica,Arial,sans-serif;font-weight: 400;margin: 0;line-height: 1.3;font-size: 16px;width: 100%;outline-width: 0px;border-collapse: collapse;">
			<div class="acym__wysid__tinymce--text_mr_css_attr mce-content-body_mr_css_attr" spellcheck="false" id="mce_22_mr_css_attr" contenteditable="false" style="height: 100%;position: relative;">
			<p class="acym__wysid__tinymce--text--placeholder_mr_css_attr" data-mce-style="font-family: Helvetica;font-size: 12px;font-weight: normal;font-style: normal;color: #7b7769;word-break: break-word;text-align: center;" style="color: rgb(123, 119, 105);font-family: Helvetica;font-weight: normal;margin: 0;text-align: center;line-height: 1.3;font-size: 12px;margin-bottom: 0;font-style: normal;word-break: break-word;position: absolute;left: 0;top: -9px;width: -webkit-fill-available; width: -moz-available;">
			<span data-mce-style="text-decoration: line-through;" style="color: #bbb; text-decoration: line-through; text-align: center;" class="MsoNormal_mr_css_attr">';

			$price = round($arr_product[2])." руб.";
			$stroka .= $price;
			$stroka .= '</span></p>
				<p class="acym__wysid__tinymce--text--placeholder_mr_css_attr" data-mce-style="font-family: Helvetica;font-size: 12px;font-weight: normal;font-style: normal;color: #c75461;word-break: break-word;text-align: center;" style="color: #c75461;font-family: Helvetica;font-weight: normal;margin: 0;text-align: center;line-height: 1.3;font-size: 12px;margin-bottom: 0;font-style: normal;word-break: break-word;padding-top: 5px;">
				<span data-mce-style="font-size: 14px;color: #808000;" style="color: #c75461;font-size: 14px;" class="MsoNormal_mr_css_attr"><strong style="font-weight: 700;">';

			$price_sale = round($arr_product[4])." руб.";
			$stroka .= $price_sale;
		}
		$stroka .= '</span><br></p></div></td></tr>';

		// ЦЕНА ------------------------------------------------------------------------------
		$stroka .= '</tbody></table></th>';
		$i++;


		// НОВАЯ СТРОКА С ТОВАРОМ
		if (($i%4 == 0) and ($i != count($array))) {
			$stroka .= '</tr></tbody></table><br>
			<table class="row acym__wysid__row__element" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); z-index: 100; position: relative; top: 0px; left: 0px; padding-top: 40px;" cellpadding="0" id="acym__wysid__row__element9995525"><tbody bgcolor="" style="background-color: inherit;"><tr>';

		}
	}

	while ($i%4 != 0) {
		$stroka .= '<th style="width: 128px;">&nbsp;</th>';
		$i++;
	}

	$stroka .= '</tr></tbody></table>';
	if ($type != 3) 

	$stroka .= $greyLine;
	$stroka .= $blueLine;

	return $stroka;
}


// ФУНКЦИЯ. Удаление старых картинок
function clear_img_folder() {
	$arr_need_img = ['bottom.jpg', 'logo-mtk.png', 'middle.jpg', 'poweredby_black.png', 'top.jpg'];
	$files = scandir('template/images/');

	foreach ($files as $file) {
		if ($file === '.' or $file === '..') {
			continue;
		}
		if (!in_array($file, $arr_need_img)) {
			unlink('template/images/' . $file);
		}
	}
}

// ФУНКЦИЯ. Добавление товаров, записанных вручную в текстовое поле
function getProductsFromList($db, $select_product, $list_product_add) {
	$arr_list_product_add = explode(',', $list_product_add);
	$arr_list_product_add = array_unique($arr_list_product_add);
	$temp = $arr_list_product_add[0];

	for ($i=1; $i<count($arr_list_product_add); $i++) {
		if ($arr_list_product_add[$i]) {
			$temp .= ', '.$arr_list_product_add[$i];
		}
	}

	$query = '
		SELECT 
		    prices.virtuemart_product_id, 
		    wxa43_virtuemart_products_ru_ru.product_name, 
		    prices.product_price, 
		    prices.override, 
		    prices.product_override_price, 
		    prices.product_price_publish_down,
		    wxa43_virtuemart_product_medias.ordering,
		    wxa43_virtuemart_medias.file_url
		FROM 
		    (SELECT * FROM wxa43_virtuemart_product_prices GROUP BY wxa43_virtuemart_product_prices.virtuemart_product_id) as prices, 
		    wxa43_virtuemart_products_ru_ru,
		    wxa43_virtuemart_product_medias,
		    wxa43_virtuemart_medias
		WHERE
		    prices.virtuemart_product_id IN('.$temp.') AND 
		    prices.virtuemart_product_id = wxa43_virtuemart_product_medias.virtuemart_product_id AND
		    wxa43_virtuemart_product_medias.virtuemart_media_id = wxa43_virtuemart_medias.virtuemart_media_id AND
		    wxa43_virtuemart_products_ru_ru.virtuemart_product_id = prices.virtuemart_product_id AND
	    	wxa43_virtuemart_product_medias.ordering = 1
	';

	$db->setQuery($query);
	$arr_list_products = $db->loadObjectList('virtuemart_product_id');

	$new_arr = [];
	for ($i = 0; $i < count($arr_list_product_add); $i++) {
		if ($arr_list_product_add[$i] != "") {
			$new_arr[$i] = $arr_list_products[$arr_list_product_add[$i]];
		}
	}

	foreach($new_arr as $product){
		$select_product[] = $product->virtuemart_product_id." /// ".$product->product_name." /// ".$product->product_price." /// ".$product->override." /// ".$product->product_override_price." /// ".$product->file_url;
	}
	return $select_product;

}


function getItemid($db, $productId) {
    $query = '
        SELECT menu.id
        FROM wxa43_virtuemart_products AS product 
	        LEFT JOIN wxa43_virtuemart_product_categories AS category ON product.virtuemart_product_id = category.virtuemart_product_id
	        LEFT JOIN wxa43_menu AS menu ON link LIKE CONCAT("%virtuemart_category_id=", category.virtuemart_category_id, "%")
    AND menu.published = 1
        WHERE product.virtuemart_product_id = ' . $productId;
	$db->setQuery($query);
	return $db->loadResult();
}

?>

