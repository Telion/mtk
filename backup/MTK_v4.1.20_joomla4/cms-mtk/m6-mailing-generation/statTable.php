<?php

require_once "repository.php";

$tableHeader = '<table class="zebra-table"><tr><th>id</th><th>Название</th><th>Цена</th><th>Остаток (сорт)</th></tr>';
$tableFooter = '</table>';

$productsWithLargeStockList = $repository->getProductsWithLargeStockList();

echo $tableHeader;
foreach ($productsWithLargeStockList as $product) {
    echo '
        <tr>
            <td>' . $product->id . '</td>
            <td class="stat-product-name">' . $product->name . '</td>
            <td class="stat-td-number">' . $product->price . '</td>
            <td class="stat-td-number">' . $product->stock . '</td>
        </tr>';
}
$tableFooter;

?>