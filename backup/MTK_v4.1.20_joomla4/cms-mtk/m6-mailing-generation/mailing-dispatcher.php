<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
require_once "repository.php";
visitsLog(__FILE__);

printr("<hr>" . "Скрипт 6. Сколько дней прошло с предыдущей рассылки?");

$countDaysAfterLastMailing = $repository->getCountDaysAfterLastMailing();
printr("Последнее письмо было отправлено " . $countDaysAfterLastMailing . " дн. назад");
message_to_telegram("<b>Рассылка</b>. С предыдущей рассылки прошло <b>" . $countDaysAfterLastMailing . " дн.</b>", $recipient);

printr("скрипт 6 отработал" . "<hr>");



?>