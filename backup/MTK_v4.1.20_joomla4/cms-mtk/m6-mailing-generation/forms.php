<div id="main-block" style="position: relative;">

	                                                        <!-- ПЕРИОД -->

<form action='mailing.php' method='post' name='dateform' id="generationformdate" >
	<div id="date-block" class="lbl block1">


		<div class="date-form">

		<?php if ($_SERVER["REQUEST_METHOD"] == "POST") { ?>
			<div class="lbl-message"><div class="lbl-message-span">

			<?php

            $generationActive = verifyArrayArgument($_POST, 'generationActive');
            if ($generationActive == 1) {
				echo "Шаблон загружен";
			} else {
				echo "Дата обнавлена";
			}

			$message = verifyArrayArgument($_POST, 'alarm_message');
			$check_alarm_message = verifyArrayArgument($_POST, 'check_alarm_message');
            $select_new_product = verifyArrayArgument($_POST, 'select_new_product');
            $select_sale = verifyArrayArgument($_POST, 'select_sale');
            $select_top_product = verifyArrayArgument($_POST, 'select_top_product');
		
			$new_products = [];
			foreach($select_new_product as $product) {
				$new_products[] = explode(' /// ', $product)[0];
			}
            $idsFromTextField = explode(", ", $list_new_product_add_print);
            $mergedArray = ($list_new_product_add_print != '') ? array_merge($new_products, $idsFromTextField) : $new_products;
			$cntCheckedNew = empty($select_new_product) ? 0 : count(array_unique($mergedArray));

			$sales = [];
			foreach($select_sale as $sale) {
				$sales[] = explode(' /// ', $sale)[0];
			}
			$cntCheckedSale = count($sales);

			$top_products = [];
			foreach($select_top_product as $product) {
				$top_products[] = explode(' /// ', $product)[0];
			}
            $idsFromTextField = explode(", ", $list_top_product_add_print);
            $mergedArray = ($list_top_product_add_print != '') ? array_merge($top_products, $idsFromTextField) : $top_products;
            $cntCheckedTop = empty($select_top_product) ? 0 : count(array_unique($mergedArray));

 ?>
			</div></div>
		<?php } ?>

		<div class="lbl2">
			<input type='date' name='start_date' id='form_start_date' value="<?php echo $start_date; ?>" autocomplete="off" form="generationformdate" list="dates" />
			<datalist id="dates">
				<option label="7 дней" value="<?php echo Date('Y-m-d', time()-60*60*24*7); ?>">
				<option label="15 дней" value="<?php echo Date('Y-m-d', time()-60*60*24*15); ?>">
				<option label="30 дней" value="<?php echo Date('Y-m-d', time()-60*60*24*30); ?>">
				<option label="45 дней" value="<?php echo Date('Y-m-d', time()-60*60*24*45); ?>">
				<option label="60 дней" value="<?php echo Date('Y-m-d', time()-60*60*24*60); ?>">
			</datalist>
		</div>
		<div class="lbl2">
			<input type='date' name='end_date' id='form_end_date' value="<?php echo $end_date; ?>"  autocomplete="off" form="generationformdate" />
		</div>
		<div class="lbl2">
			<input type="hidden" name="generationActive" value="2" form="generationformdate">
			<button type='submit' name="tbuttondate" class="btn-date">Обновить дату</button>
		</div>
		</div>
</form>




<form action='mailing.php' method='post' name='mainform' id="generationform" >

	                                                        <!-- ВАЖНОЕ -->

	<div class="lbl block2">
		<div class="label"><label>БЛОК. Важное сообщение</label></div>
		<div class="check"><input type='checkbox' name='check_alarm_message' id='form_check_alarm_message' autocomplete="off" form="generationform"
			<?php
				if (isset($check_alarm_message) && $check_alarm_message == 'on') {
					echo ' checked ';
				}
			?>/></div>
		<div class="clear"></div>
		<div class="section"><input type='text' name='alarm_message' id='form_alarm_message' autocomplete="off" form="generationform" 
			<?php
				if (isset($message) AND !empty($message)) {
					echo ' value="'.$message.'" ';
				}
			?>/></div>
	</div>

	                                                        <!-- НОВЫЕ ТОВАРЫ -->

	<div class="lbl block4">
		<div class="label"><label>БЛОК. Новые товары</label></div>
		<div class="check"><input type='checkbox' name='check_new_products' id='form_check_new_products' value='Yes' autocomplete="off" form="generationform" checked /></div>
		<table>
<?php 
		$i = 0;
        $arr_new_products = $repository->getArrayNewProducts($start_date, $end_date);
		foreach ($arr_new_products as $product) { 
			$generation_string = $product->virtuemart_product_id." /// ".$product->product_name." /// ".$product->product_price." /// ".$product->override." /// ".$product->product_override_price." /// ".$product->file_url;

			$color_class = "clr-first";
		        if (strtotime($product->created_on) < strtotime($start_date)) $color_class = "clr-second";

			$lblCheckText = '<label class="lbl-check-text" for="form_select_new_product'.$product->virtuemart_product_id.'">';
			$lblCheckImg = '<label class="lbl-check-img" for="form_select_new_product'.$product->virtuemart_product_id.'">';
?>
			<tr class="<?= $color_class ?>" id="line-new-<?= $product->virtuemart_product_id ?>">
				<td><?=$product->virtuemart_product_id?></td>
				<td><?php echo $lblCheckImg; ?><img class="img-preview" src="<?php echo "/../".$product->file_url; ?>"></label></td>
				<td style="max-width: 600px;"><?php echo $lblCheckText.$product->product_name; ?></label></td>
				<td><?php echo $lblCheckText.date("d.m.Y", strtotime($product->created_on)); ?></label></td>

				<?php
					$checked = '';
					if (isset($new_products) AND in_array($product->virtuemart_product_id, $new_products)) {
						$checked = ' checked ';
					}
				?>

				<td><input type='checkbox' class='chk-input-new' name='select_new_product[]' style="background: #D6ECF3;" id='form_select_new_product<?php echo $product->virtuemart_product_id ;?>' value='<?php echo $generation_string; ?>' onclick="counter('new', <?= $product->virtuemart_product_id ?>);" autocomplete="off" form="generationform" <?=$checked?> /></td>
			</tr>
<?php 
			$i++;
		} 
?>
		</table>
        <?php
        $list_new_product_add_print = isset($list_new_product_add_print) ? $list_new_product_add_print : '';
        ?>
		<div class="listID-new"><input type="text" class="list-id" name="list_id_new" id="list_id_new_form" autocomplete="off" form="generationform" value="<?php echo $list_new_product_add_print; ?>" placeholder="Введите список id дополнительных товаров (напр: 1024, 450, 820, 99)" onkeypress="counter('new');"></div>
	</div>





	                                                        <!-- АКЦИИ -->

	<div class="lbl block3">
		<div class="label"><label>БЛОК. Действующие акции</label></div>
		<div class="check"><input type='checkbox' name='check_sales' id='form_check_sales' value='Yes' autocomplete="off" form="generationform" checked /></div>
		<table style="width: 100%;">
<?php 
		$i = 0;
        $arr_sales = $repository->getArraySales();
		foreach ($arr_sales as $sale) { 
			$generation_string = $sale->virtuemart_product_id." /// ".$sale->product_name." /// ".$sale->product_price." /// ".$sale->override." /// ".$sale->product_override_price." /// ".$sale->file_url;
			$lblCheckText = '<label class="lbl-check-text" for="form_select_sale'.$sale->virtuemart_product_id.'">';
			$lblCheckImg = '<label class="lbl-check-img" for="form_select_sale'.$sale->virtuemart_product_id.'">';
?>
			<tr id="line-sale-<?= $sale->virtuemart_product_id ?>">    
				<td><?=$sale->virtuemart_product_id?></td>
				<td><?php echo $lblCheckImg; ?><img class="img-preview" src="<?php echo "/../".$sale->file_url; ?>"></label></td>
				<td style="max-width: 600px;"><?php echo $lblCheckText.$sale->product_name; ?></label></td>
				<td><?php echo $lblCheckText."до ".date("d.m.Y", strtotime($sale->product_price_publish_down)); ?></label></td>

				<?php
					$checked = '';
					if (isset($sales) AND in_array($sale->virtuemart_product_id, $sales)) {
						$checked = ' checked ';
					}

				?>

				<td><input type='checkbox' class='chk-input-sale' name='select_sale[]' style="background: #FFEBD4;" id='form_select_sale<?php echo $sale->virtuemart_product_id ;?>' value='<?php echo $generation_string; ?>' onclick="counter('sale', <?= $sale->virtuemart_product_id ?>);" autocomplete="off" form="generationform" <?=$checked?> /></td>
			</tr>
<?php 
			$i++;
		} 
?>
		</table>
	</div>



	                                                        <!-- ТОП ПРОДАЖ -->


	<div class="lbl block5">
		<div class="label"><label>БЛОК. Топ продаж</label></div>
		<div class="check"><input type='checkbox' name='check_top_products' id='form_check_top_products' value='Yes' autocomplete="off" form="generationform" checked /></div>
		<table style="width: 100%;">
<?php 
		$i = 0;
        $arr_top_products_summ = $repository->getArrayTopProductsSumm($start_date, $end_date);
		foreach ($arr_top_products_summ as $product) { 
			$generation_string = $product->virtuemart_product_id." /// ".$product->product_name." /// ".$product->product_price." /// ".$product->override." /// ".$product->product_override_price." /// ".$product->file_url;

			$lblCheckText = '<label class="lbl-check-text" for="form_select_top_product'.$product->virtuemart_product_id.'">';
			$lblCheckImg = '<label class="lbl-check-img" for="form_select_top_product'.$product->virtuemart_product_id.'">';
?>
			<tr id="line-top-<?= $product->virtuemart_product_id ?>">
				<td><?=$product->virtuemart_product_id?></td>
				<td><?php echo $lblCheckImg; ?><img class="img-preview" src="<?php echo "/../".$product->file_url; ?>"></label></td>
				<td style="max-width: 600px;"><?php echo $lblCheckText.$product->product_name; ?></label></td>
				<td><?php echo $lblCheckText.round($product->summ)." руб"; ?></label></td>

				<?php
					$checked = '';
					if (isset($top_products) AND in_array($product->virtuemart_product_id, $top_products)) {
						$checked = ' checked ';
					}
				?>

				<td><input type='checkbox' class='chk-input-top' name='select_top_product[]' style="background: #C8F7C8;" id='form_select_top_product<?php echo $product->virtuemart_product_id; ?>' value='<?php echo $generation_string; ?>' onclick="counter('top', <?= $product->virtuemart_product_id ?>);" autocomplete="off" form="generationform" <?=$checked?> /></td>
			</tr>
<?php
			$i++;
		}

?>              </table>
        <?php
        $list_top_product_add_print = isset($list_top_product_add_print) ? $list_top_product_add_print : '';

        ?>
		<div class="listID-top"><input type="text" class="list-id" name="list_id_top" id="list_id_top_form" autocomplete="off" form="generationform" value="<?php echo $list_top_product_add_print; ?>" placeholder="Введите список id дополнительных товаров (напр: 1024, 450, 820, 99)" onkeypress="counter('top');"></div>
	</div>


	<?php require_once "message-block.php"; ?>
</form>
</div>