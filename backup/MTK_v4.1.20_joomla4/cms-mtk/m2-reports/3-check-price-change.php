<?php


require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);

// Все опубликованные товары (ID и цена)
$query_string = '
	SELECT
		wxa43_virtuemart_products.virtuemart_product_id, 
	        wxa43_virtuemart_products_ru_ru.product_name,
		CASE 
			WHEN prices.product_override_price > 0 THEN prices.product_override_price 
			ELSE prices.product_price 
		END price 
	FROM 
 		wxa43_virtuemart_products, 
 		(SELECT virtuemart_product_id, product_override_price, MAX(product_price) as product_price FROM wxa43_virtuemart_product_prices GROUP BY virtuemart_product_id) as prices, 
 		wxa43_virtuemart_products_ru_ru
	WHERE 
		published = 1 AND
		prices.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id AND
	        wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id
';
$db->setQuery($query_string);
$arr_all_products_with_price = $db->loadObjectList('virtuemart_product_id');

// Получение POST данных
if($_SERVER["REQUEST_METHOD"] == "POST") {
	if (isset($_POST['old_price'])) {
        $isAvailibilityProductInTable = checkAvailibilityProductInTable($db, $_POST['insert_query']);
		if ($_POST['old_price'] == 0 && !$isAvailibilityProductInTable) {
			$query = 'INSERT INTO wxa43_virtuemart_product_customfields (virtuemart_product_id, virtuemart_custom_id, customfield_value) VALUES ("'.$_POST['insert_query'].'", "54", "'.floor($arr_all_products_with_price[$_POST['insert_query']]->price).'")';
			$db->setQuery($query);
			$db->execute();
		} else {
			$query = 'UPDATE wxa43_virtuemart_product_customfields SET customfield_value=' . floor($arr_all_products_with_price[$_POST['insert_query']]->price) . ' WHERE virtuemart_product_id=' . $_POST['insert_query'] . ' AND virtuemart_custom_id=54';
			$db->setQuery($query);
			$db->execute();
		}
        header("Location: https://www.mirtelcom.ru/cms-mtk/m2-reports/3-check-price-change.php");
    }

	if (!empty($_POST['product_name'])){
		$query_string = '
			SELECT * FROM (
				SELECT 
					wxa43_virtuemart_products.virtuemart_product_id, 
					wxa43_virtuemart_products_ru_ru.product_name, 
					CASE
						WHEN wxa43_virtuemart_product_prices.override = 1 THEN wxa43_virtuemart_product_prices.product_override_price
						ELSE wxa43_virtuemart_product_prices.product_price
					END new_price,
					0 as old_price
				FROM
					wxa43_virtuemart_products,
					wxa43_virtuemart_product_prices,
					wxa43_virtuemart_products_ru_ru
				WHERE
					published = 1 AND
					wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_product_prices.virtuemart_product_id AND
					wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id AND    
					wxa43_virtuemart_products.virtuemart_product_id NOT IN (
						SELECT virtuemart_product_id
						FROM wxa43_virtuemart_product_customfields
						WHERE virtuemart_custom_id = 54
					)
				UNION
				SELECT
					wxa43_virtuemart_product_prices.virtuemart_product_id,
					wxa43_virtuemart_products_ru_ru.product_name,
					CASE
						WHEN wxa43_virtuemart_product_prices.override = 1 THEN wxa43_virtuemart_product_prices.product_override_price
						ELSE wxa43_virtuemart_product_prices.product_price
					END new_price,
					wxa43_virtuemart_product_customfields.customfield_value as old_price
				FROM
					wxa43_virtuemart_product_customfields,
					wxa43_virtuemart_product_prices,
					wxa43_virtuemart_products_ru_ru
				WHERE
					wxa43_virtuemart_product_prices.virtuemart_product_id = wxa43_virtuemart_product_customfields.virtuemart_product_id AND
					wxa43_virtuemart_product_customfields.virtuemart_custom_id = 54 AND
					wxa43_virtuemart_product_prices.product_override_price != wxa43_virtuemart_product_customfields.customfield_value AND
					wxa43_virtuemart_product_prices.product_price != wxa43_virtuemart_product_customfields.customfield_value AND
					wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_product_prices.virtuemart_product_id
			) as table_main
			WHERE product_name LIKE "%'.$_POST['product_name'].'%"';
		$db->setQuery($query_string);
		$search_result = $db->loadObjectList('virtuemart_product_id');
	}
}

   
// Все товары, у которых в таблице Настраиваемых полей есть запись "Последняя цена"
$query_string = 'SELECT virtuemart_product_id FROM wxa43_virtuemart_product_customfields WHERE virtuemart_custom_id = 54';
$db->setQuery($query_string);
$arr_products_with_custom_id = $db->loadColumn();

// Товары, у которых старая и новая цена отличаются
$query_string = '
	SELECT
		prices.virtuemart_product_id,
	        wxa43_virtuemart_products_ru_ru.product_name,
		CASE 
			WHEN prices.product_override_price > 0 THEN prices.product_override_price 
			ELSE prices.product_price 
		END new_price,
		wxa43_virtuemart_product_customfields.customfield_value as old_price
	FROM 
		wxa43_virtuemart_product_customfields, 
		(SELECT virtuemart_product_id, product_override_price, MAX(product_price) as product_price FROM wxa43_virtuemart_product_prices GROUP BY virtuemart_product_id) as prices,
		wxa43_virtuemart_products_ru_ru
	WHERE 
		prices.virtuemart_product_id = wxa43_virtuemart_product_customfields.virtuemart_product_id AND
		wxa43_virtuemart_product_customfields.virtuemart_custom_id = 54 AND
		prices.product_override_price != wxa43_virtuemart_product_customfields.customfield_value AND
		prices.product_price != wxa43_virtuemart_product_customfields.customfield_value AND
	        wxa43_virtuemart_products_ru_ru.virtuemart_product_id = prices.virtuemart_product_id;
';
$db->setQuery($query_string);
$arr_products_with_dif_price = $db->loadObjectList('virtuemart_product_id');

$db->setQuery('
	SELECT
		COUNT(wxa43_virtuemart_products.virtuemart_product_id)
	FROM 
		wxa43_virtuemart_products
	WHERE 
		published = 1 AND
		wxa43_virtuemart_products.virtuemart_product_id NOT IN (
			SELECT virtuemart_product_id 
			FROM wxa43_virtuemart_product_customfields 
			WHERE virtuemart_custom_id = 54
		)
');
$count_products_with_new_price = $db->loadResult();

?>

<html>
<head>
	<title>cmsMTK. Сбор данных о ценах</title>
	<style>
		.table-header{display: inline-flex;}
		.zebra-table{margin: 25px auto 50px;}
		.new-product th {background: firebrick;}
		.old-product th {background: seagreen;}
		caption {padding: 10px 20px; font-weight: 700; font-size: large; text-align: left; color: #555;}
		td.id {text-align: center;}
		td.price {text-align: right; padding: 7px 20px;}
		form {margin: 2px;}
		.search-block {background: #add8e685; padding: 20px; border-radius: 9px; border: 2px solid #4e4e4e;}
	</style>
	<script>
		function request_form () {
		    return confirm("Подтвердите действие");
		}
	</script>

<?php
	require_once CONST_MENU;
?>

	<div style="width: fit-content; margin: 0 auto;">
	<div style="font-size: 24px; font-weight: 700; padding: 14px; padding-left: 70px; margin-top: 30px;">Изменения цен: </div>

<div class="search-block">

	<form id="search_form" method="post" class='search-form' name='search_form' id="search_form_id">
		<input type="text" name="product_name" autocomplete="off" placeholder="Название">
		<input type="submit" id="button_search" name="action" value="Искать">
		<input type="submit" id="button_clean" name="clean" value="Очистить">
	</form>

<?php
if (!empty($search_result)) { ?>

<div class="search-product">
<table class="zebra-table">
	<caption>Результат поиска</caption>
	<tr><th>ID</th><th>Название товара</th><th>Текущая цена</th><th>Старая цена</th><th></th></tr>
	<?php
	foreach($search_result as $key => $product) {
		$product_name = '<a class="related" href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$product->virtuemart_product_id.'" target="_blank">'.$product->product_name.'</a>';

		echo "
		<tr>
			<td class='id'>".$key."</td>
			<td width=900>".$product_name."</td>
			<td class='price'>".floor($product->new_price)."</td>
			<td class='price'>".floor($product->old_price)."</td>
			<td>
				<form action='3-check-price-change.php' method='post' name='form_ins' id='form_insert".$key."' onsubmit='return request_form();'>
					<input type='hidden' name='insert_query' value='".$key."' form='form_insert".$key."'>
					<input type='hidden' name='old_price' value='".floor($product->old_price)."' form='form_insert".$key."'>
					<button type='submit' form='form_insert".$key."'>x</button></form>
			</td></tr>";
	}
	?>
</table>
</div>
<?php } ?>
</div>


<?php if($count_products_with_new_price > 0) { ?>

<div class="new-product">
<table class="zebra-table">
	<caption>Новые товары</caption>
	<tr><th>ID</th><th>Название товара</th><th>Текущая цена</th><th></th></tr>
	<?php
	foreach($arr_all_products_with_price as $key => $product) {
		if (!in_array($key, $arr_products_with_custom_id)) {
			$product_name = '<a class="related" href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$product->virtuemart_product_id.'" target="_blank">'.$product->product_name.'</a>';

			echo "<tr><td class='id'>".$key."</td><td width=900>".$product_name."</td><td class='price'>".floor($product->price)."</td><td>
			<form action='3-check-price-change.php' method='post' name='form_ins' id='form_insert".$key."' onsubmit='return request_form();'>
				<input type='hidden' name='insert_query' value='".$key."' form='form_insert".$key."'>
				<input type='hidden' name='old_price' value='0' form='form_insert".$key."'>
				";


                if (isset($product->old_price)) {
                    echo "<input type='hidden' name='old_price' value='" . floor($product->old_price) . "' form='form_insert" . $key . "'>";
                }

				echo "<button type='submit' form='form_insert".$key."'>x</button></form>
			</td></tr>";
		}
	}
	?>
</table>
</div>

<?php } ?>

<?php if(count($arr_products_with_dif_price) > 0) { ?>

<div class="old-product">
<table class="zebra-table">
	<caption>Обновление цены</caption>
	<tr><th>ID</th><th>Название товара</th><th>Старая цена</th><th>Текущая цена</th><th></th></tr>
	<?php
	foreach($arr_products_with_dif_price as $key => $product) {
		$product_name = '<a class="related" href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$product->virtuemart_product_id.'" target="_blank">'.$product->product_name.'</a>';

		echo "<tr>
            <td class='id'>".$key."</td>
            <td width=900>".$product_name."</td>
            <td class='price' style='color: red;'>".floor($product->old_price)."</td>
            <td class='price' style='color: green;'>".floor($product->new_price)."</td>
            <td>
                <form action='3-check-price-change.php' method='post' name='form_upd' id='form_update".$key."' onsubmit='return request_form();'>
                    <input type='hidden' name='insert_query' value='".$key."' form='form_update".$key."'>
                    <input type='hidden' name='old_price' value='".floor($product->old_price)."' form='form_update".$key."'>
                    <button type='submit' form='form_update".$key."'>x</button></form>
                </td></tr>";
	}
	?>
</table>
</div>

<?php }

function checkAvailibilityProductInTable($db, $productId) {
    $query = 'SELECT * FROM wxa43_virtuemart_product_customfields WHERE virtuemart_product_id = ' . $productId . ' AND virtuemart_custom_id = 54';

    $db->setQuery($query);
    $count = count($db->loadAssocList('virtuemart_product_id'));
    if ($count > 0) {
        return true;
    }
    return false;
}


?>


