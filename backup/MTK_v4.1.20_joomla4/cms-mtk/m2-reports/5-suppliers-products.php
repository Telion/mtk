<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);

$cat_id = $_GET['cat_id'];
$cat_name = $_GET['cat_name'];
?>

<html>
<head>
	<title>cmsMTK. Список товаров в категории "<?php echo $cat_name ?>", без поставщика</title>

<?php
	require_once CONST_MENU;
?>
<div style="width: fit-content; margin: 0 auto;">
<div style="font-size: 20px; font-weight: 700; padding: 14px; padding-left: 110px;"><?php echo $cat_name; ?> <span style="color:grey">без поставщика:</span></div>

<?php
// Формирую список
$db->setQuery('
	SELECT 
		wxa43_virtuemart_categories_ru_ru.virtuemart_category_id, 
		wxa43_virtuemart_categories_ru_ru.category_name, 
		wxa43_virtuemart_products_ru_ru.virtuemart_product_id, 
		wxa43_virtuemart_products_ru_ru.product_name, 
		CASE 
			WHEN prices.product_override_price > 0 THEN prices.product_override_price 
			ELSE prices.product_price 
		END price 
	FROM 
	    wxa43_virtuemart_products_ru_ru, 
	    wxa43_virtuemart_categories_ru_ru, 
	    wxa43_virtuemart_product_categories, 
	    wxa43_virtuemart_products,
	    (SELECT * FROM wxa43_virtuemart_product_prices GROUP BY wxa43_virtuemart_product_prices.virtuemart_product_id) as prices
	WHERE
    	wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND
	    wxa43_virtuemart_products.published = 1 AND
		wxa43_virtuemart_categories_ru_ru.virtuemart_category_id = '.$cat_id.' AND 
		wxa43_virtuemart_categories_ru_ru.virtuemart_category_id = wxa43_virtuemart_product_categories.virtuemart_category_id AND 
		wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_product_categories.virtuemart_product_id AND 
		prices.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND 
		wxa43_virtuemart_products_ru_ru.virtuemart_product_id NOT IN (
			SELECT wxa43_virtuemart_product_customfields.virtuemart_product_id 
			FROM wxa43_virtuemart_product_customfields 
			WHERE wxa43_virtuemart_product_customfields.virtuemart_custom_id = 48 
			GROUP BY wxa43_virtuemart_product_customfields.virtuemart_product_id) 
	ORDER BY price ASC');
$arr_products = $db->loadObjectList();

?>

	<table class='zebra-table suppliers'>
	<tr><th>Код товара</th><th>Название товара</th><th>Цена</th></tr>

<?php
foreach ($arr_products as $product) {
	$pole1 = $product->virtuemart_product_id;
	$pole2 = '<a class="suppliers" href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$product->virtuemart_product_id.'" target="_blank">'.$product->product_name.'</a>';
	$pole3 = round($product->price)." руб";
	echo "\t".'<tr><td style="padding-right: 20px; text-align: right;">'.$pole1.'</td><td>'.$pole2.'</td><td style="text-align: right;">'.$pole3.'</td></tr>'."\r\n";

}
?>
	</table>
</div>