<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
visitsLog(__FILE__);

// Получение POST данных
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $query_string = '
		SELECT 
			wxa43_virtuemart_products.virtuemart_product_id as id,
			product_name as name
		FROM
			wxa43_virtuemart_products,
			wxa43_virtuemart_products_ru_ru
		WHERE 
			wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND
			wxa43_virtuemart_products.virtuemart_product_id = "' . $_POST['product_id'] . '"
		';
    echo $query_string;
    $db->setQuery($query_string);
    $product = $db->loadObjectList('');
    $product = isset($product[0]) ? $product[0] : null;
    if (isset($_POST['product_id'])) {
        $product_does_not_exist = $_POST['product_id'];
    }
}

?>

<html>
<head>
    <title>cmsMTK. Поиск товара по ID</title>
    <style>
        form#form_search_id {
            padding: 15px 15px;
            background: lightblue;
            width: fit-content;
            margin-left: 32px;
            border-radius: 5px;
        }

        .input-form input, .input-form button {
            font-size: 14px;
            padding: 5px;
            text-align: right;
        }

        .input-form input {
            padding-right: 10px;
            width: 120px;
            margin-right: 7px;
        }

        .right {
            text-align: right;
            padding-right: 20px;
        }

        label {
            position: absolute;
            top: 16px;
            border-right: solid 1px #d4d4d4;
            padding: 5px 7px 8px 9px;
            background: #eee;
        }

        .lbl-id {
            left: 16px;
        }

        .input-form button {
            padding: 5px 10px;
        }

    </style>


    <?php require_once CONST_MENU; ?>
    <?php
    $productId = '';
    if (isset($_POST['product_id'])) {
        $productId = $_POST['product_id'];
    }
    ?>
    <div style="width: fit-content; margin: 0 auto;">
        <div style="font-size: 24px; font-weight: 700; padding: 14px; margin-top: 30px;">Поиск товара по ID:</div>
        <form action='10-product-search-by-id.php' method='post' name='form_search' id="form_search_id" class='input-form' style="position: relative;">
            <label class="lbl-id">id:</label>
            <input type="text" name="product_id" autocomplete="off" form="form_search_id" value="<?= $productId; ?>">
            <button type="submit" form="form_search_id">Найти</button>
        </form>
    </div>
    <div style="width: fit-content; margin: 0 auto;">

        <?php
        if (isset($product)) {
            ?>
            <h3 style='background: white; padding: 26px 39px; border-radius: 21px; border: 2px solid; color: #5a5a5a; margin-top: 50px;'>
                <a style="color: inherit;"
                   href='https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id=<?= $product->id ?>'
                   target='_blank'>
                    <?= $product->id . ": " . $product->name ?>
                </a>
            </h3>
            <?php
        } else {
            if (isset($product_does_not_exist)) {
                ?>
                <h3 style='background: white; padding: 26px 39px; border-radius: 21px; border: 2px solid; color: red; margin-top: 50px;'>
                    <span style="color: #5a5a5a;">Товара с id </span><?= $product_does_not_exist ?> <span style="color: #5a5a5a;">не существует</span></h3>
                <?php
            }
        }
        ?>
    </div>

