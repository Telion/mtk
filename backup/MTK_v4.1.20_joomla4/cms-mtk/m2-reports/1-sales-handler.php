<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
visitsLog(__FILE__);

// Проверка акционных товаров, отправка сообщения в ТЕЛЕГРАММ АЛИЕ
$db = JFactory::getDBO();
$db->setQuery('
	SELECT wxa43_virtuemart_products_ru_ru.product_name, wxa43_virtuemart_product_prices.product_price, wxa43_virtuemart_product_prices.product_price_publish_down 
	FROM wxa43_virtuemart_product_prices, wxa43_virtuemart_products_ru_ru 
	WHERE product_price_publish_down > 0 and wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_product_prices.virtuemart_product_id 
	ORDER BY wxa43_virtuemart_product_prices.product_price_publish_down ASC
	');
$arr_products = $db->loadObjectList();

$message_text = "";
$send_message = 0;
$count_alarm_products = 0;
$telegramMessage = "<b>Акции на товары в порядке</b>";
foreach ($arr_products as $product) {

    $days = floor((strtotime($product->product_price_publish_down) - time()) / (60 * 60 * 24));
    if ($days < 4) {
        $message_text .= $product->product_name . " (" . floor($product->product_price) . " руб) - <b>" . $days . " дн. до окончания</b>\r\n\r\n";
        $send_message = 1;
        $count_alarm_products++;
    }
}

if ($send_message == 1) {
    $title = 'МТК: Скрипт 2-1. Акционные товары (' . $count_alarm_products . ')';
    $message = "Акционные товары:<br>" . $message_text;
    $telegramMessage = "<b>Акционные товары:</b>\r\n\r\n" . $message_text;
    echo $message;
//	sendEmail($title, str_replace("\r\n", "<br>", $message."<br><br> Отправил копию в телеграмм Алии"), EMAIL_TELION);
}

message_to_telegram($telegramMessage, $recipient);

printr("скрипт 2-1 отработал");

?>