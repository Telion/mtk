<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);

$db->setQuery('
	SELECT
   	    wxa43_virtuemart_product_customfields.virtuemart_product_id,
            wxa43_virtuemart_product_customfields.virtuemart_customfield_id,
	    wxa43_virtuemart_product_customfields.customfield_value,
	    wxa43_virtuemart_products_ru_ru.product_name,
	    wxa43_virtuemart_products.published,
	    wxa43_virtuemart_products.product_in_stock,
	    wxa43_virtuemart_product_categories.virtuemart_category_id 
	FROM 
	    wxa43_virtuemart_product_customfields,
	    wxa43_virtuemart_products,
	    wxa43_virtuemart_products_ru_ru,
	    wxa43_virtuemart_product_categories
	WHERE 
	    wxa43_virtuemart_product_customfields.virtuemart_custom_id = 1 AND
	    wxa43_virtuemart_product_customfields.customfield_value = wxa43_virtuemart_products.virtuemart_product_id AND
	    wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND
	    wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_product_categories.virtuemart_product_id AND
	    (wxa43_virtuemart_products.published = 0 OR wxa43_virtuemart_products.product_in_stock <= 0)  
	ORDER BY `wxa43_virtuemart_product_customfields`.`customfield_value` ASC
');
$arr_related_products = $db->loadObjectList();

$db->setQuery('
	SELECT 
	    wxa43_virtuemart_products_ru_ru.virtuemart_product_id,
	    wxa43_virtuemart_products_ru_ru.product_name, 
	    wxa43_virtuemart_product_categories.virtuemart_category_id,
	    wxa43_virtuemart_products.published,
	    wxa43_virtuemart_products.product_in_stock
	FROM 
	    wxa43_virtuemart_products_ru_ru, 
	    wxa43_virtuemart_product_categories,
	    wxa43_virtuemart_products
	WHERE
	    wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_product_categories.virtuemart_product_id AND
	    wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND
	    wxa43_virtuemart_products.published = 1 AND
	    wxa43_virtuemart_products.product_in_stock > 0
');
$arr_products = $db->loadObjectList();

echo "<h2>Перебор всех товаров и их сопутствующих</h2><hr>";

$count = 0;
$count2 = 0;
$previos = 0;
$arr_no_replacement = [];

foreach($arr_related_products as $related_product) {
	if ($related_product->customfield_value != $previos){
	    echo "<h4>Подбор похожих :</h4>";
	    $previos = $related_product->customfield_value;
	    $count2++;

	    // Принудительная подмена источника замены
//	    if ($related_product->customfield_value == 1208) {
//		$related_product->customfield_value = 2224;
//		$related_product->product_name = 'Сумка монтажника для инструментов';
//	    }

		// Подбор похожих товаров
		$str_ish = $related_product->product_name;
		$arr_products_result = [];

		for ($i = 0; $i < count($arr_products); $i++){
			$arr_products[$i]->similar = similar_text($str_ish, $arr_products[$i]->product_name, $arr_products[$i]->similar_perc); // !!!
	
			$soundex1_4_simbol = substr(soundex(translit($str_ish)), 3, 1);					// !!!
			$soundex2_4_simbol = substr(soundex(translit($arr_products[$i]->product_name)), 3, 1);		// !!!
			$arr_products[$i]->soundex_4_symbol_diff = abs($soundex1_4_simbol - $soundex2_4_simbol);	// !!!
			if (
			   ($related_product->virtuemart_category_id == $arr_products[$i]->virtuemart_category_id) && (
			      (($str_ish != $arr_products[$i]->product_name) & ($arr_products[$i]->similar_perc > 76)) || 
			      (($str_ish != $arr_products[$i]->product_name) & ($arr_products[$i]->similar_perc > 48) & ($arr_products[$i]->similar > 18) & ($arr_products[$i]->soundex_4_symbol_diff < 1))     //perc не меньше 48
			   )) {               
				$arr_products[$i]->soundex_res = 0;
				$arr_products_result[] = $arr_products[$i];
			} else $arr_products[$i]->soundex_res = 1;

			$arr_products[$i]->result_soundex = $arr_products[$i]->soundex_res;;
			$arr_products[$i]->result_levenshtein = levenshtein($str_ish, $arr_products[$i]->product_name, 100, 100, 100);
			if ($str_ish === $arr_products[$i]->product_name) continue;
		}

		$result_soundex = array_column($arr_products_result, 'result_soundex');
		$result_levenshtein = array_column($arr_products_result, 'result_levenshtein');
		array_multisort($result_soundex, SORT_ASC, $result_levenshtein, SORT_ASC, $arr_products_result);

	}

	if (!$arr_products_result){
		echo '<a class="related" style="color: red;" href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$related_product->virtuemart_product_id.'" target="_blank">'."cID: ".$related_product->virtuemart_customfield_id." // ID: ".$related_product->customfield_value." // Name: ".$related_product->product_name." -->> Замена не найдена</a><br>";
		$arr_no_replacement[] = $related_product->virtuemart_customfield_id;
		continue;
	}

	$random = rand(0, count($arr_products_result)-1);
	echo '<a class="related" href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$related_product->virtuemart_product_id.'" target="_blank">'."cID: ".$related_product->virtuemart_customfield_id." // ID: ".$related_product->customfield_value." // ".$related_product->product_name." -->> ".$arr_products_result[$random]->virtuemart_product_id." // ".$arr_products_result[$random]->product_name.'</a>'."<br>";

	$db_change = JFactory::getDBO();
	$db_change->setQuery('UPDATE wxa43_virtuemart_product_customfields SET customfield_value = "'.$arr_products_result[$random]->virtuemart_product_id.'" WHERE virtuemart_customfield_id = '.$related_product->virtuemart_customfield_id);

			/* Telion */
			if (version_compare(JVERSION, '3','lt')) {
				$db_change->query();
			} else {
				$db_change->execute();
			}

	$count++;

//	if ($count == 1) break;
}
echo "<hr><h3>Всего сопутствующих под замену: ".$count."</h3>";
echo "<h3>Разных сопутствующих: ".$count2."</h3>";
?>