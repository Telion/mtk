<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);

$cat_id = $_GET['cat_id'];
$cat_name = $_GET['cat_name'];

$db->setQuery('
SELECT 
    wxa43_virtuemart_categories_ru_ru.virtuemart_category_id, 
    wxa43_virtuemart_categories_ru_ru.category_name, 
    t_cnt.virtuemart_product_id, 
    wxa43_virtuemart_products_ru_ru.product_name, 
    t_cnt.cnt, 		
    CASE 
        WHEN wxa43_virtuemart_product_prices.product_override_price > 0 THEN ROUND(wxa43_virtuemart_product_prices.product_override_price) 
	ELSE ROUND(wxa43_virtuemart_product_prices.product_price) 
    END price
FROM (
    SELECT 
        wxa43_virtuemart_product_customfields.virtuemart_product_id,
        COUNT(*) as cnt
    FROM wxa43_virtuemart_product_customfields, wxa43_virtuemart_products
    WHERE 
        wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_product_customfields.customfield_value AND
        wxa43_virtuemart_products.published = 1 AND
        wxa43_virtuemart_products.product_in_stock > 0 AND
        wxa43_virtuemart_product_customfields.virtuemart_custom_id = 1
    GROUP BY wxa43_virtuemart_product_customfields.virtuemart_product_id ) as t_cnt,
    wxa43_virtuemart_products_ru_ru,
    wxa43_virtuemart_categories_ru_ru,
    wxa43_virtuemart_product_categories,
    wxa43_virtuemart_product_prices
WHERE
    t_cnt.cnt <> 8 AND
    t_cnt.cnt <> 12 AND
    t_cnt.cnt < 12 AND
    wxa43_virtuemart_categories_ru_ru.virtuemart_category_id = '.$cat_id.' AND 
    wxa43_virtuemart_products_ru_ru.virtuemart_product_id = t_cnt.virtuemart_product_id AND
    wxa43_virtuemart_product_prices.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND
    wxa43_virtuemart_categories_ru_ru.virtuemart_category_id = wxa43_virtuemart_product_categories.virtuemart_category_id AND
    wxa43_virtuemart_product_categories.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id  
ORDER BY price
');
$arr_products = $db->loadObjectList();

?>

<html>
<head>
	<title>cmsMTK. Список товаров в категории "<?php echo $cat_name ?>"</title>

<?php
	require_once CONST_MENU;
?>
<div style="width: fit-content; margin: 0 auto; position: relative;">
<div style="font-size: 20px; font-weight: 700; padding: 14px; padding-left: 110px;"><?php echo $cat_name ?>, <span style="color:grey">не 8, 12 и более сопутствующих:</span></div>
<a href="7-related-products-update.php" target="_blank"><div class='run-button' style='top: 8px; right: -329px;'>Попробовать добавить</div></a>

	<table class='zebra-table related'>
	<tr><th>Код товара</th><th>Название товара</th><th>Кол-во<br>сопутсвующих</th><th>Цена</th></tr>

<?php
foreach ($arr_products as $product) {
	$pole1 = $product->virtuemart_product_id;
	$pole2 = '<a class="related" href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$product->virtuemart_product_id.'" target="_blank">'.$product->product_name.'</a>';
	$pole3 = $product->price." руб";
	$pole2a = $product->cnt;
	echo "\t".'<tr><td style="padding-right: 20px; text-align: right;">'.$pole1.'</td><td>'.$pole2.'</td><td style="text-align: center">'.$pole2a.'</td><td style="text-align: right;">'.$pole3.'</td></tr>'."\r\n";

}
?>
	</table>
</div>