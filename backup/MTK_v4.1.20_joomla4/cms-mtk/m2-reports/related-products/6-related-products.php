<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);

$cat_id = $_GET['cat_id'];
$cat_name = $_GET['cat_name'];

$db->setQuery('
	SELECT 
		wxa43_virtuemart_categories_ru_ru.virtuemart_category_id, 
		wxa43_virtuemart_categories_ru_ru.category_name, 
		wxa43_virtuemart_products_ru_ru.virtuemart_product_id, 
		wxa43_virtuemart_products_ru_ru.product_name, 
		CASE 
			WHEN wxa43_virtuemart_product_prices.product_override_price > 0 THEN wxa43_virtuemart_product_prices.product_override_price 
			ELSE wxa43_virtuemart_product_prices.product_price 
		END price 
	FROM wxa43_virtuemart_products_ru_ru, wxa43_virtuemart_categories_ru_ru, wxa43_virtuemart_product_categories, wxa43_virtuemart_product_prices, wxa43_virtuemart_products 
	WHERE
    		wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND
	    	wxa43_virtuemart_products.published = 1 AND
		wxa43_virtuemart_categories_ru_ru.virtuemart_category_id = '.$cat_id.' AND 
		wxa43_virtuemart_categories_ru_ru.virtuemart_category_id = wxa43_virtuemart_product_categories.virtuemart_category_id AND 
		wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_product_categories.virtuemart_product_id AND 
		wxa43_virtuemart_product_prices.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND 
		wxa43_virtuemart_products_ru_ru.virtuemart_product_id NOT IN (
			SELECT wxa43_virtuemart_product_customfields.virtuemart_product_id 
			FROM wxa43_virtuemart_product_customfields 
			WHERE wxa43_virtuemart_product_customfields.virtuemart_custom_id = 1 
			GROUP BY wxa43_virtuemart_product_customfields.virtuemart_product_id) 
	ORDER BY price ASC');
$arr_products = $db->loadObjectList();
?>

<html>
<head>
	<title>cmsMTK. Список товаров в категории "<?php echo $cat_name ?>", без сопутствующих</title>

<?php
	require_once CONST_MENU;
?>

<div style="width: fit-content; margin: 0 auto; position: relative;">
<div style="font-size: 20px; font-weight: 700; padding: 14px; padding-left: 110px;"><?php echo $cat_name ?>, <span style="color:grey">без сопутствующих:</span></div>
<a href="6-related-products-insert.php" target="_blank"><div class='run-button' style='top: 8px; right: -329px;'>Попробовать добавить</div></a>


	<table class='zebra-table related'>
	<tr><th>Код товара</th><th>Название товара</th><th>Цена</th></tr>

<?php
foreach ($arr_products as $product) {
	$pole1 = $product->virtuemart_product_id;
	$pole2 = '<a class="related" href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$product->virtuemart_product_id.'" target="_blank">'.$product->product_name.'</a>';
	$pole3 = round($product->price)." руб";
	echo "\t".'<tr><td style="padding-right: 20px; text-align: right;">'.$pole1.'</td><td>'.$pole2.'</td><td style="text-align: right;">'.$pole3.'</td></tr>'."\r\n";

}
?>
	</table>
</div>