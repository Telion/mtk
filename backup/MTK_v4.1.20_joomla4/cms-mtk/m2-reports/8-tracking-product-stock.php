<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);

// Получение POST данных
$update_query = $productId = $productLimit = '';
if($_SERVER["REQUEST_METHOD"] == "POST"){
	if (isset($_POST['update_query'])) {
		$db->setQuery('UPDATE wxa43_virtuemart_products SET low_stock_notification = 0 WHERE virtuemart_product_id=' . $_POST['update_query']);
		$db->execute();
	}
	if (isset($_POST['productId'])) {
		$productId = $_POST['productId'];
		$productLimit = $_POST['productLimit'];
		$db->setQuery('UPDATE wxa43_virtuemart_products SET low_stock_notification = ' . $productLimit . ' WHERE virtuemart_product_id = ' . $productId);
		$db->execute();
	}
}

// Товары, за остатками которых следим
$query_string = '
	SELECT 
		wxa43_virtuemart_products.virtuemart_product_id,
		product_name,
		product_in_stock,
		low_stock_notification
    
	FROM
		wxa43_virtuemart_products,
		wxa43_virtuemart_products_ru_ru
	WHERE 
		wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND
		low_stock_notification > 0
	';
$db->setQuery($query_string);
$arr_products = $db->loadObjectList('virtuemart_product_id');


?>

<html lang="ru">
<head>
	<title>cmsMTK. Отслеживаемые остатки</title>
	<style>
		form#form_insert_id {
		    padding: 15px 15px;
		    background: lightblue;
		    width: fit-content;
		    margin-left: 32px;
		    border-radius: 5px;
		}
		.input-form input, .input-form button {
		    font-size: 14px;
		    padding: 5px;
		    text-align: right;
		}
		.input-form input {
		    padding-right: 10px;
		    width: 120px;
		    margin-right: 7px;
		}
		.right {
		    text-align: right;
		    padding-right: 20px;
		}
		label {position: absolute; top: 16px; border-right: solid 1px #d4d4d4; padding: 5px 7px 8px 9px; background: #eee;}
		.lbl-min {left: 150px;}
		.lbl-id {left: 16px;}
		.input-form button {padding: 5px 10px;}
	</style>


<?php
	require_once CONST_MENU;
?>

	<div style="width: fit-content; margin: 0 auto;">
	<div style="font-size: 24px; font-weight: 700; padding: 14px; padding-left: 70px; margin-top: 30px;">Минимальные остатки: </div>

	<form action='8-tracking-product-stock.php' method='post' name='form_insert' id="form_insert_id" class='input-form' style="position: relative;">
		<label class="lbl-id">id:</label>
		<input type="text" name="productId" autocomplete="off" form="form_insert_id">
		<label class="lbl-min">мин:</label>
		<input type="text" name="productLimit" autocomplete="off" form="form_insert_id">
		<button type="submit" form="form_insert_id">Сохранить</button>
	</form>

<div class="new-product">
<table id="mySuperTBL" class="zebra-table">
	<tr><th>ID</th><th>Название товара</th><th>Текущий остаток</th><th>Минимальный<br>остаток</th><th></th></tr>
	<?php
	foreach($arr_products as $key => $product) {
		$product_name = '<a class="related" href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$product->virtuemart_product_id.'" target="_blank">'.$product->product_name.'</a>';
		$product_name = $product->product_name;
		$class = ($product->product_in_stock < $product->low_stock_notification) ? 'red' : 'black';

		echo "<tr>

			<td class='id' style='padding: 0; text-align: right;'>
				<a href='https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id=".$key."' target='_blank'>
				<div style='padding: 7px 5px 7px 14px;'>
				<span style='padding-right: 15px;'>
					".$key."
				</span></div></a></td>
			<td>".$product_name."</td>
			<td class='right ".$class."'>".$product->product_in_stock."</td>
			<td class='right ".$class."'>".$product->low_stock_notification."</td>
			<td>
				<form action='8-tracking-product-stock.php' method='post' name='form_delete' id='form_delete_id".$key."' class='form-delete' onsubmit='return request_form();'>
					<input type='hidden' name='update_query' value='".$key."' form='form_delete_id".$key."'>
					<button type='submit' form='form_delete_id".$key."'>x</button></form>
			</td></tr>";
	}
	?>
</table>
</div>
	<script>
		onload = function () {document.getElementById ('mySuperTBL').onclick = mySuperFunc;}

		function request_form() {
		    return confirm("Подтвердите действие");
		}

		function data_to_form() {
		    document.form_insert.productId.value = input[0].textContent;
		}

		function getParentTag(node,tag) { //Найти ближайшего родителя по tagName. Здесь мы движемся вверх, пока не встретим родителя, у которого тег = нашему заданному tag
			if (node) {return (node.tagName == tag) ? node : getParentTag(node.parentElement,tag);}
			return null;
		}

		function mySuperFunc(ev){
		    // Заносим данные из таблицы в форму
		    var e = ev || window.event, tag = (e.target).closest('tr') || e.srcElement;
		    input = (e.target).closest('tr').getElementsByTagName('td');

		    // Вставить данные из таблицы в форму
		    document.form_insert.productId.value = input[0].textContent.replace(/\D+/g,"");
		    document.form_insert.productLimit.value = input[3].textContent;

		    // Выделение строки
		    var row = getParentTag(e.target,'TR');
		    if (!row) {return;}
		    var tbl = this,  idx = tbl.getAttribute('activeRowIndex');
		    if (idx) {
			tbl.rows[idx].classList.remove('activeRow');
		    }
		    row.classList.add('activeRow');
		    tbl.setAttribute('activeRowIndex', row.rowIndex);
		    }


	</script>