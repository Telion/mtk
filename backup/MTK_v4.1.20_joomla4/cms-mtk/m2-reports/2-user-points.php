<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);

/* Получение POST данных */
if ($_SERVER["REQUEST_METHOD"] == "POST") {

	// Нажата кнопка "списать"
	if (isset($_POST['spending_points'])){
	   $spending_points = ceil($_POST['spending_points']);
  	   $user_id = $_POST['user_id'];
        
	   $db->setQuery('
		INSERT INTO wxa43_virtuemart_decrease_points (points_decrease_id, virtuemart_user_id, decrease_points, decrease_date)
		VALUES ("", "'.$user_id.'", "'.$spending_points.'", "'.date("Y-m-d H:i:s", time()).'")');
	}

	// Нажата кнопка "удалить"
	if (isset($_POST['spending_points_cancel'])){
	    $spending_points_cancel = $_POST['spending_points_cancel'];

	    $db->setQuery('DELETE FROM wxa43_virtuemart_decrease_points WHERE points_decrease_id='.$spending_points_cancel);
	}

	$db->execute();

	/* Для сброса POST параметров, чтобы не отправлялись повторно по F5 */
	/* Вставлять после обработки полученных данных (например после внесения данных в базу) */
	header("Location: 2_user_points.php");
}

$db->setQuery('
	SELECT
	    wxa43_virtuemart_decrease_points.points_decrease_id,
	    wxa43_virtuemart_decrease_points.virtuemart_user_id,
	    wxa43_users.name,
	    wxa43_users.email,
	    wxa43_virtuemart_decrease_points.decrease_date,
	    wxa43_virtuemart_decrease_points.decrease_points
	FROM 
	    wxa43_virtuemart_decrease_points, 
	    wxa43_users
	WHERE
	    wxa43_users.id = wxa43_virtuemart_decrease_points.virtuemart_user_id
	');
	$arr_decrease_points = $db->loadObjectList();

$db->setQuery('
	SELECT
	    wxa43_virtuemart_decrease_points.virtuemart_user_id,
	    sum(wxa43_virtuemart_decrease_points.decrease_points) as sum_decrease_points
	FROM 
	    wxa43_virtuemart_decrease_points
	GROUP BY wxa43_virtuemart_decrease_points.virtuemart_user_id
	');
	$arr_sum_decrease_points = $db->loadObjectList(virtuemart_user_id);

$db->setQuery('
	SELECT
            wxa43_virtuemart_orders.virtuemart_user_id,
            wxa43_users.name,
            wxa43_users.email,
            SUM(CASE WHEN wxa43_virtuemart_order_items_points.points_status = 1 THEN wxa43_virtuemart_order_items_points.increase_points ELSE 0 END) as active_points,
            SUM(CASE WHEN wxa43_virtuemart_order_items_points.points_status = 0 THEN wxa43_virtuemart_order_items_points.increase_points ELSE 0 END) as unactive_points
	FROM 
	    wxa43_virtuemart_orders,
	    wxa43_virtuemart_order_items,
	    wxa43_virtuemart_order_items_points,
            wxa43_users
	WHERE
	    wxa43_virtuemart_order_items.virtuemart_order_id = wxa43_virtuemart_orders.virtuemart_order_id AND
	    wxa43_virtuemart_order_items_points.virtuemart_order_item_id = wxa43_virtuemart_order_items.virtuemart_order_item_id AND
            wxa43_users.id = wxa43_virtuemart_orders.virtuemart_user_id
        GROUP BY 
            wxa43_virtuemart_orders.virtuemart_user_id
	');
	$arr_user_points = $db->loadObjectList(virtuemart_user_id);

$db->setQuery('
	SELECT 
	    wxa43_users.id,
            wxa43_virtuemart_vmuser_shoppergroups.virtuemart_shoppergroup_id
	FROM 
	    wxa43_virtuemart_vmuser_shoppergroups,
            wxa43_users
	WHERE 
	    wxa43_users.id = wxa43_virtuemart_vmuser_shoppergroups.virtuemart_user_id AND
	    wxa43_virtuemart_vmuser_shoppergroups.virtuemart_shoppergroup_id = 7
	');
	$arr_users_connected_pointsystem = $db->loadObjectList(id);

?>
<html>
<head>
	<title>cmsMTK. Система учета бонусов</title>

	<script type="text/javascript">
		function validate_form (max_value, user_id)
		{
		    control_value = document.getElementById('spending_points_id_'+user_id).spending_points.value;
		    valid = true;

	            if ((control_value > max_value ) || (control_value <= 0) || (isNaN(control_value)))
        	    {
		        alert ( "Проверьте введенные данные" );
        	        valid = false;
	            }
 	            return valid;
	        }

		function points_click (user_id, value)
		{
		    document.getElementById('spending_points_id_'+user_id).spending_points.value = value;
		}

		function request_form ()
		{
		    return confirm("Подтвердите удаление записи о списании бонусов");
		}

	</script>

	<style> .tln-points-circle {display: block; background: black; border-radius: 50%; height: 21px; width: 21px; margin: 0 0 0 12px;} </style>

<?php
	require CONST_MENU;
?>
	<div style="width: fit-content; margin: 0 auto;">
	<div style="font-size: 20px; font-weight: 700; padding: 14px; padding-left: 110px; margin-top: 30px;">Список пользователей, имеющих накопленные бонусы: </div>

	<table class='zebra-table'>
	<tr><th>В группе?</th><th>ID покупателя</th><th>Имя покупателя</th><th>E-Mail покупателя</th><th>Активные<br>бонусы</th><th>Неактивные<br>бонусы</th><th>Списанные<br>бонусы</th><th>Доступные<br>бонусы</th><th>Списание бонусов</th></tr>
<?php
foreach ($arr_user_points as $user) {

	$url_user = '<a href="'.JURI::base().'/administrator/index.php?option=com_virtuemart&view=user&task=edit&virtuemart_user_id[]='.$user->virtuemart_user_id.'" target="blank">';
	$pole1 = $url_user.$user->virtuemart_user_id.'</a>';
	$pole2 = $url_user.$user->name.'</a>';
	$pole3 = $url_user.$user->email.'</a>';
	$pole4 = $pole5 = $pole5a = $pole5b = 0;

	if (array_key_exists($user->virtuemart_user_id, $arr_user_points)) {
		$user_points = $arr_user_points[$user->virtuemart_user_id];

		$pole4 = $user_points->active_points;
		$pole5 = $user_points->unactive_points;
		$pole5a = ($arr_sum_decrease_points[$user->virtuemart_user_id]->sum_decrease_points == 0) ? 0 : $arr_sum_decrease_points[$user->virtuemart_user_id]->sum_decrease_points;
		$pole5b = $pole4 - $pole5a;
		$pole6a = "";
	}

	if (array_key_exists($user->virtuemart_user_id, $arr_users_connected_pointsystem)) {
		$circle_color = "#6de46d"; // green ball
	} else {
		$circle_color = "#ff8787"; // pink ball
	}
	$style_circle = 'style = "background: -webkit-radial-gradient(7px 7px, circle, '.$circle_color.', #808080); background: -moz-radial-gradient(7px 7px, circle, '.$circle_color.', #808080); background: radial-gradient(7px 7px, circle, '.$circle_color.', #808080);"';

	if ($pole5b <= 0){
		$pole6a = ' disabled="disabled"';
	}

	$pole6 = '
		<form action="2-user-points.php" method="POST" name="spending_points_form_'.$user->virtuemart_user_id.'" id="spending_points_id_'.$user->virtuemart_user_id.'" style="margin: 0;" onsubmit="return validate_form('.$pole5b.', '.$user->virtuemart_user_id.');">
			<input type="text" name="spending_points" placeholder="Сумма списания" form="spending_points_id_'.$user->virtuemart_user_id.'" style="width: 140px; border: solid 1px; padding: 7px 10px; border-radius: 8px;"'.$pole6a.'>
			<input type="hidden" name="user_id" value="'.$user->virtuemart_user_id.'" form="spending_points_id_'.$user->virtuemart_user_id.'">
			<input type="submit" value="Списать" style="border: solid 1px; padding: 7px 10px; border-radius: 8px;"'.$pole6a.'>
		</form>
		';
        ?>

	<?php echo "\t"; ?>
	<tr>

	    <td class="points_user_status"><div class="tln-points-circle" <?php echo $style_circle; ?>>&nbsp;</div></td>
	    <td><?php echo $pole1; ?></td>
	    <td><?php echo $pole2; ?></td>
	    <td class="td-right"><?php echo $pole3; ?></td>
	    <td class="td-center" style="color: green;"><?php echo $pole4; ?></td>
	    <td class="td-right" style="color: red;"><?php echo $pole5; ?></td>
	    <td class="td-right" style="color: blue;"><?php echo $pole5a; ?></td>
	    <td class="td-right" style="color: hotpink; font-weight: 700;"><a href="javascript:points_click(<?php echo $user->virtuemart_user_id.', '.$pole5b ?>)"><?php echo $pole5b; ?></a></td>
	    <td><?php echo $pole6; ?></td></tr>
	<?php echo "\r\n"; } ?>
	</table>

	<hr>
	<div style="font-size: 20px; font-weight: 700; padding: 14px; padding-left: 110px; margin-top: 40px;">История списания бонусов: </div>

	<table class='zebra-table'>
	<tr><th>ID покупателя</th><th>Имя покупателя</th><th>E-Mail покупателя</th><th>Дата списания</th><th>Количество бонусов</th><th>Отменить списание</th></tr>
<?php
foreach ($arr_decrease_points as $decrease_points) {                                                              
	$pole1 = $url_user.$decrease_points->virtuemart_user_id.'</a>';
	$pole2 = $url_user.$decrease_points->name.'</a>';
	$pole3 = $url_user.$decrease_points->email.'</a>';
	$pole4 = $decrease_points->decrease_date;
	$pole5 = $decrease_points->decrease_points;
	$pole6 = '
		<form action="2-user-points.php" method="POST" name="spending_points_cancel_form" id="spending_points_cancel_id_'.$decrease_points->points_decrease_id.'" style="margin: 0;" onsubmit="return request_form();">
			<input type="hidden" name="spending_points_cancel" value="'.$decrease_points->points_decrease_id.'" form="spending_points_cancel_id_'.$decrease_points->points_decrease_id.'">
			<input type="submit" value="Удалить" style="width: -webkit-fill-available; border: solid 1px; padding: 7px 10px; border-radius: 8px;" form="spending_points_cancel_id_'.$decrease_points->points_decrease_id.'">
		</form>
		';

	echo "\t"."<tr><td>".$pole1."</td><td>".$pole2."</td><td class='td-right'>".$pole3."</td><td class='td-center'>".$pole4."</td><td class='td-right' style='color: red;'>".$pole5."</td><td>".$pole6."</td></tr>"."\r\n";

}
?>
	</table>
</div>
</body>
</html>