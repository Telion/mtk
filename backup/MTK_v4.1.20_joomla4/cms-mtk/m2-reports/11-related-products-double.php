<?php

/** PHP Doc
 * Скрипт удаляет дубли из списка сопутствующих товаров в карточке товара.
 * Для запуска раскомментируй строку №33 ( $db->execute(); )
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);

$db->setQuery('
	SELECT virtuemart_product_id as product_id, customfield_value as related_id 
	FROM wxa43_virtuemart_product_customfields 
	WHERE virtuemart_custom_id = 1 
	ORDER BY wxa43_virtuemart_product_customfields.virtuemart_product_id ASC
	    ');
$allRelatedProductsByProductIdList = $db->loadObjectList();

$doubleMap = [];
$temp = [];
$prev_product = -1;
foreach ($allRelatedProductsByProductIdList as $item) {
    if ($prev_product != $item->product_id) {
        $temp = [];
    }

    if (!in_array($item, $temp)) {
        $temp[] = $item;
    } else {
        $doubleMap[$item->related_id] = $item;
        $query = 'DELETE FROM wxa43_virtuemart_product_customfields WHERE virtuemart_product_id = ' . $item->product_id . ' AND customfield_value = ' . $item->related_id . ' LIMIT 1';
        $db->setQuery($query);
        //$db->execute();
        echo $query . "<hr>";
    }
    $prev_product = $item->product_id;
}

echo '<pre>';
print_r($doubleMap);
echo '</pre>';