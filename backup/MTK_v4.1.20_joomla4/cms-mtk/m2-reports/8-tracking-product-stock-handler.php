<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
visitsLog(__FILE__);

$notification = new Notification();

echo "<hr>" . $notification->messageGenerationResult() . "<hr>";

$title = 'МТК: Скрипт 2-8. Проверка остатков и резервов (' .
    count($notification->productsLowStock) . ', ' .
    count($notification->productsNegativeStock) . ', ' .
    count($notification->productsNegativeOrdered) . ')';
echo $title . "<hr>";

message_to_telegram($notification->messageGenerationResult(), $recipient);

printr("скрипт 2-8 отработал");


class Notification
{
    private $db;
    public $productsLowStock = [];
    public $productsNegativeStock = [];
    public $productsNegativeOrdered = [];

    function __construct()
    {
        $this->db = JFactory::getDBO();

        // Если есть товары у которых остаток меньше установленного порога
        $this->productsLowStock = $this->getQueryDB('low_stock_notification > product_in_stock');
        // Если есть товары, у которых остаток меньше 0
        $this->productsNegativeStock = $this->getQueryDB('product_in_stock < 0');
        // Если есть товары, у которых зарезервировано отрицательное число товаров
        $this->productsNegativeOrdered = $this->getQueryDB('product_ordered < 0');
    }

    public function messageGenerationResult()
    {
        $temp = $message = '<b>Остатки и резервы:</b>';
        $message .= $this->messageGenerationAlone($this->productsLowStock, "\r\n\r\n" . '<b>С остатком ниже лимита:</b>');
        $message .= $this->messageGenerationAlone($this->productsNegativeStock, "\r\n\r\n" . '<b>С отрицательным остатком:</b>');
        $message .= $this->messageGenerationAlone($this->productsNegativeOrdered, "\r\n\r\n" . '<b>С отрицательным резервом:</b>');

        if ($message === $temp) {
            return "<b>Остатки и резервы в норме</b>";
        }

        return $message;
    }

    public function messageGenerationAlone($array, $text)
    {
        if (count($array)) {
//            printr($array);
            foreach ($array as $value) {
                $text .= "\r\n" . '   ID: ' . $value['virtuemart_product_id'] . ', ' . $value['product_name'];
            }
            return $text;
        }
    }

    public function getQueryDB($condition)
    {
        $query = '
            SELECT wxa43_virtuemart_products.virtuemart_product_id, wxa43_virtuemart_products_ru_ru.product_name 
            FROM wxa43_virtuemart_products, wxa43_virtuemart_products_ru_ru 
            WHERE 
                ' . $condition . ' 
                AND wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id;';
        $this->db->setQuery($query);

        printr($this->db->loadAssocList());
        return $this->db->loadAssocList('virtuemart_product_id');

    }
}

?>