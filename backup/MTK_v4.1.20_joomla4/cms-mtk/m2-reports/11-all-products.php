<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);

?>

<html>
<head>
	<title>cmsMTK. Все товары, по категориям</title>

<?php
	require_once CONST_MENU;
?>

<div style="width: fit-content; margin: 0 auto;">

<?php


$db->setQuery('SELECT virtuemart_category_id, category_name FROM wxa43_virtuemart_categories_ru_ru ORDER BY category_name ASC');
$arr_categories = $db->loadAssocList('virtuemart_category_id');

$where = '';
if (!empty($_GET)) {
    $where = ' AND category.virtuemart_category_id = ' . $_GET['category-id'];
}

$query = '
        SELECT 
            category.virtuemart_category_id,
            category.category_name,
            product.virtuemart_product_id,
            product.product_name,
            CASE 
                WHEN price.product_override_price > 0 THEN price.product_override_price 
                ELSE price.product_price 
            END new_price,
            products.product_in_stock,
            products.product_ordered
        FROM wxa43_virtuemart_products_ru_ru as product
            LEFT JOIN wxa43_virtuemart_products as products ON product.virtuemart_product_id = products.virtuemart_product_id
            LEFT JOIN wxa43_virtuemart_product_categories as pc ON product.virtuemart_product_id = pc.virtuemart_product_id
            LEFT JOIN wxa43_virtuemart_categories_ru_ru as category ON category.virtuemart_category_id = pc.virtuemart_category_id
            LEFT JOIN wxa43_virtuemart_product_prices as price ON price.virtuemart_product_id = product.virtuemart_product_id 
        WHERE
            products.published = 1 '
        . $where .
        ' ORDER BY category.virtuemart_category_id ASC, new_price ASC;';

$db->setQuery($query);
$arr_products = $db->loadAssocList('virtuemart_product_id');
?>

    <div class="categoryForm" style="margin: 40px 0;">
        <form method='GET' name='choiceCategory'>
            <select name='category-id' style="padding: 3px 8px; border-radius: 4px;">
                <option value='0'>Выбери категорию</option>
                <?php
                foreach ($arr_categories as $category) {
                    $selected = ((!empty($_GET)) && ($category['virtuemart_category_id'] == $_GET['category-id'])) ? 'selected' : '';
                    echo "<option value = '" . $category['virtuemart_category_id'] . "' " . $selected . ">" . $category['category_name'] . "</option>";
                }
                ?>
            </select>
            <button type='submit' name='submit' style=" border-radius: 4px; border: solid 1px #aaa; padding: 4px 13px;">
                Сформировать отчет
            </button>
        </form>
    </div>

    <table class='zebra-table'>
        <tr><th>Код товара</th><th>Название товара</th><th>Цена</th><th>Остаток</th><th>В резерве</th></tr>

        <?php
        foreach ($arr_products as $product) {
            $stockColor = ($product['product_in_stock'] == 0) ? 'red' : 'black';
            $orderedColor = ($product['product_ordered'] == 0) ? 'red' : 'black';
            $category_name = empty($_GET) ? $product['category_name'] . '. ' : '';

            $product_name = '<a href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$product['virtuemart_product_id'].'" target="_blank">'
                    . $category_name . $product['product_name'].
                '</a>';
        ?>
            <tr>
                <td class="td-right" style="border: solid 1px #ccc;">
                    <?=$product['virtuemart_product_id']?></td>
                <td style="width: 700px; border: solid 1px #ccc;">
                    <?= $product_name ?></td>
                <td class="td-right" style="border: solid 1px #ccc;">
                    <?= floor($product['new_price']) ?></td>
                <td class="td-right" style="border: solid 1px #ccc; color: <?=$stockColor?>">
                    <?=$product['product_in_stock']?></td>
                <td class="td-right" style="border: solid 1px #ccc; color: <?=$orderedColor?>">
                    <?=$product['product_ordered']?></td>
            </tr>
        <?php } ?>
	</table>
</div>