<?php

/** PHP Doc
 * Скрипт удаляет из сопутствующих товары, у которых остаток равен 0.
 * Для запуска раскомментируй строку №26 ( $db->execute(); )
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);

$db->setQuery('
    SELECT customfields.virtuemart_product_id as product_id, customfields.customfield_value as related_id, products.product_in_stock 
	FROM wxa43_virtuemart_product_customfields as customfields
    LEFT JOIN wxa43_virtuemart_products as products ON customfields.customfield_value = products.virtuemart_product_id
    WHERE customfields.virtuemart_custom_id = 1
	ORDER BY customfields.virtuemart_product_id ASC;
');
$allRelatedProductsByProductIdListWithStock = $db->loadObjectList();

$doubleMap = [];
foreach ($allRelatedProductsByProductIdListWithStock as $item) {
    if ($item->product_in_stock == 0) {
        $doubleMap[$item->related_id] = $item;
        $query = 'DELETE FROM wxa43_virtuemart_product_customfields WHERE virtuemart_product_id = ' . $item->product_id . ' AND customfield_value = ' . $item->related_id . ' LIMIT 1';
        $db->setQuery($query);
        //$db->execute();
        echo $query . "<hr>";
    }
}

echo '<pre>';
print_r($doubleMap);
echo '</pre>';