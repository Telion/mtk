<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
visitsLog(__FILE__);

function getStatusName($status_code)
{
    switch ($status_code) {
        case "C":
            $status_name = "Подтвержден";
            break; // COM_VIRTUEMART_ORDER_STATUS_CONFIRMED="Подтвержден"
        case "U":
            $status_name = "Подтвержден покупателем";
            break; // COM_VIRTUEMART_ORDER_STATUS_CONFIRMED_BY_SHOPPER="Подтвержден покупателем"
        case "P":
            $status_name = "Собран";
            break; // COM_VIRTUEMART_ORDER_STATUS_PENDING="Собран"
        case "X":
            $status_name = "Отменен";
            break; // COM_VIRTUEMART_ORDER_STATUS_CANCELLED="Отменен"
        case "R":
            $status_name = "Возвращен";
            break; // COM_VIRTUEMART_ORDER_STATUS_PENDING="Собран"
        case "S":
            $status_name = "Доставлен";
            break; // COM_VIRTUEMART_ORDER_STATUS_SHIPPED="Доставлен"
        case "F":
            $status_name = "Выдан";
            break; // COM_VIRTUEMART_ORDER_STATUS_COMPLETED="Выдан"
        case "D":
            $status_name = "Выдан (без списания)";
            break; // COM_VIRTUEMART_ORDER_STATUS_DENIED="Выдан (без списания)"
    }
    return $status_name;
}

// Заказы с просроченной датой резерва
$query = '
	SELECT 
		wxa43_virtuemart_orders.virtuemart_order_id, 
		wxa43_virtuemart_order_userinfos.first_name, 
		wxa43_virtuemart_order_userinfos.email, 
		wxa43_virtuemart_order_histories.reserv_date,
	        wxa43_virtuemart_orders.order_status
	FROM 
		wxa43_virtuemart_order_histories, 
		wxa43_virtuemart_order_userinfos,
	        wxa43_virtuemart_orders
	WHERE 
		wxa43_virtuemart_order_histories.virtuemart_order_id = wxa43_virtuemart_order_userinfos.virtuemart_order_id and 
	        wxa43_virtuemart_order_histories.virtuemart_order_id = wxa43_virtuemart_orders.virtuemart_order_id and
        	(wxa43_virtuemart_orders.order_status = "C" or wxa43_virtuemart_orders.order_status = "U" or wxa43_virtuemart_orders.order_status = "P") and
		reserv_date !=0 and
		reserv_date <= CURDATE()
	GROUP BY wxa43_virtuemart_order_histories.virtuemart_order_id 
	ORDER BY wxa43_virtuemart_order_histories.reserv_date ASC
	';

echo '<pre>';
print_r($query);
echo '</pre>';

$db->setQuery($query);
$arr_orders_reserv = $db->loadObjectList();
$message = "<b>Просроченного резерва нет</b>";

if (count($arr_orders_reserv) > 0) {
    $th = "<th style='text-align: center; padding: 5px 10px;'>";
    $td = "<td style='border: solid 1px black; padding: 5px 10px;'>";

    $report = "<table style='border-collapse: collapse;'><tr>" . $th . "ID заказа</th>" . $th . "Покупатель</th>" . $th . "E-Mail</th>" . $th . "В резерве</th>" . $th . "Статус заказа</th></tr>";
    $message = "<b>Просроченный резерв:</b>";

    foreach ($arr_orders_reserv as $order) {
        $virtuemart_order_id = $td . '<a href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=orders&task=edit&virtuemart_order_id=' . $order->virtuemart_order_id . '" target="_blank">' . $order->virtuemart_order_id . '</a></td>';
        $first_name = $td . $order->first_name . "</td>";
        $email = $td . $order->email . "</td>";
        $reserv_date_format = Date("d.m.Y", strtotime($order->reserv_date));
        $reserv_date = $td . "до " . $reserv_date_format . "</td>";
        $order_status = $td . getStatusName($order->order_status) . "</td>";

        $report .= "<tr>" . $virtuemart_order_id . $first_name . $email . $reserv_date . $order_status . "</tr>";
        $message .= "\r\n\r\n" . "ID заказа: <b>" . $order->virtuemart_order_id . "</b>" . "\r\n" .
                    "   Покупатель: " . $order->first_name . "\r\n" .
                    "   Дата резерва: " . $reserv_date_format . "\r\n" .
                    "   Статус заказа: " . getStatusName($order->order_status);
    }
    $report .= "</table>";

    $title = 'МТК: Скрипт 2-9. Заказы с просроченной датой резерва (' . count($arr_orders_reserv) . ')';
    echo $title . "<hr>";
    echo $report . "<hr>";

    //sendEmail($title, $report, EMAIL_ALIA);
    //sendEmail($title, $report . "<br><br> Отправил копию на почту Алии", EMAIL_TELION);
}

message_to_telegram($message, $recipient);
printr("скрипт 2-9 отработал");


?>