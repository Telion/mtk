<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
visitsLog(__FILE__);

$db->setQuery('
	SELECT 
		prices.virtuemart_product_id, 
		wxa43_virtuemart_products_ru_ru.product_name, 
		prices.product_price, 
		prices.override, 
		prices.product_override_price, 
		prices.product_price_publish_down 
	FROM 
		(SELECT * FROM wxa43_virtuemart_product_prices GROUP BY wxa43_virtuemart_product_prices.virtuemart_product_id) as prices, 
		wxa43_virtuemart_products_ru_ru 
	WHERE 
		prices.product_price_publish_down > 0 AND 
		wxa43_virtuemart_products_ru_ru.virtuemart_product_id = prices.virtuemart_product_id 
	ORDER BY prices.product_price_publish_down ASC
');
$arr_products = $db->loadObjectList();

$db->setQuery('
	SELECT 
		prices.virtuemart_product_id, 
		wxa43_virtuemart_products_ru_ru.product_name, 
		prices.product_price, 
		prices.override, 
		prices.product_override_price, 
        	(prices.product_price - prices.product_override_price) as discount,
	        ((((prices.product_price - prices.product_override_price) / prices.product_price) * 10000) / 100) as discount_percentage
	FROM 
		(SELECT * FROM wxa43_virtuemart_product_prices GROUP BY wxa43_virtuemart_product_prices.virtuemart_product_id) as prices, 
		wxa43_virtuemart_products_ru_ru 
	WHERE 
		prices.product_price_publish_down <= 0 AND 
	        prices.product_override_price > 0 AND
		prices.override = 1 AND 
		wxa43_virtuemart_products_ru_ru.virtuemart_product_id = prices.virtuemart_product_id  
	ORDER BY discount_percentage ASC
');
$arr_products_all_discounts = $db->loadObjectList();

?>

<html>
<head>
    <title>cmsMTK. Список текущих акционных товаров</title>
    <?php require_once CONST_MENU; ?>
    <div style="width: fit-content; margin: 0 auto;">

        <!---------------------------------------------------------------------------------------------------------------------------------------------------------------------->
        <!------------------- ВЕРХНЯЯ ТАБЛИЦА - ТЕКУЩИЕ АКЦИИ ------------------------------------------------------------------------------------------------------------------>
        <!---------------------------------------------------------------------------------------------------------------------------------------------------------------------->
        <div style="font-size: 20px; font-weight: 700; padding: 14px; padding-left: 110px;">Список текущих акционных
            товаров:
        </div>
        <table class='zebra-table'>
            <tr>
                <th>ID товара</th>
                <th>Название товара</th>
                <th>До окончания<br>акции</th>
                <th>Дата окончания<br>акции</th>
                <th>Цена без скидки</th>
                <th>Переопределить<br>цену?</th>
                <th>Цена со скидкой</th>
                <th class="orange" colspan=2>Скидка</th>
            </tr>
            <?php
            foreach ($arr_products as $product) {
                $productId = $product->virtuemart_product_id;
                $productName = '<a target="_blank" href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id=' . $productId . '">' . $product->product_name . '</a>';
                $productPriceWithoutSale = ceil($product->product_price) . ' руб';
                $productPriceIsOverride = ($product->override == 1) ? 'да' : 'нет';
                $productPriceWithSale = ceil($product->product_override_price) . ' руб';
                $productSale = ceil($product->product_price) - ceil($product->product_override_price);
                $productSalePercent = ceil(($productSale / ceil($product->product_price)) * 10000) / 100;
                $productDateBeforeSaleEnd =
                    date("d.m.Y", strtotime($product->product_price_publish_down)) . '<br>' .
                    '<span style="color: #aaa;">' . date("H:i:s", strtotime($product->product_price_publish_down)) . '</span>';
                $productDaysBeforeSaleEnd = floor((strtotime($product->product_price_publish_down) - time()) / (60 * 60 * 24));

                $override_color = 'color: black';
                if ($productPriceIsOverride == 'нет') $override_color = 'color: red; font-weight: 700;';

                $warning_color = 'color: black';
                if ($productDaysBeforeSaleEnd < 4) $warning_color = 'color: red; font-weight: 700;';

                echo "\t" . '
        <tr>
            <td>' . $productId . '</td>
            <td style="max-width: 500px;">' . $productName . '</td>
            <td class="td-center" style="' . $warning_color . '">' . $productDaysBeforeSaleEnd . ' дн</td>
            <td class="td-center">' . $productDateBeforeSaleEnd . '</td>
            <td class="td-right">' . $productPriceWithoutSale . '</td>
            <td class="td-center" style="' . $override_color . '">' . $productPriceIsOverride . '</td>
            <td class="td-right">' . $productPriceWithSale . '</td>
            <td class="td-right orange">' . $productSale . ' руб</td>
            <td class="td-right orange">' . $productSalePercent . ' %</td>
        </tr>' . "\r\n";
            }
            ?>


            <!---------------------------------------------------------------------------------------------------------------------------------------------------------------------->
            <!------------------- НИЖНЯЯ ТАБЛИЦА - ВСЕ ТОВАРЫ СО СКИДКОЙ ----------------------------------------------------------------------------------------------------------->
            <!---------------------------------------------------------------------------------------------------------------------------------------------------------------------->
            <tr>
                <th colspan="9" style="background: #fff; color: #555; text-align: left;">
                    <div style="font-size: 20px; font-weight: 700; padding: 14px; padding-left: 110px; margin-top: 50px;">
                        Все товары с активной скидкой (у которых переопределена цена):
                    </div>
                </th>
            </tr>

            <tr>
                <th>ID товара</th>
                <th colspan="3">Название товара</th>
                <th>Цена без скидки</th>
                <th>Переопределить<br>цену?</th>
                <th>Цена со скидкой</th>
                <th class="orange" colspan=2>Скидка</th>
            </tr>
            <?php
            foreach ($arr_products_all_discounts as $product) {
                $productId = $product->virtuemart_product_id;
                $productName = '<a target="_blank" href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id=' . $productId . '">' . $product->product_name . '</a>';
                $productPriceWithoutSale = ceil($product->product_price) . " руб";
                $productPriceIsOverride = ($product->override == 1) ? "да" : "нет";
                $productPriceWithSale = ceil($product->product_override_price) . " руб";
                $productSale = ceil($product->discount);
                $productSalePercent = ceil($product->discount_percentage);

                $override_color = "color: black";
                if ($productPriceIsOverride == "нет") $override_color = "color: red; font-weight: 700;";
                echo "\t" . '

            <tr>
                <td>' . $productId . '</td>
                <td style="max-width: 760px;" colspan="3">' . $productName . '</td>
                <td class="td-right">' . $productPriceWithoutSale . '</td>
                <td class="td-center" style="' . $override_color . '">' . $productPriceIsOverride . '</td>
                <td class="td-right">' . $productPriceWithSale . '</td>
                <td class="td-right orange">' . $productSale . ' руб</td>
                <td class="td-right orange">' . $productSalePercent . ' %</td>
            </tr>' . "\r\n";

            }
            ?>
        </table>


    </div>
</head>