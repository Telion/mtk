<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);

?>

<html>
<head>
	<title>cmsMTK. Товары с резервом</title>

<?php
	require_once CONST_MENU;
?>

<div style="width: fit-content; margin: 0 auto;">
<div style="font-size: 20px; font-weight: 700; padding: 14px; padding-left: 110px;">Товары с резервом</div>

<?php
// Формирую список
$db->setQuery('
	SELECT 
	    wxa43_virtuemart_products.virtuemart_product_id,
	    wxa43_virtuemart_products.product_sku,
	    wxa43_virtuemart_products_ru_ru.product_name,
	    prices.product_price,
	    prices.override,
	    prices.product_override_price,
	    wxa43_virtuemart_products.product_in_stock,
	    wxa43_virtuemart_products.product_ordered
	FROM 
	    wxa43_virtuemart_products,
	    wxa43_virtuemart_products_ru_ru,
	    (SELECT * FROM wxa43_virtuemart_product_prices GROUP BY wxa43_virtuemart_product_prices.virtuemart_product_id) as prices
	WHERE 
	    wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND
	    wxa43_virtuemart_products.virtuemart_product_id = prices.virtuemart_product_id AND
	    wxa43_virtuemart_products.product_in_stock < 0  
	UNION
	SELECT 
	    wxa43_virtuemart_products.virtuemart_product_id,
	    wxa43_virtuemart_products.product_sku,
	    wxa43_virtuemart_products_ru_ru.product_name,
	    prices.product_price,
	    prices.override,
	    prices.product_override_price,
	    wxa43_virtuemart_products.product_in_stock,
	    wxa43_virtuemart_products.product_ordered
	FROM 
	    wxa43_virtuemart_products,
	    wxa43_virtuemart_products_ru_ru,
	    (SELECT * FROM wxa43_virtuemart_product_prices GROUP BY wxa43_virtuemart_product_prices.virtuemart_product_id) as prices
	WHERE 
	    wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND
	    wxa43_virtuemart_products.virtuemart_product_id = prices.virtuemart_product_id AND
	    product_ordered <> 0  
	ORDER BY product_ordered ASC;
');
$arr_products = $db->loadObjectList();

?>

	<table class='zebra-table reserv'>
	<tr><th>Код товара</th><th>Название товара</th><th>Цена</th><th>Остаток</th><th class="orange">В резерве</th></tr>

<?php
foreach ($arr_products as $product) {
	$pole1 = $product->virtuemart_product_id;
	$pole2 = $product->product_sku;
	$pole3 = '<a href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$product->virtuemart_product_id.'" target="_blank">'.$product->product_name.'</a>';
	$pole4 = ($product->override > 0) ? round($product->product_override_price)." руб" : round($product->product_price)." руб";
	$pole5 = $product->product_in_stock;
	$pole6 = $product->product_ordered;	
	echo "\t".'<tr><td class="td-right">'.$pole1.'</td><td>'.$pole3.'</td><td class="td-right">'.$pole4.'</td><td class="td-right">'.$pole5.'</td><td class="td-right orange">'.$pole6.'</td></tr>'."\r\n";

}
?>
	</table>
</div>