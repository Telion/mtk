<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);

function getStatusName($status_code) {
	switch ($status_code) {
		case "C": $status_name = "Подтвержден"; break; // COM_VIRTUEMART_ORDER_STATUS_CONFIRMED="Подтвержден"
		case "U": $status_name = "Подтвержден покупателем"; break; // COM_VIRTUEMART_ORDER_STATUS_CONFIRMED_BY_SHOPPER="Подтвержден покупателем"
		case "P": $status_name = "Собран"; break; // COM_VIRTUEMART_ORDER_STATUS_PENDING="Собран"
		case "X": $status_name = "Отменен"; break; // COM_VIRTUEMART_ORDER_STATUS_CANCELLED="Отменен"
		case "R": $status_name = "Возвращен"; break; // COM_VIRTUEMART_ORDER_STATUS_PENDING="Собран"
		case "S": $status_name = "Доставлен"; break; // COM_VIRTUEMART_ORDER_STATUS_SHIPPED="Доставлен"
		case "F": $status_name = "Выдан"; break; // COM_VIRTUEMART_ORDER_STATUS_COMPLETED="Выдан"
		case "D": $status_name = "Выдан (без списания)"; break; // COM_VIRTUEMART_ORDER_STATUS_DENIED="Выдан (без списания)"
	}
	return $status_name;
}
?>

<html>
<head>
	<title>cmsMTK. Заказы с резервом</title>

<?php
	require_once CONST_MENU;
?>

<div style="width: fit-content; margin: 0 auto;">
<div style="font-size: 20px; font-weight: 700; padding: 14px; padding-left: 110px;">Заказы с резервом</div>

<?php
$db->setQuery('
	SELECT 
		wxa43_virtuemart_orders.virtuemart_order_id, 
		wxa43_virtuemart_order_userinfos.first_name, 
		wxa43_virtuemart_order_userinfos.phone_1, 
		wxa43_virtuemart_order_userinfos.email, 
		wxa43_virtuemart_order_histories.reserv_date,
	        wxa43_virtuemart_orders.order_status
	FROM 
		wxa43_virtuemart_order_histories, 
		wxa43_virtuemart_order_userinfos,
	        wxa43_virtuemart_orders
	WHERE 
		wxa43_virtuemart_order_histories.virtuemart_order_id = wxa43_virtuemart_order_userinfos.virtuemart_order_id and 
	        wxa43_virtuemart_order_histories.virtuemart_order_id = wxa43_virtuemart_orders.virtuemart_order_id and
        	(wxa43_virtuemart_orders.order_status = "C" or wxa43_virtuemart_orders.order_status = "U" or wxa43_virtuemart_orders.order_status = "P") and
		reserv_date != 0 
	GROUP BY wxa43_virtuemart_order_histories.virtuemart_order_id 
	ORDER BY `wxa43_virtuemart_order_histories`.`virtuemart_order_id` DESC
	');
	$arr_orders_reserv = $db->loadObjectList();
?>

	<table class='zebra-table reserv'>
	<tr><th>Код заказа</th><th style="min-width: 77px;">Имя</th><th>Телефон</th><th>E-Mail</th><th>Дата резерва</th><th>Статус заказа</th></tr>

<?php
foreach ($arr_orders_reserv as $order) {

	$pole_code = '
		<td style="padding: 0; text-align: right;">
		<a href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=orders&task=edit&virtuemart_order_id='.$order->virtuemart_order_id.'" target="_blank">
		<div style="padding: 7px 5px 7px 14px;">
		<span style="padding-right: 15px;">
			'.$order->virtuemart_order_id.'
		</span></div></a></td>';

	$pole_name = '<td>'.$order->first_name.'</td>';

	$pole_phone = '<td>&nbsp;</td>';
	$pole_email = '<td>&nbsp;</td>';
	
	if ($order->phone_1 != '+7(777) 777-77-77') {
		$pole_phone = '<td>'.$order->phone_1.'</td>';
	} 

	if ($order->email != 'nsv@mirtelcom.ru') {
		$pole_email = '<td>'.$order->email.'</td>';
	}

	$rd = strtotime($order->reserv_date);
	$yet = ceil(($rd - time()) / (60*60*24));
	if ($yet > 0) {
		$symbol = '+';
		$color = 'green';
	} else {
		$symbol = '';
		$color = 'red';
	}
	$pole_date = "<td><span style='color: ".$color.";'>до ".date("d.m.Y", $rd)." (".$symbol.$yet." дн)</span></td>"; 

	$pole_status = "<td>".getStatusName($order->order_status)."</td>";

	echo "\t".'<tr>'.$pole_code.$pole_name.$pole_phone.$pole_email.$pole_date.$pole_status.'</tr>'."\r\n";

}
?>
	</table>






	<!-- ТАБЛИЦА 2 -->

<div style="font-size: 20px; font-weight: 700; padding: 14px; padding-left: 110px;">Незавершенные заказы</div>

<?php
$query = '
    SELECT
            orders.virtuemart_order_id, 
            infos.first_name, 
            infos.phone_1, 
            infos.email, 
            orders.order_status,
            orders.created_on
    FROM wxa43_virtuemart_orders AS orders
        LEFT JOIN wxa43_virtuemart_order_userinfos AS infos ON orders.virtuemart_order_id = infos.virtuemart_order_id AND infos.address_type = "BT"
        LEFT JOIN (
            SELECT 
                wxa43_virtuemart_orders.virtuemart_order_id,
                max(reserv_date) as max_date
            FROM 
                wxa43_virtuemart_order_histories, 
                wxa43_virtuemart_orders
            WHERE   	
                    wxa43_virtuemart_order_histories.virtuemart_order_id = wxa43_virtuemart_orders.virtuemart_order_id and
                    (wxa43_virtuemart_orders.order_status = "C" or wxa43_virtuemart_orders.order_status = "U" or wxa43_virtuemart_orders.order_status = "P")
            GROUP BY wxa43_virtuemart_orders.virtuemart_order_id
            ) AS max_dates ON orders.virtuemart_order_id = max_dates.virtuemart_order_id AND max_dates.max_date = 0
    WHERE
        orders.order_status = "C" 
        OR orders.order_status = "U" 
        OR orders.order_status = "P"
    ORDER BY orders.virtuemart_order_id DESC;
	';
$db->setQuery($query);
	$arr_orders_not_finish = $db->loadObjectList();
?>

	<table class='zebra-table reserv'>
	<tr><th>Код заказа</th><th>Имя</th><th>Телефон</th><th>E-Mail</th><th>Статус заказа</th><th>Дата создания</th></tr>

<?php
foreach ($arr_orders_not_finish as $order) {

	$pole_code = '
		<td style="padding: 0; text-align: right;">
		<a href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=orders&task=edit&virtuemart_order_id='.$order->virtuemart_order_id.'" target="_blank">
		<div style="padding: 7px 5px 7px 14px;">
		<span style="padding-right: 15px;">
			'.$order->virtuemart_order_id.'
		</span></div></a></td>';

	$pole_name = '<td>'.$order->first_name.'</td>';
	$pole_phone = '<td>&nbsp;</td>';
	$pole_email = '<td>&nbsp;</td>';
	
	if ($order->phone_1 != '+7(777) 777-77-77') {
		$pole_phone = '<td>'.$order->phone_1.'</td>';
	} 

	if ($order->email != 'nsv@mirtelcom.ru') {
		$pole_email = '<td>'.$order->email.'</td>';
	}

	$pole_status = "<td>".getStatusName($order->order_status)."</td>";
	$count_days = round((time() - strtotime($order->created_on)) / 60 / 60 / 24);
	$pole_date = "<td>" . Date("d.m.Y", strtotime($order->created_on)) . " (" . $count_days . " дн)" . "</td>";

	echo "\t" . '<tr>' . $pole_code . $pole_name . $pole_phone . $pole_email . $pole_status . $pole_date . '</tr>' . "\r\n";

}
?>
	</table>
</div>


</div>