<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);

$db->setQuery('
	SELECT 
		wxa43_virtuemart_categories_ru_ru.virtuemart_category_id, 
		wxa43_virtuemart_categories_ru_ru.category_name, 
		count(wxa43_virtuemart_categories_ru_ru.category_name) as tln
	FROM wxa43_virtuemart_products_ru_ru, wxa43_virtuemart_categories_ru_ru, wxa43_virtuemart_product_categories, wxa43_virtuemart_products 
	WHERE
    		wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND
	    	wxa43_virtuemart_products.published = 1 AND
		wxa43_virtuemart_categories_ru_ru.virtuemart_category_id = wxa43_virtuemart_product_categories.virtuemart_category_id AND 
		wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_product_categories.virtuemart_product_id AND 
		wxa43_virtuemart_products_ru_ru.virtuemart_product_id NOT IN (
			SELECT wxa43_virtuemart_product_customfields.virtuemart_product_id 
			FROM wxa43_virtuemart_product_customfields 
			WHERE wxa43_virtuemart_product_customfields.virtuemart_custom_id = 1 
			GROUP BY wxa43_virtuemart_product_customfields.virtuemart_product_id) 
	GROUP BY wxa43_virtuemart_categories_ru_ru.category_name 
	ORDER BY tln DESC');
$arr_categories = $db->loadObjectList();
?>

<html>
<head>
	<title>cmsMTK. Список категорий, в которых имеются товары без сопутствующих</title>

<?php
	require_once CONST_MENU;
?>

<div style="width: fit-content; margin: 0 auto; position: relative;">
<div style="font-size: 20px; font-weight: 700; padding: 14px; padding-left: 110px;">Категории с товарами без сопутствующих: </div>

<?php 
	if (count($arr_categories)) echo '<a href="6-related-products-insert.php" target="_blank"><div class="run-button" style="top: 8px; right: -329px;">Попробовать добавить</div></a>';
?>



	<table class='zebra-table related'>
	<tr><th>Название категории</th><th>Количество товаров<br> без сопутствующих</th></tr>

	<?php
	foreach ($arr_categories as $category) {
		$pole1 = $category->category_name;
		$pole2 = $category->tln;
		echo "\t".'<tr>	<td><a href="6-related-products.php?cat_id='.$category->virtuemart_category_id.'&cat_name='.$category->category_name.'">'.$pole1.'</a></td>
				<td style="text-align: right; padding-right: 50px">'.$pole2.'</td></tr>'."\r\n";
	}
	?>
	</table>
</div>

