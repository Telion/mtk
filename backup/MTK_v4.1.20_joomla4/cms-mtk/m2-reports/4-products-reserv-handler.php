<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';


// Проверка акционных товаров, отправка сообщения в ТЕЛЕГРАММ АЛИЕ 
$db = JFactory::getDBO();
$db->setQuery('SELECT wxa43_virtuemart_products_ru_ru.product_name, wxa43_virtuemart_product_prices.product_price, wxa43_virtuemart_product_prices.product_price_publish_down FROM wxa43_virtuemart_product_prices, wxa43_virtuemart_products_ru_ru WHERE product_price_publish_down > 0 and wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_product_prices.virtuemart_product_id ORDER BY `wxa43_virtuemart_product_prices`.`product_price_publish_down` ASC');
$arr_products = $db->loadObjectList();

$message_text = "";
$send_message = 0;
foreach ($arr_products as $product) {

	$days = floor((strtotime($product->product_price_publish_down) - time()) / (60*60*24));
	if ($days < 4) {
		$message_text .= $product->product_name." (".floor($product->product_price)." руб) - ".$days." дн. до окончания\r\n\r\n";
		$send_message = 1;
	}
}

if ($send_message == 1) {
	message_to_telegram("Акционные товары:\r\n\r\n".$message_text, $recipient);
	echo "Акционные товары:<br>".$message_text;
}

?>