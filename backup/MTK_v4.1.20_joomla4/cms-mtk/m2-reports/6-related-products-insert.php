<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
	visitsLog(__FILE__);

// Подбор похожих товаров
function search_replace($str_ish, $arr_products, $own_name){ # $own_name: 0 - не брать свое имя; 1 - брать свое имя
	$arr_products_result = [];
    set_time_limit(300);
	for ($i = 0; $i < count($arr_products); $i++) {
		$arr_products[$i]->similar = similar_text($str_ish, $arr_products[$i]->product_name, $arr_products[$i]->similar_perc); // !!!

		$soundex1_4_simbol = substr(soundex(translit($str_ish)), 3, 1);					// !!!
		$soundex2_4_simbol = substr(soundex(translit($arr_products[$i]->product_name)), 3, 1);		// !!!
		$arr_products[$i]->soundex_4_symbol_diff = abs($soundex1_4_simbol - $soundex2_4_simbol);	// !!!
		if (
		      (($str_ish != $arr_products[$i]->product_name) & ($arr_products[$i]->similar_perc > 76)) || 
		      (($str_ish != $arr_products[$i]->product_name) & ($arr_products[$i]->similar_perc > 52) & ($arr_products[$i]->similar > 34) & ($arr_products[$i]->soundex_4_symbol_diff < 1))     //perc не меньше 48
		   ) {               
			$arr_products[$i]->soundex_res = 0;
			$arr_products_result[] = $arr_products[$i];
		} else $arr_products[$i]->soundex_res = 1;
        		$arr_products[$i]->result_soundex = $arr_products[$i]->soundex_res;;
		$arr_products[$i]->result_levenshtein = levenshtein($str_ish, $arr_products[$i]->product_name, 100, 100, 100);
		if (($own_name == 0) && ($str_ish === $arr_products[$i]->product_name)) continue;
	}

        $result_soundex = array_column($arr_products_result, 'result_soundex');
	$result_levenshtein = array_column($arr_products_result, 'result_levenshtein');
	array_multisort($result_soundex, SORT_ASC, $result_levenshtein, SORT_ASC, $arr_products_result);

    if (!empty($arr_products_result)) {
        $random = rand(0, count($arr_products_result) - 1);
        return $arr_products_result[$random];
    }
    return null;
}

function get_related_of_product($product_id, $db){
	// СПИСОК сопутствующих в конкретном товаре по ID (используется для подбора похожих на них товары, для вставки в товар, у к/го мало сопутствующих
	$db->setQuery('
	SELECT 
	    wxa43_virtuemart_product_customfields.customfield_value,
	    wxa43_virtuemart_products_ru_ru.virtuemart_product_id,
	    wxa43_virtuemart_products_ru_ru.product_name
	FROM 
	    wxa43_virtuemart_product_customfields,
	    wxa43_virtuemart_products_ru_ru
	WHERE 
	    wxa43_virtuemart_product_customfields.virtuemart_product_id = '.$product_id.' AND 
	    wxa43_virtuemart_product_customfields.virtuemart_custom_id = 1 AND
	    wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_product_customfields.customfield_value
	');
	$arr_customfields = $db->loadObjectList();
	return $arr_customfields;
}

function get_number_element_from_arr($arr){
	return rand(0, count($arr)-1);
}

function get_element_from_arr($arr, $random){
	$result = $arr[$random];
	unset($arr[$random]);
	return $result;
}

function remove_element_from_arr($arr, $random){
	unset($arr[$random]);
	sort($arr);
	return $arr;
}

$db->setQuery('
	SELECT low8.virtuemart_product_id, wxa43_virtuemart_products_ru_ru.product_name, wxa43_virtuemart_product_customfields.customfield_value
	FROM (
			SELECT
	    		wxa43_virtuemart_product_customfields.virtuemart_product_id
			FROM 
	    		wxa43_virtuemart_product_customfields
			WHERE 
	    		wxa43_virtuemart_product_customfields.virtuemart_custom_id = 1
			GROUP BY wxa43_virtuemart_product_customfields.virtuemart_product_id
			HAVING 
	    		COUNT(wxa43_virtuemart_product_customfields.virtuemart_product_id) < 8 ) as low8,
	    wxa43_virtuemart_products_ru_ru,
        wxa43_virtuemart_product_customfields
	WHERE
	    wxa43_virtuemart_products_ru_ru.virtuemart_product_id = low8.virtuemart_product_id AND
        wxa43_virtuemart_product_customfields.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND
        wxa43_virtuemart_product_customfields.virtuemart_custom_id = 1
');
$arr_products_few_related = $db->loadObjectList();

// СПИСОК id товаров, у которых меньше 8 сопутствующих
$db->setQuery('
	SELECT
	    wxa43_virtuemart_product_customfields.virtuemart_product_id,
            wxa43_virtuemart_products_ru_ru.product_name,
   	    count(wxa43_virtuemart_product_customfields.virtuemart_product_id) as custom_count
	FROM 
	    wxa43_virtuemart_product_customfields,
            wxa43_virtuemart_products_ru_ru
	WHERE 
	    wxa43_virtuemart_product_customfields.virtuemart_custom_id = 1 AND
            wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_product_customfields.virtuemart_product_id
	GROUP BY wxa43_virtuemart_product_customfields.virtuemart_product_id
	HAVING 
	    COUNT(wxa43_virtuemart_product_customfields.virtuemart_product_id) < 8
        
    	UNION
	
 	SELECT
    	wxa43_virtuemart_products_ru_ru.virtuemart_product_id,
        wxa43_virtuemart_products_ru_ru.product_name,
   	    0
	FROM
    	wxa43_virtuemart_products_ru_ru
    WHERE
    	    wxa43_virtuemart_products_ru_ru.virtuemart_product_id NOT IN (
		SELECT wxa43_virtuemart_product_customfields.virtuemart_product_id 
		FROM wxa43_virtuemart_product_customfields 
		WHERE wxa43_virtuemart_product_customfields.virtuemart_custom_id = 1 
		GROUP BY wxa43_virtuemart_product_customfields.virtuemart_product_id
    		
    		UNION
			
		SELECT wxa43_virtuemart_products.virtuemart_product_id 
		FROM wxa43_virtuemart_products
		WHERE wxa43_virtuemart_products.published = 0 or wxa43_virtuemart_products.product_in_stock < 1)  
');
$arr_related_list = $db->loadObjectList();

$db->setQuery('
	SELECT 
	    wxa43_virtuemart_products_ru_ru.virtuemart_product_id,
	    wxa43_virtuemart_products_ru_ru.product_name, 
	    wxa43_virtuemart_product_categories.virtuemart_category_id,
	    wxa43_virtuemart_products.published,
	    wxa43_virtuemart_products.product_in_stock
	FROM 
	    wxa43_virtuemart_products_ru_ru, 
	    wxa43_virtuemart_product_categories,
	    wxa43_virtuemart_products
	WHERE
	    wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_product_categories.virtuemart_product_id AND
	    wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND
	    wxa43_virtuemart_products.published = 1 AND
	    wxa43_virtuemart_products.product_in_stock > 0
');
$arr_all_products = $db->loadObjectList();

// СПИСОК товаров с количеством сопутствующих более 8 (используется для выбора источника сопутствующих товаров)
$db->setQuery('
	SELECT
            wxa43_virtuemart_product_customfields.virtuemart_product_id,
            wxa43_virtuemart_products_ru_ru.product_name
	FROM 
	    wxa43_virtuemart_product_customfields,
            wxa43_virtuemart_products_ru_ru
	WHERE 
	    wxa43_virtuemart_product_customfields.virtuemart_custom_id = 1 AND
            wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_product_customfields.virtuemart_product_id
	GROUP BY wxa43_virtuemart_product_customfields.virtuemart_product_id
	HAVING 
	    COUNT(wxa43_virtuemart_product_customfields.virtuemart_product_id) >= 8
');
$arr_products = $db->loadObjectList();

$arr_products_not_like_that = [
/*	"69"
	"3040"*/
];

$limit_products = 20000;
if (count($arr_related_list) > $limit_products) echo '<h1 style="color: orangered;">Товаров, которым необходимо добавить сопутствующие больше '.$limit_products.', запусти скрипт еще раз</h1>';

echo "<h2>Перебор товаров, у к/х меньше 8 сопутствующих (".count($arr_related_list).")</h2>";
$i = 0;
$arr_result = [];
$db_insert = JFactory::getDBO();  
foreach ($arr_related_list as $main_product) {
	if (isset($arr_products_not_like_that)) {
		if (in_array($main_product->virtuemart_product_id, $arr_products_not_like_that)) {
			continue;
		}
	} 
	
    $i++;
	$source = search_replace($main_product->product_name, $arr_products, 0);
    if ($source != null) {
        echo "<hr>";
        $source_id = $source->virtuemart_product_id;
        $source_name = $source->product_name;

        echo '<a href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id=' . $main_product->virtuemart_product_id . '" target="_blank">' . $i . ". " . $main_product->virtuemart_product_id . ". " . $main_product->product_name . " (Сопутствующих: " . $main_product->custom_count . ")</a><br>";
        if ($source_id != 0) {
            echo "<span style='color: red;'>--- Похожий не найден !!!</span><br><br>";
            continue;
        }
        $how_many = 8 - $main_product->custom_count;

        echo "--- Похожий: " . $source_id . ". " . $source_name . "<br>";
        echo "--- --- Список сопутствующих из похожего:<br>";
        $arr_relateds_of_product = get_related_of_product($source_id, $db);
        foreach ($arr_relateds_of_product as $curr_related) {
            echo "--- --- --- " . $curr_related->product_name . "<br>";
        }
        echo "<br>";

        $counter = 0;
        $ordering_number = $main_product->custom_count;
        for ($j = 0; $j < $how_many; $j++) {
            if ($counter == 8) break;
            $number_from_relateds_of_product = get_number_element_from_arr($arr_relateds_of_product);
            $current_related = get_element_from_arr($arr_relateds_of_product, $number_from_relateds_of_product);
            $arr_relateds_of_product = remove_element_from_arr($arr_relateds_of_product, $number_from_relateds_of_product);
            echo "--- --- Беру из сопутствующих: " . $number_from_relateds_of_product . ", " . $current_related->product_name . "<br>";
            $get_analog = search_replace($current_related->product_name, $arr_all_products, 1);
            echo "--- --- --- -------------------- Аналог: " . $get_analog->product_name . "<br>";

            if (!$get_analog) {
                echo "--- --- --- Аналог не найден, беру такой же.<br>";
                $insert_text = 'INSERT INTO wxa43_virtuemart_product_customfields(virtuemart_product_id, virtuemart_custom_id, customfield_value, customfield_price, customfield_params, published, modified_by, ordering) 
				VALUES (' . $main_product->virtuemart_product_id . ', 1, ' . $current_related->virtuemart_product_id . ', 0, "", 0, 408, ' . $ordering_number . ')';
            } else {
                $insert_text = 'INSERT INTO wxa43_virtuemart_product_customfields(virtuemart_product_id, virtuemart_custom_id, customfield_value, customfield_price, customfield_params, published, modified_by, ordering) 
				VALUES (' . $main_product->virtuemart_product_id . ', 1, ' . $get_analog->virtuemart_product_id . ', 0, "", 0, 408, ' . $ordering_number . ')';
            }
            echo $insert_text . "<br>";

            $db_insert->setQuery($insert_text);
            $db_insert->execute();
            $ordering_number++;

            echo "<br>";
        }

        if ($i == $limit_products) {
            echo '<h1 style="color: orangered;">Работа скрипта остановлена принудительно, в базе еще остались товары, у которых недостаточно сопутствующих товаров</h1>';
            break;
        }
    }
}
  
echo "<hr><h2>Скрипт завершил работу</h2><hr>";
?>