<?php
require_once 'connectionDB.php';

$update_id = $_GET['update_id'];

/* Получение POST данных */
if($_SERVER["REQUEST_METHOD"]=="POST"){
	$form_comment = $_POST['form_comment'];
	$form_our_url = $_POST['form_our_url'];
	$form_compare_url = $_POST['form_compare_url'];
	$delete_query = $_POST['delete_query'];
}

/* Для сброса POST параметров, чтобы не отправлялись повторно по F5 */
if ((isset($_POST['form_our_url'])) or (isset($_POST['form_compare_url'])) or (isset($_POST['delete_query']))){
	header("Location: z_NOT_WORKING_price_checker_page.php");
}

$vendors = ['dns-shop', 'citilink', 'shop.nag'];

/* Добавление данных в базу, если заполнены url'ы */
if (($form_our_url) and ($form_compare_url)){

	foreach ($vendors as $vendor){
		if (strpos($form_compare_url, $vendor)){
			$compare_name = $vendor;
		}
	}

	$db_insert = JFactory::getDBO();
	$db_insert->setQuery('INSERT INTO tln_compare_url( comment, our_url, our_last_price, compare_url, compare_name, compare_last_price) VALUES( "'.$form_comment.'", "'.$form_our_url.'", 0,  "'.$form_compare_url.'", "'.$compare_name.'", 0)');
	$db_insert->execute();
}

/* Удаление записи из базы, если нажата кнопка "x" */
if ($delete_query){
	$db_delete = JFactory::getDBO();
	$db_delete->setQuery('DELETE FROM tln_compare_url WHERE id='.$delete_query);
	$db_delete->execute();
}

$db->setQuery('SELECT * FROM tln_compare_url');
$arr_queries = $db->loadObjectList();
        
?>

<html>
<head>
<title>cmsMTK. Сравнение цен с сайтами поставщиков</title>
<link rel="shortcut icon" href="favicon.ico">
<link href="main.css" rel="stylesheet" type="text/css" />
<style>
	.table_add_new, .table_add_report {width: max-content; margin: 40px auto;}
	.table_add_new {background: antiquewhite; padding: 10px; border-radius: 10px;}
	th {font-weight: 400; color: #3397e6;}
	td {border: solid 1px #c7c7c7; padding: 0 10px;}
	td.url {padding-top: 5px; padding-bottom: 5px; max-width: 300px; word-wrap: break-word;}
	form {margin: 5px;}
	.grey {color: #a7a7a7;}
	th div {width: min-content; margin: 0 auto;}
	input[type="text"] {width: 500px; margin: 0 0 10px 0; font-size: 15px; padding: 2px 8px; font-family: monospace;}
	button {padding: 5px 15px; font-size: 15px; font-family: monospace;}
	div.button {width: fit-content; margin: 0 auto;}
	.field {clear:both; text-align:right;}
	label {float:left; margin: 4px 15px 4px 0; font-family: monospace; font-size: 16px;}
	tr.back-red {background: #fff0f0;}
	tr.back-black {background: #bafbbc;}
	.td-red {background: #ffd1c0;}
</style>
</head>
<body>

<?php
 $menu_item = 0;
 generateMenu();
?>

<div class="table_add_new">
		<form action='z_NOT_WORKING_price_checker_page.php' method='post' name='form_add' id="form_insert">
			<div class="field">
				<label>Комментарий</label>
				<input type='text' name='form_comment' placeholder="Комментарий"  autocomplete="off" form="form_insert" />
			</div>
			<div class="field">
				<label>Наш URL</label>
				<input type='text' name='form_our_url' placeholder="Наш URL"  autocomplete="off" form="form_insert" />
			</div>
			<div class="field">
				<label>URL поставщика</label>
				<input type='text' name='form_compare_url' placeholder="URL поставщика"  autocomplete="off" form="form_insert" />
			</div>
			<div class="button">
				<button type='submit'>Добавить к сравнению</button>
			</div>
		</form>
</div>
<div class="table_add_report">
<table><tr><th>id</th><th>комментарий</th><th>наш URL</th><th><div>наша цена</div></th><!--<th>vendor</th>--><th>URL поставщика</th><th><div>цена поставщика</div></th><th><div>дата последней проверки</div></th><th></th>
<?php 
foreach ($arr_queries as $value){ 

	$class_td = "td-none";
	if (!in_array($value->compare_name, $vendors)){
		$class_td = "td-red";
	}
	
	$class = "back-black";
	if ($value->our_last_price != $value->compare_last_price) $class = "back-red";

	?> 
	<tr class="<?php echo $class; ?>">

		<!-- ID -->
		<td><?php echo $value->id ?></td>

		<!-- комментарий -->
		<td><?php echo $value->comment ?></td>

		<!-- наш URL -->
		<td class="url"><?php echo '<a href="'.$value->our_url.'" class="grey" target="_blank" rel="nofollow noopener">'.$value->our_url.'</a>' ?></td>

		<!-- наша цена -->
		<td>    
			<?php echo parsing_mirtelcom_price($value->our_id); ?>
		</td>

		<!-- vendor -->
		<!--<td>
			<?php echo $value->compare_name; ?>
		</td>-->

		<!-- URL поставщика -->
		<td class="url <?php echo $class_td; ?>"><?php echo '<a href="'.$value->compare_url.'" class="grey" target="_blank" rel="nofollow noopener">'.$value->compare_url.'</a>' ?></td>

		<!-- цена поставщика -->
		<td>
			<?php
			if ($value->compare_last_price == 0) echo "нет данных";
			else echo $value->compare_last_price;
			?>
		</td>

		<!-- дата последней проверки -->
		<td>
			<?php
			if ($value->last_check == 0) echo "нет данных";
			else {
				$d = new DateTime();
				$d->setTimestamp($value->last_check);
				$date = $d->format('d.m.Y H:m:s');
				echo "<center>".str_replace(' ','<br>',$date)."</center>";
			}
			?>		
		</td>
		<td style="padding: 0;">
		<form action='z_NOT_WORKING_price_checker_page.php' method='post' name='form_del' id='form_delete<?php echo $value->id; ?>'>
			<input type="hidden" name="delete_query" value="<?php echo $value->id; ?>" form="form_delete<?php echo $value->id; ?>">
			<button type='submit' form="form_delete<?php echo $value->id; ?>">x</button>
		</form>
		</td>

	</tr>
	<?php } 


?>



</table>
</div>