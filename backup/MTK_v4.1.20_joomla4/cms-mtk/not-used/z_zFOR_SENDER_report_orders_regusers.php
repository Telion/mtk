<?php
require_once 'connectionDB.php';

?>

<html>
<head>
	<title>cmsMTK. Зарегистрированные покупатели</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link href="main.css" rel="stylesheet" type="text/css" />
</head>
<body>

<?php
 $menu_item = 0;
 generateMenu();
?>

<div style="width: fit-content; margin: 0 auto;">
<div style="font-size: 20px; font-weight: 700; padding: 14px; padding-left: 110px;">Сумма покупок зарегистрированных пользователей</div>

<?php
// Формирую список
$db->setQuery('
SELECT 
	wxa43_users.id, 
	wxa43_users.name, 
	wxa43_users.email, 
	sum(wxa43_virtuemart_orders.order_subtotal) as summa 
FROM
	wxa43_users, 
	wxa43_virtuemart_orders 
WHERE 
	wxa43_users.id = wxa43_virtuemart_orders.virtuemart_user_id and
	wxa43_users.id != 408 and
	wxa43_users.id != 417
GROUP BY wxa43_virtuemart_orders.virtuemart_user_id 
ORDER BY `summa` DESC
');
$arr_products = $db->loadObjectList();

?>

	<table class='zebra-table reserv'>
	<tr><th>Код заказа</th><th>Имя покупателя</th><th>E-Mail покупателя</th><th>Сумма покупок</th></tr>

<?php
foreach ($arr_products as $product) {
	$pole1 = $product->id;
	$pole2 = $product->name;
	$pole3 = $product->email;
	$pole4 = round($product->summa);
	echo "\t".'<tr><td class="td-right">'.$pole1.'</td><td>'.$pole2.'</td><td>'.$pole3.'</td><td class="td-right">'.$pole4.' руб.</td></tr>'."\r\n";

}
?>
	</table>
</div>