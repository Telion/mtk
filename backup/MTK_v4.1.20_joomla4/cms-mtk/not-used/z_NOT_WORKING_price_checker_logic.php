<?php
require_once 'connectionDB.php';

$periodicity = 2; // как часто перепроверять цены (в днях)

?>

<html>
<head>
<title>логика</title>
<link rel="shortcut icon" href="favicon.ico">
</head>
<body>

<?php
$vendors = ["dns-shop", "citilink", "shop.nag"];

$db->setQuery('
	(SELECT * FROM (
		SELECT * FROM `tln_compare_url` WHERE `last_check` = 0 and `compare_name` = "'.$vendors[0].'"
		UNION
		SELECT * FROM `tln_compare_url` WHERE `last_check` <> 0  and `compare_name` = "'.$vendors[0].'" ORDER BY `last_check` ASC
		) as tbls1 LIMIT 1)
	UNION
	(SELECT * FROM (
		SELECT * FROM `tln_compare_url` WHERE `last_check` = 0 and `compare_name` = "'.$vendors[1].'"
		UNION
		SELECT * FROM `tln_compare_url` WHERE `last_check` <> 0  and `compare_name` = "'.$vendors[1].'" ORDER BY `last_check` ASC
		) as tbls2 LIMIT 1)
	UNION
	(SELECT * FROM (
		SELECT * FROM `tln_compare_url` WHERE `last_check` = 0 and `compare_name` = "'.$vendors[2].'"
		UNION
		SELECT * FROM `tln_compare_url` WHERE `last_check` <> 0  and `compare_name` = "'.$vendors[2].'" ORDER BY `last_check` ASC
		) as tbls3 LIMIT 1)
	');
$arr_compares = $db->loadObjectList(); // id, comment, our_url, our_last_price, compare_url, compare_last_price, last_check

// Перебираю записи, у которых давно не обнавлялась цена
foreach ($arr_compares as $product){

	// Если цена не обнавлялась совсем или не обнавлялась более 2ух суток
	if (($product->last_check == 0) or (((time()-$product->last_check)/24/60/60) > $periodicity)){
		echo $product->id." / ".$product->compare_name.". Запускаю парсинг<hr>";
		$current_price = parsing($product->compare_url, $product->compare_name);
//$current_price = 9999999;
		if ($current_price === "NO") echo "ALARM: не умею парсить этого поставщика: ".$product->compare_name;
		else {
			echo "Старая цена поставщика: ".$product->compare_last_price."<br>";
			echo "Текущая цена поставщика: ".$current_price."<br>";
			if (($current_price != $product->compare_last_price) and ($current_price != 0)){
				echo "Цена поставщика: Цены отличаются, меняю цену в базе<br>";
				$db_insert = JFactory::getDBO();
				$db_insert->setQuery('UPDATE tln_compare_url SET compare_last_price='.$current_price.', last_check='.time().' WHERE id='.$product->id);
				$db_insert->execute();
			}
			else echo "Цена поставщика: Цены не отличаются, не меняю.<br>";

			// ПАРСИНГ МТК
			$current_id_mtk = parsing_mirtelcom_id($product->our_url);
			$current_price_mtk = parsing_mirtelcom_price($current_id_mtk);
			echo "Наша цена: ".$current_price_mtk."<br>";
			if ($current_price_mtk != $product->our_last_price){
				echo "Наша цена: Цены отличаются, меняю цену в базе<br>";
				$db_insert = JFactory::getDBO();
				$db_insert->setQuery('UPDATE tln_compare_url SET our_last_price='.$current_price_mtk.' WHERE id='.$product->id);
				$db_insert->execute();
			}
			else echo "Наша цена: Цены не отличаются, не меняю.<br>";
	
			if ($current_price != $current_price_mtk){
				echo '<span style="color: red;">Наша цена и цена поставщика отличаются</span><br>';
			}
			else echo "У нас такая же цена, как и у поставщика.<br>";
		}
	}
	else echo $product->id." / ".$product->compare_name.". Проверять цены рано.<br>";
echo '<hr>';
}

?>
