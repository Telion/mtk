<?php 
	require_once 'header.php';
?>

<h2 style="margin-left: 30px;"> Метод getItemsAvail() </h2>

	<div class="other-block lightblue">
		<p><center><strong>Мои категории:</strong></center></p>
		<p>ML33 - ML3334 - Шкафы серверные</p>
		<p>Order - ML06 - СЕРВЕРЫ И СХД</p>
		<p>Order - ML04 - СЕТЕВОЕ ОБОРУДОВАНИЕ</p>
		<p>ML16 - ML1612 - Серверы и СХД</p>
		<p>ML16 - ML1606 - Сетевое Оборудование</p>
	</div><br>

<?php 

/*
	Входные параметры:
		cat_id (string) - ОБЯЗАТЕЛЬНЫЙ (или item_id) - код товарной группы (категории)
		item_id (ArrayOfString) - ОБЯЗАТЕЛЬНЫЙ (или cat_id) - массив скодами товаров
		shipment_method (string) - ОБЯЗАТЕЛЬНЫЙ - код метода отгрузки (используется значение из справочника getShipmentMethods())
		shipment_date (string) - ОБЯЗАТЕЛЬНЫЙ - дата отгрузки
		only_avail (string) - признак вывода по доступности товаров (0 - все активные товары; 1 - только доступные для резервирования.
	Выходные параметры:
		No (string) - код товара
		PriceClient (float) - цена клиента, в USD
		PriceClient_RG (float) - цена клиента для региона, в USD
		AvailableClient (int) - доступное количество товара
		AvailableClient_RG (int) - доступное количество товара на региональном складе
		AvailableExpected (int) - ожидаемый приход товара
		AvailableExpectedNext (int) - следующий приход товара
		DateExpectedNext (string) - дата следующего прихода товара
		RRP (float) - рекомендованная розничная цена, в руб
		PriceClientRUB (float) - цена клиента, в руб
		PriceClientRUB_RG (float) - цена клиента для региона, в руб
		Online_Reserve (int) - признак онлайн резервирования (0 - да разрешено не дефицитный; 1 - нет запрещено не просрочен; 2 - платная отмена не просрочен; 3 - дефицит не просрочен; 4 - нет запрещено просрочен; 5 - платная отмена просрочен; 6 - дефицит просрочен
		ReserveCost (float) - стоимость отмены резерва, $/шт * час (5 знаков после запятой)
		другие нам не очень подходят
	Комментарии:
		Последнюю страницу можно определить по количеству строк, которое меньше чем rows_on_page или при получении пустого ответа.
		
*/
$api = apiConnected();

// Получаю метод доставки
$shipmentMethods = getShipmentMethods($api, $code = '');
foreach ($shipmentMethods->getShipmentMethodsResult->item as $row) { 
	if ($row->IsDefault) $shipmentMethod = $row->Code;
}
echo "Метод доставки: ".$shipmentMethod."<br>";

// Получаю дату доставки
$shipmentDates = getShipmentDates($api, $shipmentMethod);
$shipmentDate = $shipmentDates->getShipmentDatesResult->item[0]->Date;
echo "Дата доставки: ".$shipmentDate."<br>";

$items = getItemsAvail($api, $catId = '', $itemId = array('1080104', "1089805"), $shipmentMethod, $shipmentDate);

foreach ($items->getItemsAvailResult->item as $row) { 
	echo $row->No." ___ ".$row->AvailableClient." ___ ".$row->PriceClientRUB." ___ ".$row->AvailableClient_RG." ___ ".$row->PriceClientRUB_RG."<br>"; 
} 

?> 