<?php 
	require_once 'header.php';
?>

<h2 style="margin-left: 30px;"> Метод getShipmentDates() </h2>

<?php 

/*
	Входные параметры:
		code (string) - код даты отгрузки
		ShipmentMethodCode (string) - код метода отгрузки
	Выходные параметры:
		Date (string) - дата отгрузки
*/
$api = apiConnected();

$shipmentDates = getShipmentDates($api, $sklad = 'ЕКБ_СRS_ДС');

foreach ($shipmentDates->getShipmentDatesResult->item as $row) { 
	echo $row->Date."<br>"; 
}

printr($shipmentDates->getShipmentDatesResult->item[0]->Date);

?> 