<?php 
	require_once 'header.php';
?>

<h2 style="margin-left: 30px;"> Метод getItems() </h2>

	<div class="other-block lightblue">
		<p><center><strong>Мои категории:</strong></center></p>
		<p>ML33 - ML3334 - Шкафы серверные</p>
		<p>Order - ML06 - СЕРВЕРЫ И СХД</p>
		<p>Order - ML04 - СЕТЕВОЕ ОБОРУДОВАНИЕ</p>
		<p>ML16 - ML1612 - Серверы и СХД</p>
		<p>ML16 - ML1606 - Сетевое Оборудование</p>
	</div><br>

<?php 

/*
	Входные параметры:
		cat_id (string) - ОБЯЗАТЕЛЬНЫЙ (или item_id) - код товарной группы (категории)
		item_id (ArrayOfString) - ОБЯЗАТЕЛЬНЫЙ (или cat_id) - массив скодами товаров
		shipment_method (string) - код метода отгрузки (используется значение из справочника getShipmentMethods())
		page (int) - номер страницы (по умолчанию равен 0, т.е. постраничный вывод выключен)
		rows_on_page (int) - количество записей на странице (по умолчанию 10000 строк)
		last_time_change (string) - изменения, начиная с указанного момента времени (в формате YYYY-MM-DDTHH:MM:SS, по умолчанию все товары)
	Выходные параметры:
		No (string) - код товара
		Name (string) - наименование товара
		другие нам не очень подходят
	Комментарии:
		Последнюю страницу можно определить по количеству строк, которое меньше чем rows_on_page или при получении пустого ответа.
		
*/
$api = apiConnected();

// getItems($api, $catId = 'ML06', $itemId = array("1033548","1089805","1089975"), $shipmentMethod = 'С/В', $Page = 1, $rowsOnPage = 10, $lastTimeChange = '');
$items = getItems($api, $catId = '', $itemId = array('1080104', "1089805"), $shipmentMethod = 'С/В', $Page = 1, $rowsOnPage = 10, $lastTimeChange = '');

foreach ($items->getItemsResult->item as $row) { 
	echo $row->No." ___ ".$row->Name."<br>"; 
} 

?> 