<?php 
	require_once 'header.php';
?>

<h2 style="margin-left: 30px;"> Метод getShipmentMethods() </h2>

<?php 

/*
	Входные параметры:
		code (string) - код метода отгрузки
	Выходные параметры:
		code (string) - код метода отгрузки
		Description (string) - описание метода отгрузки
		IsDefault (int) - используется по умолчанию, если =1
	Комментарии:
		код метода отгрузки определяет с какого склада будет отгружаться товар и каким способом (самовывоз/доставка)
*/
$api = apiConnected();

$shipmentMethods = getShipmentMethods($api, $code = '');

//printr($shipmentMethods->getShipmentMethodsResult->item[0]->Code);

foreach ($shipmentMethods->getShipmentMethodsResult->item as $row) { 
	echo $row->Code." ___ ".$row->Description." ___ ".$row->IsDefault."<br>"; 
	if ($row->IsDefault) $shipmentMethod = $row->Code;
}

printr($shipmentMethod);

?> 