<head>
	<link href="main.css" rel="stylesheet" type="text/css" />
</head>


<body>
<div class="merlion-main">
<ul>
	<li class="merlion-block"><a href="https://www.mirtelcom.ru/merlion/methods-getItems.php">getItems() - Получить название и характеристики товара</a></li>
	<li class="merlion-block"><a href="https://www.mirtelcom.ru/merlion/methods-getCatalog.php">getCatalog() - Получить подкатегории</a></li>
	<li class="merlion-block"><a href="https://www.mirtelcom.ru/merlion/methods-getItemsAvail.php">getItemsAvail() - Получить цены и количество</a></li>
	<li class="merlion-block"><a href="https://www.mirtelcom.ru/merlion/methods-getShipmentDates.php">getShipmentDates() - Получить доступные даты отгрузки</a></li>
	<li class="merlion-block"><a href="https://www.mirtelcom.ru/merlion/methods-getShipmentMethods.php">getShipmentMethods() - Получить доступные коды отгрузки</a></li>
	<li class="merlion-block"><a href="https://www.mirtelcom.ru/merlion/methods-getItemsProperties.php">getItemsProperties() - Получить характеристики</a></li>
	<hr>

	<li class="merlion-block"><a href="https://www.mirtelcom.ru/merlion/PDF-1.pdf">Техническое описание API 3.0.pdf</a></li>
	<li class="merlion-block"><a href="https://www.mirtelcom.ru/merlion/PDF-2.pdf">Рекомендации по работе с методами API.pdf</a></li>
	<li class="merlion-block"><a href="https://www.mirtelcom.ru/merlion/PDF-3.pdf">Регламент обращения в службу поддержки API.pdf</a></li>
	<li class="merlion-block"><a href="https://www.mirtelcom.ru/merlion/PDF-4.pdf">Процесс подключения Партнеров - API интеграция.pdf</a></li>
	<hr>

	<li class="merlion-block"><a href="https://www.mirtelcom.ru/merlion/temp.php">temp.php</a></li>
</ul>
</div>

<div class="footer-blocks">
<div class="footer-blocks-flex">

	<div class="other-block bisque">
		<p><center><strong>Мои коды доставки:</strong></center></p>
		<p>ЕКБ_СRS_ДС - Доставка 605</p>
		<p>РЕКБ_2_ДС - Доставка со склада РЕКБ_2</p>
	</div>

	<div class="other-block lightblue">
		<p><center><strong>Мои категории:</strong></center></p>
		<p>ML33 - ML3334 - Шкафы серверные</p>
		<p>Order - ML06 - СЕРВЕРЫ И СХД</p>
		<p>Order - ML04 - СЕТЕВОЕ ОБОРУДОВАНИЕ</p>
		<p>ML16 - ML1612 - Серверы и СХД</p>
		<p>ML16 - ML1606 - Сетевое Оборудование</p>
	</div>
</div>
</div>
</body>