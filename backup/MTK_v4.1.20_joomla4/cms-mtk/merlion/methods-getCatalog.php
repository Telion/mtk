<?php 
	require_once 'header.php';
?>

<h2 style="margin-left: 30px;"> Метод getCatalog() </h2>

<?php 

/*
	Входные параметры:
		cat_id / String / ОБЯЗАТЕЛЬНЫЙ ПАРАМЕТР - код товарной группы (категории)
		можно использовать значения All(категории всех уровней) и Order(список всех направлений)
	Выходные параметры:
		ID (String) - код группы	
		ID_PARENT (String) - код родительской группы
		Description (String) - название товарной группы
*/
$api = apiConnected();

$cat = getCatalog($api, 'All');

foreach ($cat->getCatalogResult->item as $row) { 
	echo $row->ID_PARENT." - ".$row->ID." - ".$row->Description."<br>"; 
} 



?> 
