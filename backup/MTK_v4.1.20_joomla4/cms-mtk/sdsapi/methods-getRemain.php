<?php 
	require_once 'header.php';
?>

<h2 style="margin-left: 30px;"> Метод getRemain() </h2>

<?php 

/*
	Входные параметры:
		cat_id (string) - ОБЯЗАТЕЛЬНЫЙ (или item_id) - код товарной группы (категории)
		item_id (ArrayOfString) - ОБЯЗАТЕЛЬНЫЙ (или cat_id) - массив скодами товаров
		shipment_method (string) - код метода отгрузки (используется значение из справочника getShipmentMethods())
		page (int) - номер страницы (по умолчанию равен 0, т.е. постраничный вывод выключен)
		rows_on_page (int) - количество записей на странице (по умолчанию 10000 строк)
		last_time_change (string) - изменения, начиная с указанного момента времени (в формате YYYY-MM-DDTHH:MM:SS, по умолчанию все товары)
	Выходные параметры:
		No (string) - код товара
		Name (string) - наименование товара
		другие нам не очень подходят
	Комментарии:
		Последнюю страницу можно определить по количеству строк, которое меньше чем rows_on_page или при получении пустого ответа.
		
*/

$url = "https://api-rexant.ru/api/v1/remain?productid__article=12-4721";

		$opt = [
			"http" => [
				"method" => "GET",
				"header" => "Accept: application/json\r\nAuthorization: Token 350f169dc59fd14a606c9c6a87c1ed1d7a4b21e1\r\n"]
		];

		$context = stream_context_create($opt);

		printr($opt);
		printr(json_decode(file_get_contents($url, false, $context), true));
	


?> 