<?php 
	require_once 'header.php';
?>

<h2 style="margin-left: 30px;"> <a href="https://www.mirtelcom.ru/sdsapi/report-sdsRemain.html" target="blank">Метод getRemain()</a> - получение остатков на складе СДС </h2>


<?php 

/*
	Входные параметры:
		cat_id (string) - ОБЯЗАТЕЛЬНЫЙ (или item_id) - код товарной группы (категории)
		item_id (ArrayOfString) - ОБЯЗАТЕЛЬНЫЙ (или cat_id) - массив скодами товаров
		shipment_method (string) - код метода отгрузки (используется значение из справочника getShipmentMethods())
		page (int) - номер страницы (по умолчанию равен 0, т.е. постраничный вывод выключен)
		rows_on_page (int) - количество записей на странице (по умолчанию 10000 строк)
		last_time_change (string) - изменения, начиная с указанного момента времени (в формате YYYY-MM-DDTHH:MM:SS, по умолчанию все товары)
	Выходные параметры:
		No (string) - код товара
		Name (string) - наименование товара
		другие нам не очень подходят
	Комментарии:
		Последнюю страницу можно определить по количеству строк, которое меньше чем rows_on_page или при получении пустого ответа.
		
*/

static $RESOURSE = 'sds';
$date = Date('d.m.Y', time());

class Product {
	public $articles = []; // массив артикулов
	public $countProducts = ''; // общее количество товара
	public $unit; // ед изм товара
	public $sdsName; // название товара на СДС
	public $reelSize; // размер бухты
	public $reelSizeTmpl = ['305', '100', '50']; // какие размеры ищу в названии товара
	public $id;
	public $name;
	public $gtin;
	public $countArticles = 0;
	public $prevID = '';
	public $sdsprice;
	public $mtkprice;
	public $mtkurl;
	public $sdsurl;
	public $article;
	
	function Product($article, $entry) { // конструктор записывает артикул в объект
		$this->articles = $article;
		$this->id = $entry['virtuemart_product_id'];
		$this->name = $entry['product_name'];
		$this->gtin = $entry['product_gtin'];
		$this->mtkprice = round($entry['price'], 2);
		$this->mtkurl = '<a target="_blank" href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$this->id.'">'.$this->name.'</a>';
	}


	public function arrayHandler($report) { // получает массив артикулов и вызывает обработку каждого значения массива
		foreach ($this->articles as $value) {
			echo "<hr>";
			$this->reelSize = 0;
			$this->article = $value;
			$result = $this->getProduct();
			if ($result > 0) {
				$this->countArticles++;
				$this->getRemain();
				$this->getPrices();
				$report->addToGeneralReport($this);
			} else {
				echo "Не подходящий артикул<br>"; 
			}
		}
		return $this->countProducts;
	}

	public function queryToSDS($method) { // запрос к API СДС и получение остатков товаров по артикулу
		$prefix = $this->getPrefixSDS($method);
		if ($prefix) {
			$url = "https://api-rexant.ru/api/v1/".$prefix.$this->article;
			$opt = [
				"http" => [
					"method" => "GET",
					"header" => "Accept: application/json\r\nAuthorization: Token 350f169dc59fd14a606c9c6a87c1ed1d7a4b21e1\r\n"]
			];
			$context = stream_context_create($opt);
			$queryResult = json_decode(file_get_contents($url, false, $context), true);
		} else {
			return 0;
		}

		return $queryResult;
	}

	public function getPrefixSDS($method) { // Сгенерировать префикс для подключения к нужному методу API сдс
		if ($method === 'product') {
			$prefix = 'product?article=';
		} elseif ($method === 'remain') {
			$prefix = 'remain?productid__article=';
		} elseif ($method === 'prices') {
			$prefix = 'prices?productid__article=';
		} else {
			echo "Неизветный метод API<br>";
			return 0;
		}

		return $prefix;
	}

	public function searchInText($search) { // Поиск размера бухты в названии СДС
		if (strpos($this->sdsName, $search)) {
			$this->reelSize = $search + 0;
			return 1;
		}

		return 0;
	}

	public function getReelSize() { // Получить объем бухты
		if ($this->unit === 'бухта') {
			foreach ($this->reelSizeTmpl as $value) {
				$result = $this->searchInText($value);
				if ($result) break;
			}
		}
	}

	public function getProduct() { // Получить название и ед.изм по текущему товару СДС
		if ($this->article) {
			$queryResult = $this->queryToSDS('product');
			if ($queryResult['count'] > 0) {
				$this->unit = $queryResult['results'][0]['unit'];
				$this->sdsName = $queryResult['results'][0]['name'];
				$this->sdsurl = '<a target="_blank" href="https://www.sds-group.ru/search.htm?search='.$this->article.'">'.$this->article.'</a>';
				$this->getReelSize();

				return $queryResult;
			} else {
				return 0;
			}
		} else {
			return -1;
		}

	}

	public function getRemain() {  // Получить остатки по текущему товару СДС
		if ($this->article) {
			$queryResult = $this->queryToSDS('remain');
			if ($queryResult['count'] > 0) {
				if ($this->reelSize != 0) $count = $queryResult['results'][0]['attribute']['count'] * $this->reelSize;
				else $count = $queryResult['results'][0]['attribute']['count'];
				$this->countProducts += $count;
				echo " - <span style='color: red;'>".$this->countProducts."</span>";

				return $queryResult;
			} else {
				return 0;
			}
		} else {
			return -1;
		}
	}

	public function getPrices(){  // Получить остатки по текущему товару СДС
		if ($this->article) {
			$queryResult = $this->queryToSDS('prices');
//printr($queryResult);
			if ($queryResult['count'] > 0) {
				$this->sdsprice = $queryResult['results'][0]['attribute']['value'];

				return $queryResult;
			} else {
				return 0;
			}
		} else {
			return -1;
		}
	}
}


class OurDB { // Получение данных из БД по запросу
	public $db; // подключение к БД МТК

	public function OurDB($db){ //
		$this->db = $db;
	}

	public function getProductArticle() { // Получение записей с товарами, у которых заполнено поле product_gtin из БД МТК
		$this->db->setQuery('
			SELECT 
			    wxa43_virtuemart_products.virtuemart_product_id, 
			    intnotes, 
			    product_gtin, 
			    product_name, 
			    product_unit,
			    IF (override = 1, product_override_price, product_price) as price
			FROM 
			    wxa43_virtuemart_products, 
			    wxa43_virtuemart_products_ru_ru,
			    wxa43_virtuemart_product_prices
			WHERE 
			    (wxa43_virtuemart_products.virtuemart_product_id = 30 OR
			    wxa43_virtuemart_products.virtuemart_product_id = 31 OR
			    wxa43_virtuemart_products.virtuemart_product_id = 130 OR
			    wxa43_virtuemart_products.virtuemart_product_id = 140) AND
			    wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND
			    wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_product_prices.virtuemart_product_id AND
			    product_gtin != ""
			');
		$result = $this->db->loadAssocList('virtuemart_product_id');		
		
		return $result;
	}

	public function setProductGtin($RESOURSE, $date, $id, $value) { // обновить поле product_gtin в базе МТК
		$value = $RESOURSE.", ".$date." / ".$value;
		$query = 'UPDATE wxa43_virtuemart_products SET product_gtin="'.$value.'" WHERE virtuemart_product_id = '.$id;
		printr($query);
		$this->db->setQuery($query);
		$this->db->execute();
	}

}

class Report implements Helper {
	public $generalReportFile; // имя файла отчета
	public $priceReportFile; // имя файла отчета
	public $countUnderstatedPrice = 0;

	public function Report($generalReportFile, $priceReportFile) { // создание нового отчета
		$this->generalReportFile = $generalReportFile;
		$this->createGeneralReport();

		$this->priceReportFile = $priceReportFile;
		$this->createPriceReport();
	}

	public function createUniversalHeaderReport() {
		$reportString = '<html>
		<head>
		<title>Report: Anker vs SDS (от '.date("d.m.Y").')</title>
		<link rel="shortcut icon" href="favicon.ico">
		<link href="main.css" rel="stylesheet" type="text/css" />
		</head>
		<style>
			table {border: 1px solid #494540; border-collapse: collapse;} 
			th {color: #fff; font-weight: 400; font-size: 17px; padding: 10px; width: 80px; border: solid 1px #494540; background: #494540;} 
			td {border-left: solid 0; border-right: solid 0;font-size: 14px; color: #666; text-align: center; border: solid 1px #494540; padding: 6px; border-bottom: none;} 
			.border-none {border-top: solid 1px white;}

			.zebra-table tr:nth-child(odd) {background:#e4ebed;}
			.zebra-table tr:hover {background:lightblue;}
			a {color: #555; text-decoration: none;}
			.red {color: red; font-weight: 700;}
			.right {text-align: right; padding-right: 25px;}
			.left {text-align: left; padding-left: 25px;}

		</style>
		<body>
		<center><table class="zebra-table"><tr style="background: lightgreen;">';
		
		return $reportString;
	}

	public function createGeneralReport() {
		$reportString = $this->createUniversalHeaderReport();
		$reportString .= '
			<th>ID MTK</th>
			<th style="width: 110px">Артикул СДС</th>
			<th style="width: 110px">Название МTK</th>
			<th>Цена закупа СДС</th>
			<th>Цена на МТК</th>
			<th>Кол-во было</th>
			<th>Кол-во стало</th>
			<th>Изменение?</th>
			<th style="width: 110px">Дата</th></tr>';
		$fdreport = fopen($this->generalReportFile, 'w') or die("не удалось создать файл");
		fwrite($fdreport, $reportString);
		fclose($fdreport);
	}

	public function createPriceReport() {
		$reportString = $this->createUniversalHeaderReport();
		$reportString .= '
			<th>ID MTK</th>
			<th style="width: 110px">Артикул СДС</th>
			<th style="width: 110px">Название МTK</th>
			<th>Цена закупа СДС</th>
			<th>Цена на МТК</th>
			<th style="width: 110px">Дата</th></tr>';
		$fdreport = fopen($this->priceReportFile, 'w') or die("не удалось создать файл");
		fwrite($fdreport, $reportString);
		fclose($fdreport);
	}

	public function addToGeneralReport($product) { // добавить строку с текущим товаром СДС к отчету
		$itWas = explode(' / ', $product->gtin)[1];
		$change = '';

		if ($itWas > $product->countProducts) $change = 'down';
		elseif ($itWas < $product->countProducts) $change = 'up';
	
		$class = '';
		if ($product->prevID == $product->id) $class = 'border-none';
		$product->prevID = $product->id;

		if ($product->mtkprice < $product->sdsprice) {
			$priceclass = 'red';
			$addValue = " (".($product->mtkprice - $product->sdsprice).")";
			$this->countUnderstatedPrice++;
			$this->addToPriceReport($product);
		}
		
		$code = '<tr class="line">
			<td class="'.$class.'">'.$product->id.'</td>
			<td class="'.$class.' left">'.$product->sdsurl.'</td>
			<td class="left '.$class.'">'.$product->mtkurl.'</td>
			<td class="'.$class.' '.$priceclass.' right">'.$product->sdsprice.'</td>
			<td class="'.$class.' '.$priceclass.' right">'.$product->mtkprice.$addValue.'</td>
			<td class="'.$class.' right">'.$itWas.'</td>
			<td class="'.$class.' right">'.$product->countProducts.'</td>
			<td class="'.$class.'">'.$change.'</td>
			<td class="'.$class.'">'.Date("d.m.Y", time()).'</td>
			</tr>';
		// Добавление записи по текущему товару к HTML отчету
		$fdhtml = fopen($this->generalReportFile, 'a') or die("не удалось создать файл");
		fwrite($fdhtml, $code."\r\n");
		fclose($fdhtml);
	}

	public function addToPriceReport($product) {
		$class = '';
		if ($product->prevID == $product->id) $class = 'border-none';
		$product->prevID = $product->id;

		$code = '<tr class="line">
			<td class="'.$class.'">'.$product->id.'</td>
			<td class="'.$class.' left">'.$product->sdsurl.'</td>
			<td class="left '.$class.'">'.$product->mtkurl.'</td>
			<td class="'.$class.' right">'.$product->sdsprice.'</td>
			<td class="'.$class.' right">'.$product->mtkprice.$addValue.'</td>
			<td class="'.$class.'">'.Date("d.m.Y", time()).'</td>
			</tr>';
		// Добавление записи по текущему товару к HTML отчету
		$fdhtml = fopen($this->priceReportFile, 'a') or die("не удалось создать файл");
		fwrite($fdhtml, $code."\r\n");
		fclose($fdhtml);		
	}

	public function sendEmail() { // отправка письма с отчетом
		// Отправка E-Mail
		$fdreport = file_get_contents($this->generalReportFile);
		$to  = "Telion <www.neon@mail.ru>"; 
		$subject = "API: MTK vs SDS."; 
		$message = $fdreport; 
		$headers  = "Content-type: text/html; charset=utf-8 \r\n";
		$headers .= "From: Mr. MTK72 bot <postmaster@mirtelcom.nichost.ru>\r\n"; 
		mail($to, $subject, $message, $headers); 
	}

	public function sendTelegram($text) {
		$token = '914611381:AAH-o_IN7-9SEK7RDOLHeqYUgSCsbCFvwgE';
		$chatId = '440401693';
		$ch = curl_init();
		curl_setopt_array(
			$ch,
			array(
				CURLOPT_URL => 'https://api.telegram.org/bot'.$token.'/sendMessage',
				CURLOPT_POST => TRUE,
				CURLOPT_RETURNTRANSFER => TRUE,
				CURLOPT_TIMEOUT => 10,
				CURLOPT_POSTFIELDS => array(
					'chat_id' => $chatId,
					'text' => $text,
				),
			)
		);
		curl_exec($ch);
	}        

	public function closeReport($file) { // дописать код окончания отчета
		$fdhtml = fopen($file, 'a') or die("не удалось создать файл");
		fwrite($fdhtml, '</table><center><p style="font-weight: 700; font-size: 16px; color: green;">Скрипт закончил работу</p>');
		fclose($fdhtml);
	}
}

interface Helper {
	public function sendEmail();
}

$report = new Report("report_sdsRemain.html", "report_sdsRemainPrice.html");

$db = JFactory::getDBO();
$fromDB = new OurDB($db);
$arrProductsArticle = $fromDB->getProductArticle();

$i = 5;
foreach ($arrProductsArticle as $key => $value) {

	$arrProductsArticle[$key]['intnotes'] = preg_replace('/\s/', '', $value['intnotes']);
	$arrTemp = explode(',', $arrProductsArticle[$key]['intnotes']);

	$product = new Product($arrTemp, $value);
	$generalCount = $product->arrayHandler($report);

	if ($generalCount !== '') {
		echo "<hr>Записать в базу ";
		$fromDB->setProductGtin($RESOURSE, $date, $key, $generalCount);
		echo "ID: ".$key.", общее количество = ".$generalCount."<hr style='border: solid 1px red;'>";
	}
	
	$arr_products[] = $product;

if ($i < 1) break;
$i--;
}

$report->closeReport($report->generalReportFile);
$report->closeReport($report->priceReportFile);

if ($report->countUnderstatedPrice > 0) $report->sendTelegram("На МТК есть товары дешевле чем на СДС, ".$report->countUnderstatedPrice." шт.");
//$report->sendEmail();

