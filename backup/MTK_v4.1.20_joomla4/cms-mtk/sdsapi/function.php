<?php 
require_once 'connectionDB.php';


function apiConnected(){
	class API {
		public $wsdl_url;
		public $params;
	}
	
	$api = new API();
	$api->wsdl_url = "https://apitest.merlion.com/dl/mlservice3?wsdl"; 
 	$api->params = array('login' => "TC0042421|API" 
        , 'password' => "XPE33UcS90" 
        , 'encoding' => "UTF-8" 
        , 'features' => SOAP_SINGLE_ELEMENT_ARRAYS 
    ); 
    return $api;
}

function getCatalog($api, $query) {
	try { 
		$client = new SoapClient($api->wsdl_url, $api->params); 
	        $cat = $client->getCatalog($query);

		return $cat;

	} catch (SoapFault $E) { 
        	echo $E->faultstring; 
	} 
}

function getItems($api, $catId = '', $itemId = '', $shipmentMethod = '', $Page = '', $rowsOnPage = '', $lastTimeChange = '') {
	try { 
		$client = new SoapClient($api->wsdl_url, $api->params); 
 
        	class Request { 
	               public $cat_id; 
        	       public $item_id; 
	               public $shipment_method; 
	               public $page; 
        	       public $rows_on_page; 
	               public $last_time_change; 
        	}           
     
	        $req = new Request();     
        	$req->cat_id = $catId; 
	        $req->item_id = $itemId; 
	        $req->page = $Page; 
	        $req->rows_on_page = $rowsOnPage;
	        $req->shipment_method = $shipmentMethod; 
	        $req->last_time_change = $lastTimeChange; 
     
	        $items = $client->getItems($req); 
       
		return $items;

	} catch (SoapFault $E) { 
	        echo $E->faultstring; 
	} 
}

function getItemsAvail($api, $catId = '', $itemId = '', $shipmentMethod = '', $shipmentDate = '') {
	try { 
		$client = new SoapClient($api->wsdl_url, $api->params); 
 
        	class Request { 
	               public $cat_id; 
        	       public $item_id; 
	               public $shipment_method; 
	               public $shipmentDate; 
        	}           
     
	        $req = new Request();     
        	$req->cat_id = $catId; 
	        $req->item_id = $itemId; 
	        $req->shipment_method = $shipmentMethod; 
	        $req->shipmentDate = $shipmentDate; 
     
	        $items = $client->getItemsAvail($req); 
       
		return $items;

	} catch (SoapFault $E) { 
	        echo $E->faultstring; 
	} 
}

function getItemsProperties($api, $catId = '', $itemId = '', $Page = '', $rowsOnPage = '', $lastTimeChange = '') {
	try { 
		$client = new SoapClient($api->wsdl_url, $api->params); 
 
        	class Request { 
	               public $cat_id; 
        	       public $item_id; 
	               public $shipment_method; 
	               public $page; 
        	       public $rows_on_page; 
	               public $last_time_change; 
        	}           
     
	        $req = new Request();     
        	$req->cat_id = $catId; 
	        $req->item_id = $itemId; 
	        $req->page = $Page; 
	        $req->rows_on_page = $rowsOnPage;
	        $req->shipment_method = $shipmentMethod; 
	        $req->last_time_change = $lastTimeChange; 
     
	        $items = $client->getItems($req); 
       
		return $items;

	} catch (SoapFault $E) { 
	        echo $E->faultstring; 
	} 
}



function getShipmentMethods ($api, $code = '') {
	try { 
		$client = new SoapClient($api->wsdl_url, $api->params); 
	        $method = $client->getShipmentMethods($code);

		return $method;

	} catch (SoapFault $E) { 
        	echo $E->faultstring; 
	} 
}

function getShipmentDates ($api, $sklad = '') {
	try { 
		$client = new SoapClient($api->wsdl_url, $api->params); 
	        $method = $client->getShipmentDates($sklad);

		return $method;

	} catch (SoapFault $E) { 
        	echo $E->faultstring; 
	} 
}

function printr ($string) {
	echo "<pre>";
	print_r($string);
	echo "</pre>";
}


?> 
