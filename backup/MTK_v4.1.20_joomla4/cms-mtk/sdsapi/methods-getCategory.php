<?php 
	require_once 'header.php';
?>

<h2 style="margin-left: 30px;"> Метод getCategory() </h2>

<?php 

/*
	Входные параметры:
		cat_id / String / ОБЯЗАТЕЛЬНЫЙ ПАРАМЕТР - код товарной группы (категории)
		можно использовать значения All(категории всех уровней) и Order(список всех направлений)
	Выходные параметры:
		ID (String) - код группы	
		ID_PARENT (String) - код родительской группы
		Description (String) - название товарной группы
*/

	$opt = [
		"http" => [
			"method" => "GET",
			"header" => "Accept: application/json\r\nAuthorization: Token 350f169dc59fd14a606c9c6a87c1ed1d7a4b21e1\r\n"]
	];
	$context = stream_context_create($opt);

	$url = "https://api-rexant.ru/api/v1/category";
	$result = json_decode(file_get_contents($url, false, $context), true);

	printr($result);




?> 
