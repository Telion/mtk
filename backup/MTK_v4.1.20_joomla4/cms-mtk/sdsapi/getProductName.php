<?php 
	require_once 'header.php';
?>

<h2 style="margin-left: 30px;"> Метод getProduct() - получение остатков на складе СДС </h2>


<?php 

/*
	
	ТАБЛИЦА соответствия Названий с СДС и из нашей базы, выдает следующие поля: mtk->ID, sds->article, sds->name, mtk->name, mtk->product_gtin
		
*/

static $RESOURSE = 'sds';
$date = Date('d.m.Y', time());

class Product {
	public $articles = []; // массив артикулов
	public $dbID;
	public $dbIntnotes;
	public $dbGtin;
	public $dbName;
	public $dbUnit;
	
	function Product($article, $entry) { // конструктор записывает артикул в объект
		$this->articles = $article;
		$this->dbID = $entry['virtuemart_product_id'];
		$this->dbIntnotes = $entry['intnotes'];
		$this->dbGtin = $entry['product_gtin'];
		$this->dbName = $entry['product_name'];
		$this->dbUnit = $entry['product_unit'];
	}

	public function arrayHandler() { // получает массив артикулов и вызывает обработку каждого значения массива
		foreach ($this->articles as $value) {
			$this->checkClearArticle($value);
		}
	}

	public function queryToSDS($article) { // запрос к API СДС и получение остатков товаров по артикулу
		$url = "https://api-rexant.ru/api/v1/product?article=".$article;
		$opt = [
			"http" => [
				"method" => "GET",
				"header" => "Accept: application/json\r\nAuthorization: Token 350f169dc59fd14a606c9c6a87c1ed1d7a4b21e1\r\n"]
		];
		$context = stream_context_create($opt);
		$queryResult = json_decode(file_get_contents($url, false, $context), true);


		//printr($queryResult);
		$this->checkWrongArticle($queryResult);
	}

	public function checkClearArticle($article) { // Проверка артикула на пустоту (иначе отображается 1000 товаров)
		if ($article) {
			$this->queryToSDS($article);
		}
	}

	public function checkWrongArticle($queryResult) { // Проверка артикула на наличие в базе СДС
		if ($queryResult['count'] > 0) {
			$this->showResult($queryResult);
		}
	}

	public function showResult($data){
		echo "<tr>";
		echo "<td>".$this->dbID."</td>";
		echo "<td>".$this->dbIntnotes."</td>";
		echo "<td>".$data['results'][0]['name']."</td>";
		echo "<td>".$data['results'][0]['unit']."</td>";
		echo "<td>".$this->dbName."</td>";
		echo "<td>".$this->dbUnit."</td>";
		echo "<td>".$this->dbGtin."</td>";
		echo "</tr>";
	}
}


class OurDB { // Получение данных из БД по запросу
	public $db;

	public function OurDB($db){
		$this->db = $db;
	}

	public function getProductArticle() { // Получение записи 
		$this->db->setQuery('SELECT wxa43_virtuemart_products.virtuemart_product_id, intnotes, product_gtin, product_name, product_unit FROM wxa43_virtuemart_products, wxa43_virtuemart_products_ru_ru WHERE wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND product_gtin != ""');
		$result = $this->db->loadAssocList('virtuemart_product_id');		
		
		return $result;
	}
}

$db = JFactory::getDBO();
$fromDB = new OurDB($db);
$arrProductsArticle = $fromDB->getProductArticle();

//printr($arrProductsArticle);

$i = 5;
?>

<head>
<style>
	td {border: solid 1px #777; padding: 5px 8px;}
</style>
</head>
<body>
<table>

<?php
foreach ($arrProductsArticle as $key => $value) {
	$arrProductsArticle[$key]['intnotes'] = preg_replace('/\s/', '', $value['intnotes']);
	$arrExplodeIntnotes = explode(',', $arrProductsArticle[$key]['intnotes']);
	$product = new Product($arrExplodeIntnotes, $value);
	$product->arrayHandler();

//	if ($i < 1) break;
	$i--;
}
echo "</table>";



