<?php
$URI = 'https://www.anker72.ru/sdsapi/';
?>

<head>
	<link href="main.css" rel="stylesheet" type="text/css" />
</head>


<body>
<div class="merlion-main">
<ul>
	<li class="merlion-block"><a href="<?php echo $URI; ?>checker-price.php">checker_price() - Сравнение цен на товары</a></li>
	<hr>

	<li class="merlion-block"><a href="<?php echo $URI; ?>methods-getProduct.php">getProduct() - Получить товар</a></li>
	<li class="merlion-block"><a href="<?php echo $URI; ?>methods-getCategory.php">getCategory() - Получить категории</a></li>
	<li class="merlion-block"><a href="<?php echo $URI; ?>methods-getPrices.php">getPrices() - Получить цены</a></li>
	<li class="merlion-block"><a href="<?php echo $URI; ?>methods-getRemain.php">getRemain() - Получить остатки</a></li>
	<li class="merlion-block"><a href="<?php echo $URI; ?>methods-getPhoto.php">getPhoto() - Получить фотографии</a></li>
	<hr>

	<li class="merlion-block"><a href="<?php echo $URI; ?>Info1.pdf" target="blank">SDS API Интеграция</a></li>
	<hr>

	<li class="merlion-block"><a href="<?php echo $URI; ?>temp.php">temp.php</a></li>
</ul>
</div>
</body>