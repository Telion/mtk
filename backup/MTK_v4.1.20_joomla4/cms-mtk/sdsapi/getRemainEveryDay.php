<?php 
//	require_once 'header.php';
?>

<h2 style="margin-left: 30px;"> Метод getRemain() - получение остатков на складе СДС </h2>


<?php 

/*
	Входные параметры:
		cat_id (string) - ОБЯЗАТЕЛЬНЫЙ (или item_id) - код товарной группы (категории)
		item_id (ArrayOfString) - ОБЯЗАТЕЛЬНЫЙ (или cat_id) - массив скодами товаров
		shipment_method (string) - код метода отгрузки (используется значение из справочника getShipmentMethods())
		page (int) - номер страницы (по умолчанию равен 0, т.е. постраничный вывод выключен)
		rows_on_page (int) - количество записей на странице (по умолчанию 10000 строк)
		last_time_change (string) - изменения, начиная с указанного момента времени (в формате YYYY-MM-DDTHH:MM:SS, по умолчанию все товары)
	Выходные параметры:
		No (string) - код товара
		Name (string) - наименование товара
		другие нам не очень подходят
	Комментарии:
		Последнюю страницу можно определить по количеству строк, которое меньше чем rows_on_page или при получении пустого ответа.
		
*/

static $RESOURSE = 'sds';
$date = Date('d.m.Y', time());

class Product {
	public $articles = []; // массив артикулов
	public $count; // общее количество товара
	public $unit; // ед изм товара
	public $sdsName; // название товара на СДС
	public $reelSize; // размер бухты
	public $reelSizeTmpl = ['305', '100', '50']; // какие размеры ищу в названии товара

	
	function Product($article) { // конструктор записывает артикул в объект
		$this->articles = $article;
	}


	public function arrayHandler() { // получает массив артикулов и вызывает обработку каждого значения массива
		foreach ($this->articles as $value) {
			echo "<hr>";
			$this->reelSize = 0;
			$result = $this->getProduct($value);
			if ($result > 0) {
				$this->getRemain($value);
			} else {
				echo "Не подходящий артикул<br>"; 
			}
		}
		return $this->count;
	}

	public function queryToSDS($method, $article) { // запрос к API СДС и получение остатков товаров по артикулу
		$prefix = $this->getPrefixSDS($method);
		if ($prefix) {
			$url = "https://api-rexant.ru/api/v1/".$prefix.$article;
			$opt = [
				"http" => [
					"method" => "GET",
					"header" => "Accept: application/json\r\nAuthorization: Token 350f169dc59fd14a606c9c6a87c1ed1d7a4b21e1\r\n"]
			];
			$context = stream_context_create($opt);
			$queryResult = json_decode(file_get_contents($url, false, $context), true);
				printr($queryResult);
		} else {
			return 0;
		}

		return $queryResult;
	}

	public function getPrefixSDS($method) {
		if ($method === 'product') {
			$prefix = 'product?article=';
		} elseif ($method === 'remain') {
			$prefix = 'remain?productid__article=';
		} else {
			echo "Неизветный метод API<br>";
			return 0;
		}

		return $prefix;
	}

	public function searchInText($search) {
		if (strpos($this->sdsName, $search)) {
			$this->reelSize = $search + 0;
			return 1;
		}

		return 0;
	}

	public function getReelSize() {
		if ($this->unit === 'бухта') {
			foreach ($this->reelSizeTmpl as $value) {
				$result = $this->searchInText($value);
				if ($result) break;
			}
		}
	}

	public function getProduct($article) {
		if ($article) {
			$queryResult = $this->queryToSDS('product', $article);
			if ($queryResult['count'] > 0) {
				$this->unit = $queryResult['results'][0]['unit'];
				$this->sdsName = $queryResult['results'][0]['name'];
				$this->getReelSize();
				return $queryResult;
			} else {
				return 0;
			}
		} else {
			return -1;
		}

	}

	public function getRemain($article) {
		if ($article) {
			$queryResult = $this->queryToSDS('remain', $article);
			if ($queryResult['count'] > 0) {
				if ($this->reelSize != 0) $count = $queryResult['results'][0]['attribute']['count'] * $this->reelSize;
				else $count = $queryResult['results'][0]['attribute']['count'];
				$this->count += $count;
				echo " - <span style='color: red;'>".$this->count."</span>";

				return $queryResult;
			} else {
				return 0;
			}
		} else {
			return -1;
		}
	}
}


class OurDB { // Получение данных из БД по запросу
	public $db;

	public function OurDB($db){
		$this->db = $db;
	}

	public function getProductArticle() { // Получение записи 
		$this->db->setQuery('SELECT virtuemart_product_id, intnotes FROM wxa43_virtuemart_products WHERE product_gtin != ""');
//		$this->db->setQuery('SELECT virtuemart_product_id, intnotes FROM wxa43_virtuemart_products WHERE virtuemart_product_id = 130');    //1824 //1843 //2007 //2426 //2629 //2976
		$result = $this->db->loadAssocList('virtuemart_product_id');		
		
		return $result;
	}

	public function setProductGtin($RESOURSE, $date, $id, $value) {
		$value = $RESOURSE.", ".$date." / ".$value;
		$query = 'UPDATE wxa43_virtuemart_products SET product_gtin="'.$value.'" WHERE virtuemart_product_id = '.$id;
		printr($query);
		$this->db->setQuery($query);
		$this->db->execute();
	}

}

$db = JFactory::getDBO();
$fromDB = new OurDB($db);
$arrProductsArticle = $fromDB->getProductArticle();

$i = 5;
foreach ($arrProductsArticle as $key => $value) {
	$arrProductsArticle[$key]['intnotes'] = preg_replace('/\s/', '', $value['intnotes']);
	$arrTemp = explode(',', $arrProductsArticle[$key]['intnotes']);
	$product = new Product($arrTemp);
	$generalCount = $product->arrayHandler();
	if ($generalCount !== '') {
		echo "<hr>Записать в базу ";
		$fromDB->setProductGtin($RESOURSE, $date, $key, $generalCount);
		echo "ID: ".$key.", общее количество = ".$generalCount."<hr style='border: solid 1px red;'>";
	}
	

if ($i < 1) break;
$i--;
}







// Получение остатка товара на складе СДС
/* $article1 = "12-4721";
$article2 = "17-6972";

$article3 = "607-013";
$article4 = "";

$product = new Product($article1);
$product->checkClearArticle(); */







?> 