<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/connectionDB.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/constants.php';

function apiConnected(){
	class API {
		public $wsdl_url;
		public $params;
	}
	
	$api = new API();
	$api->wsdl_url = "https://apitest.merlion.com/dl/mlservice3?wsdl"; 
 	$api->params = array('login' => "TC0042421|API" 
        , 'password' => "XPE33UcS90" 
        , 'encoding' => "UTF-8" 
        , 'features' => SOAP_SINGLE_ELEMENT_ARRAYS 
    ); 
    return $api;
}

function getCatalog($api, $query) {
	try { 
		$client = new SoapClient($api->wsdl_url, $api->params); 
	        $cat = $client->getCatalog($query);

		return $cat;

	} catch (SoapFault $E) { 
        	echo $E->faultstring; 
	} 
}

function verifyArrayArgument($array, $argument) {
    if (isset($array[$argument])) {
        return $array[$argument];
    }
    return array();
}

function verifyArrayArgumentArray($array, $argument) {
    if (isset($array[$argument])) {
        return $array[$argument];
    }
    return array();
}

function verifyArrayArgumentNULL($array, $argument) {
    if (isset($array[$argument])) {
        return $array[$argument];
    }
    return null;
}

function verifyArrayArgumentString($array, $argument) {
    if (isset($array[$argument])) {
        return $array[$argument];
    }
    return '';
}

function getItems($api, $catId = '', $itemId = '', $shipmentMethod = '', $Page = '', $rowsOnPage = '', $lastTimeChange = '') {
	try { 
		$client = new SoapClient($api->wsdl_url, $api->params); 
 
        	class Request { 
	               public $cat_id; 
        	       public $item_id; 
	               public $shipment_method; 
	               public $page; 
        	       public $rows_on_page; 
	               public $last_time_change; 
        	}           
     
	        $req = new Request();     
        	$req->cat_id = $catId; 
	        $req->item_id = $itemId; 
	        $req->page = $Page; 
	        $req->rows_on_page = $rowsOnPage;
	        $req->shipment_method = $shipmentMethod; 
	        $req->last_time_change = $lastTimeChange; 
     
	        $items = $client->getItems($req); 
       
		return $items;

	} catch (SoapFault $E) { 
	        echo $E->faultstring; 
	} 
}

function getItemsAvail($api, $catId = '', $itemId = '', $shipmentMethod = '', $shipmentDate = '') {
	try { 
		$client = new SoapClient($api->wsdl_url, $api->params); 
 
        	class Request { 
	               public $cat_id; 
        	       public $item_id; 
	               public $shipment_method; 
	               public $shipmentDate; 
        	}           
     
	        $req = new Request();     
        	$req->cat_id = $catId; 
	        $req->item_id = $itemId; 
	        $req->shipment_method = $shipmentMethod; 
	        $req->shipmentDate = $shipmentDate; 
     
	        $items = $client->getItemsAvail($req); 
       
		return $items;

	} catch (SoapFault $E) { 
	        echo $E->faultstring; 
	} 
}

function getItemsProperties($api, $catId = '', $itemId = '', $Page = '', $rowsOnPage = '', $lastTimeChange = '') {
	try { 
		$client = new SoapClient($api->wsdl_url, $api->params); 
 
        	class Request { 
	               public $cat_id; 
        	       public $item_id; 
	               public $shipment_method; 
	               public $page; 
        	       public $rows_on_page; 
	               public $last_time_change; 
        	}           
     
	        $req = new Request();     
        	$req->cat_id = $catId; 
	        $req->item_id = $itemId; 
	        $req->page = $Page; 
	        $req->rows_on_page = $rowsOnPage;
	        $req->shipment_method = $shipmentMethod; 
	        $req->last_time_change = $lastTimeChange; 
     
	        $items = $client->getItems($req); 
       
		return $items;

	} catch (SoapFault $E) { 
	        echo $E->faultstring; 
	} 
}



function getShipmentMethods ($api, $code = '') {
	try { 
		$client = new SoapClient($api->wsdl_url, $api->params); 
	        $method = $client->getShipmentMethods($code);

		return $method;

	} catch (SoapFault $E) { 
        	echo $E->faultstring; 
	} 
}

function getShipmentDates ($api, $sklad = '') {
	try { 
		$client = new SoapClient($api->wsdl_url, $api->params); 
	        $method = $client->getShipmentDates($sklad);

		return $method;

	} catch (SoapFault $E) { 
        	echo $E->faultstring; 
	} 
}

function printr($string) {
	if (is_countable($string)) {
		echo '<span style="color: orangered;"><hr>Количество записей в массиве: ' . count($string) . '<hr></span>';
	}
	echo '<pre>';
	print_r($string);
	echo '</pre>';
}

// Транслитерация кирилицы в латиницу
function translit($s) {
	$s = (string) $s; // преобразуем в строковое значение
	$s = strip_tags($s); // убираем HTML-теги
	$s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
	$s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
	$s = trim($s); // убираем пробелы в начале и конце строки
	$s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
	$s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
	$s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
	$s = str_replace("-", " ", $s); // заменяем минусы пробелами
	$s = mb_strtolower($s); // переводим в нижний регистр
	return $s; // возвращаем результат
}

// Перевод даты из строки неопределенного формата в мой формат.
function edit_date($date){
	if ($date == 0) {return '-';}
	return date("d.m.Y", strtotime($date));
}


function message_to_telegram($text, $telegram_chatid, $botid='914611381:AAH-o_IN7-9SEK7RDOLHeqYUgSCsbCFvwgE') {
	// Отправка сообщения ботом в чат телеграмм
	//
	// $text - текст сообщения
	// $telegram_chatid - ID получателя (440401693 - телион, 1016039173 - алия)

	$ch = curl_init();
	curl_setopt_array(
		$ch,
		array(
			CURLOPT_URL => 'https://api.telegram.org/bot' . $botid . '/sendMessage',
			CURLOPT_POST => TRUE,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_TIMEOUT => 10,
			CURLOPT_POSTFIELDS => array(
				'chat_id' => $telegram_chatid,
				'text' => $text,
		                'parse_mode' => 'html',
			),
		)
	);
	curl_exec($ch);
}

function sendEmail($header, $content, $receiver, $sendFile=0, $sender=EMAIL_SENDER) {
	// Отправка E-Mail
	//
	// $header - Заголовок письма
	// $content - Содержимое письма (файл или текст)
	// $receiverName - имя получателя
	// $receiver - email получателя
	// $sendFile - отправка файла или текста, по умолчанию текст. 1 - файл, 0 - текст.
	// $sender - email отправителя
	// $senderName - имя отправителя

	if ($sendFile) $content = file_get_contents($content);
	$to = "4you<".$receiver.">";
	$subject = $header; 
	$headers  = "Content-type: text/html; charset=utf-8 \r\n";
	$headers .= "From: cmsMTK <".$sender.">\r\n"; 
	mail($to, $subject, $content, $headers);
}

function visitsLog($fileName) {
	// Запись об открытии страницы cmsMTK в лог, таблица tln_visits_log
	// 
	// $userID - ID пользователя, открывшего страницу
	// $pageName - имя открытой страницы

	$userAgent = $_SERVER["HTTP_USER_AGENT"];
	$fileName = pathinfo($fileName)['basename'];
	$currDate = Date("Y-m-d H:i:s", time() + 2*60*60);
	$query = 'INSERT INTO tln_visits_log(id, user_agent, file_name, time) VALUES(NULL, "'.$userAgent.'", "'.$fileName.'", "'.$currDate.'")';

	$db = JFactory::getDBO();
	$db->setQuery($query);
	$db->execute();
}

function rusEnding($number, $var1, $var2, $var3) {
	if ($number >= 11 and $number <=19) {
		return $var3;
	}
	$number = $number % 10;
	if ($number == 1) return $var1;
	if ($number >= 2 and $number <= 4) {
		return $var2;
	}
	return $var3;
}

function getFileSuffix($fileName) {
	// если пустота, если число, если одни слеши
	$resultArray = explode('/', $fileName);
	$result = $resultArray[count($resultArray)-1];

	return $result;
}

function getClassSuffix($inputText) {
	// active-equipments -> _ActiveEquipments
	// если пустота, если число, если одни дефисы
	$inputText = explode('.', $inputText)[0];
	$parts = explode('-', $inputText);
	$result = '_';
	foreach($parts as $part) {
		$result .= ucfirst($part);
	}

	return $result;
}

function requireClass ($name, $fileSuffix, $classSuffix) {
	require $name . '-' . $fileSuffix;
	$className = ucfirst($name) . $classSuffix;
	return $className;
}

?>