<?php 

class View_Dashboard { // Получение данных из БД по запросу
	public $modelDashboard;
	public $goodColor;
	public $alarmColor;
	public $errorColor;
	public $colors = [
		'red' => '#E45252',
		'orange' => '#E28940',
		'darkGrey' => '#161719',
		'grey' => '#4e4e4e',
		'green' => '#31a72b',
		'violet' => '#5a2791',
		'blue' => '#1f78c1',
		'yellow' => '#cca300',
		'white' => '#d8d9da'
	];

	public function __construct($modelDashboard) {
		$this->modelDashboard = $modelDashboard;
		$this->goodColor = $this->colors['grey'];
		$this->alarmColor = $this->colors['orange'];
		$this->errorColor = $this->colors['red'];
	}

	public function getBlockHowManyDaysPromotionEnd($title) {
		$howManyDaysPromotionEnd = $this->modelDashboard->getHowManyDaysPromotionEnd();
		$howManyDaysPromotionEnd = floor((strtotime($howManyDaysPromotionEnd) - time()) / (60*60*24));
		$bgcolor = $this->identifyColor($howManyDaysPromotionEnd, $howManyDaysPromotionEnd > 3, $howManyDaysPromotionEnd <= 0);

		$howManyDaysPromotionEnd .= ' дн';
		$block = $this->createBlock($bgcolor, $title, $howManyDaysPromotionEnd, '/m2-reports/1-sales.php');

		return $block;
	}

	public function getHowManySales() {
		$countProduct = $this->modelDashboard->getCountProductsAlarmSales();
		$result = rusEnding($countProduct, $countProduct." акция скоро закончится", $countProduct." акции скоро закончатся", $countProduct." акций скоро закончатся");

		return $result;
	}

	public function	getBlockCountProductWithChangePrice($title) {
		$newProduct = $this->modelDashboard->getCountNewProductWithChangePrice();
		$oldProduct = $this->modelDashboard->getCountOldProductWithChangePrice();
		$bgcolor = $this->identifyColorTwoParameters($newProduct, $oldProduct);
		$value = '<span class="new-old-price right-border">' . $newProduct . '</span><span class="new-old-price">' . $oldProduct . '</span>';
		$block = $this->createBlock($bgcolor, $title, $value, '/m2-reports/3-check-price-change.php');

		return $block;
	}

	public function	getBlockCountProductWithLowStock($title) {
		$value = $this->modelDashboard->getCountProductWithLowStock();
		$bgcolor = $this->identifyColor($value, $value == 0, $value < 0);
		$block = $this->createBlock($bgcolor, $title, $value, '/m2-reports/8-tracking-product-stock.php');

		return $block;
	}

	public function	getBlockNegativeStock($title) {
		$value = $this->modelDashboard->getNegativeStock();
		$bgcolor = $this->identifyColor($value, $value == 0, $value > 0);
		$block = $this->createBlock($bgcolor, $title, $value, '/m2-reports/4-products-reserv.php');

		return $block;
	}

	public function	getBlockCountReservedOrdersOverdue($title) {
		$value = $this->modelDashboard->getCountReservedOrdersOverdue();
		$bgcolor = $this->identifyColor($value, $value == 0, $value == 0);
		$block = $this->createBlock($bgcolor, $title, $value, '/m2-reports/9-orders-reserv.php');

		return $block;
	}

	public function	getBlockCountExpiredReservedOrders($title) {
		$value = $this->modelDashboard->getCountExpiredReservedOrders();
		$bgcolor = $this->identifyColor($value, $value == 0, $value < 0);
		$block = $this->createBlock($bgcolor, $title, $value, '/m2-reports/9-orders-reserv.php');

		return $block;
	}

	public function	getBlockCountOrdersInProgress($title) {
		$value = $this->modelDashboard->getCountOrdersInProgress();
		$bgcolor = $this->identifyColor($value, $value > 0, $value < 0);
		$block = $this->createBlock($bgcolor, $title, $value, '/m2-reports/9-orders-reserv.php');

		return $block;
	}

	public function	getBlockSdsPrices($title) {
		$value = $this->modelDashboard->getCountPriceMoreExpansiveThanSds();
		$bgcolor = $this->identifyColor($value, $value == 0, $value > 50);
		$block = $this->createBlock($bgcolor, $title, $value, '/m3-api-reports/2-sds-report.php');

		return $block;
	}

	public function getBlockNextMailing($title) {
		$value = $this->modelDashboard->getNextMailing();
		$now = new DateTime();
		$date = DateTime::createFromFormat("Y-m-d", $value);
		$interval = $now->diff($date);
		$value = $interval->d;	
		$bgcolor = $this->identifyColor($value, $value > 5, $value < 3);
		$block = $this->createBlock($bgcolor, $title, $value." дн", '/m6-mailing-generation/mailing.php');

		return $block;
	}

	public function identifyColor($value, $goodCondition, $errorCondition) {
		if ($goodCondition) {
			return $this->goodColor;
		} else if ($errorCondition) {
			return $this->errorColor;
		}

		return $this->alarmColor;
	}

	public function identifyColorTwoParameters($newProduct, $oldProduct) {
		$bgcolorNewProduct = $this->identifyColor($newProduct, $newProduct == 0, $newProduct > 30);
		$bgcolorOldProduct = $this->identifyColor($oldProduct, $oldProduct == 0, $oldProduct > 100);

		if ($bgcolorNewProduct == $this->errorColor or $bgcolorOldProduct == $this->errorColor) {
			return $this->errorColor;
		} else if ($bgcolorNewProduct == $this->alarmColor or $bgcolorOldProduct == $this->alarmColor) {
			return $this->alarmColor;
		}

		return $this->goodColor;
	}


	public function setTextColor($text, $color) {
		return '<span style="color: '.$color.';">' . $text . '</span>';
	}

	public function createBlock($bgcolor, $title, $value, $href) {
		$path = '/cms-mtk' . $href;

		return '<td style="background: '.$bgcolor.'; box-shadow: '.$bgcolor.' 0 2 0px 0;"><a href="'.$path.'" target="blank"><div class="blockTitle">'.$title.'<div>'.$value.'<span></span></div></div><div class="blockValue">&nbsp;</div></td>';
	}

}

?>