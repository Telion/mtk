<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
require_once 'model-dashboard.php';
require_once 'view-dashboard.php';

visitsLog(__FILE__);
$db = JFactory::getDBO();
$modelDashboard = new Model_Dashboard($db);
$viewDashboard = new View_Dashboard($modelDashboard);
$vendorList = $modelDashboard->getVendors();
?>

<html>
<head>
	<title>cmsMTK. Главная. Остатки по поставщикам</title>
	<style>
		body {background: #323232;}
		.dashboard {height: 570px;}
		.back-dashboard {width: fit-content; margin: 0 auto; padding: 1px 12px; border-radius: 7px;}
		.dashboard table{border-spacing: 25px;}
		.dashboard td {width: 230px; height: 130px; padding: 0; background: #3b3b3b; border-radius: 3px; color: #d8d9da; box-shadow: #3b3b3b 0 0 2px 0;}
		.back-dashboard a div {color: #d8d9da;}
		.center-top {height: 50px;}
		.blockTitle {height: 20%; font-family: monospace; font-size: 13px; text-align: center; padding-top: 10px; position: relative;}
		.blockValue {height: 80%; display: table;}
		.blockTitle div {position: absolute; font-size: 50px; font-weight: 700; width: 230px; top: 48px; color: #d8d9da;}
		.new-old-price {padding: 0 13px;}
		.right-border {border-right: 3px dotted #fff;}
	</style>

<?php
	require_once CONST_MENU;
?>

	<div class="mainBlock">

	<div class="centerBlock">
		<div class="center-top"></div>
		<div class="dashboard">
			<div class="back-dashboard">     
			<table>
				<tr>
					<?=$viewDashboard->getBlockHowManyDaysPromotionEnd($viewDashboard->getHowManySales());?>
					<?=$viewDashboard->getBlockCountProductWithChangePrice("Новые и измененные цены");?>
					<?=$viewDashboard->getBlockCountProductWithLowStock("Заканчиваются товары");?>
				</tr>                    
				<tr>
					<?=$viewDashboard->getBlockSdsPrices("На SDS цены выше");?>
					<?=$viewDashboard->getBlockNegativeStock("Отрицательные остатки/резерв");?>
					<!--<td style="background: #323232; box-shadow: inherit; text-align: center;"><a href="https://mirtelcom.ru" target="_blank"><img style="height: 70%; padding: 20px;" src="../images/mtk-logo.png"></a></td> -->
					<?=$viewDashboard->getBlockNextMailing("До следующей рассылки");?>
				</tr>
				<tr>
					<?=$viewDashboard->getBlockCountReservedOrdersOverdue("Заказы. Просроченный резерв");?>
					<?=$viewDashboard->getBlockCountExpiredReservedOrders("Заказы. В резерве");?>
					<?=$viewDashboard->getBlockCountOrdersInProgress("Заказы. Без резерва");?>

				</tr>
			</table>
			</div>
		</div>

		<div class="supplierForm">
		<form action='../m1-suppliers/vendor-products/report-vendor-products.php' method='get' name='form_choice'>
			<select name='select_suppliers'>
			<option value='0'>Выбери поставщика</option>
			<?php
			foreach ($vendorList as $vendor) {
                $countProductsWithZeroStock = (isset($vendor[1])) ? $vendor[1] : 0;
				echo "<option value = '".$vendor[0]."' >".$vendor[0]." (".$countProductsWithZeroStock." из ".$vendor[2].")</option>";
			}
			?>
			</select>
			<button type='submit' name='submit'>Сформировать отчет</button>
		</form>
		</div>
	</div>


</div>

<?php
	require "../footer.php";
?>