<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/function.php';
visitsLog(__FILE__);

$supplier_name = $_GET['select_suppliers'];

$db->setQuery('
    SELECT 
		wxa43_virtuemart_products.product_sku,
		wxa43_virtuemart_products_ru_ru.product_name,
		wxa43_virtuemart_products.product_in_stock, 
		wxa43_virtuemart_products.product_ordered, 
		wxa43_virtuemart_products.intnotes,
		prices.product_price,
		wxa43_virtuemart_product_customfields.customfield_value,
		wxa43_virtuemart_products.virtuemart_product_id,
		vendor_price.customfield_value as vendor_price,
		vendor_article.customfield_value as vendor_article
    FROM 
		wxa43_virtuemart_products_ru_ru, 
        (SELECT * FROM wxa43_virtuemart_product_prices GROUP BY wxa43_virtuemart_product_prices.virtuemart_product_id) as prices,
        wxa43_virtuemart_product_customfields,
    	wxa43_virtuemart_products 
        LEFT JOIN wxa43_virtuemart_product_customfields as vendor_price ON wxa43_virtuemart_products.virtuemart_product_id = vendor_price.virtuemart_product_id AND vendor_price.virtuemart_custom_id = 60
        LEFT JOIN wxa43_virtuemart_product_customfields as vendor_article ON wxa43_virtuemart_products.virtuemart_product_id = vendor_article.virtuemart_product_id AND vendor_article.virtuemart_custom_id = 58
    WHERE 
		wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND
        wxa43_virtuemart_products.virtuemart_product_id = prices.virtuemart_product_id AND
        wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_product_customfields.virtuemart_product_id AND
        wxa43_virtuemart_product_customfields.virtuemart_custom_id = 48 AND
		wxa43_virtuemart_product_customfields.customfield_value = "'.$supplier_name.'"
	');
$arr_products = $db->loadObjectList();
?>

<html>
<head>
	<title>cmsMTK. Остатки по поставщикам</title>

<?php
	require_once CONST_MENU;
?>

<div style="width: fit-content; margin: 0 auto;">
<div style="font-size: 20px; font-weight: 700; padding: 14px; padding-left: 110px;">Остатки по поставщику: <span style="color: crimson; font-size: 24px; padding-left: 10px;"><?php echo $supplier_name ?></span></div>

	<table class='zebra-table'>
	<tr><th>Артикул</th><th>Название товара</th><th>Остаток</th><th>В резерве</th><th>Цена</th></tr>
<?php
foreach ($arr_products as $product) {

	$article = $product->product_sku;
	$intnotes = $product->intnotes;
	$isEmptySkuAndIntnotes = (!empty($article) and !empty($intnotes));
	$isEmptySku = !empty($article);
	$isEmptyIntnotes = !empty($intnotes);

    $metka = '';
    if ($product->vendor_price == null) {
        $metka = '<span style="color: red;">(x)</span>';
    }

	if ($isEmptySkuAndIntnotes) {
		$pole1 = "Артикул: " . $article . "<br>Комментарий: " . $intnotes;
	} elseif ($isEmptySku) {
		$pole1 = "Артикул: " . $article;
	} elseif ($isEmptyIntnotes) {
		$pole1 = "Комментарий: " . $intnotes;
	} else {
		$pole1 = '';
	}
    $pole1 .= ' ' . $metka;

	$pole2 = '<a href="https://www.mirtelcom.ru/administrator/index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$product->virtuemart_product_id.'" target="_blank">'.$product->product_name.'</a>';
	$pole3 = $product->product_in_stock;
	$pole4 = $product->product_ordered;
	$pole5 = ceil($product->product_price);

	$color_stock = "black";
	if ($pole3 < 10) $color_stock = "red; font-weight: 700;";

	$color_stock_null = "";
	if ($pole3 == 0) $color_stock_null = "background: #fde8e5;";
    //if (isset($product->vendor_article)) $color_stock_null = "background: khaki;";

	$color_ordered = "black";
	if ($pole4 > 0) $color_ordered = "red; font-weight: 700;";
 
	echo "\t"."<tr style='".$color_stock_null."'><td style='width: 300px;'>".$pole1."</td><td>".$pole2."</td><td style='color:".$color_stock."; text-align: right; padding-right: 20px;'>".$pole3."</td><td style='color:".$color_ordered."; text-align: right; padding-right: 20px;'>".$pole4."</td><td style='text-align: right;'>".$pole5." р.</td></tr>"."\r\n";

}
?>

	</table>
</div>