<?php
	require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' . '/connectionDB.php';

if (isset($db)) {

// 2-1. Акционные товары // Получаю дату первого заканчивающегося акционного товара
$db->setQuery('
	SELECT 
		wxa43_virtuemart_product_prices.product_price_publish_down 
	FROM 
		wxa43_virtuemart_product_prices, 
		wxa43_virtuemart_products_ru_ru 
	WHERE 
		wxa43_virtuemart_product_prices.product_price_publish_down > 0 AND 
		wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_product_prices.virtuemart_product_id 
	ORDER BY wxa43_virtuemart_product_prices.product_price_publish_down ASC
   	LIMIT 1');
	$date_sales = $db->loadResult();
	$count_day_sales = floor((strtotime($date_sales) - time()) / (60*60*24));

// 2-3. Изменения цен // Количество новых товаров (цена еще не в базе сравнения)
$db->setQuery('
	SELECT
		COUNT(wxa43_virtuemart_products.virtuemart_product_id)
	FROM 
		wxa43_virtuemart_products
	WHERE 
		published = 1 AND
		wxa43_virtuemart_products.virtuemart_product_id NOT IN (
			SELECT virtuemart_product_id 
			FROM wxa43_virtuemart_product_customfields 
			WHERE virtuemart_custom_id = 54
		)
	');
	$count_products_with_new_price = $db->loadResult();

// 2-3. Изменения цен // Количество товаров с измененной ценой
$db->setQuery('
	SELECT
		COUNT(wxa43_virtuemart_product_prices.virtuemart_product_id)
	FROM 
		wxa43_virtuemart_product_customfields, 
		wxa43_virtuemart_product_prices
	WHERE 
		wxa43_virtuemart_product_prices.virtuemart_product_id = wxa43_virtuemart_product_customfields.virtuemart_product_id AND
		wxa43_virtuemart_product_customfields.virtuemart_custom_id = 54 AND
		wxa43_virtuemart_product_prices.product_override_price != wxa43_virtuemart_product_customfields.customfield_value AND
		wxa43_virtuemart_product_prices.product_price != wxa43_virtuemart_product_customfields.customfield_value
	');
	$count_products_with_change_price = $db->loadResult();

// 2-4. Товары с резервом
$db->setQuery('
	SELECT COUNT(product_ordered) AS count_products
	FROM 
	    wxa43_virtuemart_products
	WHERE 
	    product_ordered <> 0  
	');
	$count_products_with_ordered = $db->loadResult();

// 2-4. Товары с отрицательным резервом
$db->setQuery('
	SELECT COUNT(product_ordered) AS count_products
	FROM 
	    wxa43_virtuemart_products
	WHERE 
	    product_ordered < 0  
	');
	$count_products_with_negative_ordered = $db->loadResult();

// 2-4. Товары с отрицательным остатком
$db->setQuery('
	SELECT COUNT(virtuemart_product_id) AS count_products
	FROM 
	    wxa43_virtuemart_products
	WHERE 
	    product_in_stock < 0
	');
	$count_products_with_negative_stock = $db->loadResult();

// 2-5. Товары без поставщика // Получаю количество товаров без поставщиков
$db->setQuery('
	SELECT COUNT(wxa43_virtuemart_products_ru_ru.virtuemart_product_id) as count_suppliers
	FROM wxa43_virtuemart_products_ru_ru, wxa43_virtuemart_products 
	WHERE
    	wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND
    	wxa43_virtuemart_products.published = 1 AND
		wxa43_virtuemart_products_ru_ru.virtuemart_product_id NOT IN (
			SELECT wxa43_virtuemart_product_customfields.virtuemart_product_id 
			FROM wxa43_virtuemart_product_customfields 
			WHERE wxa43_virtuemart_product_customfields.virtuemart_custom_id = 48 
			GROUP BY wxa43_virtuemart_product_customfields.virtuemart_product_id)
	');
	$count_products_without_supplier = $db->loadResult();

// 2-6. Без сопутсвующих // Получаю количество товаров без сопутствующих
$db->setQuery('
	SELECT COUNT(wxa43_virtuemart_products_ru_ru.virtuemart_product_id) as count_suppliers
	FROM wxa43_virtuemart_products_ru_ru, wxa43_virtuemart_products 
	WHERE
    	wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND
    	wxa43_virtuemart_products.published = 1 AND
		wxa43_virtuemart_products_ru_ru.virtuemart_product_id NOT IN (
			SELECT wxa43_virtuemart_product_customfields.virtuemart_product_id 
			FROM wxa43_virtuemart_product_customfields 
			WHERE wxa43_virtuemart_product_customfields.virtuemart_custom_id = 1 
			GROUP BY wxa43_virtuemart_product_customfields.virtuemart_product_id)
	');
	$count_products_without_related = $db->loadResult();

// 2-7. Не 8 сопутсвующих
$db->setQuery('
	SELECT COUNT(t_cnt.virtuemart_product_id)
	FROM (
		SELECT 
		    wxa43_virtuemart_product_customfields.virtuemart_product_id,
		    COUNT(*) as cnt
		FROM wxa43_virtuemart_product_customfields, wxa43_virtuemart_products
		WHERE 
		    wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_product_customfields.customfield_value AND
		    wxa43_virtuemart_products.published = 1 AND
		    wxa43_virtuemart_products.product_in_stock > 0 AND
	    	    wxa43_virtuemart_product_customfields.virtuemart_custom_id = 1
		GROUP BY wxa43_virtuemart_product_customfields.virtuemart_product_id ) as t_cnt,
	    wxa43_virtuemart_products_ru_ru,
	    wxa43_virtuemart_categories_ru_ru,
	    wxa43_virtuemart_product_categories
	WHERE
	    t_cnt.cnt <> 8 AND
	    t_cnt.cnt <> 12 AND
	    t_cnt.cnt < 12 AND
	    wxa43_virtuemart_products_ru_ru.virtuemart_product_id = t_cnt.virtuemart_product_id AND
	    wxa43_virtuemart_categories_ru_ru.virtuemart_category_id = wxa43_virtuemart_product_categories.virtuemart_category_id AND
	    wxa43_virtuemart_product_categories.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id
	');
	$count_products_without_related_not812 = $db->loadResult();

// 2-8. Отслеживание остатков // Количество товаров, у которых отслеживаем остатки
$db->setQuery('
	SELECT COUNT(wxa43_virtuemart_products.virtuemart_product_id) FROM wxa43_virtuemart_products WHERE low_stock_notification > 0
	');
	$count_products_low_stock = $db->loadResult();

// 2-8. Отслеживание остатков // Количество товаров, у которых остатки ниже заданного порога
$db->setQuery('
	SELECT COUNT(wxa43_virtuemart_products.virtuemart_product_id) FROM wxa43_virtuemart_products WHERE low_stock_notification > product_in_stock AND product_in_stock > 0
	');
	$count_products_low_stock_warning = $db->loadResult();

// 2-9. Заказы с резервом // Текущие заказы, у которых просрочена дата резерва
$db->setQuery('
	SELECT COUNT(virtuemart_order_id)
	FROM (
		SELECT 
			wxa43_virtuemart_orders.virtuemart_order_id
		FROM 
			wxa43_virtuemart_order_histories, 
			wxa43_virtuemart_orders
		WHERE   	
		        wxa43_virtuemart_order_histories.virtuemart_order_id = wxa43_virtuemart_orders.virtuemart_order_id and
	        	(wxa43_virtuemart_orders.order_status = "C" or wxa43_virtuemart_orders.order_status = "U" or wxa43_virtuemart_orders.order_status = "P") and
		        reserv_date != 0 and
			reserv_date < CURDATE()
		GROUP BY wxa43_virtuemart_orders.virtuemart_order_id) t1
	');
	$count_orders_overdue_reserv_date = $db->loadResult();

// 2-9. Заказы с резервом // Текущие заказы, у которых не просрочена дата резерва
$db->setQuery('
	SELECT COUNT(virtuemart_order_id)
	FROM (
		SELECT 
			wxa43_virtuemart_orders.virtuemart_order_id
		FROM 
			wxa43_virtuemart_order_histories, 
			wxa43_virtuemart_orders
		WHERE   	
		        wxa43_virtuemart_order_histories.virtuemart_order_id = wxa43_virtuemart_orders.virtuemart_order_id and
	        	(wxa43_virtuemart_orders.order_status = "C" or wxa43_virtuemart_orders.order_status = "U" or wxa43_virtuemart_orders.order_status = "P") and
		        reserv_date != 0 and
			reserv_date >= CURDATE()
		GROUP BY wxa43_virtuemart_orders.virtuemart_order_id) t1
	');
	$count_orders_with_reserv_date = $db->loadResult();

// 2-9. Заказы с резервом // Текущие заказы, у которых не установлена дата резерва
$db->setQuery('
	SELECT 	
		COUNT(virtuemart_order_id)
	FROM 
		(SELECT 
			wxa43_virtuemart_orders.virtuemart_order_id,
	        	max(reserv_date) as max_date
		FROM 
			wxa43_virtuemart_order_histories, 
			wxa43_virtuemart_orders
		WHERE   	
		        wxa43_virtuemart_order_histories.virtuemart_order_id = wxa43_virtuemart_orders.virtuemart_order_id and
	        	(wxa43_virtuemart_orders.order_status = "C" or wxa43_virtuemart_orders.order_status = "U" or wxa43_virtuemart_orders.order_status = "P")
		GROUP BY wxa43_virtuemart_orders.virtuemart_order_id) t1
	WHERE max_date = 0
	');
	$count_orders_not_have_reserv_date = $db->loadResult();



// 3-5. Неопубликованные товары
$db->setQuery('
	SELECT 
		count(wxa43_virtuemart_products_ru_ru.virtuemart_product_id)
	FROM
		wxa43_virtuemart_products_ru_ru, wxa43_virtuemart_products, wxa43_virtuemart_categories_ru_ru, wxa43_virtuemart_product_categories
	WHERE
		wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_products_ru_ru.virtuemart_product_id AND
		wxa43_virtuemart_products.published = 0 AND
		wxa43_virtuemart_products_ru_ru.virtuemart_product_id NOT IN (
			SELECT virtuemart_product_id FROM wxa43_virtuemart_order_items
			GROUP BY virtuemart_product_id) AND
		wxa43_virtuemart_categories_ru_ru.virtuemart_category_id = wxa43_virtuemart_product_categories.virtuemart_category_id AND
		wxa43_virtuemart_product_categories.virtuemart_product_id = wxa43_virtuemart_products.virtuemart_product_id  
	ORDER BY wxa43_virtuemart_categories_ru_ru.category_name ASC
	');
	$count_products_not_published = $db->loadResult();

// Количество невыполненных задач программиста
$db->setQuery('SELECT count(task_id) FROM tln_programmer_tasks WHERE task_status = 0');
	$count_unfulfilled_tasks = $db->loadResult();

// Количество выполненных задач программиста
$db->setQuery('SELECT count(task_id) FROM tln_programmer_tasks WHERE task_status = 1');
	$count_completed_tasks = $db->loadResult();

}

?>