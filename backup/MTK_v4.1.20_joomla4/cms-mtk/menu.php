<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/cms-mtk' .'/helpers/menu-values.php';

if (isset($db)) {
	if ($count_day_sales <= 3) $count_day_sales_view = "<span class='yellow'>".$count_day_sales." дн</span>";
	else $count_day_sales_view = $count_day_sales.' дн';

	$count_products_with_new_price_view = $count_products_with_new_price."/".$count_products_with_change_price;
	if (($count_products_with_new_price > 0) or ($count_products_with_change_price > 0)) $count_products_with_new_price_view = "<span class='yellow'>".$count_products_with_new_price." и ".$count_products_with_change_price."</span>";

	$class1 = $class2 = '';
	if ($count_products_with_negative_stock > 0) $class1 = 'darkred';
	if ($count_products_with_negative_ordered > 0) $class2 = 'darkred';
	$count_products_with_ordered_view = $count_products_with_ordered.": <span class=".$class1.">".$count_products_with_negative_stock."</span> и <span class=".$class2.">".$count_products_with_negative_ordered."</span>";

	$count_products_without_related_view = $count_products_without_related;
	if ($count_products_without_related > 0) $count_products_without_related_view = "<span class='yellow'>".$count_products_without_related."</span>";

	$count_products_without_related_not812_view = $count_products_without_related_not812;
	if ($count_products_without_related_not812 > 0) $count_products_without_related_not812_view = "<span class='yellow'>".$count_products_without_related_not812."</span>";

	$count_products_low_stock_warning_view = $count_products_low_stock_warning." из ".$count_products_low_stock;
	if ($count_products_low_stock_warning > 0) $count_products_low_stock_warning_view = "<span class='darkred'>".$count_products_low_stock_warning." из ".$count_products_low_stock."</span>";

	// 2.9
	$count_products_orders_reserv_view = $count_orders_overdue_reserv_date." / ".$count_orders_with_reserv_date." / ".$count_orders_not_have_reserv_date;
	if ($count_orders_overdue_reserv_date > 0) $count_products_orders_reserv_view = "<span class='darkred'>".$count_products_orders_reserv_view."</span>";

}


// determining the current directory
$current_dir_arr = explode('/', getcwd());
$dir_prefix = "";
for ($i = count($current_dir_arr)-1; $i > 0; $i--) {
	if ($current_dir_arr[$i] == "cms-mtk") {
		break;
	}
	$dir_prefix .= "../";
}

?>

<link rel="shortcut icon" href="<?=$dir_prefix;?>favicon.ico">
<link href="<?=$dir_prefix;?>main.css" rel="stylesheet" type="text/css" />
<style>
	.yellow {color: #ead881; font-weight: 700;}
	.darkred {color: #734545b8; font-weight: 700;}
</style>
</head>
<body id="wrap" style="position: relative;">
	<div class="tz-navbar"><div class="tz-align">
	  <a href="https://www.mirtelcom.ru/administrator/index.php?trend">Админка</a>
	  <a href="<?=$dir_prefix;?>m1-suppliers/dashboard.php">CMS-MTK</a>

	  <div class="tz-dropdown">
	    <button class="tz-dropbtn">Отчеты 
	      <i class="tz-fa fa-caret-down"></i>
	    </button>
	    <div class="tz-dropdown-content">
	      <a href="<?=$dir_prefix.DIR_M2;?>1-sales.php">1. Акционные товары<?php if (isset($db)) echo ' ('.$count_day_sales_view.')'; ?></a>
	      <a href="<?=$dir_prefix.DIR_M2;?>2-user-points.php">2. Пользовательские баллы</a>
	      <a href="<?=$dir_prefix.DIR_M2;?>3-check-price-change.php">3. Изменения цен<?php if (isset($db)) echo ' ('.$count_products_with_new_price_view.')'; ?></a>
	      <a href="<?=$dir_prefix.DIR_M2;?>4-products-reserv.php">4. Товары с резервом<?php if (isset($db)) echo ' ('.$count_products_with_ordered_view.')'; ?></a>
	      <a href="<?=$dir_prefix.DIR_M2;?>5-suppliers-categories.php">5. Товары без поставщика<?php if (isset($db)) echo ' ('.$count_products_without_supplier.')'; ?></a>
	      <a href="<?=$dir_prefix.DIR_M2;?>6-related-categories.php">6. Без сопутствующих<?php if (isset($db)) echo ' ('.$count_products_without_related_view.')'; ?></a>
	      <a href="<?=$dir_prefix.DIR_M2;?>7-related-categories-not8.php">7. Не 8 сопутствующих<?php if (isset($db)) echo ' ('.$count_products_without_related_not812_view.')'; ?></a>
	      <a href="<?=$dir_prefix.DIR_M2;?>8-tracking-product-stock.php">8. Отслеживание остатков<?php if (isset($db)) echo ' ('.$count_products_low_stock_warning_view.')'; ?></a>
	      <a href="<?=$dir_prefix.DIR_M2;?>9-orders-reserv.php">9. Зарезервированные заказы<?php if (isset($db)) echo ' ('.$count_products_orders_reserv_view.')'; ?></a>
	      <a href="<?=$dir_prefix.DIR_M2;?>10-product-search-by-id.php">10. Поиск товара по ID</a>
          <a href="<?=$dir_prefix.DIR_M2;?>11-all-products.php">11. Список товаров</a>
	    </div>
	  </div> 
	
	  <div class="tz-dropdown">
	    <button class="tz-dropbtn">Отчеты API
	      <i class="tz-fa fa-caret-down"></i>
	    </button>
	    <div class="tz-dropdown-content">
	      <a href="<?=$dir_prefix.DIR_M3;?>1-statistic.php">1. Статистика</a>
	      <a href="<?=$dir_prefix.DIR_M3;?>2-vendors/report.php">2. Цены и остатки поставщиков</a>
	      <!--<a href="<?=$dir_prefix.DIR_M3;?>3-report-anker.php">3. Сравнение цен Анкер и СДС</a>
	      <a href="<?=$dir_prefix.DIR_M3;?>4-report-airtek.php">4. Сравнение цен Аиртек и Русклимат</a>-->
	      <a href="<?=$dir_prefix.DIR_M3;?>5-not-published.php">5. Неопубликованные товары<?php if (isset($db)) echo ' ('.$count_products_not_published.')'; ?></a>
	      <!--<a href="<?=$dir_prefix.DIR_M3;?>6-vendor-urls.php">6. Сравнение цен с поставщиками</a>-->
            <a href="<?=$dir_prefix.DIR_M3;?>7-unnecessary-urls">7. Неправильные ссылки в поиске</a>
	    </div>
	  </div> 	

	  <div class="tz-dropdown">
	    <button class="tz-dropbtn">Списки
	      <i class="tz-fa fa-caret-down"></i>
	    </button>
	    <div class="tz-dropdown-content">
	      <a href="<?=$dir_prefix.DIR_M4;?>1-active-equipments/active-equipments.php">1. Активное оборудование</a>
	      <a href="<?=$dir_prefix.DIR_M4;?>2-defective-goods.php">2. Брак</a>
	    </div>
	  </div>

	  <!--<a href="<?=$dir_prefix.DIR_M5;?>programmer-tasks.php">Задачи</a>-->
	  <a href="<?=$dir_prefix.DIR_M6;?>mailing.php">Рассылка</a>  
	</div></div>

<script>
var arr = document.getElementsByTagName("link");
for (var x = 0; x < arr.length; x++){
	var str = arr[x].href;
	if (str.includes('main.css')){
		arr[x].href += '?6';
	}
}
</script>

<?php 
	function printMenuElement ($prefix, $file, $title, $param) {
		echo '<a href="'.$prefix.$file.'">'.$title.' ('.$param.')</a>';
	}
?>