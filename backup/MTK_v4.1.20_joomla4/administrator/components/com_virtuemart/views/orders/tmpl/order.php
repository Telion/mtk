<?php
/**
 * Display form details
 *
 * @package	VirtueMart
 * @subpackage Orders
 * @author Oscar van Eijk, Max Milbers, Valérie Isaksen
 * @link https://virtuemart.net
 * @copyright Copyright (c) 2004 - 2021 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: order.php 10765 2022-12-05 18:17:23Z Milbo $
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

AdminUIHelper::startAdminArea($this);
AdminUIHelper::imitateTabs('start','COM_VIRTUEMART_ORDER_PRINT_PO_LBL');

// Get the plugins
vDispatcher::importVMPlugins('vmpayment');

$jsOrderStatusShopperEmail = '""';
$j = 'if (typeof Virtuemart === "undefined")
	var Virtuemart = {};
	Virtuemart.confirmDelete = "'. vmText::_('COM_VIRTUEMART_ORDER_DELETE_ITEM_JS', true) .'";
	jQuery(document).ready(function() {
		Virtuemart.onReadyOrderItems();
	});
	var editingItem = 0;';
vmJsApi::addJScript('onReadyOrder',$j);

vmJsApi::addJScript('/administrator/components/com_virtuemart/assets/js/orders.js',false,false);

/* Mirtelcom #1.8.7.1 POINTS */
if (in_array(7, explode(',', $this->orderdetails["details"]["BT"]->user_shoppergroups))) {
	$db = JFactory::getDBO();
	$orderStatus = $this->orderdetails["details"]["BT"]->order_status;
	switch ($orderStatus){
		case "D":  // $status = 1 - активирую баллы; $status = 0 - не активные баллы; $status = -1 - не отображать баллы, заказ отменен.
		case "S": 
		case "F": $status = 1; break;
		case "R": 
		case "X": $status = -1; break;
		case "P": 
		case "C": 
		case "U": $status = 0; break;
	}
	$arr_items = json_decode(json_encode($this->orderdetails["items"]), true);
	$update_text = '';
	foreach ($arr_items as $item){
		if ($update_text == '') {
			$update_text .= ' UPDATE wxa43_virtuemart_order_items_points SET points_status = '.$status.' WHERE virtuemart_order_item_id = '.$item["virtuemart_order_item_id"];
		} else {
			$update_text .= ' or virtuemart_order_item_id = '.$item["virtuemart_order_item_id"];
		}
	}
	$db->setQuery($update_text);
	$db->execute();
	/* UPDATE POINTS, when changed item values */
	$db->setQuery('
		SELECT 
			wxa43_virtuemart_order_items_points.*
		FROM 
			wxa43_virtuemart_orders, 
			wxa43_virtuemart_order_items, 
			wxa43_virtuemart_order_items_points 
		WHERE 
			wxa43_virtuemart_order_items.virtuemart_order_id = wxa43_virtuemart_orders.virtuemart_order_id AND 
			wxa43_virtuemart_order_items_points.virtuemart_order_item_id = wxa43_virtuemart_order_items.virtuemart_order_item_id AND
			wxa43_virtuemart_orders.virtuemart_order_id = '.$this->orderdetails["details"]["BT"]->virtuemart_order_id.'
		');
	$arr_points_from_db = $db->loadObjectList('virtuemart_order_item_id');
	$points_status = $arr_points_from_db[array_key_first($arr_points_from_db)]->points_status;
	foreach ($this->orderdetails["items"] as $item) {
		$curr_points = round($item->product_subtotal_with_tax * 0.02);
		if (array_key_exists($item->virtuemart_order_item_id, $arr_points_from_db)) {
			$increase_points_from_db = $arr_points_from_db[$item->virtuemart_order_item_id]->increase_points;
			if ($curr_points != $increase_points_from_db)
				$update_text = 'UPDATE wxa43_virtuemart_order_items_points SET increase_points = '.$curr_points.' WHERE virtuemart_order_item_id = '.$item->virtuemart_order_item_id;
		} else {
			$update_text = 'INSERT INTO wxa43_virtuemart_order_items_points (virtuemart_order_item_id, points_status, increase_points) 
				VALUES ('.$item->virtuemart_order_item_id.', '.$points_status.', '.$curr_points.')';	
		}
		$db->setQuery($update_text);
		$db->execute();
	}
}
/* end */

?>
<div style="text-align: left;">
<form name='adminForm' id="adminForm">
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="option" value="com_virtuemart" />
		<input type="hidden" name="view" value="orders" />
		<input type="hidden" name="virtuemart_order_id" value="<?php echo $this->orderID; ?>" />
		<?php echo JHtml::_( 'form.token' ); ?>


<table class="adminlist table table-striped" width="100%">
	<tr>
		<td width="100%">
		<?php echo $this->displayDefaultViewSearch ('COM_VIRTUEMART_ORDER_PRINT_NAME'); ?>
			<span class="btn btn-small " >
		<a class="updateOrder" href="#"><span class="icon-nofloat vmicon vmicon-16-save"></span>
		<?php echo vmText::_('COM_VIRTUEMART_ORDER_SAVE_USER_INFO'); ?></a></span>
		&nbsp;&nbsp;
				<span class="btn btn-small " >
		<a href="#" onClick="javascript:Virtuemart.resetOrderHead(event);" ><span class="icon-nofloat vmicon vmicon-16-cancel"></span>
		<?php echo vmText::_('COM_VIRTUEMART_ORDER_RESET'); ?></a>
					</span>

		<?php $this->createPrintLinks($this->orderbt,$print_link,$deliverynote_link,$invoices_link);
		?>
			<span style="float:right">
			<?php
			echo $print_link;
			echo $deliverynote_link;
			echo $invoices_link;
			?>
			</span>
		</td>
	</tr>
</table>
</form>


<?php
//================================================================================================================================================================================================================================================================================================================================================
//================================================================================================================================================================================================================================================================================================================================================
	     // СТАРТ
//================================================================================================================================================================================================================================================================================================================================================
//================================================================================================================================================================================================================================================================================================================================================
?>

<!-- Две верхние таблицы -->
<table class="adminlist table mirtelcom-orders" style="table-layout: fixed;">
	<tr>

		<td class="block mirtelcom-customer" style="border-bottom: solid 1px #fff; padding-bottom: 0;" rowspan="2">
			<!-- Блок "Информация о покупателе" -->
			<table style="margin-left: 50px;">
				<?php block_customer($this); ?>
			</table>
		</td>


		<td valign="top" class="block mirtelcom-order" style="border-right: solid 1px #C5D0DD; border-bottom: solid 1px #fff; padding-bottom: 0;" rowspan="2">
			<!-- Блок "Информация о заказе" -->
			<table class="adminlist" cellspacing="0" cellpadding="0">
				<?php block_order($this); ?>
			</table>
		</td>

		<!-- Блок "Комментаний к заказу" -->
		<td valign="top" class="block mirtelcom-orderstatus" style="border-left: solid 1px #C5D0DD; border-bottom: solid 1px #C5D0DD;">
			<span style="font-weight: 500; font-size: 15px; border-bottom: solid 1px #ddd; padding-bottom: 5px; color: #212529">Комментарий к заказу</span>
			<form action="index.php" method="post" name="orderForm" id="orderForm"><!-- Update order head form -->
				<textarea class="textarea" name="order_note" cols="60" rows="2"><?= $this->orderbt->order_note ?></textarea>
				<input type="hidden" name="task" value="updateOrderHead" />
				<input type="hidden" name="option" value="com_virtuemart" />
				<input type="hidden" name="view" value="orders" />
				<input type="hidden" name="virtuemart_order_id" value="<?php echo $this->orderID; ?>" />
				<?php echo JHtml::_( 'form.token' ); ?>
			</form>
		</td>
		
	</tr>

<tr>

		<!-- Блок "Статус заказа" -->
		<td valign="top" class="block mirtelcom-orderstatus" rowspan="4" style="border-left: solid 1px #C5D0DD;">
			<table class="adminlist table">
				<tr>
					<td><div id="updateOrderStatus"><?php echo $this->loadTemplate('editstatus'); ?></div></td>
				</tr>
			</table>

			<table class="adminlist table">
				<?php block_status($this); ?>
			</table>
		</td>                      


</tr>

	<tr>

		<!-- Блок "Комментарий покупателя" -->
		<td colspan="2" class="block mirtelcom-comment" style="border-bottom: solid 1px #C5D0DD; border-right: solid 1px #C5D0DD; padding-top: 17px; padding-left: 30px;">
			<?php 
			if ($this->userfields['fields']['customer_note']['value'] != '') {
				$color = getColorByValue($this->userfields['fields']['customer_note']['value'], '', 'black', 'red'); ?>
				<span style="font-weight: 500; font-size: 15px; border-bottom: solid 1px #ddd; padding-bottom: 5px; color: #212529"><?= $this->userfields['fields']['customer_note']['title'] ?></span>
				<div style="color: <?= $color ?>; white-space: pre-wrap; padding-top: 12px;"><?= $this->userfields['fields']['customer_note']['value'] ?></div>
			<?php } ?>
		</td>

	</tr>

	<tr>
		<td class="block mirtelcom-products" colspan="2" style="border-right: solid 1px #C5D0DD; border-bottom: solid 1px #fff;">
			<?php  $oId=0;  ?>
	
			<!-- Блок "Кнопки" -->
			<table width="100%">
				<?php button($this); ?>
			</table>

			<!-- Блок "Список товаров" -->
			<table width="100%" id="order-items-table">
				<?php block_items($this); ?>
			</table>
		</td>
	</tr>

	<tr>
		<td colspan="2" style="height: 1000px;">&nbsp;</td>
	</tr>
</table>



&nbsp;
<!-- Нижняя синяя таблица BOTTOMBLUE -->
<table width="100%" style="display: none;">
	<?php bottomblue($this); ?>
</table>

</div>


<?php
//================================================================================================================================================================================================================================================================================================================================================
//================================================================================================================================================================================================================================================================================================================================================
	     // ФИНИШ
//================================================================================================================================================================================================================================================================================================================================================
//================================================================================================================================================================================================================================================================================================================================================
?>


<?php
AdminUIHelper::imitateTabs('end');
AdminUIHelper::endAdminArea();

/*
<script type="text/javascript">


// jQuery('select#order_items_status').change(function() {
	////selectItemStatusCode
	// var statusCode = this.value;
	// jQuery('.selectItemStatusCode').val(statusCode);
	// return false
// });

</script>*/

vmJsApi::addJScript( '/administrator/components/com_virtuemart/assets/js/dynotable.js', false, false );

$j = 'jQuery(document).ready(function ($) {
        jQuery("#order-items-table").dynoTable({
            removeClass: ".order-item-remove", //remove class name in  table
            cloneClass: ".order-item-clone", //Custom cloner class name in  table
            addRowTemplateId: "#add-tmpl", //Custom id for  row template
            addRowButtonId: "#add-order-item", //Click this to add a new order item
            lastRowRemovable:true, //let the table be empty.
            orderable:true, //items can be rearranged
            dragHandleClass: ".order-item-ordering", //class for the click and draggable drag handle
            insertRowPlace: ".order-item", //class for the click and draggable drag handle
            onRowRemove:function () {
            },
            onBeforeRowInsert:function (newTr) {
            	var randomNumber = Math.floor(Math.random() * 100);
            	$(newTr).find("*").addBack().filter("[name]").each(function () {
            		var name=this.name;
            		var needle = "item_id["
					var newname = name.replace(needle, needle+"0-"+randomNumber+"-");
                    this.name = newname;
                    this.id += randomNumber;
				});
            },
             onRowClone:function () {
            },
            onRowAdd:function (newTr) {
            	$(".orderEdit").show();
				$(".orderView").hide();
            },
            onTableEmpty:function () {
            },
            onRowReorder:function () {
            }
        });
        
      
        
    });';
vmJsApi::addJScript('dynotable_order_item_ini',$j);



//quorvia
$orderstatusForShopperEmail = VmConfig::get('email_os_s',array('U','C','S','R','X'));
if(!is_array($orderstatusForShopperEmail)) $orderstatusForShopperEmail = array($orderstatusForShopperEmail);
$jsOrderStatusShopperEmail = vmJsApi::safe_json_encode($orderstatusForShopperEmail);

$Q = 'if (typeof Virtuemart === "undefined")
	var Virtuemart = {};
	Virtuemart.orderstatus = '.$jsOrderStatusShopperEmail.';
	jQuery(document).ready(function() {
		Virtuemart.onReadyOrderEdit()
	});';
vmJsApi::addJScript('onReadyOrderEdit',$Q);

vmJsApi::addJScript('/administrator/components/com_virtuemart/assets/js/orders.js',false,false);


//================================================================================================================================================================================================================================================================================================================================================
//================================================================================================================================================================================================================================================================================================================================================
	     // block_order
//================================================================================================================================================================================================================================================================================================================================================
//================================================================================================================================================================================================================================================================================================================================================

function block_order($current) { ?>
			<thead>
			<tr>
				<th style="height: 40px; padding-left: 20px;" colspan="2"><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PO_LBL') ?></th>
			</tr>
			</thead>
			<tbody class="mirtelcom-tbody">
			<tr>
				<td class="key" width="80"><strong><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PO_NUMBER') ?></strong></td>
				<?php
				$orderLink=JURI::root() .'index.php?option=com_virtuemart&view=orders&layout=details&order_number='.$current->orderbt->order_number.'&order_pass='.$current->orderbt->order_pass;

				?>
				<td><?php echo $current->orderbt->order_number; ?></td>
			</tr>
			<tr>
				<td class="key"><strong><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PO_PASS') ?></strong></td>
				<td><?php echo  $current->orderbt->order_pass;?></td>
			</tr>
			<tr>
				<td class="key"><strong><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PO_DATE') ?></strong></td>
				<td><?php  echo vmJsApi::date($current->orderbt->created_on,'LC2',true); ?></td>
			</tr>
			<tr>
				<td class="key"><strong><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PO_STATUS') ?></strong></td>
				<td><?php echo $current->orderstatuslist[$current->orderbt->order_status]; ?></td>
			</tr>
			<tr>
				<td class="key"><strong><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_NAME') ?></strong></td>
				<td><?= $current->orderbt->order_name ?></td>
			</tr>
			<tr>
				<td class="key"><strong><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PO_IPADDRESS') ?></strong></td>
				<td><?php echo $current->orderbt->ip_address; ?></td>
			</tr>
			<?php
			if ($current->orderbt->coupon_code) { ?>
			<tr>
				<td class="key"><strong><?php echo vmText::_('COM_VIRTUEMART_COUPON_CODE') ?></strong></td>
				<td><?php echo $current->orderbt->coupon_code; ?></td>
			</tr>
			</tbody>
			<?php } ?>

<?php }

//================================================================================================================================================================================================================================================================================================================================================
//================================================================================================================================================================================================================================================================================================================================================
	     // block_status
//================================================================================================================================================================================================================================================================================================================================================
//================================================================================================================================================================================================================================================================================================================================================

function block_status($current) { ?>

			<thead>
				<tr>
					<th class="title"><?php echo vmText::_('COM_VIRTUEMART_ORDER_HISTORY_DATE_ADDED') ?></th>
					<th class="title" style="white-space: break-spaces; width: 106px"><?php echo vmText::_('COM_VIRTUEMART_ORDER_HISTORY_CUSTOMER_NOTIFIED') ?></th>
					<th class="title"><?php echo vmText::_('COM_VIRTUEMART_ORDER_LIST_STATUS') ?></th>
					<th class="title"><?php echo vmText::_('COM_VIRTUEMART_COMMENT') ?></th>
				</tr>
			</thead>

			<?php

			$order_history_array = array_reverse($current->orderdetails['history']);
			foreach ($order_history_array as $current->orderbt_event ) {
				echo "<tr >";
				echo "<td class='key'>". str_replace(' ', '&nbsp;', vmJsApi::date($current->orderbt_event->created_on,'LC2',true)) ."</td>\n";
				if ($current->orderbt_event->customer_notified == 1) {
					echo '<td align="center">'.vmText::_('COM_VIRTUEMART_YES').'</td>';
				}
				else {
					echo '<td align="center">'.vmText::_('COM_VIRTUEMART_NO').'</td>';
				}
				if(!isset($current->orderstatuslist[$current->orderbt_event->order_status_code])){
					if(empty($current->orderbt_event->order_status_code)){
						$current->orderbt_event->order_status_code = 'unknown';
					}
					$current->orderstatuslist[$current->orderbt_event->order_status_code] = vmText::sprintf('COM_VIRTUEMART_UNKNOWN_ORDER_STATUS',$current->orderbt_event->order_status_code);
				}

				echo '<td align="center">'.$current->orderstatuslist[$current->orderbt_event->order_status_code].'</td>';
				echo "<td style='overflow-wrap: anywhere;'>".$current->orderbt_event->comments."</td>\n";
				echo "</tr>\n";
			}
			?>

			<?php
				// Load additional plugins

				$_returnValues1 = vDispatcher::trigger('plgVmOnUpdateOrderBEPayment',array($current->orderID));
				$_returnValues2 = vDispatcher::trigger('plgVmOnUpdateOrderBEShipment',array(  $current->orderID));
				$_returnValues = array_merge($_returnValues1, $_returnValues2);
				$_plg = '';
				foreach ($_returnValues as $_returnValue) {
					if ($_returnValue !== null) {
						$_plg .= ('	<td colspan="4">' . $_returnValue . "</td>\n");
					}
				}
				if ($_plg !== '') {
					echo "<tr>\n$_plg</tr>\n";
				}
			?>



<?php }

//================================================================================================================================================================================================================================================================================================================================================
//================================================================================================================================================================================================================================================================================================================================================
	     // block_customer
//================================================================================================================================================================================================================================================================================================================================================
//================================================================================================================================================================================================================================================================================================================================================

function block_customer($current) { ?>
			<thead>
			<tr>
				<th style="height: 40px; padding-left: 20px;" colspan="2"><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_BILL_TO_LBL') ?></th>
			</tr>
			</thead>
			<tbody class="mirtelcom-tbody">

<?php			foreach ($current->userfields['fields'] as $_field ) { 
				if (($_field['name'] === 'BT_tos') || ($_field['name'] === 'BT_customer_note')) continue; 
?>
				<tr>
					<td class="key">				
						<?= $_field['title'] ?>
					</td>
					<td>
						<pre><?= $_field['value'] ?></pre>
					</td>
				</tr>
<?php			} ?>

		<tr>
			<td><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PAYMENT_LBL') ?></td>
			<?php
			$model = VmModel::getModel('paymentmethod');
			$payments = $model->getPayments();
			$model = VmModel::getModel('shipmentmethod');
			$shipments = $model->getShipments();
			?>
			<td>
				<?php
				$current_payment = '';
				foreach($payments as $payment) {
					if($payment->virtuemart_paymentmethod_id == $current->orderbt->virtuemart_paymentmethod_id) $current_payment = $payment->payment_name;
				}
				$color = getColorByValue($current_payment, 'Оплата в магазине при получении товара ', 'black', 'red'); 
?>
				<span style="color: <?= $color ?>"><?= $current_payment ?></span>
			</td>
		</tr>
		<tr>
			<td><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_SHIPMENT_LBL') ?></td>
			<td>
				<?php
				$current_shipment = '';
				foreach($shipments as $shipment) {
					if($shipment->virtuemart_shipmentmethod_id == $current->orderbt->virtuemart_shipmentmethod_id) $current_shipment = $shipment->shipment_name;
				}
				$color = getColorByValue($current_shipment, 'Самовывоз ', 'black', 'red'); 
				?>
				<span style="color: <?= $color ?>"><?= $current_shipment ?></span>
			</td>
		</tr>
</tbody>

<?php }

function getColorByValue($testVariable, $value, $inputColor, $outputColor) {
	if (strcmp($testVariable, $value)) return $outputColor;
	return $inputColor;
}

//================================================================================================================================================================================================================================================================================================================================================
//================================================================================================================================================================================================================================================================================================================================================
	     // BUTTON
//================================================================================================================================================================================================================================================================================================================================================
//================================================================================================================================================================================================================================================================================================================================================

function button($current) { ?>

		<tr id="updateOrderItemStatus" class="viewMode">
			<td colspan="11">

				<?php if(vmAccess::manager('orders.edit')) { ?>

					<a href="#" class="btn btn-small enableEdit">
						<span class="icon-nofloat vmicon vmicon-16-edit"></span><?php echo '&nbsp;' . vmText::_('COM_VIRTUEMART_ORDER_ITEMS_EDIT'); ?>
					</a>
					<a href="#" id="add-order-item" class="btn btn-small orderEdit">
						<span class="icon-nofloat vmicon vmicon-16-new"></span><?php echo '&nbsp;' . vmText::_('COM_VIRTUEMART_ORDER_ITEM_NEW'); ?>
					</a>
					<a href="#" class="btn btn-small cancelEdit orderEdit">
						<span class="icon-nofloat vmicon vmicon-16-remove 4remove"></span>
						<?php echo '&nbsp;' . vmText::_('COM_VIRTUEMART_ORDER_ITEMS_EDIT_CANCEL'); ?>
					</a>
				<?php } ?>

				<?php if(vmAccess::manager('orders.status') or vmAccess::manager('orders.edit')) { ?>
					<a class="btn btn-small  updateOrderItemStatus orderEdit" href="#"><span
								class="icon-nofloat vmicon vmicon-16-save"></span><?php echo vmText::_('COM_VIRTUEMART_ORDER_ITEMS_SAVE'); ?></a>
				<?php } ?>
			</td>
		</tr>

<?php }

//================================================================================================================================================================================================================================================================================================================================================
//================================================================================================================================================================================================================================================================================================================================================
	     // block_items
//================================================================================================================================================================================================================================================================================================================================================
//================================================================================================================================================================================================================================================================================================================================================

function block_items($current) { ?>

	<tr>
		<td colspan="2">
		<form action="index.php" method="post" name="orderItemForm" id="orderItemForm">
		<table class="adminlist table mirtelcom-items"  id="itemTable" >
			<thead>
				<tr>
					<th class="title" width="3">#</th>
					<th class="title" width="110">ID товара</th>
					<th class="title" width="*"><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_NAME') ?></th>
					<th class="title" width="80">Статус</th>
					<th class="title" width="80">Кол-во</th>
					<th class="title" width="80">Цена</th>
					<th class="title" width="80">Скидка</th>
					<th class="title" width="120">Сумма</th>
				</tr>
			</thead>
		<?php $i=1;
		$i=1;
		$rowColor = 0;
		$nbItems=count($current->orderdetails['items']);
		$current->itemsCounter=0;

		foreach ($current->orderdetails['items'] as  $index=> $item) { ?>
			<?php
			$current->item=$item;
			$tmpl = "add-tmpl-" . $index;

			?>
			<tr id="<?php echo $tmpl ?>" class="order-item <?php echo $rowColor?> ">
				<?php //echo vmText::_ ('COM_VIRTUEMART_PRODUCT_PRICE_ORDER');
				echo $current->loadTemplate ('item'); ?>
			</tr>

			<?php
		}
		$emptyItem=new stdClass();
		$emptyItem->product_quantity=0;
		$emptyItem->virtuemart_order_item_id=0; // 0-xx-yy : cloned or new order tiem
		$emptyItem->virtuemart_product_id='';
		$emptyItem->order_item_sku='';
		$emptyItem->order_item_name='';
		$emptyItem->order_status='';
		$emptyItem->product_discountedPriceWithoutTax='';
		$emptyItem->product_item_price='';
		$emptyItem->product_basePriceWithTax='';
		$emptyItem->product_final_price='';
		$emptyItem->product_tax='';
		$emptyItem->product_subtotal_discount='';
		$emptyItem->product_subtotal_with_tax='';
		$emptyItem->order_status='P';
		$emptyItem->linkedit='';
		$emptyItem->tax_rule=0;
		$emptyItem->tax_rule_id=array();
		$current->item=$emptyItem;
		?>
		<tr id="add-tmpl" class="removable row<?php echo $rowColor?>">
			<?php echo $current->loadTemplate ('item'); ?>
		</tr>
		<!--/table -->
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="option" value="com_virtuemart" />
		<input type="hidden" name="view" value="orders" />
		<input type="hidden" name="virtuemart_order_id" value="<?php echo $current->orderID; ?>" />
		<input type="hidden" name="virtuemart_paymentmethod_id" value="<?php echo $current->orderbt->virtuemart_paymentmethod_id; ?>" />
		<input type="hidden" name="virtuemart_shipmentmethod_id" value="<?php echo $current->orderbt->virtuemart_shipmentmethod_id; ?>" />
		<input type="hidden" name="order_total" value="<?php echo $current->orderbt->order_total; ?>" />
		<?php echo JHtml::_( 'form.token' ); ?>
		</form>
			<tr style="border-top: dashed 1px #ddd;">
				<td align="right" colspan="7" style="padding-top: 7px;"><div align="right"><strong> <?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_SUBTOTAL') ?>:</strong></div></td>
				<td align="right" style="padding-right: 5px; padding-top: 7px;"><?php echo $current->currency->priceDisplay($current->orderbt->order_salesPrice); ?></td>
			</tr>
			<?php	if ($current->orderbt->coupon_discount > 0 || $current->orderbt->coupon_discount < 0) {	?>
			<tr>
				<td align="right" colspan="3"><strong><?php echo vmText::_('COM_VIRTUEMART_COUPON_DISCOUNT') ?></strong></td>
				<td  align="right" style="padding-right: 5px;">&nbsp;</td>
				<td  align="right" style="padding-right: 5px;">&nbsp;</td>
				<td  align="right" style="padding-right: 5px;">&nbsp;</td>
				<td  align="right" style="padding-right: 5px;">&nbsp;</td>
				<td  align="right" style="padding-right: 5px;">&nbsp;</td>
				<td   align="right" style="padding-right: 5px;"><?php
				    echo $current->currency->priceDisplay($current->orderbt->coupon_discount);  ?>
                    <input class='orderEdit' type="text" size="8" name="coupon_discount" value="<?php echo $current->orderbt->coupon_discount; ?>"/>
				</td>
			</tr>
			<?php
				//}
			}?>



	<?php
		foreach($current->orderdetails['calc_rules'] as $rule){
			if ($rule->calc_kind == 'DBTaxRulesBill') { ?>
			<tr >
				<td colspan="5"  align="right"  ><?php echo $rule->calc_rule_name ?> </td>
				<td align="right" colspan="7" > </td>

				<td align="right">
				</td>
				<td align="right"><?php echo  $current->currency->priceDisplay($rule->calc_amount);  ?></td>
				<td align="right"  style="padding-right: 5px;">
					<?php echo  $current->currency->priceDisplay($rule->calc_amount);?>
					<input class='orderEdit' type="text" size="8" name="calc_rules[<?php echo $rule->calc_kind ?>][<?php echo $rule->virtuemart_order_calc_rule_id ?>]" value="<?php echo $rule->calc_amount; ?>"/>
				</td>
			</tr>
			<?php
			} elseif ($rule->calc_kind == 'taxRulesBill') { ?>
			<tr >
				<td colspan="5"  align="right"  ><?php echo $rule->calc_rule_name ?> </td>
				<td align="right" colspan="7" > </td>
				<td align="right"><?php echo  $current->currency->priceDisplay($rule->calc_amount);  ?></td>
				<td align="right"> </td>
				<td align="right"  style="padding-right: 5px;">
					<?php echo  $current->currency->priceDisplay($rule->calc_amount);  ?>
					<input class='orderEdit' type="text" size="8" name="calc_rules[<?php echo $rule->calc_kind ?>][<?php echo $rule->virtuemart_order_calc_rule_id ?>]" value="<?php echo $rule->calc_amount; ?>"/>
				</td>
			</tr>
			<?php
			 } elseif ($rule->calc_kind == 'DATaxRulesBill') { ?>
			<tr >
				<td colspan="5"   align="right"  ><?php echo $rule->calc_rule_name ?> </td>
				<td align="right" colspan="7" > </td>

				<td align="right"> </td>
				<td align="right"><?php echo  $current->currency->priceDisplay($rule->calc_amount);  ?></td>
				<td align="right"  style="padding-right: 5px;">
					<?php echo  $current->currency->priceDisplay($rule->calc_amount);  ?>
					<input class='orderEdit' type="text" size="8" name="calc_rules[<?php echo $rule->calc_kind ?>][<?php echo $rule->virtuemart_order_calc_rule_id ?>]" value="<?php echo $rule->calc_amount; ?>"/>
				</td>
			</tr>

			<?php
			 }

		}
		?>

			<tr>
				<td align="right" colspan="7"><strong><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_SHIPPING') ?>:</strong></td>
				<td  align="right" style="padding-right: 5px;"><?php echo $current->currency->priceDisplay($current->orderbt->order_shipment); ?>
					<input class='orderEdit' type="text" size="8" name="order_shipment" value="<?php echo $current->orderbt->order_shipment; ?>"/>
				</td>

			</tr>
			<?php
			if(is_array($current->taxBill) and count($current->taxBill)!=1){
				reset($current->taxBill);
				foreach($current->taxBill as $rule){
					if ($rule->calc_kind != 'taxRulesBill' and $rule->calc_kind != 'VatTax' ) continue;
					?>
                    <tr>
                    <td colspan="5" align="right"><?php echo $rule->calc_rule_name ?> </td>
                    <td align="right" colspan="3"></td>
                    <td align="right" style="padding-right: 5px;">
						<?php echo $current->currency->priceDisplay( $rule->calc_amount );
						/* <input class='orderEdit' type="text" size="8"
								name="calc_rules[<?php echo $rule->calc_kind ?>][<?php echo $rule->virtuemart_calc_id ?>]"
								value="<?php echo $rule->calc_amount; ?я>"/>*/
						?>
                    </td>
                    <td align="right" colspan="2"></td>
                    </tr><?php
				}
			}

			?>
			<tr>
				<td align="right" colspan="7"><strong><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_TOTAL') ?>:</strong></td>
				<td align="right" style="padding-right: 5px;"><strong><?php echo $current->currency->priceDisplay($current->orderbt->order_total); ?></strong>
				</td>
			</tr>

		</table>
		</td>

	</tr> 

<?php }

//================================================================================================================================================================================================================================================================================================================================================
//================================================================================================================================================================================================================================================================================================================================================
	     // BOTTOMBLUE
//================================================================================================================================================================================================================================================================================================================================================
//================================================================================================================================================================================================================================================================================================================================================

function bottomblue($current) { ?>
	<tr>
		<td valign="top" width="50%"><?php

		$returnValues = vDispatcher::trigger('plgVmOnShowOrderBEShipment',array(  $current->orderID,$current->orderbt->virtuemart_shipmentmethod_id, $current->orderdetails));

		foreach ($returnValues as $returnValue) {
			if ($returnValue !== null) {
				echo $returnValue;
			}
		}
		?>
		</td>
		<td valign="top"><?php

		$_returnValues = vDispatcher::trigger('plgVmOnShowOrderBEPayment',array( $current->orderID,$current->orderbt->virtuemart_paymentmethod_id, $current->orderdetails));

		foreach ($_returnValues as $_returnValue) {
			if ($_returnValue !== null) {
				echo $_returnValue;
			}
		}
		?></td>
	</tr>


<?php }

?>