<?php 

class View { // Получение данных из БД по запросу
	public $model;
	public $green = 'green';
	public $orange = 'orange';
	public $red = 'red';

	public function __construct($model) {
		$this->model = $model;
	}

	public function getEndingOfWordByNumbers($count) {
		$mod = $count % 10;
		if (($count >= 10) && ($count <= 19)) {
			return 'й';			
		} else if ($mod == 1) {
			return 'я';
		} else if (($mod >= 2) && ($mod <= 4)) {
			return 'и';
		} else {
			return 'й';
		}		
	}

	/* 1. Sales */
	public function getCountProductsAlarmSales() {
		$count = $this->model->countProductsAlarmSales;
		$msgSales = 'акци' . $this->getEndingOfWordByNumbers($count);
		return 'Завершается ' . $count . ' ' . $msgSales;
	}

	public function getHowManyDaysPromotionEnd() {
		$howManyDaysPromotionEnd = strtotime($this->model->howManyDaysPromotionEnd);
		return floor(($howManyDaysPromotionEnd - time()) / (60*60*24)) . ' дн';
	}

	/* 2. Negative stock/reserv */ 
	public function	getNegativeStock() {
		return $this->model->negativeStock;
	}

	/* 3. Low stock products */
	public function	getCountProductWithLowStock() {
		return $this->model->countProductWithLowStock;
	}

	/* 4. */
	public function	getCountReservedOrdersOverdue() {
		return $this->model->countReservedOrdersOverdue;
	}

	/* 5. */
	public function	getCountExpiredReservedOrders() {
		return $this->model->countExpiredReservedOrders;
	}

	/* 6. */
	public function	getCountOrdersInProgress() {
		return $this->model->countOrdersInProgress;
	}

	/* 8. */
	public function	getCountDaysAfterLastMailing() {
		return $this->model->countDaysAfterLastMailing . ' дн';
	}

	/* 2. */
	public function	getCountProductWithChangePrice() {
		$newProduct = $this->model->countNewProductWithChangePrice;
		$oldProduct = $this->model->countOldProductWithChangePrice;
		return '<span class="new-old-price right-border">' . $newProduct . '</span>/<span class="new-old-price">' . $oldProduct . '</span>';
	}

	public function choiceColor($param) {
		switch ($param) {
			case 'show_sales':	return $this->identifySalesColor();
			case 'show_price_change':	return $this->identifyPriceChangeColor();
			case 'show_reserv':	return $this->identifyNegativeStockColor();
			case 'show_stock':	return $this->identifyLowStockColor();
			case 'show_orders_a':	return $this->identifyReservedOrdersOverdueColor();
			case 'show_orders_b':	return $this->identifyExpiredOrdersOverdueColor();
			case 'show_orders_c':	return $this->identifyOrderInProgressColor();
			case 'show_mailing_lost':	return $this->identifyDaysAfterLastMailing();

		}
		return 'green';
	}

	public function identifySalesColor() {
		$value = floor((strtotime($this->model->howManyDaysPromotionEnd) - time()) / (60*60*24));
		$positiveCondition = $value > 3;
		$negativeCondition = $value <= 0;
		return $this->choiceColorByCondition($positiveCondition, $negativeCondition);
	}

	public function identifyPriceChangeColor() {
		$newProduct = $this->model->countNewProductWithChangePrice;
		$oldProduct = $this->model->countOldProductWithChangePrice;
		$positiveCondition = $newProduct == 0 && $oldProduct == 0;
		$negativeCondition = $newProduct > 30 || $oldProduct > 100;
		return $this->choiceColorByCondition($positiveCondition, $negativeCondition);
	}

	public function identifyNegativeStockColor() {
		$value = $this->model->negativeStock;
		$positiveCondition = $value == 0;
		$negativeCondition = $value > 0;
		return $this->choiceColorByCondition($positiveCondition, $negativeCondition);
	}

	public function identifyLowStockColor() {
		$value = $this->model->countProductWithLowStock;
		$positiveCondition = $value == 0;
		$negativeCondition = $value < 0;
		return $this->choiceColorByCondition($positiveCondition, $negativeCondition);
	}

	public function identifyReservedOrdersOverdueColor() {
		$value = $this->model->countReservedOrdersOverdue;
		$positiveCondition = $value == 0;
		$negativeCondition = $value == 0;
		return $this->choiceColorByCondition($positiveCondition, $negativeCondition);
	}

	public function identifyExpiredOrdersOverdueColor() {
		$value = $this->model->countExpiredReservedOrders;
		$positiveCondition = $value == 0;
		$negativeCondition = $value < 0;
		return $this->choiceColorByCondition($positiveCondition, $negativeCondition);
	}

	public function identifyOrderInProgressColor() {
		$value = $this->model->countOrdersInProgress;
		$positiveCondition = $value > 0;
		$negativeCondition = $value < 0;
		return $this->choiceColorByCondition($positiveCondition, $negativeCondition);
	}

	public function identifyDaysAfterLastMailing() {
		$value = $this->model->countDaysAfterLastMailing;
		$positiveCondition = $value < 10;
		$negativeCondition = $value > 17;
		return $this->choiceColorByCondition($positiveCondition, $negativeCondition);
	}

	public function choiceColorByCondition($positiveCondition, $negativeCondition) {
		if ($positiveCondition) {
			return $this->green;
		} else if ($negativeCondition) {
			return $this->red;
		}
		return $this->orange;
	}

	public function showblock($params, $param, $url, $icon, $name, $count, $header) {
		if ($params->get($param, 1)) { 
			$color = $this->choiceColor($param); 
			return '
				<li class="quickicon quickicon-single ' . $color . '"><a href="' . $url . '">
					<div class="quickicon-header">' . $header . '</div>
					<div class="quickicon-info">
						<div class="quickicon-icon">
							<div>' . $count . '</div>
						</div>
					</div>
					<div class="quickicon-name d-flex align-items-end">' . $name . '</div>
				</a></li>
			';
		}
	}

	public function showLogo($params, $param, $url, $image) { 
	if ($params->get($param, 1)) { ?> 
		<li class="quickicon quickicon-single green"><a href="<?= $url ?>" target="blank">
			<img class="logo" src="<?= $image ?>">
		</a></li>
	<?php } }
}

?>