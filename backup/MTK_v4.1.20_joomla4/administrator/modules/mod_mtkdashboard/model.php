<?php 

class Model {
	public $repository;
	public $countProductsAlarmSales;
	public $howManyDaysPromotionEnd;
	public $negativeStock;
	public $countProductWithLowStock;
	public $countReservedOrdersOverdue;
	public $countExpiredReservedOrders;
	public $countOrdersInProgress;
	public $countNewProductWithChangePrice;
	public $countOldProductWithChangePrice;
	public $countDaysAfterLastMailing;

	public function __construct($repository){ //
		$this->repository = $repository;
		$this->uploadValues();
	}

	public function uploadValues() {
		$this->countProductsAlarmSales = $this->repository->getCountProductsAlarmSales();
		$this->howManyDaysPromotionEnd = $this->repository->getHowManyDaysPromotionEnd();
		$this->negativeStock = $this->repository->getNegativeStock();
		$this->countProductWithLowStock = $this->repository->getCountProductWithLowStock();
		$this->countReservedOrdersOverdue = $this->repository->getCountReservedOrdersOverdue();
		$this->countExpiredReservedOrders = $this->repository->getCountExpiredReservedOrders();
		$this->countOrdersInProgress = $this->repository->getCountOrdersInProgress();
		$this->countNewProductWithChangePrice = $this->repository->getCountNewProductWithChangePrice();
		$this->countOldProductWithChangePrice = $this->repository->getCountOldProductWithChangePrice();	
		$this->countDaysAfterLastMailing = $this->repository->getCountDaysAfterLastMailing();	

	}
}


?>