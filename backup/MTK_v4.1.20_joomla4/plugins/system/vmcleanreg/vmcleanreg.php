<?php
/**
 /*------------------------------------------------------------------------
 # VM Clean Registration 1.0
 # ------------------------------------------------------------------------
 # (C) 2018 Все права защищены.
 # Лицензия http://www.gnu.org/licenses/gpl-3.0.html GNU/GPLv3
 # Автор: Vladimir Pronin
 # Сайт: http://virtuemart.su
 -------------------------------------------------------------------------*/

// No direct access

defined('_JEXEC') or die('Restricted access..');


jimport('joomla.plugin.plugin');

class plgSystemVmcleanreg extends JPlugin {
	public function onBeforeRender() {

	/* Mirtelcom megrate to Joomla 4 */
	if (JFactory::getApplication()->isClient('administrator')) {
	/* base
        if (JFactory::getApplication()->isAdmin()) { */

            return;
        }
        $doc = JFactory::getDocument();


	/* Mirtelcom megrate to Joomla 4 */		
	if (version_compare(JVERSION, '3','lt')) {
	        $view = JRequest::getVar('view');
	} else {
		$input = JFactory::getApplication()->input;
		$view = $input->get('view');
	}
        
        // get parameters
        $name = $this->params->get('name', '1'); // имя пользователя
        $name2 = $this->params->get('name2', '1'); // отображаемое имя
        $password = $this->params->get('password', '1'); // повторить пароль
        
        if($view == 'user' || $view == 'cart'){
            
            $js = $style = '';
            
            if($name) {
                $js .= "$('#email_field').change(function () {             
				    var reg_email = $('#email_field').val();
                    $('#username_field').val(reg_email);
                    });"; 
                $style .= 'input#username_field, label[for="username_field"]{display: none}';
            }
            
            if($name2) {
                $js .= "$('#first_name_field').change(function () {             
				    var reg_name = $('#first_name_field').val();
                    $('#name_field').val(reg_name);
                    });";  
                $style .= 'input#name_field, label[for="name_field"]{display: none}';
            }
            
            if($password) {
                $js .= "$('#password_field').change(function () {             
				    var reg_pas = $('#password_field').val();
                    $('#password2_field').val(reg_pas);
                    });"; 
                $style .= 'input#password2_field, label[for="password2_field"]{display: none}';
            }   
            
            $doc->addScriptDeclaration("jQuery(document).ready(function($) {".$js."})");
            $doc->addStyleDeclaration($style);
            
        } 
	}	
}
?>