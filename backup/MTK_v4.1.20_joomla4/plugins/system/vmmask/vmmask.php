<?php
/**
 /*------------------------------------------------------------------------
 # VmMask 1.2
 # ------------------------------------------------------------------------
 # (C) 2016 Все права защищены.
 # Лицензия http://www.gnu.org/licenses/gpl-3.0.html GNU/GPLv3
 # Автор: Vladimir Pronin
 # Сайт: http://virtuemart.su
 -------------------------------------------------------------------------*/

// No direct access

defined('_JEXEC') or die('Restricted access..');


jimport('joomla.plugin.plugin');

class plgSystemVmmask extends JPlugin {
	public function onBeforeRender() {

	/* Mirtelcom megrate to Joomla 4 */
        if (JFactory::getApplication()->isClient('administrator')) {
	/* base
        if (JFactory::getApplication()->isAdmin()) { */

            return;
        }
        $doc = JFactory::getDocument();

	/* Mirtelcom megrate to Joomla 4 */
	if (version_compare(JVERSION, '3','lt')) {
	        $view = JRequest::getVar('view');
	} else {
	        $view = JFactory::getApplication()->input->getVar('view');
	}
        
        // get parameters
        $phoneId = $this->params->get('phoneId', '');
        $phonePlaceholder = $this->params->get('phonePlaceholder', '');
        $phoneMask = $this->params->get('phoneMask', '');
        $fieldId1 = $this->params->get('fieldId1', '');
        $fieldPlaceholder1 = $this->params->get('fieldPlaceholder1', '');
        $fieldId2 = $this->params->get('fieldId2', '');
        $fieldPlaceholder2 = $this->params->get('fieldPlaceholder2', '');
        $fieldId3 = $this->params->get('fieldId3', '');
        $fieldPlaceholder3 = $this->params->get('fieldPlaceholder3', '');
        $fieldId4 = $this->params->get('fieldId4', '');
        $fieldPlaceholder4 = $this->params->get('fieldPlaceholder4', '');
        $fieldId5 = $this->params->get('fieldId5', '');
        $fieldPlaceholder5 = $this->params->get('fieldPlaceholder5', '');
        
		if ($view == 'cart' || $view == 'user') { 
            // Подключение скриптов
            $doc->addScript(JURI::base().'plugins/system/vmmask/media/jquery.maskedinput.min.js');
            
            $js = "jQuery(document).ready(function(){
                jQuery('#".$phoneId."').mask('".$phoneMask."').attr('placeholder','".$phonePlaceholder."');
            });";
            if($fieldId1 && $fieldPlaceholder1){
               $js .= "jQuery(document).ready(function(){
                    jQuery('#".$fieldId1."').attr('placeholder','".$fieldPlaceholder1."');
                });"; 
            }
            if($fieldId2 && $fieldPlaceholder2){
               $js .= "jQuery(document).ready(function(){
                    jQuery('#".$fieldId2."').attr('placeholder','".$fieldPlaceholder2."');
                });"; 
            }
            if($fieldId3 && $fieldPlaceholder3){
               $js .= "jQuery(document).ready(function(){
                    jQuery('#".$fieldId3."').attr('placeholder','".$fieldPlaceholder3."');
                });"; 
            }
            if($fieldId4 && $fieldPlaceholder4){
               $js .= "jQuery(document).ready(function(){
                    jQuery('#".$fieldId4."').attr('placeholder','".$fieldPlaceholder4."');
                });"; 
            }
            if($fieldId5 && $fieldPlaceholder5){
               $js .= "jQuery(document).ready(function(){
                    jQuery('#".$fieldId5."').attr('placeholder','".$fieldPlaceholder5."');
                });"; 
            }
            $doc->addScriptDeclaration($js);
		}
	}	
}
?>