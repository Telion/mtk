<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  System.redirect
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');

class PlgSystemVmLabel extends JPlugin
{
    public function __construct(& $subject, $config) {
        parent::__construct($subject, $config);
        $this->loadLanguage();
    }
    public function onBeforeRender() {

	/* Mirtelcom megrate to Joomla 4 */
        if (JFactory::getApplication()->isClient('administrator')) {
	/* base
        if (JFactory::getApplication()->isAdmin()) { */

            return;
        }
        $doc = JFactory::getDocument();  
        $doc->addStyleSheet(JURI::base().'plugins/system/vmlabel/vmlabel.css');
	}
}
