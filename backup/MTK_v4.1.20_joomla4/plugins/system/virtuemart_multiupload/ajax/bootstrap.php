<?php 
/*------------------------------------------------------------------------
 * CMSMART Virtuemart Multiupload Plugin
* author : CmsMart Team
* copyright Copyright (C) 2012 www.cms-extensions.net All Rights Reserved.
* @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
* Websites: http://cmsmart.net
* Version : 4.0.0
-------------------------------------------------------------------------*/

/* Mirtelcom */
error_reporting(E_ALL);
/* base
error_reporting(E_NONE); */

define( 'DS', DIRECTORY_SEPARATOR );
$rootFolder = explode(DS,dirname(__FILE__));

//current level in diretoty structure
$currentfolderlevel = 4;

array_splice($rootFolder,-$currentfolderlevel);


$base_folder = implode(DS,$rootFolder);


if(!is_dir($base_folder.DS.'libraries'.DS.'joomla')) exit('Error: Could not loaded Joomla.');

define( '_JEXEC', 1 );
define('JPATH_BASE',implode(DS,$rootFolder));


require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );

$application  = JFactory::getApplication('site');
$application ->initialise();
$rootUri = explode('/',JURI::root(true));
array_splice($rootUri,-$currentfolderlevel);
$base_uri =   JURI::getInstance()->getScheme().'://'
		. JURI::getInstance()->getHost()
		. implode('/',$rootUri).'/';
