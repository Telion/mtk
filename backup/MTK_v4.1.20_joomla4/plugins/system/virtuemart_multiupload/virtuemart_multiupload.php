<?php
/*------------------------------------------------------------------------
* CMSMART Virtuemart Multiupload Plugin
* author : CmsMart Team
* copyright Copyright (C) 2012 www.cms-extensions.net All Rights Reserved.
* @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
* Websites: http://cmsmart.net
* Version : 4.0.0
-------------------------------------------------------------------------*/
// no direct access
defined('_JEXEC') or die('Restricted access'); 
jimport( 'joomla.plugin.plugin' );

/**
 * Virtemart Multiupload system plugin
 */
class plgSystemVirtuemart_multiupload extends JPlugin {

	function __construct( &$subject, $config ) {
		parent::__construct( $subject, $config );
	}

	function onAfterDispatch() {
        //only load in option=com_virtuemart&view=product&task=edit
        if (!$this->isPage('com_virtuemart', 'product' ,'edit')) return;
        
	/* Mirtelcom megrate to Joomla 4 */
	if (version_compare(JVERSION, '3','lt')) {
		$p_id =JRequest::getVar('virtuemart_product_id');
	} else {
		$p_id = JFactory::getApplication()->input->getVar('virtuemart_product_id');
	}      
        
	    $document = JFactory::getDocument();
        $document->addStyleSheet(JURI::root(true) . '/plugins/system/virtuemart_multiupload/assets/css/style.css');
        $document->addScript    (JURI::root(true) . '/plugins/system/virtuemart_multiupload/assets/js/nx.fileuploader.js');
        
        $base_path = JURI::root();

	/* Mirtelcom megrate to Joomla 4 */
	if (version_compare(JVERSION, '3','lt')) {
	        $virtuemart_product_ids = implode(',', JRequest::getVar('virtuemart_product_id', array(), 'default', 'array')) ;
	} else {
		$input = \Joomla\CMS\Factory::getApplication()->input;
		$getvar = $input->get('virtuemart_product_id', array(), 'default', 'array');
		if (is_array($getvar)) {
	        	$virtuemart_product_ids = implode(',', $getvar);
		} else {
			$virtuemart_product_ids = $getvar;
		}
	}

        $token = JSession::getFormToken();
        $js = "
			var iCount=0;
            function createUploader(){
            var uploader = new qq.FileUploader({
            
                element: document.getElementById('file-uploader'),
                listElement: document.getElementById('separate-list'),
                action:'".$base_path."plugins/system/virtuemart_multiupload/ajax/process.php',
                debug: false,
                params: {virtuemart_product_id:".$virtuemart_product_ids.",token:'".$token."'},
                onComplete:function(id, fileName,result){
				
					iCount = iCount+1;
					var li=jQuery('#separate-list').find('li');
					var iTotal=li.length;
						if(iCount==iTotal && jQuery('#cms_autosave').is(':checked')){
							window.location.reload();
						}
	                }
	            });  
	        }  
		";            
             
            if( ($this->isPage('com_virtuemart', 'product' ,'edit')) && ($p_id > 0)) {
                $document->addScriptDeclaration($js);
            }       
	}
	public function onAfterRender() {
		
		if (!$this->isPage('com_virtuemart', 'product' ,'edit')) return;
		
		//append javascript at the end of page
		$this->appendJS('nx.fileuploader.func.js', 'plugins/system/virtuemart_multiupload/assets/js/');		
	}
	
	protected function isPage($option,$view,$task=null,$layout=null) {

		$input = JFactory::getApplication()->input;
		if($input->get('option', '', 'cmd') != $option) return false;
		if($input->get('view', '', 'cmd') != $view) return false;
		if($task)
			if($input->get('task', '', 'cmd') != $task) return false;
		if($layout)
			if($input->get('layout', '', 'cmd') != $layout) return false;
		
		return true;
	}
	
	// only call onAfterRender
	protected function appendJS($jsfile,$path) {

		/* Mirtelcom megrate to Joomla 4 */
		if (version_compare(JVERSION, '3','lt')) {
			$buffer = JResponse::getBody();
		} else {
			$buffer = JFactory::getApplication()->getBody();
		}      

		$root	= JURI::root(true).'/';
		$js = '<script src="'.$root.$path.$jsfile.'" type="text/javascript"></script>';
		$buffer = str_ireplace('</body>', $js.'</body>', $buffer);

		/* Mirtelcom megrate to Joomla 4 */
		if (version_compare(JVERSION, '3','lt')) {
			JResponse::setBody($buffer);
		} else {
			JFactory::getApplication()->setBody($buffer);
		}      
	}
}
?>
