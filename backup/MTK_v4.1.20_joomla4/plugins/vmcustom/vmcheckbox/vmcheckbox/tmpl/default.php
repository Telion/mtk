<?php
defined('_JEXEC') or die();
$product = $viewData[0];
$params = $viewData[1];

$class='vmcustom-vmcheckbox';
?>

<?php
$doc = JFactory::getDocument();

/* Mirtelcom megrate to Joomla 4 */
if (version_compare(JVERSION, '3','lt')) {
	$view = JRequest::getVar('view');
} else {
	$view = JFactory::getApplication()->input->getVar('view');
}

?>

<?php 
// получаем параметры из настроек
$textarea = $params->textarea;

// каждую строку помещаем в массив
if(strpos($textarea, '&#13;&#10;')){
   $textRows = explode("&#13;&#10;",$textarea); 
} else {
   $textRows = explode("\r\n",$textarea); 
}

?>
<div class="<?php echo $class; ?>" id="vmcustom-vmcheckbox<?php echo $params->virtuemart_customfield_id; ?>">

<?php
// невидимое поле
$nameHidden = 'customProductData['.$product->virtuemart_product_id.']['.$params->virtuemart_custom_id.']['.$params->virtuemart_customfield_id .'][vmcheckbox]'; ?>
<label class="radio" style="display: none;">
	<input type="radio" name="<?php echo $nameHidden; ?>" value="0">
</label>
<?php $currency = CurrencyDisplay::getInstance(); 

foreach($textRows as $k => $textRow): ?>
    <?php 
    //уникальное имя для каждого паарметра
    $name = 'customProductData['.$product->virtuemart_product_id.']['.$params->virtuemart_custom_id.']['.$params->virtuemart_customfield_id .'][vmcheckbox'.$k.']';
    
    //получаем массив для опций каждого параметра
    $option = explode('#',$textRow);
    ?>
 <div class="checkbox-wrap">
   <label class="checkbox checkbox-blue">
    <input type="checkbox" data-toggle="checkbox" id="<?php echo $name; ?>" name="<?php echo $name; ?>" value="<?php echo $option[0]; ?>" <?php if(in_array('check',$option)) echo 'checked'; ?>>

    <?php 
     // имя параметра
     echo $option[0]; 

     // дополнительная стоимость
     if(array_key_exists(1,$option) && $option[1] !== 'check') echo ' ('.$option[1].$currency->getSymbol().')';
     ?>
</label>
    </div>
<?php endforeach; ?>
</div>

<script>
jQuery(document).ready(function($){
    $('#vmcustom-vmcheckbox<?php echo $params->virtuemart_customfield_id;?> .checkbox').click(function(){
        function selectField(){
            $('input[name="<?php echo $nameHidden;?>"]').attr('checked', false).trigger('click');
        }
        setTimeout(selectField, 100);      
    });
});
</script>