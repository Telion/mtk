<?php
/**
 * @version		$Id: advanced_calculation.php 2014-4-7 13:52 sakis Terz $2
 * @package		advanced_calculation
 * @copyright	Copyright (C) 2010 - 2018 breakdesigns.net . All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

if (!defined('_JEXEC')) die;

if (!class_exists('vmCalculationPlugin')) require(JPATH_VM_PLUGINS.DIRECTORY_SEPARATOR.'vmcalculationplugin.php');

class plgVmCalculationAdvanced_calculation extends vmCalculationPlugin
{

	function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$this->tableFields = array(
		'id',
		'virtuemart_calc_id',
		'conditionl',
		'operant',
		'conditionr',
		'isbundle',
		'apply_when_missing_prod'
		);
		$this->_tableId = 'id';
		$this->_type = 'vmcalculation';
		$this->_name = 'advanced_calculation';
	}

	/**
	 * Gets and returns the sql for the creation of the table
	 * @author Max Milbers
	 */
	public function getVmPluginCreateTableSQL()
	{
		// if table exist , then update the fields
		$db = JFactory::getDBO();
		$query = 'SHOW TABLES LIKE "%' . str_replace('#__', '', $this->_tablename) . '"';
		$db->setQuery($query);
		$result = $db->loadResult();
		$tablesFields = 0;
		if ($result) {
			$SQLfields = $this->getTableSQLFields();
			$loggablefields = $this->getTableSQLLoggablefields();
			$tablesFields = array_merge($SQLfields, $loggablefields);
			$update[$this->_tablename] = array($tablesFields, array(), array());
			if (!class_exists('GenericTableUpdater'))
			require(JPATH_VM_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'tableupdater.php');
			$updater = new GenericTableUpdater();
			$updater->updateMyVmTables($update);
		}

		return $this->createTableSQL('advanced calculation',$tablesFields);
	}

	/**
	 * The sql fields used for the creation of the table
	 *
	 * @see vmPlugin::getTableSQLFields()
	 */
	function getTableSQLFields()
	{
		$SQLfields = array(
			'id' => ' int(11) NOT NULL AUTO_INCREMENT',
			'virtuemart_calc_id' => '  int(11)',
			'conditionl' => ' varchar(255)',
			'operant' => ' char(12)',
			'conditionr' => ' varchar(255)',
			'isbundle'=>' smallint(1)',
			'apply_when_missing_prod'=>'smallint(1)'
			);
			return $SQLfields;
	}


	function plgVmAddMathOp(&$entryPoints)
	{
	}

	/**
	 * Displays the html code into the calculation form
	 *
	 * @param 	object $calc
	 * @param 	string $html
	 * @return 	booleab true or false
	 * @since 	1.0
	 */

	/* Mirtelcom */
	function plgVmOnDisplayEdit($calc, &$html)
	/* base
	function plgVmOnDisplayEdit(&$calc, &$html) */

	{
		$customfield_value='virtuemart_product_customfields.customfield_value';
		$virtuemart_custom_id='virtuemart_product_customfields.virtuemart_custom_id';
		if(empty($calc->conditionl) && empty($calc->conditionr))$this->setEmptyData($calc);

		$conditions_left=array();
		$conditions_left[] = JHtml::_('select.option', '0',JText::_('PLG_ADVANCED_CALCULATION_FIELD_TYPE'));
		$conditions_left[] = JHtml::_('select.option', $virtuemart_custom_id ,JText::_('PLG_ADVANCED_CALCULATION_CUSTOM_ID'));
		$conditions_left[] = JHtml::_('select.option', $customfield_value ,JText::_('PLG_ADVANCED_CALCULATION_CUSTOM_VALUE'));
		$operant=!empty($calc->operant)?$calc->operant:'=';
		$isbundle_checked='';
		if(!empty($calc->isbundle))$isbundle_checked=' checked';

		$prd_missing_checked='';
		if(!empty($calc->apply_when_missing_prod))$prd_missing_checked=' checked';

		$myhtml= '<fieldset>
		<legend>'.JText::_('PLG_ADVANCED_CALCULATION').'</legend>
		<table class="admintable">
		<tbody>
		<tr>
		<td><span class="hasTip" title="'.JText::_('PLG_ADVANCED_CALCULATION_IS_BUNDLE_DESC').'">'.JText::_('PLG_ADVANCED_CALCULATION_IS_BUNDLE_LABEL').'</span>
		<td><input type="checkbox" value="1" name="isbundle" '.$isbundle_checked.'/></td>
		</tr>

		<tr>
		<td><span class="hasTip" title="'.JText::_('PLG_ADVANCED_CALCULATION_IS_BUNDLE_WHEN_MISSING_PROD_DESC').'">'.JText::_('PLG_ADVANCED_CALCULATION_IS_BUNDLE_WHEN_MISSING_PROD_LABEL').'</span>
		<td><input type="checkbox" value="1" name="apply_when_missing_prod" '.$prd_missing_checked.'/></td>
		</tr>

		<tr>
		<td class="key">'.JText::_('PLG_ADVANCED_CALCULATION_CONDITION').'</td>
		</tr>

		<tr>
		<td>'.JHtml::_('select.genericlist', $conditions_left, 'conditionl', 'class="vm-chzn-select" id="adv_calc_conditionl"', 'value','text', $calc->conditionl).'</td>

		<td><input type="text" value="'.$operant.'" name="operant" readonly="readonly" size="2"/></td>
		<td><input type="text" value="'.$calc->conditionr.'" name="conditionr" placeholder="'.JText::_('PLG_ADVANCED_CALCULATION_FIELD_VALUE').'"/></td>
		</tr>

		</tbody>
		</table>
		</fieldset>';
		$html.=$myhtml;

		return true;
	}

	/**
	 * Save the calc rules data - VM2
	 *
	 * @param 	array $data Contains all the data of the caluclation rule form
	 * @since 	1.0
	 * @author 	Sakis Terz
	 */
	public function plgVmStorePluginInternalDataCalc(&$data)
	{
		$this->handleStorage($data);
	}

	/**
	 * Save the calc rules data - VM3/VM2
	 *
	 * In VM3 is called to handle the whole saving process
	 *
	 * @param 	array $data Contains all the data of the caluclation rule form
	 * @since 	2.0
	 * @author 	Sakis Terz
	 */
	public function plgVmOnStoreInstallPluginTable($jplugin_name, $data, $table=0)
	{
	    vmdebug('Bundle Save triggered','true');
		$this->handleStorage($data);
	}

	/**
	 * Generic function that handles the saving process
	 *
	 * @param 	object $data
	 * @since	2.0
	 * @author	Sakis Terz
	 */
	private function handleStorage(&$data)
	{
		//not used
		if(empty($data['conditionl']) && empty($data['conditionr'])){
			$this->removePluginInternalData($data['virtuemart_calc_id']);
			return false;
		}
		//used without filling all the fields
		if((empty($data['conditionl']) && !empty($data['conditionr'])) || (!empty($data['conditionl']) && empty($data['conditionr']))){
			$this->removePluginInternalData($data['virtuemart_calc_id']);
			vmError('Cant store advanced calc. rule, Left or Right part of the condition is empty');
		}
		//used correctly
		else {
			if(!empty($data['virtuemart_calc_id'])){
				if(empty($data['operant']))$data['operant']='='; //default
				$data['conditionr'] = trim($data['conditionr']);
				$this->storePluginInternalData($data);
			} else {
				vmError('Cant store advanced calc. rule, virtuemart_calc_id empty');
			}
		}
	}

	/**
	 * Affect the price in all the views except the cart and the checkout
	 *
	 * @param object $calculationHelper
	 * @param array $rules
	 */
	function plgVmInGatherEffectRulesProduct(&$calculationHelper,&$rules)
	{
		$this->validateRules($calculationHelper, $rules,$iscart=true);
	}

	/**
	 * Used for calculating the price in the cart and the checkout
	 *
	 * @param 	object $calculationHelper
	 * @param 	array $rules
	 *
	 * @since 	1.0
	 * @author	Sakis Terz
	 */
	function plgVmInGatherEffectRulesBill(&$calculationHelper,&$rules)
	{
		$this->validateRules($calculationHelper, $rules, $iscart=true);
	}

    /**
     * Keeps enabled the calc. rules which meet the criteria and disables the rest
     * It is deciding if a bundle discount (or else) should be applied or not
     *
     * @param unknown $calculationHelper
     * @param array $rules
     * @param bool $iscart
     */
	function validateRules(&$calculationHelper, &$rules, $iscart=false)
	{
		$input=JFactory::getApplication()->input;
		$displayed_bundle=$input->get('bundled_products',0,'int');
		$view=$input->get('view','','cmd');

		//is product details page
		$displayed_bundle=$displayed_bundle&&$view=='productdetails'?$displayed_bundle:false;

		foreach ($rules as $key => $rule) {
			$ruleData=null;
			$this->getPluginInternalDataCalc($ruleData,$rule['virtuemart_calc_id']);

			//do nothing. Another calc. rule or empty fields
			if(empty($ruleData) || empty($ruleData->conditionl) || empty($ruleData->conditionr)){
				continue;
			}

			//if not certain conditions met the rule is not applicable
			if(($ruleData->isbundle && !$iscart) || ($ruleData->isbundle && empty($calculationHelper->_cart->products) && ($displayed_bundle==false))){
				unset($rules[$key]);
				continue;
			}
			$cart_product_ids=array();

			//get products where that rule should be applied
			$affected_products=$this->getProducts($ruleData);

			if(count($affected_products)==0){
			    unset($rules[$key]);
			    continue;
			}

			//When we are not displaying bundled products. e.g. Cart page
			if($displayed_bundle==false){
				$cart_products=array();
				if(!empty($calculationHelper->_cart->products))$cart_products=$calculationHelper->_cart->products;

				//if not is bundle then just apply the rule if the product meets the conditions without checking the cart
				if(!$ruleData->isbundle && in_array($calculationHelper->_product->virtuemart_product_id, $affected_products)){
					//apply the rule normally
				}

				//if it is bundle then all the bundled products should be in the cart
				else{
				    //get the custom. we need it's configuration settings
				    if($ruleData->conditionl=='virtuemart_product_customfields.virtuemart_custom_id')$custom_id=(int)$ruleData->conditionr;

				    if(!empty($custom_id) && class_exists('BundleCustomfield')){
				        $customfield = BundleCustomfield::getInstance($custom_id);
				        $custom_params = $customfield->getCustomfieldParams();
				    }

					foreach($cart_products as $product){
					    $quantity_restriction_met=true;
					    if(!empty($custom_params['bundled_products'])){
					        $bundled_products=(array)$custom_params['bundled_products'];

					        //check the $max_allowed_qty condition for that product
                            foreach ($bundled_products as $prod_id => $param) {
                                $max_allowed_qty=1;
                                if ($prod_id == $product->virtuemart_product_id || $prod_id == $product->product_parent_id) {
                                    $max_allowed_qty = ! empty($param->max_allowed_quantity) ? $param->max_allowed_quantity : 1;
                                    if($product->quantity>$max_allowed_qty){
                                        $quantity_restriction_met=false;
                                        break;
                                    }
                                }
                            }
					    }

						if($quantity_restriction_met && in_array($product->virtuemart_product_id, $affected_products) && !in_array($product->virtuemart_product_id, $cart_product_ids)) {
							$cart_product_ids[]=$product->virtuemart_product_id;
						}
					}
					//that rule should not be applied- Remove it
					if(count($affected_products)<=1 || count($affected_products)!=count($cart_product_ids) || !in_array($calculationHelper->_product->virtuemart_product_id, $affected_products))unset($rules[$key]);
				}
			}

			//bundle is displayed in that page. Check if the products of the bundle are there
			else{
				if(in_array($calculationHelper->_product->virtuemart_product_id, $affected_products)){
				    //apply the rule normally
				}else{
					unset($rules[$key]);
				}
			}
		}
	}

	/**
	 * Get the product ids which meet the conditions
	 *
	 * @param 	Object $ruleData
	 * @author	Sakis Terz
	 * @since	1.0
	 */
	function getProducts($ruleData)
	{
		$conditionl=$ruleData->conditionl;
		$operant=$ruleData->operant;
		$conditionr=$ruleData->conditionr;
		$apply_when_missing_prod=$ruleData->apply_when_missing_prod;
		$results=array();
		//points to a table
		if(strpos($conditionl, '.') && !empty($conditionl) && !empty($operant) && !empty($conditionr)){
			$dbfields=explode('.', $conditionl);
			$db=JFactory::getDbo();
			$query=$db->getQuery(true);
			$query->select('DISTINCT tbl.virtuemart_product_id');
			$query->from('#__'.$dbfields[0].' AS tbl');
			$query->innerJoin('#__virtuemart_products AS p ON p.virtuemart_product_id=tbl.virtuemart_product_id');
			$query->where('p.published=1');

			//out of stock
			if ($apply_when_missing_prod){
				if(VmConfig::get('stockhandle','none')=='disableit' || VmConfig::get('stockhandle','none')=='disableadd')$query->where('p.`product_in_stock` - p.`product_ordered`>0');
				$query->where('((prd_prc.product_price IS NOT NULL) OR (p.product_parent_id>0 AND (SELECT product_price FROM #__virtuemart_product_prices WHERE virtuemart_product_id=p.product_parent_id)IS NOT NULL))');
			}
			$query->leftJoin('#__virtuemart_product_prices AS prd_prc ON p.virtuemart_product_id=prd_prc.virtuemart_product_id');

			$query->where($db->quoteName($dbfields[1]).$operant.$db->quote($conditionr));
			$db->setQuery($query);
			$results=$db->loadColumn();
		}
		return $results;
	}

	/**
	 * Gets the stored data from the plugin's db tables
	 *
	 * @param 	object $calcData
	 *
	 * @since 	1.0
	 * @author	Sakis Terz
	 */
	public function plgVmGetPluginInternalDataCalc(&$calcData)
	{
		return $this->getPluginInternalDataCalc($calcData,$calcData->virtuemart_calc_id);
	}

	/**
	 * Gets the stored data from the plugin's db tables for a specific calc rule
	 *
	 * @param 	object $calcData
	 *
	 * @since 	1.0
	 * @author	Sakis Terz
	 */
	protected function getPluginInternalDataCalc(&$calcData,$id=0)
	{
		//if id is 0 (new) set data for the plug-in
		if($id==0){
			$this->setEmptyData($calcData);
			return false;
		}
		//get from db because it seems that the parent::getPluginInternalData is using cache and retrieves previous data
		$this->_tableChecked = true;
		$db=JFactory::getDbo();
		$query=$db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName($this->_tablename));
		$query->where('virtuemart_calc_id='.(int)$id);
		$db->setQuery($query);
		$data=$db->loadAssoc();
		if($data){
			if(!is_object($calcData))$calcData = new stdClass();
			foreach($data as $key=>$value){
				$calcData->$key =$value;
			}
			return true;
		}
		else return false;
	}

	/**
	 * Sets the data for the case that there is no db record
	 *
	 * @param unknown_type $calcData
	 */
	public function setEmptyData(&$calcData)
	{
		$calcData->conditionl = '';
		$calcData->operant = '';
		$calcData->conditionr = '';
		$calcData->isbundle = 0;
		$calcData->apply_when_missing_prod=0;
	}

	/**
	 * Delete the calc rule from the db tables
	 *
	 * @param int $id - the id of the calc rule
	 */
	public function plgVmDeleteCalculationRow($id)
	{
		$this->removePluginInternalData($id);
	}


	function getOwnUrl()
	{
		if(JVM_VERSION!=1){
			$url = '/plugins/'.$this->_type.'/'.$this->_name;
		} else{
			$url = '/plugins/'.$this->_type;
		}
		return $url;
	}

}
