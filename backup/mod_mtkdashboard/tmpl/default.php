<?php
use Joomla\CMS\Language\Text;

?>

<div class="card-body"><nav class="quick-icons px-3 pb-3" aria-label="Быстрые ссылки Site">
<ul class="nav flex-wrap">

<?php

$path = "/cms-mtk/m2-reports";

// Так много циклов нужно, для сортировки блоков в зависимости от порядкового номера в конфигурациях
$blockNames = array(
	'show_sales_position',
	'show_price_change_position',
	'show_reserv_position',
	'show_stock_position',
	'show_orders_a_position',
	'show_orders_b_position',
	'show_orders_c_position',
	'show_mailing_lost_position',
	'show_logo_position'
);

$size = count($blockNames);

$positions = [];
for ($i = 0; $i < $size; $i++) {
	$positions[$i]['name'] = $blockNames[$i];
	$positions[$i]['position'] = $params[$blockNames[$i]];
}

for ($i = 0; $i < $size - 1; $i++) {
	for ($j = $i + 1; $j < $size; $j++) {
		if ($positions[$i]['position'] > $positions[$j]['position']) {
			$temp = $positions[$i];
			$positions[$i] = $positions[$j];
			$positions[$j] = $temp;
		}
	}
}

for ($i = 0; $i < $size; $i++) {
	$positions[$i]['position'] = $i + 1;
}

$namePosition = [];
for ($i = 0; $i < $size; $i++) {
	$namePosition[$positions[$i]['name']] = $positions[$i]['position'];
}

$blocks = [];
	$blocks[$namePosition['show_sales_position']] = $view->showblock($params, 'show_sales', $path."/1-sales.php", "icon-bolt", $view->getCountProductsAlarmSales(), $view->getHowManyDaysPromotionEnd(), 'Акции');
	$blocks[$namePosition['show_price_change_position']] = $view->showblock($params, 'show_price_change', $path."/3-check-price-change.php", "icon-bolt", "Новые и измен-е цены", $view->getCountProductWithChangePrice(), 'Товары');
	$blocks[$namePosition['show_reserv_position']] = $view->showblock($params, 'show_reserv', $path."/4-products-reserv.php", "icon-bolt", "Отриц. остатки/резерв", $view->getNegativeStock(), 'Товары');
	$blocks[$namePosition['show_stock_position']] = $view->showblock($params, 'show_stock', $path."/8-tracking-product-stock.php", "icon-bolt", "Заканчиваются товары", $view->getCountProductWithLowStock(), 'Товары');
	$blocks[$namePosition['show_orders_a_position']] = $view->showblock($params, 'show_orders_a', $path."/9-orders-reserv.php", "icon-bolt", "Просроченный резерв", $view->getCountReservedOrdersOverdue(), 'Заказы');
	$blocks[$namePosition['show_orders_b_position']] = $view->showblock($params, 'show_orders_b', $path."/9-orders-reserv.php", "icon-bolt", "Заказы в резерве", $view->getCountExpiredReservedOrders(), 'Заказы');
	$blocks[$namePosition['show_orders_c_position']] = $view->showblock($params, 'show_orders_c', $path."/9-orders-reserv.php", "icon-bolt", "Заказы без резерва", $view->getCountOrdersInProgress(), 'Заказы');
	$blocks[$namePosition['show_mailing_lost_position']] = $view->showblock($params, 'show_mailing_lost', "/cms-mtk/m6-mailing-generation/mailing.php", "icon-bolt", "Последняя рассылка", $view->getCountDaysAfterLastMailing(), 'Рассылка');
	$blocks[$namePosition['show_logo_position']] = $view->showLogo($params, 'show_logo', "/", "modules/mod_mtkdashboard/images/logo.png", '');

for ($i = 0; $i < $size; $i++) {
	echo $blocks[$i + 1];
}

?>    

</ul>
</div>
