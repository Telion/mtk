<?php 

class Helper {
	public $db;

	public function __construct($db){ //
		$this->db = $db;
	}

	public function getHowManyDaysPromotionEnd() {
		return $this->getResultFromDB('
			SELECT 
				wxa43_virtuemart_product_prices.product_price_publish_down 
			FROM 
				wxa43_virtuemart_product_prices, 
				wxa43_virtuemart_products_ru_ru 
			WHERE 
				wxa43_virtuemart_product_prices.product_price_publish_down > 0 AND 
				wxa43_virtuemart_products_ru_ru.virtuemart_product_id = wxa43_virtuemart_product_prices.virtuemart_product_id 
			ORDER BY wxa43_virtuemart_product_prices.product_price_publish_down ASC
   			LIMIT 1
		');
	}

	public function getCountProductsAlarmSales() {
		return $this->getResultFromDB('
			SELECT COUNT(wxa43_virtuemart_product_prices.virtuemart_product_id) as count_alarm_sales
			FROM wxa43_virtuemart_product_prices
			WHERE
				wxa43_virtuemart_product_prices.product_price_publish_down > 0 AND 
		                DATEDIFF(wxa43_virtuemart_product_prices.product_price_publish_down, NOW()) <= 4;
		');
	}


	public function	getCountOldProductWithChangePrice() {
		return $this->getResultFromDB('
			SELECT
				COUNT(wxa43_virtuemart_product_prices.virtuemart_product_id)
			FROM 
				wxa43_virtuemart_product_customfields, 
				wxa43_virtuemart_product_prices
			WHERE 
				wxa43_virtuemart_product_prices.virtuemart_product_id = wxa43_virtuemart_product_customfields.virtuemart_product_id AND
				wxa43_virtuemart_product_customfields.virtuemart_custom_id = 54 AND
				wxa43_virtuemart_product_prices.product_override_price != wxa43_virtuemart_product_customfields.customfield_value AND
				wxa43_virtuemart_product_prices.product_price != wxa43_virtuemart_product_customfields.customfield_value
		');
	}

	public function	getCountNewProductWithChangePrice() {
		return $this->getResultFromDB('
			SELECT
				COUNT(wxa43_virtuemart_products.virtuemart_product_id)
			FROM 
				wxa43_virtuemart_products
			WHERE 
				published = 1 AND
				wxa43_virtuemart_products.virtuemart_product_id NOT IN (
					SELECT virtuemart_product_id 
					FROM wxa43_virtuemart_product_customfields 
					WHERE virtuemart_custom_id = 54
				)
		');
	}

	public function	getCountProductWithLowStock() {
		return $this->getResultFromDB('
			SELECT COUNT(wxa43_virtuemart_products.virtuemart_product_id) 
			FROM wxa43_virtuemart_products 
			WHERE low_stock_notification > product_in_stock AND product_in_stock > 0
		');
	}

	public function	getCountExpiredReservedOrders() {
		return $this->getResultFromDB('
			SELECT COUNT(virtuemart_order_id)
			FROM (
				SELECT 
					wxa43_virtuemart_orders.virtuemart_order_id
				FROM 
					wxa43_virtuemart_order_histories, 
					wxa43_virtuemart_orders
				WHERE   	
				        wxa43_virtuemart_order_histories.virtuemart_order_id = wxa43_virtuemart_orders.virtuemart_order_id and
			        	(wxa43_virtuemart_orders.order_status = "C" or wxa43_virtuemart_orders.order_status = "U" or wxa43_virtuemart_orders.order_status = "P") and
				        reserv_date != 0 and
					reserv_date > CURDATE()
				GROUP BY wxa43_virtuemart_orders.virtuemart_order_id) t1
		');
	}                  

	public function	getCountReservedOrdersOverdue() {
		return $this->getResultFromDB('
			SELECT COUNT(virtuemart_order_id)
			FROM (
				SELECT 
					wxa43_virtuemart_orders.virtuemart_order_id
				FROM 
					wxa43_virtuemart_order_histories, 
					wxa43_virtuemart_orders
				WHERE   	
			        	wxa43_virtuemart_order_histories.virtuemart_order_id = wxa43_virtuemart_orders.virtuemart_order_id and
			        	(wxa43_virtuemart_orders.order_status = "C" or wxa43_virtuemart_orders.order_status = "U" or wxa43_virtuemart_orders.order_status = "P") and
					reserv_date != 0 and
					reserv_date <= CURDATE()
				GROUP BY wxa43_virtuemart_orders.virtuemart_order_id) t1
		');
	}                  


	public function	getCountOrdersInProgress() {
		return $this->getResultFromDB('
			SELECT 	
				COUNT(virtuemart_order_id)
			FROM 
				(SELECT 
					wxa43_virtuemart_orders.virtuemart_order_id,
			        	max(reserv_date) as max_date
				FROM 
					wxa43_virtuemart_order_histories, 
					wxa43_virtuemart_orders
				WHERE   	
				        wxa43_virtuemart_order_histories.virtuemart_order_id = wxa43_virtuemart_orders.virtuemart_order_id and
				       	(wxa43_virtuemart_orders.order_status = "C" or wxa43_virtuemart_orders.order_status = "U" or wxa43_virtuemart_orders.order_status = "P")
				GROUP BY wxa43_virtuemart_orders.virtuemart_order_id) t1
			WHERE max_date = 0
		');
	}

	public function	getCountPriceMoreExpansiveThanSds() {
		return $this->getResultFromDB('
			SELECT COUNT(virtuemart_product_id)
			FROM (
				SELECT a.virtuemart_product_id, a.published, a.product_mpn, p.product_price, p.override, p.product_override_price 
				FROM wxa43_virtuemart_products as a, wxa43_virtuemart_product_prices as p 
				WHERE 
					a.virtuemart_product_id = p.virtuemart_product_id AND
					a.published = 1 AND
					(
				        	p.override = 0 AND a.product_mpn > p.product_price OR
					        p.override = 1 AND a.product_mpn > p.product_override_price
					)
		                ) t1
		');
	}

	public function getResultFromDB($query) {
		$this->db->setQuery($query);
		return $this->db->loadResult();
	}

	public function getVendors() {
		$this->db->setQuery('
			SELECT T2.customfield_value, T1.zero_cnt, T2.total_cnt
	        FROM
			    (
					SELECT customfield_value, count(wxa43_virtuemart_products.virtuemart_product_id) as total_cnt
					FROM wxa43_virtuemart_products, wxa43_virtuemart_product_customfields 
					WHERE wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_product_customfields.virtuemart_product_id AND virtuemart_custom_id = 48 
					GROUP BY customfield_value
				) T2
                LEFT JOIN 
				(
					SELECT customfield_value, count(wxa43_virtuemart_products.virtuemart_product_id) as zero_cnt
					FROM wxa43_virtuemart_products, wxa43_virtuemart_product_customfields 
					WHERE wxa43_virtuemart_products.virtuemart_product_id = wxa43_virtuemart_product_customfields.virtuemart_product_id AND virtuemart_custom_id = 48 AND wxa43_virtuemart_products.product_in_stock <= 0
					GROUP BY customfield_value
				) T1
                ON T1.customfield_value = T2.customfield_value  
            ORDER BY `T2`.`customfield_value` ASC
		');
		return $this->db->loadRowList();
	}

	public function getNextMailing() {
		$this->db->setQuery('
			SELECT task_date_deadline FROM tln_programmer_tasks WHERE task_id = 76;
		');
		return $this->db->loadResult();
	}

	public function getNegativeStock() {
		return $this->getResultFromDB('
			SELECT COUNT(virtuemart_product_id) as cnt_negative_stock 
			FROM wxa43_virtuemart_products 
			WHERE 
				product_in_stock < 0 
				OR product_ordered < 0
		');
	}
}


?>