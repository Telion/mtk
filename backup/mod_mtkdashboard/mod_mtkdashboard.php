<?php

defined('_JEXEC') or die;
require_once dirname(__FILE__) . '/repository.php';
require_once dirname(__FILE__) . '/model.php';
require_once dirname(__FILE__) . '/view.php';

jimport('joomla.database.table');
$db = JFactory::getDBO();

$repository = new Repository($db);
$model = new Model($repository);
$view = new View($model);

require_once dirname(__FILE__) . '/tmpl/default.php';

?>