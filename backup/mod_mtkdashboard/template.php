
<div class="card-body"><nav class="quick-icons px-3 pb-3" aria-label="Быстрые ссылки Site">
<ul class="nav flex-wrap">

<?php

$path = "/cms-mtk/m2-reports";

if ($params->get('show_sales', 1)) {
	$view->showblockValue($path."/1-sales.php", "icon-bolt", $view->getCountProductsAlarmSales(), $view->getHowManyDaysPromotionEnd());
}
if ($params->get('show_price_change', 1)) {
	$view->showblockValue($path."/3-check-price-change.php", "icon-bolt", "Новые и измен-е цены", $view->getCountProductWithChangePrice());
}
if ($params->get('show_reserv', 1)) {
	$view->showblockValue($path."/4-products-reserv.php", "icon-bolt", "Отриц. остатки/резерв", $view->getNegativeStock());
}
if ($params->get('show_stock', 1)) {
	$view->showblockValue($path."/8-tracking-product-stock.php", "icon-bolt", "Заканчиваются товары", $view->getCountProductWithLowStock());
}
if ($params->get('show_orders_a', 1)) {
	$view->showblockValue($path."/9-orders-reserv.php", "icon-bolt", "Заказы. Просроч-й резерв", $view->getCountReservedOrdersOverdue());
}
if ($params->get('show_orders_b', 1)) {
	$view->showblockValue($path."/9-orders-reserv.php", "icon-bolt", "Заказы. В резерве", $view->getCountExpiredReservedOrders());
}
if ($params->get('show_orders_c', 1)) {
	$view->showblockValue($path."/9-orders-reserv.php", "icon-bolt", "Заказы. Без резерва", $view->getCountOrdersInProgress());
}
if ($params->get('show_logo', 1)) {
	$view->showLogo("/", "modules/mod_mtkdashboard/logo.png");
}
                                                                                                            
?>    

</ul>
</div>
